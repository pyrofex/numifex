<?php
/**
 * Plugin Name: Numifex Payment Gateway
 * Description: An offline type gateway for Numifex cryptocurrency payment processing.
 * Author: Pyrofex
 * Author URI: http://www.pyrofex.net/
 * Version: 0.4.2
 * Text Domain: woocommerce-extension
 *
 * Copyright: ...
 *
 * License: ...
 * License URI: ...
 *
 * @package   Pyrofex-Ethereum-Gateway
 * @author    Pyrofex
 * @copyright ...
 * @license  ...
 *
 * This Ethereum gateway forks the WooCommerce core "Cheque" payment gateway 
 * to create another offline payment method.
 */

defined( 'ABSPATH' ) or exit;

// Make sure WooCommerce is active
if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 
    'active_plugins', get_option('active_plugins'))) ) {
    return;
}

function render_numifex_admin() {
    // Numifex URL
    $url_error = NULL;
    if (array_key_exists('numifex_url_update', $_POST)){
        // trim whitespace
        $cleaned_url = esc_url_raw(trim($_POST['url']));

        // remove extra "/" if they have one
        if (substr($cleaned_url, -1) == "/") {
            $cleaned_url = substr($cleaned_url, 0, -1);
        }

        // save and load to SQL escape
        update_option('numifex_url', $cleaned_url);
        $numifex_url = get_option('numifex_url', 'none');

        $response = wp_remote_get($numifex_url . "/", array("sslverify" => false));
        $response_code = wp_remote_retrieve_response_code($response);

        if (is_wp_error($response)) {
            $url_error = $response->get_error_message();
        } elseif ($response_code == '' || ($response_code < 200 || $response_code >= 400)) {
            $url_error = "bad response " . strval($response_code);
        }
    }
    $numifex_url = get_option('numifex_url', 'none');

    // Secrete Key/Token
    $token_error = NULL;
    if (array_key_exists('secret_key_update', $_POST)){
        // trim whitespace
        // save and load to SQL escape
        update_option('secret_key', trim($_POST['secret_key']));
        $secret_key = get_option('secret_key', 'none');

        $args = array(
            'body' => array( 
                'password' => $secret_key,
                'grant_type' => 'password',
                'username' => 'wordpress',
            ),
            'sslverify' => false,
        );
        $response = wp_remote_post($numifex_url . '/api-clients/authorize', $args );
        $response_code = wp_remote_retrieve_response_code($response);

        if (is_wp_error($response)) {
            $token_error = $response->get_error_message();
            update_option('access_token', 'error');
        } elseif ($response_code == '' || ($response_code < 200 || $response_code >= 400)) {
            $token_error = "bad response " . strval($response_code);
            update_option('access_token', 'error');
        } else {
            $obj = json_decode($response['body'], true);

            if (is_null($obj) || !isset($obj['access_token'])) {
                $token_error = "server didn't return an access token";
                update_option('access_token', 'error');
            } else {
                // token is all good
                update_option('access_token', $obj['access_token']);    
            }
        }
    }

    $secret_key = get_option('secret_key', 'none');
    $client_token = get_option('access_token', 'none');

    // Admin Page HTML
?>
    <form method="post" action="" style="margin-top: 50px">
    <div style="margin-bottom: 5px;font-size: 16px;">Current Numifex URL: &nbsp;
    <span style="color:green;">
    <?php
    if (!is_null($url_error)) {
    ?>
        <span style='color:red'>
            <?php echo esc_url($numifex_url); ?>
        </span>
        <div style='color:red; margin-top:5px;'>
            Cannot connect to Numifex: <?php echo $url_error; ?>
        </div>
    <?php
    } else {
    ?>
       <span style='color:green'><?php echo esc_url($numifex_url); ?> </span>
    <?php
    } 
    ?>
    </span>
    </div>
    <label>Numifex URL:</label>
    <input type="text" name="url" required placeholder="example: http://numifex.com" 
            pattern="\s*https?://.+" minlength=3 
            title=
            "Please use a valid web address e.g. 
            https://mywordpress.com or http://127.0.0.1" >
    <input type="submit" name="numifex_url_update" value="Submit" class="button button-primary">
    </form>

    <form method="post" action="" style="margin-top: 30px">
        <div style="margin-bottom: 5px;font-size: 16px;">Current Secret Key: &nbsp;
        <?php 
            if (!is_null($token_error)) {
        ?>
                <span style='color:red'><?php echo sanitize_text_field($secret_key); ?></span>
        <?php
            } else {
        ?>
                <span style='color:green'><?php echo sanitize_text_field($secret_key); ?></span>
        <?php
            } 
        ?>
        </div>
        <div style="margin-bottom: 5px;font-size: 16px;">
        <span style="color:green;">
        <?php 
            if (!is_null($token_error)) {
        ?>
                <div style='color:red; margin-top:5px;'>
                    This Secret Key is invalid. Please check your key on Numifex, 
                    and re-enter.
                </div>
        <?php
            } 
        ?>
    </span></div>
    <label>Secret Key:</label>
    <input type="text" name="secret_key" placeholder=
            "example: TzBZg/KzBXw+UkP49Fs+xDUDk6chE9cwfPXXXXXXXXXX"  
            required minLength="44"
            title="This key is not valid." style="width: 460px;">
    <input type="submit" name="secret_key_update" value="Submit" 
    class="button button-primary">
    </form>
<?php
}


// Adds config panel page to Admin, currently just for entering the Numifex URL.
add_action('admin_menu', 'numifex_plugin_setup_menu');
function numifex_plugin_setup_menu() {
    add_menu_page( 'Numifex Plugin', 'Numifex', 
        'manage_options', 'wc-numifex-admin', 'render_numifex_admin' );
}

function wc_numifex_add_to_gateways( $gateways ) {
    // append new gateways to avialable
    $gateways[] = 'WC_Numifex_ETH_Gateway';
    $gateways[] = 'WC_Numifex_BTC_Gateway';
    return $gateways;
}
add_filter( 'woocommerce_payment_gateways', 'wc_numifex_add_to_gateways' );

// Adds plugin page links
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 
    'numifex_plugin_links' );
function numifex_plugin_links( $links ) {
    $plugin_links = array(
        '<a href="' . admin_url( 
            'admin.php?page=wc-settings&tab=checkout' ) 
            . '">' . __( 'Gateways', 'wc-numifex-gateway' ) . '</a>',

        '<a href="' . admin_url( 
            'admin.php?page=wc-numifex-admin' ) 
            . '">' . __( 'Settings', 'wc-numifex-gateway' ) . '</a>',
        );
    return array_merge( $plugin_links, $links );
}



/**
 * Ethereum Payment Gateway
 *
 * Provides an Ethereum Payment Gateway
 * We load it later to ensure WC is loaded first since we're extending it.
 */

add_action( 'plugins_loaded', 'wc_numifex_gateway_init', 11 );
function wc_numifex_gateway_init() {
    // extraordinary cryptos
    // do not need to inherit from this, but they
    // may need to do more work in that case

    class WC_Numifex_Gateway extends WC_Payment_Gateway {
        public function __construct() {
            //$this->icon = apply_filters('woocommerce_offline_icon', '');
            $this->has_fields = false;

            // Load the settings.
            $this->init_form_fields();
            $this->init_settings();

            // Define user set variables
            $this->title = $this->get_option( 'title' );
            $this->description = $this->get_option( 'description' );
            $this->instructions = $this->get_option( 'instructions' );
            $this->risk_level = $this->get_option( 'risk_level' );
            $this->time_to_sell = $this->get_option( 'time_to_sell' );
            $this->custom_conv_rate = trim($this->get_option('custom_conv_rate'));

            // Actions
            // hook for our superclass
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( 
                $this, 'process_admin_options' ) );
        }


        /**
         * Initialize Gateway Settings Form Fields
         */
        public function init_form_fields() {
            $this->form_fields = apply_filters( 'wc_offline_form_fields', array(

                'enabled' => array(
                    'title'   => __( 'Enable/Disable', 'wc-numifex-gateway' ),
                    'type'    => 'checkbox',
                    'label'   => __( 'Enable this crypto currency', 'wc-numifex-gateway' ),
                    'default' => 'no'
                ),

                'title' => array(
                    'title'       => __( 'Title', 'wc-numifex-gateway' ),
                    'type'        => 'text',
                    'description' => __( 'This controls the title for the payment method' . 
                    'the customer sees during checkout.', 'wc-numifex-gateway' ),
                    'default'     => $this->default_tab_title,
                    'desc_tip'    => true,
                ),

                'description' => array(
                    'title'       => __( 'Description', 'wc-numifex-gateway' ),
                    'type'        => 'textarea',
                    'description' => __( 'Payment method description that the customer' . 
                    'will see on your checkout.', 'wc-numifex-gateway' ),
                    'default'     => __( 'The total shown above is approximate. ' .
                    'Your exchange rate will be locked in and provided ' .
                    'once you select "Place order" below, ' . 
                    'along with your total due. You will be provided with an address' .
                    ' to send the payment to.', 'wc-numifex-gateway' ),
                    'desc_tip'    => true,
                ),

                'instructions' => array(
                    'title'       => __( 'Instructions', 'wc-numifex-gateway' ),
                    'type'        => 'textarea',
                    'description' => __( 'Instructions that will be added to the thank you' . 
                    'page and emails.', 'wc-numifex-gateway' ),
                    'default'     => $this->default_instructions,
                    'desc_tip'    => true,
                ),

                'risk_level' => array(
                    'title'    => __('Price Margin', 'wc-numifex-gateway'),
                    'type'     => 'select',
                    'options' => array(
                        'low' => 'Safe (large margin)',
                        'medium' => 'Moderate (medium margin)',
                        'high' => 'Aggressive (small margin)',
                        'market' => 'Market price (no margin)',
                    ),
                    'default' => 'high',
                    'description' => __('Adjusts the exchange rate to protect against changing crypto prices.'),
                    'desc_tip' => false
                ),

                'time_to_sell' => array(
                    'title' => __('Time To Sell', 'wc-numifex-gateway'),
                    'type' => 'select',
                    'options' => array(
                        '1' => 'Every Day',
                        '3' => 'Up to 3 Days',
                        '7' => 'Up to 1 Week',
                    ),
                    'default' => '3',
                    'description' => __('How often you exchange crypto for local currency. Affects exchange rate adjustment. Ignored for market pricing.'),
                    'desc_tip' => false
                ),

                'custom_conv_rate' => array(
                    'title'       => __( 'Custom Exchange Rate (fiat/crypto)', 'wc-numifex-gateway' ),
                    'type'        => 'text',
                    'description' => __( 'Override exchange rate and price margin with a custom exchange rate. Useful for custom tokens or extreme market conditions. Recommended to leave it blank.'),
                    'default'     => '',
                    'desc_tip'    => false,
                ),
           ));
        }

        protected function calculate_price($currency, $crypto_currency, $total) {
            $numifex_url = get_option('numifex_url', 'none');
            $client_token = get_option('access_token', 'none');

            $body = array(
                "order_total" => $total,
                "order_currency" => $crypto_currency,
                "currency" => $currency,
                "risk_level" => $this->risk_level,
                "time_to_sell" => $this->time_to_sell,
            );

            // optional custom conversion rate
            $conv_rate = null;
            if ($this->custom_conv_rate !== '') {
                $conv_rate = floatval($this->custom_conv_rate);
                if ($conv_rate > 0) {
                    $body['currency_conv_rate'] = $conv_rate;
                }
            }

            // retrieve conversion rate and store in postmeta table
            $response = wp_remote_post($numifex_url . "/calculate-price", array(
                "body" => $body,
                "headers" => array("Authorization" => "Bearer " . $client_token),
                "sslverify" => false,
            ));

            $response_code = wp_remote_retrieve_response_code( $response );
            if (is_wp_error($response)) {
                return array(
                    'result' => 'failure',
                    'error' => $response->get_error_message()
                );
            } elseif ($response_code < 200 || $response_code >= 400) {
                return array(
                    'result' => 'failure',
                    'error' => strval($response_code),
                );
            } else {
                $obj = json_decode($response['body'], true);

                if (!isset($obj['rate']) ||
                    !isset($obj['total']) ||
                    !isset($obj['total_formatted'])) {
                    return array(
                        'result' => 'failure',
                        'value' => "missing fields"
                    );
                } else {
                    return array(
                        'result' => 'success',
                        'value' => $obj
                    );
                }
            }   
        }

        // Process the payment and return the result
        public function process_payment( $order_id ) {
            $order = wc_get_order( $order_id );
            $currency = $order->get_currency();
            $price_response = $this->calculate_price($currency, $this->crypto_currency, $order->total);

            if ($price_response['result'] !== 'success') {
                wc_add_notice('Crypto error. Contact website owner or try again later. calculate price:' . $price_response['error'], 'error');
                return $price_response;
            }

            $price = $price_response['value'];

            // since price calculation has succeeded,
            // submit the order
            $access_token = get_option('access_token', 'none');
            $numifex_url = get_option('numifex_url', 'none');

            $body = array(
                'commerce_id' => $order->id,
                'commerce' => 'woocommerce',
                'order_total' => $price['total'],
                'currency_conv_rate' => $price['rate'],
                'currency' => $currency,
                'order_currency' => $this->crypto_currency
            );

            $response = wp_remote_post($numifex_url . '/placeorder', array(
                "body" => $body,
                "headers" => array("Authorization" =>  "Bearer " . $access_token),
                "sslverify" => false,
            ));

            $obj = json_decode($response['body'], true);

            $response_code = wp_remote_retrieve_response_code($response);
            if (is_wp_error($response)) {
                error_log(print_r($obj));
                wc_add_notice('Crypto error. Contact website owner or try again later. ' . $response->get_error_message(), 'error');
                return array(
                    'result' => 'failure',
                );
 
            } elseif ($response_code < 200 || $response_code >= 400) {
                error_log(print_r($obj));
                wc_add_notice('Crypto error. Contact website owner or try again later. ' . strval($response_code), 'error');
                return array(
                    'result' => 'failure',
                );
            } 

	    if (is_null($obj) ||
	        !isset($obj['address'])) {
                wc_add_notice('Cryptocurrency error. Contact website owner or try again later. invalid json.', 'error');
                return array(
                    'result' => 'failure',
                );
            }
	    
            // store results in the order body
            update_post_meta( $order->id, 'crypto_address', sanitize_text_field($obj['address']));
            update_post_meta( $order->id, 'crypto_rate', floatval($price['rate']));
            update_post_meta( $order->id, 'crypto_total_formatted', sanitize_text_field($price['total_formatted']));
            update_post_meta( $order->id, 'crypto_total', sanitize_text_field($price['total']));
            update_post_meta( $order->id, 'crypto_currency', $this->crypto_currency);

            // Mark as on-hold (we're awaiting the payment)
            $order->update_status( 'on-hold', __('Awaiting crypto payment', 'wc-numifex-gateway'));

            // Reduce stock levels
            $order->reduce_order_stock();

            // Remove cart
            WC()->cart->empty_cart();

            // Return thankyou redirect
            return array(
                'result' => 'success',
                'redirect' => $this->get_return_url( $order )
            );
        }

        // this function can be done with a
        // filter, although this seems more messy
        // add_filter('woocommerce_gateway_description', array( $this, 'add_total_to_description'), 10, 2);
        public function get_description() {
            $description = apply_filters( 'woocommerce_gateway_description', $this->description, $this->id );

            $cart_total = WC()->cart->cart_contents_total;
            $currency = get_woocommerce_currency();
            $response = $this->calculate_price($currency, $this->crypto_currency, $cart_total);

            if ($response['result'] == 'success') {
                $obj = $response['value'];
                $crypto_total = sanitize_text_field($obj['total_formatted']);
                $html = '<p>Estimated total: ' . $crypto_total . ' ' . $this->crypto_currency . '</p>';
                return $html . $description;
            } else {
                return "This option is not available. " . sanitize_text_field($response['error']);
            }
        }

        public function add_instructions_to_order_received($text, $order) {
            if ($order == null ||
                $order->get_payment_method() != $this->id) {
                // don't modify other payments
                return $text;
            }

            return render_numifex_instruction_table($this->instructions, $order, false) . $text;
        }

        public function add_instructions_to_order_email($order, $sent_to_admin, $plain_text, $email) {
            if ($order == null ||
                $order->get_payment_method() != $this->id ||
                $sent_to_admin) {
                return '';
            }

            // only attach to reasonable emails
            if ( !($email->id == 'customer_invoice' || 
                   $email->id == 'customer_on_hold_order')) {
                return '';
            }

            echo render_numifex_instruction_table($this->instructions, $order, true);
        }
    } 

    class WC_Numifex_ETH_Gateway extends WC_Numifex_Gateway {
        public function __construct() {
            $this->id = 'numifex_eth_gateway';
            $this->method_title = __( 'Ethereum (Numifex)', 'wc-numifex-gateway' );

            $this->method_description = __( 'Accepts Ethereum cryptocurrency payments through Numifex.', 'wc-numifex-gateway');

            $this->default_tab_title = __('Ethereum Payment', 'wc-numifex-gateway');
            $this->default_instructions = __('You have chosen to pay with Ethereum. ' .
                             'Please follow the instructions below for sending your payment. ' .
                                     'Your order will not be processed until a payment is received.', 'wc-numifex-gateway');

            $this->crypto_currency = 'ETH';

            add_filter('woocommerce_thankyou_order_received_text', array($this, 'add_instructions_to_order_received'), 10, 2 );
            add_filter('woocommerce_email_before_order_table', array($this, 'add_instructions_to_order_email'), 10, 4 );
            parent::__construct();
        }   

    }

    class WC_Numifex_BTC_Gateway extends WC_Numifex_Gateway {
        public function __construct() {
            $this->id = 'numifex_btc_gateway';
            $this->method_title = __( 'Bitcoin (Numifex)', 'wc-numifex-gateway' );

            $this->method_description = __( 'Accepts Bitcoin cryptocurrency payments through Numifex.', 'wc-numifex-gateway' );

            $this->default_tab_title = __('Bitcoin Payment', 'wc-numifex-gateway');
            $this->default_instructions = __('You have chosen to pay with Bitcoin. ' .
                                             'Please follow the instructions below for sending your Bitcoin payment. ' .
                                             'Your order will not be processed until a payment is received.', 'wc-numifex-gateway');

            $this->crypto_currency = 'BTC';

            add_filter('woocommerce_thankyou_order_received_text', array($this, 'add_instructions_to_order_received'), 10, 2 );
            add_filter('woocommerce_email_before_order_table', array($this, 'add_instructions_to_order_email'), 10, 4 );
    
            parent::__construct();
        }
    }
}


function render_numifex_instruction_table($instructions, $order, $for_email) {
    $numifex_url = get_option('numifex_url', 'none');

    $id = $order->get_id();
    $address = get_post_meta( $id, 'crypto_address', true );
    $crypto_rate = get_post_meta( $id, 'crypto_rate', true );
    $crypto_currency = get_post_meta( $id, 'crypto_currency', true );
    $order_total = $order->get_total();
    $crypto_total = get_post_meta( $id, 'crypto_total_formatted', true );
    $currency = $order->get_currency();
    $currency_symbol = get_woocommerce_currency_symbol($currency);

    if (!$for_email) {
        $access_token = get_option('access_token', 'none');

        $qr_path = '/qr/' . $crypto_currency . '/' . $address . '?' . http_build_query(array('amount' => $crypto_total));
        $response = wp_remote_get(esc_url_raw($numifex_url . $qr_path), array(
            "headers" => array("Authorization" =>  "Bearer " . $access_token),
            "sslverify" => false,
        ));

        $qr_base64 = NULL;

        $response_code = wp_remote_retrieve_response_code($response);
        if (!is_wp_error($response)) {
            $qr_base64 = base64_encode($response['body']);
        }
    }

    // this is so we can capture the below
    // part in a string
    ob_start();
?>
    <style>
    #numifexSection span {
        color: red;
        font-size: 1.2rem;
    }
    #numifexSection div {
        background-color:#f8f8f8;
        margin: 20px 0 40px 0;
        padding: 1rem;
    }

    #numifexSection h3 {
        font-weight: 500;
        text-decoration: underline;
    }

    #numifexSection p {
        margin: 0 0 .5rem;
    }

    #numifexSection .crypto-address,
    #numifexSection .crypto-total {
        color: green;
        font-size: 1.3rem;
    }

    #numifexSection table tr {
        background-color: transparent;
    }

    #numifexSection table .total-row {
        font-size: 1.3rem;
    }

    #numifexSection table .total-row td {
        border-top: 2px solid black;
    }

    #numifexSection .qr-code {
        max-width: 325px;
        width: 100%;
    }

    </style>
    <div id="numifexSection">
        <p></p> 

        <div>
            <h3>Payment Instructions</h3> 

            <p>
                <?php echo $instructions; ?>
            </p>

            <table> 
                <tr>
                    <th></th>
                    <th>Conversion Rate</th>
                    <th><?php echo $currency_symbol; ?> Total</th>
                    <th><?php echo $crypto_currency; ?> Total</th>
                </tr> 
                <tr>
                    <td></td>
                    <td>1 <?php echo $crypto_currency; ?> = &nbsp; <?php echo $currency_symbol . strval(round($crypto_rate, 2)); ?> </td>
                    <td><?php echo $currency_symbol . $order_total; ?></td>
                    <td><?php echo $crypto_total . ' ' . $crypto_currency; ?></td>
                </tr>
                <tr class="total-row">
                    <td>Total Due</td>
                    <td></td>
                    <td></td>
                    <td><?php echo $crypto_total . ' ' . $crypto_currency; ?></td>
                </tr>
            </table>

            <p>
                Please send your payment of &nbsp;
                <span class="crypto-total"><?php echo $crypto_total . ' ' . $crypto_currency; ?></span>
                &nbsp; to the following address: &nbsp;
                <span class="crypto-address"><?php echo $address; ?></span>
            </p>

            <?php if (!$for_email && !is_null($qr_base64)) { ?>
                <img class="qr-code" alt="qr code" src="data:image/svg+xml;base64, <?php echo sanitize_text_field($qr_base64); ?>"/>
            <?php } ?>
        </div>
    </div>
<?php

    $html = ob_get_clean();
    return $html;
}

?>