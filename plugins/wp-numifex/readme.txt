=== Numifex Payment Gateway ===
Contributors: pyrofex
Tags: cryptocurrency, payment gateway, numifex, pyrofex, bitcoin, ethereum, blockchain
Donate link: https://numifex.io/
Requires at least: 4.6
Tested up to: 4.7
Stable tag: 4.3
Requires PHP: 5.2.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Numifex is a merchant solution for processing cryptocurrency payments. This plugin is a Woocommerce integration for the standalone Numifex software. You must purchase and run the software to use the Woocommerce plugin.

== Description ==

Numifex is a payment processor for cryptocurrency, that you, the merchant, own and operate. Unlike a credit card processor, you receive payments from your customers directly rather than receiving payments through a mediator. Cutting out the “middle-man” eliminates transaction fees, data harvesting, and credit risk from your business.

You run the Numifex software independently from your eCommerce store. Currently we have integrations built for Woocommerce (Wordpress) and Magento. More are in the works, including custom built sites. If you have a question or request regarding platform integrations, please send us a message.

Currently you can accept payment in BTC(Bitcoin) and ETH(Ethereum) with Numifex. We are in the process of adding more crypto tokens, including ERC20. If you have a question or request regarding tokens, please send us a message.

== Installation ==

1. Installation of the Numifex software is independent of this plugin, and should be done first.
2. Upload the plugin files to the `/wp-content/plugins/Numifex` directory, or install the plugin through the WordPress plugins screen directly.
3. Activate the plugin through the 'Plugins' screen in WordPress
4. Use the Settings->Plugin Name screen to configure the plugin
5. (Make your instructions match the desired user flow for activating and installing your plugin. Include any steps that might be needed for explanatory purposes)


== Frequently Asked Questions ==

= What is Numifex? =

Numifex is a payment processor for cryptocurrency, that you, the merchant, own and operate. Unlike a credit card processor, you receive payments from your customers directly rather than receiving payments through a mediator. Cutting out the “middle-man” eliminates transaction fees, data harvesting, and credit risk from your business.

= How is Numifex different than other Crypto platforms? =

Typically online payment platforms operate as a SAAS (software as a service). You pay a subscription fee, and the SAAS company accepts payments on your behalf. However, this completely eliminates most of the benefits that come from doing business with cryptocurrency. Instead of trusting Visa, you need to trust your payment service, give them access to your business’s payment traffic, and continue to pay them regular fees.

Numifex is software that you can own and operate. You can run it on your own servers entirely independent of us. We can’t receive your payments, withdraw your money, or even view your transactions. You get the full benefit of using Cryptocurrency, peer-to-peer transactions with zero enforcement costs.

= What if I don’t want to host it on my own server? =

We offer a hosting and support service. We will setup a dedicated instance of Numifex just for your use. Although this service is not as decentralized as if you hosted it, it is still superior to SAAS models. We don’t process your payments for you, or collect data, we just host your software.

Contact Us: https://numifex.io/contact-us/

= Will it integrate with my Ecommerce platform? =

Currently, we have a plugin for the WooCommerce WordPress plugin. If your business uses another ecommerce platform, contact us (https://numifex.io/contact-us/). We want to support the ecommerce platforms that you use.

= Which cryptocurrencies do you support? =

Currently, we support Ethereum and Bitcoin. However, we plan to support other major cryptocurrencies in the near future.

We chose Ethereum to launch with Numifex because transactions are fairly quick and cheap. If there is a cryptocurrency you are interested in. Let us know.

= I am hesitant about holding funds in cryptocurrency. =

Accepting cryptocurrency as a payment method does not mean that your business needs to be invested in cryptocurrency. After a transaction is complete, you can sell your cryptocurrency at an exchange for USD or your local currency. This process could even be automated.

As with any foreign currency, there is a volatility risk involved because of changing exchange rates. This is particularly acute with cryptocurrency. Numifex provides the option to set a “cushion” as a percentage of an order total that will be added to the total price to protect against changing prices.

You can learn more about this on the Setup Ethereum page.

= Is it secure? =

Numifex uses industry standard authentication to secure access to the application.

Numifex relies on the security of the blockchain to process payments. It doesn’t store sensitive customer information and it never needs your treasury private key. The data it keeps track of is a simple accounting record.

= How do I get started? =

To take full advantage of the benefits of being an independent merchant, you need to host Numifex on a server you rent or own. This typically means that you will need an IT professional on your team. The level of technical knowledge required is the ability to setup and configure a Linux server. The Server Preparation page will explain these requirements.

If hosting your own server is not something you are interested in, or you do not have access to IT support, we offer a service where we can install and host everything for you for a fee. Please contact us if you are interested.

Note that this is still very different from typical payment processors. Pyrofex does not process your transactions or hold your money. We just host your software.

== Changelog ==

= 1.0 =
* A change since the previous version.
* Another change.

= 0.5 =
* List versions from most recent at top to oldest at bottom.

== Upgrade Notice ==

= 1.0 =
Upgrade notices describe the reason a user should upgrade.  No more than 300 characters.

= 0.5 =
This version fixes a security related bug.  Upgrade immediately.

