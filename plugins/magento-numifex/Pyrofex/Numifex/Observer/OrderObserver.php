<?php
namespace Pyrofex\Numifex\Observer;
 
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;
 
class OrderObserver implements ObserverInterface
{
    public function __construct(
        \Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
        $this->curlFactory = $curlFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_storeManager = $storeManager;
    }

    public function calculatePrice($order, $numifexUrl, $accessToken) {
        $url =  $numifexUrl . '/calculate-price';
        $requstbody = [
            'order_total'=>$order->getGrandTotal(),
            'order_currency'=>'ETH',
            'currency'=>$this->_storeManager->getStore()->getCurrentCurrencyCode()
        ];
        $httpAdapter = $this->curlFactory->create();
        $httpAdapter->write(\Zend_Http_Client::POST, $url, '1.1', [
            "Content-Type:application/json",
            "Authorization: Bearer " . $accessToken
        ],json_encode($requstbody));
        $result = $httpAdapter->read();
        $body = \Zend_Http_Response::extractBody($result);
        $response = \Zend_Http_Response::fromString($result);
        if ($response->isSuccessful()) {
            $response = $this->jsonHelper->jsonDecode($body);
            $order->setCryptoRate($response['rate']);
            $order->setTotalFormatted($response['total_formatted']);
            $order->save();
            $this->placeOrder($order, $numifexUrl, $accessToken);
        } else {
            throw new \Magento\Framework\Exception\CouldNotSaveException(
                __('Failed auth request.')); //Not Working
        }
    }

    public function placeOrder($order, $numifexUrl, $accessToken) {
        $url =  $numifexUrl . '/placeorder'; 
        $requstbody = [
            'commerce_id' => $order->getEntityId(),
            'increment_id' => $order->getIncrementId(),
            'commerce' => 'magento',
            'order_total' => $order->getGrandTotal(),
            'currency_conv_rate' => $order->getCryptoRate(),
            'currency' => $this->_storeManager->getStore()->getCurrentCurrencyCode(),
            'order_currency' => 'ETH'
        ];
        $httpAdapter = $this->curlFactory->create();
        $httpAdapter->write(\Zend_Http_Client::POST, $url, '1.1', [
            "Content-Type:application/json",
            "Authorization: Bearer " . $accessToken
        ],json_encode($requstbody));
        $result = $httpAdapter->read();
        $body = \Zend_Http_Response::extractBody($result);
        $response = $this->jsonHelper->jsonDecode($body);
        $order->setCryptoAddress($response['address']);
        $order->setState('pending_payment')->setStatus('pending_payment');
        $order->save();
        return;
    }


    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $sql = "SELECT numifex_url, access_token FROM core_config_data limit 1";
        $result = $connection->fetchAll($sql); 
        $numifexUrl = $result[0]['numifex_url'];
        $accessToken = $result[0]['access_token'];
        $order = $observer->getEvent()->getOrder();
        $payment = $order->getPayment(); 

        if ($payment->getMethod() == 'numifex') {
            $this->calculatePrice($order, $numifexUrl, $accessToken);
        }
        return;
    }
}
