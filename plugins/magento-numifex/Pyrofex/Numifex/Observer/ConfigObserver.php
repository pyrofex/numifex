<?php

namespace Pyrofex\Numifex\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

class ConfigObserver implements ObserverInterface
{

    protected $helperData;

    public function __construct(
        \Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory,
        \Pyrofex\Numifex\Helper\Data $helperData,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
        $this->curlFactory = $curlFactory;
        $this->helperData = $helperData;
        $this->jsonHelper = $jsonHelper;
    }

    public function checkNumifex($numifexUrl, $secretKey) {
        $httpAdapter = $this->curlFactory->create();
        $httpAdapter->write(\Zend_Http_Client::GET, $numifexUrl, '1.1');
        $result = $httpAdapter->read();
        $statusCode = \Zend_Http_Response::extractCode($result);
        $response = \Zend_Http_Response::fromString($result);
        if ($response->isSuccessful()) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $sql = "UPDATE core_config_data SET numifex_url = " 
            . $connection->quote($numifexUrl);
            $connection->query($sql);
            $this->getToken($numifexUrl, $secretKey);
        } else {
            throw new \Magento\Framework\Exception\CouldNotSaveException(
                __("There is a problem with your Numifex URL. 
                    Please check your Numifex Admin. Make sure you're using a valid URL and that it contains 'https://' or 'https://.")
            ); 
        }
    }

    public function getToken($numifexUrl, $secretKey) {
        $requstbody = [
            'password' => $secretKey,
            'grant_type' => 'password',
            'username' => 'wordpress'
        ];
        $httpAdapter = $this->curlFactory->create();
        $httpAdapter->write(\Zend_Http_Client::POST, $numifexUrl . 
            "/api-clients/authorize", '1.1', [
            "Content-Type:application/json"],json_encode($requstbody));
        $result = $httpAdapter->read();
        $statusCode = \Zend_Http_Response::extractCode($result);
        if ($statusCode == 200) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $body = \Zend_Http_Response::extractBody($result);
            $response = $this->jsonHelper->jsonDecode($body);
            $sql = "UPDATE core_config_data SET secret_key = " . $connection->quote($secretKey) . 
                "," . " access_token = " . $connection->quote($response['access_token']);
            $connection->query($sql);
            return;
        } else {
            throw new \Magento\Framework\Exception\CouldNotSaveException(
                __('This Secret Key is invalid. Please check your key on Numifex, and re-enter.')
            ); 
        }
    }

    public function execute(EventObserver $observer)
    {
        $numifexUrl = $this->helperData->getGeneralConfig('numifex_url');
        $secretKey = $this->helperData->getGeneralConfig('secret_key');
        $this->checkNumifex($numifexUrl, $secretKey);
    }
}