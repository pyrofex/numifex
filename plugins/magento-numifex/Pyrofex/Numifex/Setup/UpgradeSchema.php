<?php

namespace Pyrofex\Numifex\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    
    public function upgrade(
        SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $orderTable = 'sales_order';

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'crypto_currency',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'ETH'
                    ]
                    );
            $setup->endSetup();

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'crypto_rate',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        'length' => '12,2',
                        ['nullable' => false],
                        'comment' => 'ETH Conversion Rate'
                    ]
                    );
            $setup->endSetup();

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'total_formatted',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        'length' => '12,6',
                        ['nullable' => false],
                        'comment' => 'Total String'
                    ]
                    );
            $setup->endSetup();

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'crypto_address',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        ['nullable' => false],
                        'comment' => 'ETH Address'
                    ]
                    );
            $setup->endSetup();

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable('core_config_data'),
                    'numifex_url',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        ['nullable' => false],
                        'comment' => 'Numifex URL'
                    ]
                    );
            $setup->endSetup();

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable('core_config_data'),
                    'secret_key',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        ['nullable' => false],
                        'comment' => 'Secret Key'
                    ]
                    );
            $setup->endSetup();

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable('core_config_data'),
                    'access_token',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        ['nullable' => false],
                        'comment' => 'Access Token'
                    ]
                    );
            $setup->endSetup();
    }
}