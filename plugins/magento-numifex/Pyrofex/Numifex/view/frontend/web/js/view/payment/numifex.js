define(
[
     'uiComponent',
     'Magento_Checkout/js/model/payment/renderer-list',
     'Magento_Checkout/js/view/summary/abstract-total',
     'Magento_Checkout/js/model/quote'
],
function (
     Component,
     rendererList
) {
     'use strict';
     rendererList.push(
         {
             type: 'numifex',
             component: 'Pyrofex_Numifex/js/view/payment/method-renderer/numifex-method'
         }
     );
     /** Add view logic here if needed */
     return Component.extend({});
}
);
