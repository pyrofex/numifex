define(
    [
         'Magento_Checkout/js/view/payment/default',
         'Magento_Checkout/js/view/summary/abstract-total',
         'Magento_Checkout/js/model/quote'
    ],
    function (Component, quote) {
         'use strict';
         return Component.extend({
             defaults: {
                 template: 'Pyrofex_Numifex/payment/numifex'
             },
             /** Returns send check to info */
             getMailingAddress: function() {
                 return window.checkoutConfig.payment.checkmo.mailingAddress;
             },

             /**
             * @return {*|String}
             */
            getEthTotal: function () {
                var convRate = window.checkoutConfig.customData;
                if (convRate.total_formatted) {
                    return convRate.total_formatted + ' ETH';
                }
                else {
                    return 'ERROR - ETH TOTAL NOT AVAILABLE';
                }
            }
         });
    }
);
