<?php

namespace Pyrofex\Numifex\Controller\Index;

class Config extends \Magento\Framework\App\Action\Action
{

	protected $helperData;

	public function __construct(
        \Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
		\Pyrofex\Numifex\Helper\Data $helperData

	)
	{
        $this->curlFactory = $curlFactory;
        $this->helperData = $helperData;
        $this->jsonHelper = $jsonHelper;
		return parent::__construct($context);
	}

	public function execute()
	{

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $sql = "SELECT numifex_url FROM core_config_data limit 1";
        $result = $connection->fetchAll($sql); 
        echo var_dump($result[0]['numifex_url']);
        echo '<pre>'; print_r($result); echo '</pre>'; 
		exit();

	}
}