<?php

namespace Pyrofex\Numifex\Model;

use Magento\Sales\Model\Order;

/**
 * Pay In Store payment method model
 */
class Numifex extends \Magento\Payment\Model\Method\AbstractMethod
{

/**
* Payment code
*
* @var string
*/
protected $_code = 'numifex';

/**
* Availability option
*
* @var bool
*/
//protected $_isOffline = true;
protected $_canAuthorize = true;

/**
     * Authorize a payment.
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return $this
     */
    public function authorize(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        try {
            
        } catch (\Exception $e) {
            $this->debug($payment->getData(), $e->getMessage());
        }
        if(isset($response['transactionID'])) {
            // Successful auth request.
            // Set the transaction id on the payment so the capture request knows auth has happened.
            $payment->setTransactionId($response['transactionID']);
            $payment->setParentTransactionId($response['transactionID']);
        }
        //processing is not done yet.
        $payment->setIsTransactionClosed(0);

        return $this;
    }
}
