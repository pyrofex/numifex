<?php

namespace Pyrofex\Numifex\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

class CustomConfigProvider implements ConfigProviderInterface
{       

    public function __construct(
        \Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
        $this->curlFactory = $curlFactory;
        $this->jsonHelper = $jsonHelper;
    }

    public function getConfig() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
        $grandTotal = $cart->getQuote()->getGrandTotal();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $sql = "SELECT numifex_url, access_token FROM core_config_data limit 1";
        $result = $connection->fetchAll($sql); 
        $numifexUrl = $result[0]['numifex_url'];
        $accessToken = $result[0]['access_token'];
        $url =  $numifexUrl . '/calculate-price'; //pass dynamic url
        $requstbody = [
            'order_total'=>$grandTotal,
            'order_currency'=>'ETH',
            'currency'=>'USD'
        ];
        /* Create curl factory */
        $httpAdapter = $this->curlFactory->create();
        /* Forth parameter is POST body */
        $httpAdapter->write(\Zend_Http_Client::POST, $url, '1.1', [
            "Content-Type:application/json",
            "Authorization: Bearer " . $accessToken
        ],json_encode($requstbody));
        $result = $httpAdapter->read();$config = [];
        $response = \Zend_Http_Response::fromString($result);
        if ($response->isSuccessful()) {
            $body = \Zend_Http_Response::extractBody($result);
            $response = $this->jsonHelper->jsonDecode($body);
            $config['customData'] = $response;
            return $config;
        } else {
            $config['customData'] = 'null';
            return $config;
        }
    }
}