import React, { Component } from "react";
import { HashRouter, Switch, Route, Redirect } from "react-router-dom";
import MainPage from "./components/MainPage/MainPage";
import Confirmation from "./components/Confirmation/Confirmation";
import CoinSelect from "./components/Confirmation/CoinSelect";
import FinalizeOrder from "./components/Confirmation/FinalizeOrder";
import Login from "./components/Login/Login";
import OrderList from "./components/OrderList/OrderList";
import Cookies from 'js-cookie';
import "./App.css";

function NoMatch() {
  return <Redirect to={{ pathname: "/" }} />;
}

class App extends Component {
  constructor() {
    super();
    let authToken;
    let logged = false;
    const startToken = Cookies.get('access_token');
    if (startToken && startToken.length > 0) {
      logged = true;
      authToken = startToken;
    }

    this.state = {
      logged,
      authToken,
      orderTotal: 0,
      orderId: '',
    };

    this.updateLogin = this.updateLogin.bind(this);
    this.updatePriceInfo = this.updatePriceInfo.bind(this);
    this.updateFinalOrder = this.updateFinalOrder.bind(this)
    
  }


  updateLogin(token) {
    Cookies.set('access_token', token, { expires: 1 });
    this.setState({ logged: true, authToken: token });
  }
  
  updatePriceInfo(orderTotal, orderId) {
    this.setState({orderTotal, orderId})
  }

  updateFinalOrder(obj) {
    this.setState({finalOrderObj: obj});
  }

  render() {
    const { logged, authToken, orderId, orderTotal, finalOrderObj } = this.state;
    return (
      <HashRouter>
          {logged && authToken ? (
            <div className='App'>
              <div className='numifex-tag'>
                <h1>Powered by</h1>
                <img className='power-img' src='numifex-media/numifex_w_logo.png' alt='numifex' />
              </div>
              <Switch>
                <Route
                  path="/"
                  exact
                  render={renderProps => (
                    <MainPage authToken={authToken} {...renderProps} updatePriceInfo={this.updatePriceInfo} />
                  )}
                />
                <Route
                  path="/coinSelect"
                  render={renderProps => (
                    <CoinSelect authToken={authToken} {...renderProps} />
                  )}
                />
                <Route
                  path="/confirmation/:coin"
                  render={renderProps => (
                    <Confirmation authToken={authToken} orderTotal={orderTotal} orderId={orderId} updateFinalOrder={this.updateFinalOrder} {...renderProps} />
                  )}
                />
                <Route
                  path="/finalize"
                  render={renderProps => (
                    <FinalizeOrder authToken={authToken} finalOrderObj={finalOrderObj} orderTotal={orderTotal} {...renderProps} />
                  )}
                />
                <Route
                  path="/orders"
                  render={renderProps => (
                    <OrderList authToken={authToken} updateFinalOrder={this.updateFinalOrder} {...renderProps} />
                  )}
                />
                <Route component={NoMatch} />
              </Switch>
            </div>
          ) : (
            <div className='App'>
              <Switch>
                <Route
                  path="/"
                  exact
                  render={renderProps => (
                    <Login updateLogin={this.updateLogin} {...renderProps} />
                  )}
                />
                <Route component={NoMatch} />
              </Switch>
            </div>
          )}
      </HashRouter>
    );
  }
}

export default App;
