import React, { Component } from "react";
import { Link } from "react-router-dom";
import NumPad from "./NumPad";
import "./MainPage.css";

class MainPage extends Component {
  constructor() {
    super();
    this.state = {
      orderArr: [],
      orderTotal: 0,
      orderId: ""
    };

    this.numPadCalc = this.numPadCalc.bind(this);
    this.updateOrderId = this.updateOrderId.bind(this);
    this.clearPrice = this.clearPrice.bind(this);
    this.priceBack = this.priceBack.bind(this);
    this.submitOrder = this.submitOrder.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.inputOnBlur = this.inputOnBlur.bind(this);
    this.inputOnFocus = this.inputOnFocus.bind(this);
  }

  componentDidMount() {
    document.addEventListener("keydown", this.handleKeyDown);
  }

  numPadCalc(val) {
    let { orderArr } = this.state;
    if (val === "00") orderArr.push(parseFloat(val));
    orderArr.push(parseFloat(val));
    if (orderArr.length > 8) orderArr = [9, 9, 9, 9, 9, 9, 9, 9];
    const orderTotal = orderArr.join("") / 100;
    if (orderTotal === 0) orderArr = [];
    this.setState({ orderArr, orderTotal });
  }

  handleKeyDown(e) {
    const nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
    if (nums.includes(parseFloat(e.key))) this.numPadCalc(e.key);
  }

  inputOnFocus(e) {
    document.removeEventListener("keydown", this.handleKeyDown);
  }

  inputOnBlur(e) {
    document.addEventListener("keydown", this.handleKeyDown);
  }

  clearPrice() {
    this.setState({ orderArr: [], orderTotal: 0 });
  }

  priceBack() {
    let { orderArr } = this.state;
    orderArr.pop();
    const orderTotal = orderArr.join("") / 100;
    if (orderTotal === 0) orderArr = [];
    this.setState({ orderArr, orderTotal });
  }

  updateOrderId(e) {
    this.setState({ orderId: e.target.value });
  }

  submitOrder() {
    const { orderId, orderTotal } = this.state;
    this.props.updatePriceInfo(orderTotal, orderId);
  }

  render() {
    const { orderTotal, orderId } = this.state;
    return (
      <div className="mainpage-outer">
        <NumPad
          numPadCalc={this.numPadCalc}
          orderTotal={orderTotal}
          clearPrice={this.clearPrice}
          priceBack={this.priceBack}
        />
        <div className="mainpage-options">
          <Link to="/orders" className="mainpage-button">
            <button>Previous Orders</button>
          </Link>
          <div className="submit-section">
            <input
              autocomplete="off"
              autocorrect="off"
              autocapitalize="off"
              spellcheck="false"
              type="text"
              value={orderId}
              onChange={this.updateOrderId}
              placeholder="Order ID -- Required"
              onFocus={this.inputOnFocus}
              onBlur={this.inputOnBlur}
            />
            <Link to="/coinSelect" className="mainpage-button">
              <button
                id="setup-btn"
                disabled={orderId && orderTotal ? false : true}
                onClick={this.submitOrder}
              >
                Setup Order
              </button>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default MainPage;
