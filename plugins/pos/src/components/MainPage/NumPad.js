import React from "react";

function NumPad(props) {
  const { orderTotal, numPadCalc, clearPrice, priceBack } = props;
  const usdFormat = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
  });
  const displayVal = usdFormat.format(orderTotal.toFixed(2));
  return (
    <div className="numpad-outer">
      <table>
        <tbody>
          <tr>
            <td colSpan="4" className='numpad-display'>{displayVal}</td>
          </tr>
          <tr>
            <td onClick={() => numPadCalc(1)}>
              1
            </td>
            <td onClick={() => numPadCalc(2)}>
              2
            </td>
            <td onClick={() => numPadCalc(3)}>
              3
            </td>
            <td rowSpan="2" onClick={priceBack} className='numpad-button'>Back</td>
          </tr>
          <tr>
            <td onClick={() => numPadCalc(4)}>
              4
            </td>
            <td onClick={() => numPadCalc(5)}>
              5
            </td>
            <td onClick={() => numPadCalc(6)}>
              6
            </td>
          </tr>
          <tr>
            <td onClick={() => numPadCalc(7)}>
              7
            </td>
            <td onClick={() => numPadCalc(8)}>
              8
            </td>
            <td onClick={() => numPadCalc(9)}>
              9
            </td>
            <td rowSpan="2" onClick={clearPrice} className='numpad-button'>Clear</td>
          </tr>
          <tr>
            <td colSpan="2" id="00" onClick={() => numPadCalc('00')}>
              00
            </td>
            <td id="0" onClick={() => numPadCalc(0)}>
              0
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default NumPad;
