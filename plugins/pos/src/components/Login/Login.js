import React, { Component } from "react";
import axios from 'axios';
import './Login.css';

class Login extends Component {
  constructor() {
    super();
    this.state = {
      username: 'point-of-sale',
      password: ''
    };

    this.sumbitLogin = this.sumbitLogin.bind(this);
    this.updateSecret = this.updateSecret.bind(this);

  }

  sumbitLogin() {
    const { password, username } = this.state;
    axios.post('/api-clients/authorize', { password, username, grant_type: 'password' }).then(res => {
      this.props.updateLogin(res.data.access_token);
    });
  }


  updateSecret(e){
    this.setState({password: e.target.value});
  }

  render() {
    const background = {
      background: 'url("numifex-media/section2_BG.png")',
      backgroundPosition: 'bottom',
      backgroundSize: '100vw 80vh',
      backgroundRepeat: 'no-repeat'
    };
    return (
      <div className='login-outer' style={background}>
        <div className='login-container'>
          <img src='numifex-media/numifex_full_icon.png' alt='Numifex' className='login-logo' />
          <input placeholder='Client Secret' onChange={this.updateSecret} type='password' />
          <button onClick={this.sumbitLogin}>Login</button>
        </div>
      </div>
    );
  }
}

export default Login;
