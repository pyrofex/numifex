import React, { Component } from "react";
import axios from "axios";
import { Link } from 'react-router-dom';
import './OrderList.css';

class OrderList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authToken: props.authToken,
      orderPage: 1,
      hasNextPage: true,
      orderArr: [],
      statusColor: {
        created: '#d2b400',
        paid: '#005bd2',
        finished: '#0a7500',
        cancelled: '#000000',
        cancelling: '#ac0000'
      }
    };
    
    this.setupOrderList = this.setupOrderList.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.getOrders = this.getOrders.bind(this);

  }
  
  componentDidMount() {
    const { orderPage } = this.state;
    this.getOrders(orderPage);
  }

  getOrders(orderPage) {
    const { authToken } = this.state;
    axios.get('/list-orders-summary', {
      headers: { Authorization: `Bearer ${authToken}`},
      params: { page: orderPage }
    })
    .then(res => {
      const { orders, hasNextPage } = res.data;
      this.setState({orderArr: orders, hasNextPage, orderPage});
    })  }

  setupOrderList(arr) {
    const usdFormat = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
    });
    return arr.map(order => {
      const { id, commerce_id, date_created, order_total, order_currency, status, currency_conv_rate } = order;
      const { statusColor }= this.state;
      const usdTotal = usdFormat.format((order_total * currency_conv_rate).toFixed(2));
      const dateFormat = new Date(date_created);
      const month = dateFormat.getMonth() + 1;
      const day = dateFormat.getDate();
      const year = dateFormat.getFullYear();
      const hours = dateFormat.getHours();
      let minutes = dateFormat.getMinutes().toString();
      let time;
      if ( minutes.toString().length === 1 ) {
        minutes = `0${minutes}`;
      }
      if ( hours > 12 ) {
        time = `${(hours - 12)}:${minutes} PM`;
      } else if ( hours === 0 ) {
        time = `12:${minutes} AM`;
      } else {
        time = `${hours}:${minutes} AM`;
      }
      return (
        <Link to='/finalize' key={id} className='list-link smallgrow'>
          <div onClick={() => this.handleOrderClick(order, usdTotal)} className='list-tile'>
              <h4>{month}-{day}-{year}<span> : </span>{time}<span> | Order ID:</span> {commerce_id}</h4>
              <div className='list-price'>
                <h3>{usdTotal} USD</h3>
                <h3><span>---  {order_total} {order_currency}</span></h3>
              </div>
              <div style={{backgroundColor: statusColor[status.toLowerCase()]}} className='list-status'>
                <h5>{status.toUpperCase()}</h5>
              </div>
          </div>
        </Link>
      );
    })
  }

  handleOrderClick(order, usdTotal){
    const { order_total, order_currency, pub_key } = order;
    const orderDataObj = {
      total_formatted: order_total,
      coin: order_currency,
      address: pub_key,
      orderList: true,
      usdTotal,
    }
    this.props.updateFinalOrder(orderDataObj);
  }

  handlePageChange(e) {
    const { orderPage } = this.state; 
    const { name } = e.target;
    let nextPage;
    if (name === 'next') {
      nextPage = orderPage + 1;
    } else if (name === 'back') {
      nextPage = orderPage - 1;
    }
    this.getOrders(nextPage);
  }

  render() {
    const { orderArr, orderPage, hasNextPage } = this.state;
    let displayMap = [];
    if (orderArr) {
      displayMap = this.setupOrderList(orderArr);
    }
    const background = {
      background: 'url("numifex-media/background.png")',
      backgroundPosition: 'center',
      backgroundSize: 'cover',
    };
    return (
      <div className='main-outer' id='list-outer' style={background}>
        <div className='main-container' id='list-container'>
          <Link to='/'>
            <button className='conf-button' id='list-btn'>Go Back</button>
          </Link>
          <h1>Order List</h1>
          <div className='page-selection'>
            <h2>Page</h2>
            <div className='page-actions'>
              <div>
                { orderPage > 1 ?
                  <button name='back' onClick={this.handlePageChange}>Back</button>
                  :
                  null
                }
              </div>
              <h2>{orderPage}</h2>
              <div>
                { hasNextPage ?
                  <button name='next' onClick={this.handlePageChange}>Next</button>
                  :
                  null
                }
              </div>
            </div>
          </div>
          <div className='list-section'>
            {displayMap}
          </div>
        </div>
      </div>
    );
  }
}

export default OrderList;
