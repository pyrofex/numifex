import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import "./Confirmation.css";

class FinalizeOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orderTotal: props.orderTotal,
      orderData: props.finalOrderObj,
      authToken: props.authToken,
      imgData: "",
      showAddress: false
    };

    this.addressToggle = this.addressToggle.bind(this);
  }

  componentDidMount() {
    const { orderData, authToken } = this.state;
    const { coin, address, total_formatted } = orderData;
    console.log(orderData);
    axios
      .get(`/qr/${coin}/${address}`, {
        headers: { Authorization: `Bearer ${authToken}` },
        params: { amount: total_formatted }
      })
      .then(res => {
        const imgData = window.btoa(res.data);
        this.setState({ imgData });
      });
  }

  addressToggle() {
    const { showAddress } = this.state;
    this.setState({ showAddress: !showAddress });
  }

  render() {
    const { orderData, imgData, showAddress, orderTotal } = this.state;
    const { coin, address, total_formatted, orderList, usdTotal } = orderData;
    let displayTotal = usdTotal;
    if (orderTotal) displayTotal = orderTotal;
    const background = {
      background: 'url("numifex-media/hero_BG.jpg")',
      backgroundPosition: 'center',
      backgroundSize: 'cover',
    };
    return (
      <div className="main-outer" style={background}>
        <div className="main-container" id="conf-section">
          <div className="order-section">
            <h1 className='conf-title'>Complete Transaction</h1>
            <div className="final-inner">
              <h3 id="final-price">{total_formatted} {coin}<span> | {displayTotal} USD</span></h3>
              <h2>Scan the QR code to complete your order</h2>
              <div className="address-outer">
                {imgData ? (
                  <img
                    className={showAddress ? "qr-img hidden" : "qr-img shown"}
                    src={`data:image/svg+xml;base64, ${imgData}`}
                    alt="QR"
                  />
                ) : null}
                <h1 className={showAddress ? "final-address shown" : "final-address hidden"}>
                  {address}
                </h1>
              </div>
              <button onClick={this.addressToggle} className="conf-button" id='address-btn'>
                {showAddress ? "Get QR Code" : "Get Full Address"}
              </button>
            </div>
          </div>
          <div className='orderbtn-list'>
            {orderList ? (
              <Link to="/orders" id='f-order-link'>
                <button className="conf-button" id='f-order-btn'>Order List</button>
              </Link>
            ) : null}
            <Link to="/">
              <button className="conf-button" id='finish-btn'>Finish</button>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default FinalizeOrder;
