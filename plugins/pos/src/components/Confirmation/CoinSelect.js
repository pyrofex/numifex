import React, { Component } from "react";
import { Link } from 'react-router-dom';
import './Confirmation.css';

class CoinSelect extends Component {
  constructor() {
    super();
    this.state = {
      cryptoArr: ['BTC', 'ETH'],
    };
  }

  coinDisplay(arr) {
    return arr.map((coin, i) => {
      return (
        <Link className='coin-tile grow' to={`/confirmation/${coin}`} key={i}>
            <img src={`coin-media/logo_${coin.toLowerCase()}.png`} alt={`${coin}`} className='coin-img'/>
        </Link>
      );
    })
  }

  render() {
    const { cryptoArr } = this.state;
    const displayMap = this.coinDisplay(cryptoArr);
    const background = {
      background: 'url("numifex-media/hero_BG.jpg")',
      backgroundPosition: 'center',
      backgroundSize: 'cover',
    };
    return (
      <div className='main-outer' style={background} >
        <div className='main-container' id='conf-section'>
          <div className='coin-container'>
            <h1 className='conf-title'>Select your preferred Cryptocurrency:</h1>
            <div className='coin-display'>
              {displayMap}
            </div>
          </div>
          <button onClick={this.props.history.goBack} className='conf-button'>Go Back</button>
        </div>
      </div>
    );
  }
}

export default CoinSelect;
