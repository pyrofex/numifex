import React, { Component } from "react";
import axios from "axios";
import { Redirect, Link } from "react-router-dom";
import "./Confirmation.css";

class Confirmation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orderTotal: props.orderTotal,
      orderId: props.orderId,
      selectCoin: props.match.params.coin,
      authToken: props.authToken,
      orderSubmit: false
    };

    this.setupCryptoChoices = this.setupCryptoChoices.bind(this);
    this.coinToggle = this.coinToggle.bind(this);
    this.submitOrderDetails = this.submitOrderDetails.bind(this);
  }

  componentDidMount() {
    const { authToken, orderTotal } = this.state;
    if (authToken && orderTotal) {
      this.setupCryptoChoices();
    }
  }

  setupCryptoChoices() {
    const { orderTotal, authToken, selectCoin } = this.state;
    axios
      .post(
        "/calculate-price",
        { order_total: orderTotal, order_currency: selectCoin },
        { headers: { Authorization: `Bearer ${authToken}` } }
      )
      .then(res => {
        res.data.coin = selectCoin;
        this.setState({ coinPriceData: res.data });
      }).catch(err => {
        let error = 'API ERROR';
        if (err.response.data) {
          error = err.response.data.error
        }
        this.setState({apiError: error});
      });
  }

  coinToggle(e) {
    const { coinClick } = this.state;
    const value = e.target.value;
    for (let x in coinClick) {
      if (x !== value) coinClick[x] = false;
    }
    if (value in coinClick) coinClick[value] = !coinClick[value];
    this.setState({ coinClick });
  }

  submitOrderDetails() {
    const { coinPriceData, selectCoin, authToken, orderId } = this.state;
    const orderDetails = {
      commerce_id: orderId,
      commerce: "pos",
      order_total: coinPriceData.total,
      order_currency: selectCoin,
      currency_conv_rate: coinPriceData.rate,
      currency: "USD"
    };

    axios
      .post("/placeorder", orderDetails, {
        headers: { Authorization: `Bearer ${authToken}` }
      })
      .then(res => {
        coinPriceData.address = res.data.address;
        this.props.updateFinalOrder(coinPriceData);
        this.setState({ orderSubmit: true });
      }).catch(err => {
        let error = 'API ERROR';
        if (err.response.data) {
          error = err.response.data.error
        }
        this.setState({apiError: error});
      });
  }

  render() {
    const { coinPriceData, orderSubmit, orderTotal, apiError } = this.state;
    const usdFormat = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
    });
    if (orderSubmit) {
      return <Redirect to="/finalize" />;
    }
    const background = {
      background: 'url("numifex-media/hero_BG.jpg")',
      backgroundPosition: 'center',
      backgroundSize: 'cover',
    };
    return (
      <div className="main-outer" style={background}>
        <div className="main-container" id="conf-section">
          <div className='order-section'>
            <h1 className='conf-title'>Order Details</h1>
            {coinPriceData ?
              <div className='order-inner'>
                <img src={`coin-media/logo_${coinPriceData.coin.toLowerCase()}.png`} alt={`${coinPriceData.coin}`}/>
                <div className='order-price-section'>
                  <h3 id="total-price">{coinPriceData.total_formatted} {coinPriceData.coin}</h3>
                  <div className='sub-price'>
                    <h3>Total: <span>{usdFormat.format(orderTotal.toFixed(2))} USD</span></h3>
                    <h3>Exchange Rate: <span>{parseFloat(coinPriceData.rate).toFixed(2)} {coinPriceData.coin}/USD</span></h3>
                  </div>
                </div>
                <div className='orderbtn-list'>
                  <button onClick={this.submitOrderDetails} className='conf-button' id='submit-btn'>
                    Submit
                  </button>
                  <Link to="/">
                    <button className='conf-button' id='cancel-btn'>Cancel</button>
                  </Link>
                </div>
                <p>*The total {coinPriceData.coin} amount is an approximate. Your exchange rate will be locked in and provided once you submit and finalize your order. Once submitted you will recieve your total due and provided an address to send the payment to.</p>
              </div>
            : apiError ?
            <div className='order-inner'>
              <h1>{apiError}</h1>
            </div>
            :
            <div className='order-inner'>
              <h1>Loading Order Details...</h1>
            </div>
            }
          </div>
          <button onClick={this.props.history.goBack} className='conf-button'>Go Back</button>
        </div>
      </div>
    );
  }
}

export default Confirmation;
