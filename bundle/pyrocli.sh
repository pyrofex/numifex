#!/bin/bash
set -e

get_ipv4 () {
    # Get ip address of running vm guest
    # "virsh domifaddr <vm-name>" works sometimes but not all the time
    vm_name=$1
    for i in {1..20}; do
        mac=$(virsh domiflist "${vm_name}" | grep -o -E "([0-9a-f]{2}:){5}([0-9a-f]{2})")
        ip=$(ip neighbor | grep "${mac}" | grep -o -P "^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}") || true
        if [[ "${ip}" ]]; then
            echo "${ip}"
            break
        fi
        echo "Waiting for ip address." >&2
        sleep 5
        vm_check=$(virsh list | grep "${vm_name}")
        if [[ ! "${vm_check}" ]]; then
            echo "E: No vm guest instance found running for ${vm_name}." >&2
            exit 1
        fi
    done
    if [[ ! "${ip}" ]]; then
        echo "E: No ip address assigned for vm guest ${vm_name}." >&2
        exit 1
    fi
}

vm_create () {
    vm_base_image_url="https://repo.pyrofex.io/cloud/images/bionic/current/pyro-bionic-server-cloudimg-amd64.qcow2"
    vm_name="numifex"
    vm_username="numifex"
    vm_userpass="numifex"
    vm_desc="Numifex VM Host"
    vm_base_image="base-image-numifex.qcow2"
    vm_image="numifex.qcow2"
    init_iso="init.iso"
    os_variant="ubuntu18.04"  # fedora28, ubuntu18.04, rhel7,  ...
    user_data="user-data"
    meta_data="meta-data"

    if [[ ! -f "${vm_base_image}" ]]; then
        curl -q -o "${vm_base_image}" "${vm_base_image_url}"
    fi

    virsh destroy "${vm_name}" || true
    virsh undefine "${vm_name}" || true
    rm -f "${init_iso}" || true
    rm -f "${vm_image}" || true

    cp -i "${vm_base_image}" "${vm_image}"

    # genisoimage -input-charset utf-8 -output init.iso -volid cidata -joliet -rock user-data meta-data
    # genisoimage -input-charset utf-8 -output init.iso -volid cidata -joliet -rock << EOF
    cat << EOF > ${user_data} 
#cloud-config
password: ${vm_userpass}
chpasswd: {expire: False}
ssh_pwauth: True
ssh_authorized_keys:
  - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICtn+/vPgYmOKwNSoJmLY2xHldS07a0CY4eDk9t2iZBr tmpkey
runcmd:
  - echo "Example 1" >> test.txt
EOF
    cat << EOF > ${meta_data} 
instance-id: numifex 
local-hostname: numifex.numifex
EOF
# instance-id: "${vm_name}"
# local-hostname: "${vm_name}".numifex
    genisoimage -input-charset utf-8 -output init.iso -volid cidata -joliet -rock "${user_data}" "${meta_data}"

    virt-install --name "${vm_name}" \
    --description "${vm_desc}" \
    --ram 2048 \
    --vcpus 2 \
    --disk path=./"${vm_image}" \
    --os-type linux \
    --os-variant "${os_variant}" \
    --network bridge=virbr0 \
    --cdrom "${init_iso}" \
    --noautoconsole
    ipv4=$(get_ipv4 "$vm_name")
    echo $ipv4
    echo "ssh $vm_username@$ipv4"
}


vm_create
trap "{ rm -f ${meta_data}; rm -f ${user_data}; }" EXIT
