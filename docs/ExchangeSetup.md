# Basic Numifex to Exchange Setup

Numifex creates a new account/wallet on-the-fly per customer order. So if you're running an ecommerce store Wordpress, every time a customer places an order in your store, and uses a Numifex payment method, a new account/wallet will be created just for that order. 

You will give Numifex a treasury account per crypto token (i.e. BTC, ETH), during setup. When payment is received for an order, the tokens from the account for that order will be automatically transferred into your treasury account. So, the treasury account is a sort of master account you give Numifex for each crypto you're accepting.

Typically, a Numifex user will use an exchange account, such as a Coinbase wallet, as their treasury account. From a Coinbase account, you can make an exchange into a different crypto, or cash out into a fiat currency deposited in your bank account.

But for privacy and security reasons, a Numifex user may want to create an extra step, moving crypto into a different exchange account, to avoid blacklisting or other disciplinary actions by Coinbase. In this case, you would consider your Coinbase account as your 'cold wallet', and you would have another account with an exchange like Kucoin, Gemini, or Kraken. This non-Coinbase account would be your 'hot wallet', which you will more actively use for making crypto-purchases and trading, and your Coinbase account would strictly be a go-between. With this setup, it will be more difficult for Coinbase to associate your account with actions they consider unacceptable (but still legal).