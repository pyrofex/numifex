.. _woocommerce-label:

WooCommerce
===========

This page will help you install the Numifex plugin for WooCommerce.

Setup
-----

You can install the plugin from the WordPress admin page, or manually through FTP/SSH.

WordPress Plugin
~~~~~~~~~~~~~~~~

**Option 1: Admin Installation**

1. Download ``wp-numifex.zip`` from your unique Numifex link.

.. code-block:: console

    $ wget https://repo.pyrofex.io/numifex/{YOUR_KEY}/latest/wp-numifex.zip

2. Open the Admin page and click on **Plugins**. Click **Add New**.
3. Click **Upload plugin** and select the ``numifex-wp.zip`` file.

    .. image:: /_static/wp-upload.png

4. After the package is processed, press **Install Now**.
5. After installing the plugin needs to be enabled. Click **Activate Plugin** on this screen. Find **Numifex Payment Gateway** and click **Activate Plugin**.

    .. image:: /_static/wp-activate.png

6. On the admin sidebar there should be a link called **Numifex Plugin** click on it.

7. In the field called **numifex URL** you need to enter a URL for the payment processing server.

**Option 2: Manual Installation**

Use FTP or SSH to upload the archive `numifex-wp.zip` to your server. Unzip the archive and copy the `numifex-wp` to your WooCommerce plugins folder.

The plugins folder is typically located at:

    /wp-content/plugins

For more information about installing WordPress plugins see `here <https://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation_by_Uploading_a_Zip_Archive>`_.

Give WooCommerce Key to Payment Processor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You need to allow the payment processor to send order information to WooCommerce.

1. Open the WooCommerce admin. Click on **WooCommerce -> Settings** in the admin sidebar.

2. Click on **Advanced -> Legacy API**. Ensure that **Enable the legacy REST API** is checked.

3. Click on **Advanced -> REST API**. Click **Add Key**.

4. Give it a description. For example "Numifex payments". Change the permission to **Read/Write**.

5. Click **Generate API Key**.

6. In another tab, open the Numifex admin ``https://yoursite:4000/admin/``.
   Copy the fields **Consumer Key** and **Consumer Secret** from the WooCommerce page to the fields on the Numifex admin page.
   Save those by pressing the **Submit** button.

For more information see `WooCommerce Rest API <https://docs.woocommerce.com/document/woocommerce-rest-api/>`_.

Give Payment Processor Key to WooCommerce
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. In your browser, go to the payment processor admin page. For example http://numifex-host:4000/admin.

2. Click on **Commerce** and expand the **WooCommerce** section.

3. Enter the URL to your WordPress site in the "WordPress URL" field. For example http://numifex-host.

.. image:: /_static/woocommerce_config.png

4. Click on the **API** Tab. Click **Create new API Token**.

5. In another tab, open the WooCommerce admin. Click on the Numifex tab. 
   Copy the field **Client Secret** from the Numifex page to the WordPress page.

.. image:: /_static/creating_api_token.png

Give Payment Processor URL to WooCommerce
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You also need to tell the WooCommerce plugin where to find the payment processor.

1. Click on "Numifex" in the admin sidebar in WordPress.

2. In the field "Numifex URL" enter the URL for the payment processor. For example "http://numifex-host:4000".

Settings
---------------

You can enable/disable and configure each crypto currency in **WooCommerce -> Settings -> Payments**.

Pricing
~~~~~~~~

You can set price settings for each coin individually.
Read more about how these at :ref:`price-conversion-label`.

