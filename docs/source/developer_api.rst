.. _developer-api-label:

Developer API
=============

Numifex provides a simple REST API
which allows you integrate with a custom eCommerce platform.

Authorization
-------------

All requests must include an access token
in the ``Authorization`` header.
This should follow the bearer pattern:

``Authorization: Bearer l/zIdEbx7iDf/m4cLejrwY1ZrbqLeFpVL72xesBbFfo=``

To obtain an access token, you
must present an API secret in an authorization request.
You can create an API Secret from the Numifex admin:

.. image:: /_static/creating_api_token.png

.. http:post:: /api-clients/authorize

    Obtain a session token from an API token.
    This attempts to follow the `OAuth spec, section 4.4 <https://tools.ietf.org/html/rfc6749#section-4.4.2>`_.

    .. sourcecode:: json

        {
            "username": "ecommerce",
            "password": "8ol92H3/02xMZYsr9SXXz72vwplsT4xci9nyupZH4qk=",
            "grant_type": "password"
        }

    :<json string username:
        A username associated with the API secret.
    
    :<json string password: 
        The API secret.

    :<json string grant_type:
        Must be ``password``.

    .. sourcecode:: json

        {
            "access_token": "l/zIdEbx7iDf/m4cLejrwY1ZrbqLeFpVL72xesBbFfo=",
            "token_type": "bearer"
        }

    :>json string access_token:
        The access token which you will include in request headers.

    :>json string token_type:
        Will be ``bearer``.

Creating Orders
---------------

.. http:post:: /placeorder

    Submit a new cryptocurrency order.

    .. sourcecode:: json

        {
            "commerce_id": "woocommerce",
            "order_total": "100000000",
            "order_currency": "ETH",
            "currency_conv_rate": 256.5,
            "currency": "USD",
        }


    :<json string commerce_id:
        An identifier for the eCommerce platform which submitted the order.
        If you are writing a custom client, you should define your own.
        ``woocommerce`` and ``magento`` are reserved.

    :<json string order_total:
        The total amount to charge for this order.
        Typically this is represented in the smallest denomination for the currency
        (wei for Ethereum, Satoshi for Bitcoin, etc).
        Stored as a string to hold big numbers.

    :<json string order_currency:
        A code identifying which crypto currency the order is payed in.
        For example ETH or BTC.

    :<json number currency_conv_rate:
        The conversation rate from fiat to crypto.
        ``fiat/crypto``. For example ``8000.0`` for 8000 USD/BTC.
        This is only for record keeping purposes and is optional.

    :<json string currency: 
        A string code identifying the fiat currency that the order originated from.
        For example EUR or USD.
        This is only for record keeping purposes and is optional.

    .. sourcecode:: json

        {
            "address": "0xa04eA47131ccA5dAe66b7bDbc57241525e3c22EE"
        }

    :>json string address:
        The crypto currency address created for this order.
        The order is payed when this settlement address is funded with the correct balance.


Pricing Orders
----------------

.. http:post:: /calculate-price

    Most merchants and eCommerce platforms do business in fiat currency (USD, EUR, etc).
    This route is provided to convert the price of an order from fiat, to crypto currency.

    It also has parameters for risk level which allow the route to pick
    a slightly higher price which accounts for volatility.
    For details about these options see :ref:`price-conversion-label`

    .. sourcecode:: json

        {
            "order_total": "100.0",
            "order_currency": "ETH",
            "time_to_sell": 3,
            "risk_level": "market"
        }
    
    :<json number order_total:
        The total amount in fiat to convert to crypto.
        For example ``100.0`` for $100 USD.

    :<json string order_currency:
        A code identifying a crypto currency to convert to.
        For example ``ETH`` or ``BTC``.

    :<json string currency:
        A code identifying the fiat currenct to convert from.
        For example ``USD`` or ``EUR``.

    :<json number time_to_sell:
        The number of days the merchant intends to wait
        before exchanging the crypto for fiat at an exchange.

    :<json string risk_level:
        The risk levels are ``low``, ``medium``, ``high``, or ``market``.
        The default is ``market``.

    :>json string total_formatted:
        The total crypto amount in natural units.

    :>json string total:
        The total crypto amount in base units.

    :>json number rate:
        The exchange rate ``fiat/crypto`` which was used in the calculation.

QR Codes
--------

QR codes provide a convenient way for customers
to pay, without needing to type in tedious information.

Numifex can generate QR code images for payments.
Each crypto currency uses a unique QR code format.

.. http:get:: /qr/(string:crypto)/(string:address)

    Generate a QR code image. ``crypto`` is a code identifying 
    a crypto currency. For example ``BTC`` or ``ETH``.

    ``address`` is the crypto currency address to pay.

    The image is returned as an SVG in the response body.

    :query amount: 
        The amount of crypto to charge in natural units such as ETH or BTC.
        (optional)

QR codes attempt to follow the standard formats
for each crypto:

- `Bitcoin BIP 21 <https://github.com/bitcoin/bips/blob/master/bip-0021.mediawiki#Transfer%20amount/size>`_
- `Ethereum EIP 681 <https://eips.ethereum.org/EIPS/eip-681>`_

Those that do not have a standard
will contain only the address.

Currency Codes
--------------

This `page <https://www.ibm.com/support/knowledgecenter/en/SSZLC2_7.0.0/com.ibm.commerce.payments.developer.doc/refs/rpylerl2mst97.htm>`_
lists currency codes for major currencies.


