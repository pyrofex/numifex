.. _bitcoin-label:

Bitcoin
=======

This page will help you setup Bitcoin.
It also has supplemental information and links to resources about Bitcoin.

Setup
--------

1. Visit the payment processor admin page in your browser.
   For example ``https://yournumifex/admin``. Login if necessary.

2. Click on the **Cryptos** tab and expand the **Bitcoin** section.

.. image:: /_static/bitcoin_config.png

3. Enter your credentials for your Bitcoin Core node.
   If you are unsure what this is, see :ref:`bitcoin-nodes`.

4. Enter your treasury address. 
   This should be a Bitcoin address that you control. See :ref:`bitcoin-treasury-address`

5. Review the fee and confirmation settings to ensure they are adequate.
   See :ref:`ethereum-gas-label`.


You should now be ready to accept Bitcoin payments!

.. _bitcoin-nodes:

Bitcoin Node
------------

The payment processor needs access to the Bitcoin network to send transactions and confirm payments.

**Option 1: Axia Codex**

If you are signed into `Axia Codex <https://axiacodex.io/services>`_, access to the Bitcoin
network is provided.
You can still override this with a custom URL.

**Option 2: Run Bitcoin Core**

The Bitcoin community emphasizes the importance of `running your own node <https://bitcoin.org/en/full-node>`_.

The official node software is `Bitcoin Core <https://bitcoin.org/en/bitcoin-core/>`_.
Unlike most cryptocurrencies, it requires only a reasonable amount of disk space (250 GB at time of writing)
and network bandwidth.

Bitcoin Core also includes wallet software which you can use to store your keys and manually send transaction.

To use Bitcoin core, `download <https://bitcoin.org/en/bitcoin-core/>`_ and install it on the same computer
as the payment processor. We recommend versions **0.17.x** at this time.
Run it with the following command to enable RPC.

.. code-block:: console

    $ bitcoind -server -rpcuser={YOUR USER} -rpcpassword={YOUR PASSWORD}

The default numifex URL is set to find a local ``bitcoind`` instance.

**Shared Node**

If you do not wish to run your own node,
we offer a rental service through Axia Codex.

However, whenever you use another party's node, you are trusting that person or organization to give you honest information, and to process your transactions correctly.

.. _bitcoin-treasury-address:

Treasury Address
----------------

The treasury account is a Bitcoin address that payments are sent to after an order is finished or canceled.
This can be an exchange account you trust such as a CoinBase wallet. 
From a CoinBase account, you can make an exchange into a different crypto, or cash out into a fiat currency deposited in your bank account.
**Make sure the treasury address is correct before using Numifex.
If you enter an address that you do not control, payments will be lost.**

Note that Numifex *never* needs access to your private key, so you can be sure your funds are safe.

Fees
----

Transferring money to the treasury does cost a small fee.
Numifex will do its best to use a reasonable gas price, but you may want to adjust it to your own preferences.
The option **target number of blocks** is a parameter which helps fee estimation.
A larger number will slow transactions but reduce fees.
A higher number will speed up transactions but increase fees.


You can also disregard fee estimation entirely and enter a custom fee rate.

Read more about `fees <https://en.bitcoin.it/wiki/Miner_fees>`_.

Minimum Confirmations
---------------------

This option determines how many blocks to wait before accepting a payment.
The default of **6** blocks is standard and very conservative.

When a transaction is submitted to the Bitcoin network,
it may be accepted and included in a block.
But, the network may later chose to reject that block
in favor of another longer chain.
Waiting for your block to be attached to a longer chain decrease the probability of this happening.

Using a lower number will allow transactions
to confirm faster, but at the risk that they
become invalid and not included in the chain.

Read more about `confirmations <https://en.bitcoin.it/wiki/Confirmation>`_.

