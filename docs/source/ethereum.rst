.. _ethereum-label:

Ethereum
=========

This page will help you setup Ethereum.
It also has supplemental information and links to resources about Ethereum.

Setup
-----

1. Visit the payment processor admin page in your browser.
   For example ``https://yournumifex/admin``. Log in if necessary.

2. Click on the **Cryptos** tab and expand the **Ethereum** section.

.. image:: /_static/ethereum_config.png

3. Enter the URL for an Ethereum node. (See :ref:`ethereum-nodes` below.)

4. Enter your treasury address. This should be an Ethereum address that you control.
   See :ref:`ethereum-treasury-address`

5. Review the gas settings to ensure they are adequate.
   (See :ref:`ethereum-gas-label`.)


You should now be ready to accept Ethereum payments!

.. _ethereum-nodes:

Ethereum Nodes
--------------

The payment processor needs a URL for an Ethereum node to send transactions and confirm payments.

**Option 1: Axia Codex**

If you are signed into `Axia Codex <https://axiacodex.io/services>`_ access to the Ethereum
network is provided.
You can still override this with a custom URL.

**Option 2: Community Node**

Running an Ethereum node is somewhat involved and requires powerful hardware.
Many are freely available such as `MyEtherWallet <https://www.myetherwallet.com/>`_ ``https://api.myetherwallet.com/eth``.

You can also rent Ethereum nodes for a small fee from `Infura <https://infura.io/>`_
or `Quik Node <https://quiknode.io/>`_.

Note that whenever you use another party's node, you are trusting that person or organization
to give you honest information, and to process your transactions correctly.

**Option 3: Run Geth or Parity**

If you are concerned with maximum security and reliability we recommend running your own node.
Included in the software folder is a `geth <https://github.com/ethereum/go-ethereum/wiki/geth>`_ VM which can be used to run a node. 
You could install geth directly.
Numifex is compatible with other Ethereum implementations such as Parity.

.. _ethereum-treasury-address:

Treasury Address
----------------

The treasury account is an Ethereum address that payments are sent to after an order is finished or canceled.
Ideally, you should control the private keys to this account.
But, it can also be an exchange account that you trust such as a CoinBase wallet. 
From a CoinBase account, you can make an exchange into a different crypto, or cash out into a fiat currency deposited in your bank account.
**Make sure the treasury address is correct before using Numifex.
If you enter an address that you do not control, payments will be lost.**

Note that Numifex *never* needs access to your private key, so you can be sure your accounts are safe.

You can make one with :ref:`metamask-label`.

.. _ethereum-gas-label:

Gas Price
----------

A small amount of ETH (at the data of writing less than a cent) must be payed to the network when money is moved
from settlement accounts to the treasury.
This fee is called the `gas price <https://kb.myetherwallet.com/gas/what-is-gas-ethereum.html>`_.

Offering a higher gas price encourages the transaction to transfer into your treasury quickly.
Numifex will do its best to use a reasonable gas price, but you may want to adjust it to your own preferences.

For most users it makes sense to use lower gas prices and slower transactions.
A good source for gas prices is at `ETH Gas Station <https://ethgasstation.info/>`_.

Note that this has no effect on how quickly your customers payment is received,
only on how long it will take to deposit funds into your treasury.

.. _metamask-label:

Wallet
------

We recommend the `Metamask <https://metamask.io/>`_ wallet for storing private keys and transferring around ETH manually.
Metamask can also be used to generate and store private keys.

Minimum Confirmations
---------------------

This option determines how many blocks to wait before accepting a payment.
The default of **8** should be adequeate for most users.

When a transaction is submitted to the Ethereum network
it may be accepted and included in a block.
However, the network may later chose to reject that block
in favor of a longer chain.
Waiting for your block to be attached to a longer chain decrease the probability of this happening.

Using a lower number will allow transactions
to confirm faster, but at the risk that they
become invalid and not included in the chain.

Testing Ethereum
----------------

If you want to test out Numifex without using real ETH,
you can use a test blockchain or try running your own test chain.

Option 1: Test Network
~~~~~~~~~~~~~~~~~~~~~~

Change the Ethereum node URL to a test network such as https://api.myetherapi.com/rop for Ropsten.

Option 2: Ganache
~~~~~~~~~~~~~~~~~

If you have NPM & NodeJS you can install `Ganache <https://github.com/trufflesuite/ganache-cli>`_. You can install it through terminal:

.. code-block:: console

    $ npm install -g ganache-cli


