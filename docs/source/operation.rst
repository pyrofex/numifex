.. _operation-label:

Operation
=========

This page describes how Numifex processes payments
so you can better understand how to use the software.

Payment Overview
-----------------

1. Purchasing
^^^^^^^^^^^^^

When the customer purchases an item, the price in USD (or local currency)
is converted into the crypto amount using an exchange rate (See :ref:`price-conversion-label`).
An order is submitted with this price. 

Numifex also generates a cryptocurrency address for this order and sends it to the customer.
This acts as a settlement account for this transaction.
The customer is then free to send their payment to the address, at their own leisure, using whichever tools they prefer.

2. Processing
^^^^^^^^^^^^^

The payment processor keeps track of all orders which are expecting payments.
It will regularly request the balances for each account from the network.
When an account balance is greater or equal to the order price, the transaction is marked as complete
and the money is transferred from the settlement account to the treasury account.

The ecommerce platform will be notified.
In WooCommerce the order status will change from "Pending" to "Processing" when the payment is received.

3. Cancelling Orders
^^^^^^^^^^^^^^^^^^^^

Orders are cancelled if they are not paid within a reasonable amount of time from when they are created.
This timeout period defaults to 1 week.
You can configure this option on the Numifex admin.

This protects you against volatility in the price of cryptocurrency.
At the time of purchase, you agree to sell an item for your customer at a given price.
If the exchange rate changes within the timeout period, the value of the proposition may also change.
A shorter timeout is likely to settle transactions at a price close to the USD value,
but may also inconvenience customers.

If an order is cancelled, whatever amount is in the settlement account is transferred to the treasury.
The order will be marked as cancelled.

Order List
-----------

The admin page shows a list of all the orders which have been processed with Numifex.
From here you can look up past orders and follow the state of ongoing orders.

.. image:: /_static/order_list_screenshot.png

Listed Amounts
^^^^^^^^^^^^^^

The orders list shows several balances and totals.

- **Order Total** The amount the customer agreed to pay with their order.
- **Settlement Balance** The last known balance of the settlement account.
- **Deposited** The amount that was actually transferred to the treasury for this order.
  This is slightly lower than the last balance, because of the fees from transferring to the treasury.

Status
^^^^^^

The order status tells you at what stage the order is in.

- **Created** A customer has submitted an order, but has not yet paid.
  The "Settlement Balance" will show the amount the customer has sent to the address of the settlement account.
  When the balance is greater or equal to the order total the order status will change to paid.
- **Paid** The settlement account has been sufficiently funded.
  At this stage Numifex will attempt to transfer the money from the settlement account to the treasury and notify the ecommerce platform.
  When both of these steps are complete. The status will change to finished.
- **Finished** The order is completed and the full balance was transferred to the treasury.
  The settlement balance is no longer updated. 
- **Cancelling** The order timed out, or was manually cancelled.
  At this stage Numifex will attempt to transfer the money from the settlement account to the treasury 
  and change the status of the ecommerce order.
  When both of these steps are complete, the status will changed to cancelled.
- **Cancelled** The order is completed and the full balance was transferred to the treasury.
  The settlement balance is no longer updated. 


Issuing Refunds
---------------

There are a few situations where you may need to issue refunds to your customers.
Numifex will help you keep track of which orders need refunding.
However, you must issue refunds manually. This is necessary because Numifex
doesn't have access to your customers' contact information, and
it cannot identify a legitimate refund situation on its own.

Here are a few indicators that you may need to return some money to a customer:

- The customer accidentally sent too much money.
  This will be indicated by the deposited amount being significantly higher than the order total.

- An order was partially funded before timing out (the customer didn't send enough money in time), or being manually cancelled.
  This will be indicated by an order status of "cancelled" and a non-zero deposited amount.


