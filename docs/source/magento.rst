.. _magento-label:

Magento
========

This page will help you install the Numifex plugin for Mangento.

Setup
-----

Magento Plugin
~~~~~~~~~~~~~~~~

1. Download ``magento-numifex.zip`` from your unique Numifex link to the server.
   You can also download it on another machine and then transfer it with an FTP client.

.. code-block:: console

    $ wget https://repo.pyrofex.io/numifex/{YOUR_KEY}/latest/magento-numifex.zip

2. Navigate to the magento root folder.
   Typically it is located in `/var/www/magento` or `/opt/user/magento/htdocs`
   depending on your server.
   Inside it should contain something like:

.. code-block:: console

    CHANGELOG.md
    COPYING.txt
    Gruntfile.js.sample
    LICENSE.txt
    LICENSE_AFL.txt
    app
    auth.json.sample
    bin
    composer.json
    composer.lock
    dev
    generated
    grunt-config.json.sample
    index.php
    lib
    licenses
    nginx.conf.sample
    package.json.sample
    phpserver
    pub
    setup
    update
    var
    vendor

3. Inside the `app/` folder create a `code/` folder if it does not already exist.

.. code-block:: console

    $ mkdir -p app/code/

4. Extract the zip in this folder. The path should look like: `app/code/Pyrofex/Numifex`.

5. Enter the command below and check that the module appears in the status list.
   If it does not appear, double check the path and file permissions.

.. code-block:: console
  
    $ php bin/magento module:status

    List of enabled modules:
    Magento_Store
    Magento_Directory
    Magento_Theme
    Magento_Backend

    ...

    List of disabled modules:
    Pyrofex_Numifex


6. Enter the commands below to enable the module and clear cache.
   (For more info see `here <https://devdocs.magento.com/guides/v2.3/install-gde/install/cli/install-cli-subcommands-enable.html>`_)

.. code-block:: console

    $ php bin/magento module:enable Pyrofex_Numifex
    $ sudo php bin/magento setup:upgrade
    $ sudo php bin/magento setup:di:compile
    $ sudo php bin/magento setup:static-content:deploy -f
    $ sudo php bin/magento indexer:reindex
    $ sudo php bin/magento cache:clean
    $ sudo php bin/magento cache:flush 


Give Magento Key to Payment Processor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You need to allow the payment processor to send order information to Magento.

Read more from Magento `here <https://devdocs.magento.com/guides/v2.3/get-started/authentication/gs-authentication-token.html>`_.


Give Payment Processor Key to Magento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Now that Magento is setup we need to configure the Numifex payment processor.

1. In your browser, go to the payment processor admin page. For example http://numifex-host:4000/admin.

2. Click on **Commerce** and expand the **Magento** section.

3. Enter the URL to your Magento site in the "Magento URL" field. For example http://numifex-host.

.. image:: /_static/magento_config.png

4. Click on the **API** Tab. Click **Create new API Token**.

5. In another tab, open the WooCommerce admin. Click on the Numifex tab. 
   Copy the field **Client Secret** from the Numifex page to the WordPress page.

.. image:: /_static/creating_api_token.png


Give Payment Processor URL to Magento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You also need to point the Magento plugin at the payment processor.

2. In the field "Numifex URL" enter the URL for the payment processor. For example "http://numifex-host:4000".

Settings
---------------

Pricing
~~~~~~~~

You can set price settings for each coin individually.
Read more about how these at :ref:`price-conversion-label`.

