Getting Started
===============

To get started, follow the instructions in each section.
They will help you to make decisions and set up the software.

Choose Hosting Option
-----------------------

Numifex is designed to be operated by you, the merchant.
Self hosting will provide you with the lowest
costs and the full benefits of decentraliztion and cryptocurrency.

For convenience we also offer a shared hosting service for Numifex called Axia Codex,
This hosting service has a small annual fee.
If you choose this option, we will handle setup and provide you with access to the server.
*We do not process your payments for you, or collect your data, we just host your software.*

Read through the options below and 
decide which makes the most sense for you.

**Option 1: I want Axia Codex to host it for me.**

Great! We will setup the payment processing component for you.
Contact us at: info@numifex.io

Included in the hosting agreement is access to the Axia Codex crypto currency data.

**Option 2: I have access to servers and want to host it myself.**

If your organization already has servers that host other software 
and an IT team it should be easy to add Numifex.
Follow the instructions at :ref:`server-setup-label`

Choose Cryptocurrency Services
-------------------------------

Each cryptocurrency needs access to a node which gives
it access to the network.
Numifex also needs data about cryptocurrency exchange rates
to convert prices from your site.

**Option 1: I want reliable cryptocurrency services and pricing data.**

Axia Codex provides reliable access to network nodes
and tracks historial price data for cryptocurrencies.

Deciding how much to charge your customers for cryptocurrency
can be tricky due to price volatility and exchange fees.
Axia Codex will provide Numifex with a price that takes
these variables into account, so that you get the revenue
you expect.

Axia Codex is included in the hosting agreement at no extra charge.
Access is also available to those who self-host for a small fee.

**Option 2: I will use community cryptocurrency services.**

By default, Numifex will pull pricing data from freely available sources.
Most crypto communities also provide nodes which are available
for public use.

But, because they are operated by third parties, we can provide
no support or guarantees of reliablity or accuracy.

**Option 3: I don't need cryptocurrency services. I host my own, or use another vendor.**

You may choose to set your own prices, and use your own crypto nodes
if you have access to them.

During the cryptocurrency setup processes below,
it should be clear when to enter this information.

.. _config-payment-processor-label:

Configure Payment Processor
----------------------------

Now that the payment processor is installed,
login to the payment processor admin and change the user information.

1. Visit the admin page of the payment processor. For example ``https://your-numifex-host/admin``. 

2. You will be greeted with a login screen. Login with username: ``admin`` and password: ``admin``.

3. Click on the **Admin** tab and enter a new username and password.

.. image:: /_static/change_admin.png

You are now ready to set up a cryptocurrency or eCommerce site!

Configure Cryptocurrencies
--------------------------

You will need to go through a short setup process
for each crypto currency you would like to accept.
 
Follow the instructions on the page for each crypto 
you would like to accept.
Each is optional, but you need at least one to accept payments.

- :ref:`ethereum-label` 
- :ref:`bitcoin-label` 

Configure eCommerce Platform
----------------------------

Numifex will integrate with several eCommerce platforms.
Each is optional, and none are required.

- :ref:`woocommerce-label` 
- :ref:`magento-label` 

