FAQ
===

What is Numifex?
----------------

.. image:: /_static/payment_gateway.png

Numifex is a payment processor for cryptocurrency, that you (the merchant) own and operate.
Unlike a credit card processor, you receive payments from your customers directly rather than receiving payments through a mediator.
Cutting out the "middle-man" eliminates transaction fees, data harvesting, and credit risk from your business.

How is Numifex different than other Crypto platforms?
-----------------------------------------------------

Typically online payment platforms operate as a SaaS (software as a service).
You pay a subscription fee, and the SaaS company accepts payments on your behalf.
However, this completely eliminates the key benefits of doing business with cryptocurrency.
Instead of trusting Visa, you trust your crypto payment service,
give them access to your business's payment traffic,
and pay them regular fees.

Numifex is software that you own and operate.
You can run it on your own servers entirely independent of us.
We can't receive your payments, withdraw your money, or even view your transactions.
You get the full benefit of using Cryptocurrency, peer-to-peer transactions with zero enforcement costs.

Will it integrate with my eCommerce platform?
---------------------------------------------

Currently, we have plugins for:

- `WooCommerce <https://woocommerce.com/>`_ on WordPress.
- `Magento 2 <https://magento.com/>`_.

If your business uses another eCommerce platform, tell us at info@numifex.io.
We want to support the eCommerce platforms that you use.

If you use a custom eCommerce solution, 
it is easy for a developer to integrate:
See :ref:`developer-api-label`.

Which cryptocurrencies does Numifex support?
-----------------------------------------------

Currently, we support:

- `Ethereum (ETH) <https://www.ethereum.org/>`_ 
- `Bitcoin (BTC) <https://bitcoin.org>`_.

We plan to support other major currencies in the near future.
If there is one you are interested in, let us know.

What if I don't want to host it on my own server?
-------------------------------------------------

Many of our customers do not have the infastructure or techincal expertise to host themselves.

We offer a hosting and support service called `Axia Codex <https://axiacodex.io/services>`_.
We will setup a dedicated instance of the Numifex payment processor just for your use.
Although this service is not as decentralized as if you hosted it yourself, it is still superior to SAAS models.
We don't process your payments for you, or collect your data.
We just host your software.

I am hesitant about holding funds in cryptocurrency.
-----------------------------------------------------

Accepting cryptocurrency as a form of payment does not mean that you need to be invested in it long-term.
After a transaction is complete, you can sell your cryptocurrency at an exchange for USD or your local currency.
Numifex includes several features to help protect against volatility and exchange costs.
You can learn more at :ref:`exchange-label`.

Is it secure?
-------------

Numifex is very secure because it relies on the security of the blockchain to process payments.
It doesn't store sensitive customer information, only simple anonymous accounting records.
It never needs your personal private key.
When funds get to your account, you can be sure they are safe.

Access to the payment processor is authenticated with industry standard practices.

How does it work?
-----------------

See the :ref:`operation-label` page.

