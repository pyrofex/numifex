Other Hosting Info
===================

This page contains assorted techincal information about server configuration. This page
may be helpful for troubleshooting, or setting up an atypical installation.

Shared hosting Security
-----------------------

If you install WordPress and Numifex on the same machine, this setup is easy and convenient however it **may** be less secure.
In the event that WordPress has a security flaw, and is compromised, Numifex may also be compromised.
If you still decide to run on same host as WordPress you should set Numifex service port to only listen locally (localhost) and/or block any outside requests using a firewall.


.. _reverse-proxy-label:

Reverse Proxy for Numifex
-------------------------

Apache
^^^^^^^

.. code-block:: console

    $ sudo a2enmod proxy
    $ sudo a2enmod proxy_http
    $ sudo a2enmod proxy_balancer
    $ sudo a2enmod lbmethod_byrequests
    $ sudo systemctl restart apache2

    edit apache config
    $ nano /etc/apache2/sites-available/default-ssl.conf

    ProxyPass /numifex/ http://localhost:4000/
    ProxyPassReverse /numifex/ http://localhost:4000/

    Test and restart/reload Apache
    $ apachectl configtest
    $ sudo systemctl restart apache2

NIGNX
^^^^^

Look at Numifex example on VM:

.. code-block:: console

    $ sudo nano /etc/nginx/sites-available/default

That looks something like below:

.. code-block:: console

    server {
        listen 80 default_server;
        listen [::]:80 default_server;
        if ($scheme != "https") {
            return 301 https://$host$request_uri;
        }

        listen 443 ssl default_server;
        listen [::]:443 ssl default_server;
        ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;
        ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;

        server_name _;
        root /var/www/html;

        location / {
            client_max_body_size 10m;
            proxy_pass http://localhost:4000;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $http_host;
            proxy_set_header X-NginX-Proxy true;
        }
    }


Test Network Access
-------------------

.. code-block:: console

    $ ping 8.8.8.8

    or if using ipv6

    $ ping6 2001:4860:4860::8888


Add Your Own SSL & Use Let's Encrypt
------------------------------------

.. _ssl-label:

We have installed a pre-generated self-signed key. This should be changed before use in production.

Here is an example of using openssl for replacing selfsigned cert:

.. code-block:: console

    $ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt -subj "/C=US/ST=Utah/L=Provo/O=My Business/OU=CI/CN=example.com"

You will notice you have to use curl --insecure https://localhost to access numifex box because of self signed cert. It is recommened to replace it with a valid cert.

You may use your own certificate and install it for NGINX. See http://nginx.org/en/docs/http/configuring_https_servers.html#chains

On our prebuilt vm Let's Encrypt certbot app is already installed. Make sure you have a DNS record for your host, i.e. numifex.example.org pointing to X.X.X.X where X.X.X.X is your ip version 4 address and your Numifex host port 443 open to the internet. After that run:

.. code-block:: console

    $ sudo certbot --nginx certonly

It might give you a few simple prompts.

This cert it good for 3 months. You can setup autorenew or renew it manually with above command.

To test auto renew run command:

.. code-block:: console

    $ sudo certbot renew --dry-run

A full example for setting up certbot with nginx on Ubuntu Bionic is here:

https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx

Or select your platform setup from all platforms https://certbot.eff.org/


Test your certificate setup via SSLLabs
---------------------------------------

https://www.ssllabs.com/ssltest/

Although Numifex application use requires an access token in you http requests, you may want to restrict access to your site by using a firewall.


.. _firewall-label:

Firewall Setup
--------------

If you are using Ubuntu, `Uncomplicated Firewall (UFW) <http://manpages.ubuntu.com/manpages/cosmic/man8/ufw.8.html>`_ is easy to setup. This is already enabled on our prebuilt vm image with tcp ports 22 (ssh) and 443 (NGINX reverse proxy for Numifex application service) open. If you have an Ubuntu box of your own from a new install it is recommended to enable it.

- Allow ssh access, enable ufw and deny Numifex service port 4000

.. code-block:: console

    $ ufw allow ssh; ufw enable; ufw deny 4000

- To allow a specific IP address to access port 4000

.. code-block:: console

    $ ufw allow from 10.1.1.2 to any port 4000 

    or if using NGINX or secure proxy app and https

    $ ufw allow from 10.1.1.2 to any port 443 

- Using iptables

.. code-block:: console

   $ iptables -I INPUT -p tcp -s 10.1.1.2/32 --dport 4000 -j ACCEPT

If using IPv6 for communications you will need to use ip6tables to add ipv6 rule as well.

.. _vm-ip-label:

Detect VM IP Addresses
----------------------

By default the virtual machine is assigned asn IP address via DHCP.
You may want to set it to static IP address or at least create a DHCP static mapping for the macaddress.

Depending how your VM is setup you may have both a public and private IP address.

Getting public ip address on Linux:

.. code-block:: console

    $ dig @resolver1.opendns.com ANY myip.opendns.com +short

    $ curl ifconfig.me


Getting private ip address on Linux:

.. code-block:: console

    $ ip addr show <my-interface-name>

    $ ip addr show

    $ hostname -I

Getting your default gateway on Linux:

.. code-block:: console

    $ ip route | grep default 


.. _modifying-server-label:

Modifying Server 
----------------

.. _vm-remove-key:

Remove Pyrofex Key
^^^^^^^^^^^^^^^^^^

We leave a public key in the VM to make it easier for you to allow us to admin your server.
However, it cannot be used unless you give Pyrofex network access to your ssh port.
You may still wish to remove it.

Login to console using **user:** ``ubuntu`` **password:** ``GetMeTheCrypt0_``

.. code-block:: console

    $ nano ~/.ssh/authorized_keys 

Delete line tmpkey 

Troubleshooting Server 
----------------------

Install performance monitor package htop

.. code-block:: console
    
    $ sudo apt-update 
    $ sudo apt-get install htop
    $ htop
  

Check listening tcp ports 4000 & 443

.. code-block:: console

    $ ss -lnt | grep ":4000\|:443" 

