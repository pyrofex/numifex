.. _server-setup-label:

Self Hosting
=============

This page will help you install the Numifex payment processor on a server.
The level of technical knowledge required is the ability to setup and configure a Linux server.

We provide two options for installation.

1. Install using a Deb package for Debian or Ubuntu.
2. Install a virtual machine image.

You only need to follow **one** of them. Pick the one that you are most familiar with.

Setup
------

Network Configuration
~~~~~~~~~~~~~~~~~~~~~

Before picking an installation option, you should consider its network configuration.

The Numifex server needs to have network access to your eCommerce/WordPress site, and the site also needs network access to Numifex.
However, these communication channels need to be setup securely so that Numifex is not exposed to the public. **Do not expose Numifex to the internet.** 

Here a few example setups that accomplish this goal:

- If your eCommerce site is already hosted on a compatible version of Linux, you can install Numifex on the same machine.
  Use a firewall to block access to the Numifex port from outside.

- If your eCommerce site is hosted on your organization's network. You can install Numifex on another machine on the local network.
  Numifex and your site will have access to each other on the local network.
  However, Numifex will be not be available outside this network.

- If your eCommerce site is hosted on someone else's network (like AWS, Digital Ocean, Go Daddy, etc) then you can expose the port publicly, and restrict access to the IP address site.
  See :ref:`firewall-label`.

Before beginning one of the installation options, pick a plan for where to install the server and how to setup the network based on the examples above.
You may need to read through installation process first to decide.

Option 1: Install Debian Package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This option may be best those who are familiar with Ubuntu or Debian.

Server Preparation
^^^^^^^^^^^^^^^^^^

The ``.deb`` package is designed for `Ubuntu Server 18.04 (LTS), Bionic Beaver <http://releases.ubuntu.com/18.04.1/>`_ or Debian 10 Buster. 
It's tested on these releases but **may** run on other Linux distributions (**at your own risk**).

Setup a server with one of these operating systems and configure the network. When installed
from the deb package Numifex uses port 4000.

Install the Deb Package
^^^^^^^^^^^^^^^^^^^^^^^

2. Download the ``.deb`` package from your unique Numifex link.

.. code-block:: console

    $ wget https://repo.pyrofex.io/numifex/{YOUR_KEY}/latest/numifex.deb

3. Install the deb package:

.. code-block:: console

    $ sudo apt-get update

    $ sudo dpkg -i ./numifex.deb

    $ sudo apt-get install -f -y

4. Enable the service in systemd.

.. code-block:: console

    $ sudo systemctl enable numifex

    $ sudo systemctl start numifex

5. Check that you can access the admin. Visit the servers host/IP address in your browser:

.. code-block:: console

    http://your-numifex-host:4000

You should be greeted by a welcome message.

**Additional Options**

The default Numifex port is 4000. You can change the port with the environmental variable ``PORT`` or in ``config.json``.

If you want to handle requests through NGINX or Apache (for example for HTTPS) see :ref:`reverse-proxy-label`


Option 2: Install Virtual Machine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This option may be best if your organization is already using virtual machines.
A virtual machine is also helpful if you need to run Numifex on another operating system, for example Windows server.
We provide a virtual machine image that you can import into your virtualization software.

**Server Preparation**

We provide the virtual machine image as a `qcow2 <https://en.wikipedia.org/wiki/Qcow>`_  and `ova <https://en.wikipedia.org/wiki/Open_Virtualization_Format>`_. These formats should work with VirtualBox, libvirt, qemu-kvm, etc.
If neither of these formats work for your system, please let us know.

Unlike the deb package, the virtual machine handles requests through NGINX.
By default it is configured for HTTPS on port 443.
If you know how to deploy a virtual machine, you can use whatever tools that you like.
Otherwise follow the instructions below for `VirtualBox <https://www.virtualbox.org/>`_.

**VirtualBox Install**

(You can use other virtualization software, but we only provide these instructions for VirtualBox.)

1. Download ``numifex.ova`` from your unique Numifex link.

.. code-block:: console

    $ wget https://repo.pyrofex.io/numifex/{YOUR_KEY}/latest/numifex.ova

2. Import ``numifex.ova`` into VirtualBox. File -> Import Appliance.

    .. image:: /_static/vbox_import.png

    .. image:: /_static/vbox_import_settings.png

3. Configure the serial port. Right click on the numifex VM in the list -> Settings -> Serial Ports. Enable port 1 and change the mode to ``Raw File``.
   If you are using macOS or Linux set the path to ``/dev/null``. If you are using Windows, set the path to ``NUL``.

    .. image:: /_static/vbox_serial_settings.png


3. Startup Virtual Box. It will prompt you to press a key. Go ahead and do so. You should see a login prompt soon.
   
    .. image:: /_static/vbox_login.png


    If the boot hangs and won't continue at a step close to the one show below, then review step 3.
    You should not see this if you setup a COM port.

    .. image:: /_static/vbox_error.png

4. Change the device password. Login  using **user:** ``ubuntu`` **password:** ``GetMeTheCrypt0_``. Type the ``passwd`` command to change the password.

.. code-block:: console

    $ passwd 
    Changing password for ubuntu.
    (current) UNIX password:
    Enter new UNIX password:
    Retype new UNIX password:
    passwd: password updated successfully


At this time you may also wish to remove the Pyrofex public key. See :ref:`vm-remove-key`.

5. Check that you can access the admin. Visit the server's host/IP address in your browser. (How can you find this? :ref:`vm-ip-label`)

.. code-block:: console

        https://your-numifex-host

You should be greeted by a welcome message.

**Additional Options**

See :ref:`modifying-server-label`


