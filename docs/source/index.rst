.. Numifex documentation master file, created by
   sphinx-quickstart on Mon Jan 28 08:37:22 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the `Numifex <https://numifex.io>`_ docs!
====================================================

This site contains information for how to install and operate Numifex. It may be helpful to review
the server setup and installation process before purchasing.

If you haven't purchased Numifex yet, you can `return to the main site. <https://numifex.io>`_





.. toctree::
   :maxdepth: 3

   faq
   getting_started
   operation
   exchanging_crypto
   woocommerce
   magento
   ethereum
   bitcoin
   self_hosting
   other_hosting_info
   developer_api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
