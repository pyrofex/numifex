.. _exchange-label:

Exchanging Crypto
==================

This page provides information about pricing and exchanging crypto currency
for local fiat currency.

.. _price-conversion-label:

Pricing
-------

When an order is placed on an ecommerce platform,
its price in fiat currency needs to be converted to crypto currency.
Numifex needs an **exchange rate** to make this conversion.

Choosing a favorable exchange rate in a volatile market is challenging.
Numifex provides tools to help you get a good price.
The effectiveness of this system is greatly improved by using Axia Codex.

The screenshot below shows these settings in WooCommerce, but they
can be found in all our ecommerce plugins.

.. image:: /_static/woocommerce_price_settings.png

- **Price Margin** The price margin specifies a "cushion" to add to the final price.
  If you have Axia Codex this cushion depends on recent market volatility.
  Otherwise, it will be a simple percentage increase in price.

  If you do not want any margin, you can choose **Market Price** to get the most recent trade price.

- **Time To Sell** Voltatility depends on how long you expect to hold crypto before cashing out.
  Typically, setting a longer period of time will result in more conservative pricing.
  We recommend you choose a short time and stick with it.

- **Custom Exchange Rate** If you do not want to use automatic pricing, you can override the above
  settings and simply specify an exchange rate to use.
  This is especially useful for coins/tokens which do not have reliable markets so pricing is uncertain.

Sources
~~~~~~~

Exchange rates are obtained from several sources.
Axia Codex is the preferred source, and will be used if you have an account
and it tracks the coin. Otherwise, Numifex will use `CoinBase <https://www.coinbase.com/charts?locale=en-US>`_.

We expect both of these sources to give consistent and accurate data.
However, it is possible for them to return exchange information which is incorrect, delayed, or otherwise unexpected.
This could cause your orders to be priced inaccurately.
This is a risk you accept by using automatic price conversion with Numifex.

Exchanges
----------

After receiving crypto
you will probably want to exchange it for local fiat currency.
We recommend this be done regularly and frequently.

This can be done at a cryptocurrency exchange.
There are many different exchanges to choose from,
each with their own selection of coins to trade.

For Americans dealing in Ethereum and Bitcoin, we recommend `CoinBase <https://www.coinbase.com/>`_ or `Gemini <https://gemini.com/>`_.
Other exchanges are available in many other countries to trade for local currency.

Privacy
~~~~~~~

Typically, a Numifex user will use an exchange account, such as a Coinbase wallet, as their treasury account.
From a Coinbase account, you can exchange into a different crypto, or cash out into a fiat currency deposited in your bank account.
For privacy and security reasons, a Numifex user may want to create an extra step;
moving crypto into a different exchange account, to avoid blacklisting or other censorship actions by CoinBase.

In this case, you would consider your CoinBase account as your 'cold wallet',
and you would have another account with an exchange like Kucoin, Gemini, or Kraken.

This non-Coinbase account would be your 'hot wallet' for actively making crypto purchases and trading, and your Coinbase account would strictly be a go-between.
With this setup, it will be more difficult for Coinbase to associate your account with actions that are legal, but that they consider unacceptable.


