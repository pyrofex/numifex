const assert = require('assert');
const bigInt = require('big-integer');
const ChildProcess = require('child_process');
const fs = require('fs');
const path = require('path');
const routeControllers = require('../controllers/routeControllers');
const logicRegistry = require('../daemon/logicRegistry');
const btcUtil = require('../modules/logic/BTC/util');
const { dbQuery } = require('../controllers/db');
const cron = require('../daemon/cron');
const { runBasicModuleTests } = require('./module-test');

const btcDataDir = path.join(__dirname, 'bitcoin_datadir');
// delete BTC datadir if it exists
const deleteFolder = (f) => {
    if (fs.existsSync(f)) {
        fs.readdirSync(f).forEach((file) => {
            const filePath = path.join(f, file);
            if (fs.lstatSync(filePath).isDirectory()) {
                deleteFolder(filePath);
            } else {
                fs.unlinkSync(filePath);
            }
        });
        fs.rmdirSync(f);
    }
};

before('pause until registry is completed', function() {
    return logicRegistry.afterRegistration();
});

describe('BTC tests', function() {
    console.log('btc tests started');
    deleteFolder(btcDataDir);
    fs.mkdirSync(btcDataDir);

    console.log('btcDataDir', btcDataDir);
    const BTCProcess = ChildProcess.spawn(path.join(__dirname, 'bitcoind'), [
        '-server',
        '-rpcuser=numifex',
        '-rpcpassword=numifex',
        `-datadir=${btcDataDir}`,
        '-regtest'
    ], {
        stdio: 'ignore'
    });

    // BTCProcess.stdout.on('data', function(data) {
    //     // This clutters up the log too much. Only put it in when trying to debug.
    //     console.log('---', data.toString());
    // });


    BTCProcess.once('exit', () => {
        console.log('deleting BTC data dir', btcDataDir);
        deleteFolder(btcDataDir);
    });


    const minimum_confirmations = 6;

    // testing address to mine into
    const privKey = 'cNH1rRSkeSK5x5xkovCsuyfrwfFkHD23Yf8amUCesic8QST54Npe';
    const address = '2MxKs4KcWEAKVvnCTEZh57SRXUFkxSvAZjE';
    const redeemScript = Buffer.from('0014c7fe51de1c58be3d9173fcf937089cfc9bd8d515', 'hex');

    // testing treasury
    // const privKey2 = 'cTPT1QcGxqYQwXz4Wx2zBx8g6exYdp6Up9cy2GmiwyRoKDHhaE9v';
    const address2 = '2MxvnffBnGs9WEwKx1eumQgGvnSroeuidUa';

    let testContext;
    const basicModuleTestsContext = {};

    let logic;
    let client;
    let oldTreasury;
    let oldMinConf;
    let oldRpcUser;
    let oldRpcPassword;
    let oldNodeUrl;

    before(function() {
        return new Promise(resolve => {
            setTimeout(resolve, 2000); // give BTCCore node some time to get itself together
        }).then(() => {
            logic = logicRegistry.getLogic('BTC');
            basicModuleTestsContext.logic = logic;
            return Promise.all([
                logic.getConfigByKey('treasury'),
                logic.getConfigByKey('minimum_confirmations'),
                logic.getConfigByKey('rpc_user'),
                logic.getConfigByKey('rpc_password'),
                logic.getConfigByKey('node_url')
            ]).then(configValues => {
                [
                    oldTreasury,
                    oldMinConf,
                    oldRpcUser,
                    oldRpcPassword,
                    oldNodeUrl
                ] = configValues;
            });
        }).then(() => {
            return Promise.all([
                logic.setConfigByKey('treasury', address2),
                logic.setConfigByKey('minimum_confirmations', 0),
                logic.setConfigByKey('rpc_user', 'numifex'),
                logic.setConfigByKey('rpc_password', 'numifex'),
                logic.setConfigByKey('node_url', 'http://localhost:18443')
            ]);
        }).then(() => {
            return logic.createContext();
        }).then((ctx) => {
            testContext = ctx;
            client = testContext.client;
            return Promise.all([
                client.importAddress(address, '', false),
                client.importAddress(address2, '', false)
            ]);
        }).then(() => {
            return client.generateToAddress(121, address);
        });
    });

    after(function() {
        BTCProcess.kill();
        return Promise.all([
            logic.setConfigByKey('treasury', oldTreasury),
            logic.setConfigByKey('minimum_confirmations', oldMinConf),
            logic.setConfigByKey('rpc_user', oldRpcUser),
            logic.setConfigByKey('rpc_password', oldRpcPassword),
            logic.setConfigByKey('node_url', oldNodeUrl)
        ]);
    });

    describe('Safe conversions', function() {
        it('BTC to Satoshi', function() {
            assert(btcUtil.safeConvertBTCToSatoshi(1).eq(bigInt(100000000)));
            assert(btcUtil.safeConvertBTCToSatoshi(0.001).eq(bigInt(100000)));
        });

        it('Satoshi to BTC', function() {
            assert.strictEqual(btcUtil.safeConvertSatoshiToBTC(bigInt(100000000)), '1.00000000');
            assert.strictEqual(btcUtil.safeConvertSatoshiToBTC(bigInt(100000)), '0.00100000');
        });
    });

    runBasicModuleTests(basicModuleTestsContext);

    describe('full transaction life cycle', function() {
        const priceInUsd = 1000.0;
        let testPriceSatoshi;
        let testPubKey;
        let startTreasuryBalance = bigInt(0);

        before(function() {
            return testContext.getConfirmedInputs(address2, minimum_confirmations).then(btcUtil.sumInputs).then(balance => {
                startTreasuryBalance = balance;
            });
        });

        // Delete test order after tests have run
        after(function() {
            return dbQuery({
                text: `SELECT id FROM orders WHERE commerce = 'testing' AND pub_key = $1 AND order_currency = 'BTC'`,
                values: [testPubKey]
            }).then(res => {
                const id = res.rows[0].id;
                return dbQuery({
                    text: `DELETE FROM transfers WHERE order_id = $1`,
                    values: [id]
                }).then(() => {
                    return dbQuery({
                        text: `DELETE FROM payments WHERE order_id = $1`,
                        values: [id]
                    });
                }).then(() => {
                    return dbQuery({
                        text: `DELETE FROM orders where id = $1`,
                        values: [id]
                    });
                });
            });
        });

        it('calculate the price. create an order', function () {
            return Promise.all([
                logic.requestExchangeRate('USD'),
                logic.createCurrencyFormatter()
            ]).then(([fiatPerCrypto, currencyFormatter]) => {
                const fiatTotal = priceInUsd / fiatPerCrypto;

                testPriceSatoshi = currencyFormatter.naturalToBase(fiatTotal);

                const order = {
                    orderTotal: testPriceSatoshi,
                    commerce: 'testing',
                    convRate: fiatPerCrypto,
                    currency: 'USD'
                };
                return logic.createOrder(order);
            }).then(order => {
                testPubKey = order.address;
            });
        });

        it('send it some btc', function() {
            return testContext.getConfirmedInputs(address, minimum_confirmations).then(unspentInputs => {
                const fee = btcUtil.getFeeForTransfer(client.getNetworkName(), unspentInputs, testPubKey, privKey, testContext.feeRate);
                assert(fee.gt(bigInt(0))); // fee should be greater than zero
                const feeForExactPayment = btcUtil.sumInputs(unspentInputs).minus(testPriceSatoshi);
                assert(feeForExactPayment.gt(bigInt(0)));
                const rawTransaction = btcUtil.createRawTransaction(client.getNetworkName(), unspentInputs, testPubKey, privKey, redeemScript, feeForExactPayment);
                return client.sendRawTransaction(rawTransaction, true);
            }).then(() => {
                return client.generateToAddress(7, address); // confirm the transaction, and send the fee back to the funding address
            }).then(() => {
                return testContext.getConfirmedInputs(testPubKey, minimum_confirmations).then(btcUtil.sumInputs);
            }).then(balance => {
                assert(balance.eq(testPriceSatoshi));
            });
        });

        it('process the transactions', function() {
            return cron.processOrders().then(() => {
                return client.generateToAddress(7, address); // confirm transfer transactions, and send the fee back to the funding address
            });
        });

        it('check that the transaction got finished', function() {
            const sql = {
                text: `SELECT status FROM orders WHERE order_currency = 'BTC' AND pub_key = $1`,
                values: [testPubKey]
            };

            return dbQuery(sql).then((res) => {
                const result = res.rows[0];
                if (result.status !== 'finished') {
                    throw `transaction is not finished, status is ${result.status}`;
                }
            });
        });

        it('check that the treasury got some money!', function() {
            return testContext.getConfirmedInputs(address2, minimum_confirmations).then(btcUtil.sumInputs).then(balance => {
                assert(balance.minus(startTreasuryBalance).isPositive());
            });
        });
    });

    describe('cancelling an order', function() {
        const testPriceSatoshi = btcUtil.safeConvertBTCToSatoshi(0.4);
        let testPubKey;

        let startBalance;

        before(function() {
            return testContext.getConfirmedInputs(address2, minimum_confirmations).then(btcUtil.sumInputs).then(balance => {
                startBalance = balance;
            });
        });

        // Delete test order after tests have run
        after(function() {
            return dbQuery({
                text: `SELECT id FROM orders WHERE commerce = 'testing' AND pub_key = $1 AND order_currency = 'BTC'`,
                values: [testPubKey]
            }).then(res => {
                const id = res.rows[0].id;
                return dbQuery({
                    text: `DELETE FROM transfers WHERE order_id = $1`,
                    values: [id]
                }).then(() => {
                    return dbQuery({
                        text: `DELETE FROM payments WHERE order_id = $1`,
                        values: [id]
                    });
                }).then(() => {
                    return dbQuery({
                        text: `DELETE FROM orders where id = $1`,
                        values: [id]
                    });
                });
            });
        });

        it('create an order', function() {
            const order = {
                orderTotal: testPriceSatoshi.toString(),
                commerce: 'testing',
                currency: 'USD',
                convRate: '1.0'
            };
            return logic.createOrder(order).then(res => {
                testPubKey = res.address;
            });
        });

        it('send it some btc', function() {
            return testContext.getConfirmedInputs(address, minimum_confirmations).then(unspentInputs => {
                const feeForExactPayment = btcUtil.sumInputs(unspentInputs).minus(testPriceSatoshi);
                assert(feeForExactPayment.gt(bigInt(0)));
                const rawTransaction = btcUtil.createRawTransaction(client.getNetworkName(), unspentInputs, testPubKey, privKey, redeemScript, feeForExactPayment);
                return client.sendRawTransaction(rawTransaction, true);
            }).then(() => {
                return client.generateToAddress(7, address); // confirm the transaction, and send the fee back to the funding address
            }).then(() => {
                return testContext.getConfirmedInputs(testPubKey, minimum_confirmations).then(btcUtil.sumInputs);
            }).then(balance => {
                assert(balance.eq(testPriceSatoshi));
            });
        });

        it('cancel it', function() {
            const sql = {
                text: `SELECT id FROM orders WHERE order_currency = 'BTC' AND pub_key = $1`,
                values: [testPubKey]
            };

            return dbQuery(sql).then((res) => {
                return routeControllers.cancelOrder(res.rows[0].id);
            });
        });

        it('process the transactions', function() {
            return cron.processOrders().then(() => {
                return client.generateToAddress(7, address);
            });
        });

        it('check that the transaction got cancelled', function() {
            const sql = {
                text: 'SELECT status FROM orders WHERE pub_key = $1',
                values: [testPubKey]
            };

            return dbQuery(sql).then((res) => {
                assert.strictEqual(res.rows[0].status, 'cancelled');
            });
        });

        it('check that the treasury got some money!', function() {
            return testContext.getConfirmedInputs(address2, minimum_confirmations).then(btcUtil.sumInputs).then(balance => {
                assert(balance.minus(startBalance).isPositive());
            });
        });
    });
});
