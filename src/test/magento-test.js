const assert = require('assert');
const routeControllers = require('../controllers/routeControllers');

describe('Magento tests', function() {
    describe('set and get config values', function() {
        let originalMagentoToken;
        it('check magento token', function() {
            return routeControllers.getConfigByKey('commerce', 'magento', 'magento_token').then((token) => {
                originalMagentoToken = token;
            });
        });

        it('set magento tokent', function() {
            return routeControllers.setConfigByKey('commerce', 'magento', 'magento_token', 'test_magento_token');
        });

        it('check magento token', function() {
            return routeControllers.getConfigByKey('commerce', 'magento', 'magento_token').then((user) => {
                assert(user === 'test_magento_token');
            });
        });

        it('restore magento token', function() {
            return routeControllers.setConfigByKey('commerce', 'magento', 'magento_token', originalMagentoToken);
        });
    });
});