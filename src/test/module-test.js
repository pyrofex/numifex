const { dbQuery } = require('../controllers/db');
const routeControllers = require('../controllers/routeControllers');
const cron = require('../daemon/cron');
const assert = require('assert');

// use lambda to avoid overwriting Mocha's "this"
const runBasicModuleTests = (context) => {
    let logic;

    before(function() {
        logic = context.logic;
    });

    describe('order pricing and submission', function () {
        // 100 USD
        const priceInUsd = 100.0;
        const testCommerceId = '8000';

        // Delete test order after tests have run
        after(function () {
            const sql = {
                text: `DELETE FROM orders WHERE commerce = 'testing' AND commerce_id = $1`,
                values: [testCommerceId]
            };

            return dbQuery(sql);
        });


        it('calculate the price. create an order', function () {
            return Promise.all([
                logic.requestExchangeRate('USD', 'market', 3),
                logic.createCurrencyFormatter()
            ]).then(([fiatPerCrypto, currencyFormatter]) => {
                assert(fiatPerCrypto > 0.0, 'crypto pricing is 0');
                const naturalTotal = priceInUsd / fiatPerCrypto;

                const order = {
                    commerceId: testCommerceId,
                    orderTotal: currencyFormatter.naturalToBase(naturalTotal),
                    commerce: 'testing',
                    convRate: fiatPerCrypto,
                    currency: 'USD'
                };
                return logic.createOrder(order);
            });
        });

        it('try getting it by commerce id. generate a qr code', function () {
            return routeControllers.getOrderByCommerceId('testing', testCommerceId).then(order => {
                assert.strictEqual(testCommerceId, order[0].commerce_id);
                assert(order[0].pub_key.length > 1, 'pub key was not correct');
                assert(logic.qrCodeText(order[0].pub_key).length > 1);
            });
        });

        it('make sure its in active orders with "created" status', function () {
            return routeControllers.listOrders().then((orders) => {
                const found = orders.find(order => order.commerce_id === testCommerceId);
                assert.strictEqual(found.status, 'created');
            });
        });
    });

    describe('cancel an empty order', function () {
        let testAddress;
        // Delete test order after tests have run
        after(function () {
            return dbQuery({
                text: `SELECT id FROM orders WHERE commerce = 'testing' AND pub_key = $1`,
                values: [testAddress]
            }).then(res => {
                // TODO: foreign key with cascade?
                const id = res.rows[0].id;
                return dbQuery({
                    text: `DELETE FROM transfers WHERE order_id = $1`,
                    values: [id]
                }).then(() => {
                    return dbQuery({
                        text: `DELETE FROM payments WHERE order_id = $1`,
                        values: [id]
                    });
                }).then(() => {
                    return dbQuery({
                        text: `DELETE FROM orders where id = $1`,
                        values: [id]
                    });
                });
            });
        });

        it('create an order (but dont send any money)', function () {
            const order = {
                orderTotal: 1000,
                commerce: 'testing',
                currency: 'USD',
                convRate: '1.0'
            };

            return logic.createOrder(order).then(res => {
                testAddress = res.address;
            });
        });

        it('cancel it', function () {
            const sql = {
                text: `SELECT id FROM orders WHERE commerce = 'testing' AND pub_key = $1`,
                values: [testAddress]
            };

            return dbQuery(sql).then((res) => {
                return routeControllers.cancelOrder(res.rows[0].id);
            });
        });

        it('process the transactions', function () {
            return cron.processOrders();
        });

        it('check that the transaction got cancelled', function () {
            const sql = {
                text: `SELECT status FROM orders WHERE commerce = 'testing' AND pub_key = $1`,
                values: [testAddress]
            };

            return dbQuery(sql).then((res) => {
                assert.strictEqual(res.rows[0].status, 'cancelled');
            });
        });
    });

    describe('timing out an order', function () {
        let testAddress;

        // Delete test order after tests have run
        after(function () {
            return dbQuery({
                text: `SELECT id FROM orders WHERE commerce = 'testing' AND pub_key = $1`,
                values: [testAddress]
            }).then(res => {
                const id = res.rows[0].id;
                return dbQuery({
                    text: `DELETE FROM transfers WHERE order_id = $1`,
                    values: [id]
                }).then(() => {
                    return dbQuery({
                        text: `DELETE FROM payments WHERE order_id = $1`,
                        values: [id]
                    });
                }).then(() => {
                    return dbQuery({
                        text: `DELETE FROM orders where id = $1`,
                        values: [id]
                    });
                });
            });
        });

        it('create an order', function () {
            const order = {
                orderTotal: 10000,
                commerce: 'testing',
            };

            return logic.createOrder(order).then(res => {
                testAddress = res.address;
                const newDateCreated = new Date();
                newDateCreated.setFullYear(1980); // set the year to 1980
                return dbQuery({
                    text: `UPDATE orders SET date_created = $1 WHERE commerce = 'testing' AND pub_key = $2`,
                    values: [newDateCreated.toUTCString(), testAddress]
                });
            });
        });

        it('process the transactions', function () {
            return cron.processOrders();
        });

        it('check that the transaction timed out', function () {
            const sql = {
                text: `SELECT status FROM orders WHERE commerce = 'testing' AND pub_key = $1`,
                values: [testAddress]
            };

            return dbQuery(sql).then((res) => {
                assert.strictEqual(res.rows[0].status, 'cancelled');
            });
        });
    });

    describe('fail at notification, but persist', function () {
        let testPubKey;
        // Delete test order after tests have run
        after(function () {
            return dbQuery({
                text: `SELECT id FROM orders WHERE commerce = 'testing' AND pub_key = $1 AND commerce = 'testing'`,
                values: [testPubKey]
            }).then(res => {
                const id = res.rows[0].id;
                return dbQuery({
                    text: `DELETE FROM transfers WHERE order_id = $1`,
                    values: [id]
                }).then(() => {
                    return dbQuery({
                        text: `DELETE FROM payments WHERE order_id = $1`,
                        values: [id]
                    });
                }).then(() => {
                    return dbQuery({
                        text: `DELETE FROM orders where id = $1`,
                        values: [id]
                    });
                });
            });
        });

        it('create an order (but dont send any money)', function () {
            const order = {
                commerceId: 101,
                orderTotal: 1000,
                commerce: 'testing',
            };

            return logic.createOrder(order).then(res => {
                testPubKey = res.address;
            });
        });

        it('mark it as paid', function () {
            const sql = {
                text: `UPDATE orders SET status='paid' WHERE commerce = 'testing' AND pub_key = $1`,
                values: [testPubKey]
            };

            return dbQuery(sql);
        });


        it('cron cycle 1 (process transaction)', function () {
            return cron.processOrders();
        });

        it('cron cycle 2', function() {
            return cron.processOrders();
        });

        it('cron cycle 3', function() {
            return cron.processOrders();
        });

        it('check that the transaction got paid, but not finished', function () {
            const sql = {
                text: `SELECT status, commerce_notify_date FROM orders WHERE  commerce = 'testing' AND pub_key = $1`,
                values: [testPubKey]
            };

            return dbQuery(sql).then((res) => {
                assert.strictEqual(res.rows[0].status, 'paid');
                assert.equal(res.rows[0].commerce_notify_date, null);
            });
        });

        it('mark it as notified, and send it on the way', function () {
            const sql = {
                text: `UPDATE orders SET commerce_notify_date = CURRENT_TIMESTAMP WHERE  commerce = 'testing' AND pub_key = $1`,
                values: [testPubKey]
            };

            return dbQuery(sql).then(() => {
                return cron.processOrders();
            });
        });

        it('check that the transaction got finished', function () {
            const sql = {
                text: `SELECT o.status, t.amount AS transfer_total FROM orders AS o
                        LEFT JOIN transfers AS t ON o.id = t.order_id AND
                        t.date_created = (SELECT max(date_created) FROM transfers WHERE order_id = t.order_id AND valid = true)
                        WHERE commerce = 'testing' AND pub_key = $1`,
                values: [testPubKey]
            };

            return dbQuery(sql).then((res) => {
                assert.strictEqual(res.rows[0].status, 'finished');
            });
        });
    });
};


module.exports.runBasicModuleTests = runBasicModuleTests;
