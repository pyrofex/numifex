
if [ ! -f test/bitcoind ]; then 
    curl -O https://bitcoincore.org/bin/bitcoin-core-0.17.1/bitcoin-0.17.1-x86_64-linux-gnu.tar.gz
    tar xzvf bitcoin-0.17.1-x86_64-linux-gnu.tar.gz --strip-components=2 bitcoin-0.17.1/bin/bitcoind
    rm bitcoin-0.17.1-x86_64-linux-gnu.tar.gz
    mv bitcoind test
fi

