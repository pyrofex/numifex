const assert = require('assert');
const ethers = require('ethers');
const cron = require('../daemon/cron');
const logicRegistry = require('../daemon/logicRegistry');
const rhocContract = require('./rhoc');
const { runBasicModuleTests } = require('./module-test');
const { dbQuery } = require('../controllers/db');

// use ganache in testing for the node. It will take care of everything!
const ganache = require('ganache-cli');

before(function() {
    return logicRegistry.afterRegistration();
});

describe('ERC20 tests', function() {
    const ganacheProvider = ganache.provider();

    const basicModuleTestsContext = {};

    const ethersProvider = new ethers.providers.Web3Provider(ganacheProvider);
    let JsonRpcProviderOriginal;
    let ethLogic;
    let erc20Logic;
    let oldConfirmations;
    let oldContractAddress;
    let oldTreasury;
    let oldEthTreasury;
    let oldReserve;

    before(function() {
        // mock the ethers provider with ganache
        JsonRpcProviderOriginal = ethers.providers.JsonRpcProvider;
        ethers.providers.JsonRpcProvider = ethers.providers.Web3Provider.bind(null, ganacheProvider, null);

        ethLogic = logicRegistry.getLogic('ETH');
        erc20Logic = logicRegistry.getLogic('BAT');
        basicModuleTestsContext.logic = erc20Logic;

        return Promise.all([
            ethLogic.getConfigByKey('minimum_confirmations'),
            ethLogic.getConfigByKey('treasury'),
            erc20Logic.getConfigByKey('treasury'),
            erc20Logic.getConfigByKey('contract_address'),
            erc20Logic.getConfigByKey('reserve_priv_key')
        ]).then((results) => {
            [
                oldConfirmations,
                oldEthTreasury,
                oldTreasury,
                oldContractAddress,
                oldReserve
            ] = results;
        }).then(() => {
            return ethLogic.setConfigByKey('minimum_confirmations', '0');
        });
    });

    after(function() {
        ethers.providers.JsonRpcProvider.constructor = JsonRpcProviderOriginal;
        return Promise.all([
            ethLogic.setConfigByKey('minimum_confirmations', oldConfirmations),
            ethLogic.setConfigByKey('treasury', oldEthTreasury),
            erc20Logic.setConfigByKey('treasury', oldTreasury),
            erc20Logic.setConfigByKey('contract_address', oldContractAddress),
            erc20Logic.setConfigByKey('reserve_priv_key', oldReserve)
        ]);
    });

    const DeployAccount = 4;
    const TokenHolderAccount = 4;

    describe('setup a token', function() {
        let signer;
        it('deploy a contract', function() {
            return ethersProvider.listAccounts().then((accounts) => {
                // pick the first address
                // this has money prepopulated in ganache
                signer = ethersProvider.getSigner(accounts[DeployAccount]);
                const factory = new ethers.ContractFactory(rhocContract.abi, rhocContract.bytecode, signer);
                const tokenSupply = ethers.utils.parseUnits('929177614350717842000', 18);

                return factory.deploy(tokenSupply, accounts[TokenHolderAccount]).then(contract => {
                    return erc20Logic.setConfigByKey('contract_address', contract.address);
                });
            });
        });

        it('fund a reserve account', function() {
            const newWallet = ethers.Wallet.createRandom();
            return erc20Logic.setConfigByKey('reserve_priv_key', newWallet.privateKey).then(() => {
                const rawTransaction = {
                    'gasPrice': ethers.utils.parseUnits('1.0', 'gwei'),
                    'gasLimit': 21000,
                    'to': newWallet.address,
                    'value': ethers.utils.parseEther('0.5')
                };
                return signer.sendTransaction(rawTransaction);
            });
        });

        it('generate a treasury', function() {
            const newWallet = ethers.Wallet.createRandom();
            return erc20Logic.setConfigByKey('treasury', newWallet.address).then(() => {
                return ethLogic.setConfigByKey('treasury', newWallet.address);
            });
        });
    });

    runBasicModuleTests(basicModuleTestsContext);

    describe('full transaction life cycle', function() {
        const testPriceBase = ethers.utils.parseEther('15.185325648427508').toString();

        let testAddress;
        let signer;
        let testTreasury;
        let contractAddress;

        //Delete test order after tests have run
        after(function () {
            return dbQuery({
                text: `SELECT id FROM orders WHERE commerce = 'testing' AND pub_key = $1`,
                values: [testAddress]
            }).then(res => {
                // TODO: foreign key with cascade?
                const id = res.rows[0].id;
                return dbQuery({
                    text: `DELETE FROM transfers WHERE order_id = $1`,
                    values: [id]
                }).then(() => {
                    console.log('got those transfers');
                    return dbQuery({
                        text: `DELETE FROM payments WHERE order_id = $1`,
                        values: [id]
                    });
                }).then(() => {
                    return dbQuery({
                        text: `DELETE FROM orders where id = $1`,
                        values: [id]
                    });
                });
            });
        });
        it('pull settings', function() {
            return Promise.all([
                erc20Logic.getConfigByKey('contract_address'),
                erc20Logic.getConfigByKey('treasury')
            ]).then(([address, treasury]) => {
                contractAddress = address;
                testTreasury = treasury;

                return ethersProvider.listAccounts();
            }).then((accounts) => {
                signer = ethersProvider.getSigner(accounts[TokenHolderAccount]);
            });
        });

        it('create an order', function() {
            const order = {
                orderTotal: testPriceBase,
                commerce: 'testing'
            };

            return erc20Logic.createOrder(order).then((res) => {
                testAddress = res.address;
            });
        });

        it('pay the account', function() {
            const options = {
                gasLimit: 150000,
                gasPrice: ethers.utils.parseUnits('1.0', 'gwei')
            };
            const contract = new ethers.Contract(contractAddress, rhocContract.abi, signer);
            console.log('FUNDING STEP CONTRACT: ' + contractAddress);
            const contractWithSigner = contract.connect(signer);
            return contractWithSigner.transfer(testAddress, testPriceBase, options).then(response => {
                return response;
            });
        });

        it('cron cycle 1. process the transactions', function() {
            return cron.processOrders();
        });

        it('cron cycle 2. process the transactions', function() {
            return cron.processOrders();
        });

        it('check that the treasury got some money!', function() {
            const contract = new ethers.Contract(contractAddress, rhocContract.abi, signer);
            console.log('CHECK STEP CONTRACT: ' + contractAddress);
            const contractWithSigner = contract.connect(signer);
            return contractWithSigner.balanceOf(testTreasury).then(balance => {
                return balance;
            }).then((balance) => {
                assert(balance.eq(testPriceBase));
            });
        });
    });
});
