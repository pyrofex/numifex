const assert = require('assert');
const routeControllers = require('../controllers/routeControllers');

describe('WooCommerce tests', function() {
    describe('set and get config values', function() {
        let originalWordpressUser;
        it('check wordpress user', function() {
            return routeControllers.getConfigByKey('commerce', 'woocommerce', 'wordpress_user').then((user) => {
                originalWordpressUser = user;
            });
        });

        it('set wordpress user', function() {
            return routeControllers.setConfigByKey('commerce', 'woocommerce', 'wordpress_user', 'my_numifex_user');
        });

        it('check wordpress user', function() {
            return routeControllers.getConfigByKey('commerce', 'woocommerce', 'wordpress_user').then((user) => {
                assert(user === 'my_numifex_user');
            });
        });

        it('restore wordpress user', function() {
            return routeControllers.setConfigByKey('commerce', 'woocommerce', 'wordpress_user', originalWordpressUser);
        });
    });
});
