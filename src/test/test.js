const assert = require('assert');
const cron = require('../daemon/cron');
const routeControllers = require('../controllers/routeControllers');
const auth = require('../controllers/auth');
const { requestCoinbaseRate } = require('../modules/logic/pricing');

describe('common tests', function() {
    describe('set and get config values', function() {
        it('try getting order limits', function() {
            return cron.test.getOrderTimeouts().then(timeouts => {
                assert(typeof timeouts.hardTimeout === 'number');
            });
        });
    });

    describe('basic order functionality', function() {
        it('try a fake commerce id', function() {
            const commerce_id = '4VT5';
            return routeControllers.getOrderByCommerceId('ETH', commerce_id).then((orders) => {
                if (orders.length > 0) {
                    throw 'Invalid order_id accepted';
                }
            }, () => {});
        });
    });

    describe('check conversion', function() {
        it('request several exchange rates', function() {
            const fiats = [
                'USD',
                'EUR', // EURO
                'HKD', // hong kong dollar
                'CNY', // chinese renminbi
                'JPY', // Japanese Yen
                'GBP', // British pound
            ];

            const fiatAmount = 100.0;

            const promises = fiats.map((fiat) => {
                return requestCoinbaseRate('ETH', fiat).then((fiatPerCrypto) => {
                    console.log(`${fiat}: ${fiatPerCrypto}`);

                    // how much ETH is 100?
                    const ethTotal = fiatAmount / fiatPerCrypto;
                    console.log(`${fiat} eth: ${ethTotal}`);
                });
            });

            return Promise.all(promises);
        });
    });

    describe('order list', function() {
        return routeControllers.listOrders().then((list) =>{
            console.log(JSON.stringify(list.map(order => order.id)));
        });
    });

    describe('admin authentication', function() {
        let token;
        it('get an admin token', function() {
            return auth.authorizeAdmin('admin', 'admin').then(adminToken => {
                assert(adminToken); // make sure that the token is something
                token = adminToken;
            });
        });

        it('check if token is valid', function() {
            return auth.checkAdminToken('Bearer ' + token);
        });
    });
});
