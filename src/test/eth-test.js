const assert = require('assert');
const ethers = require('ethers');
const cron = require('../daemon/cron');
const logicRegistry = require('../daemon/logicRegistry');
const { runBasicModuleTests } = require('./module-test');
const { dbQuery } = require('../controllers/db');
const routeControllers = require('../controllers/routeControllers');


// use ganache in testing for the node. It will take care of everything!
const ganache = require('ganache-cli');

before(function() {
    return logicRegistry.afterRegistration();
});

describe('ETH tests', function() {
    const ganacheProvider = ganache.provider();
    const ethersProvider = new ethers.providers.Web3Provider(ganacheProvider);

    const basicModuleTestsContext = {};

    let JsonRpcProviderOriginal;
    const testTreasury = '0xc895ac7b8641577309EFaCa49BB2A175e2d7f9C1';
    let logic;
    let oldTreasury;
    let oldConfirmations;
    let oldGasPriceSelection;

    before(function() {
        // mock the ethers provider with ganache
        JsonRpcProviderOriginal = ethers.providers.JsonRpcProvider;
        ethers.providers.JsonRpcProvider = ethers.providers.Web3Provider.bind(null, ganacheProvider, null);

        logic = logicRegistry.getLogic('ETH');
        basicModuleTestsContext.logic = logic;

        return logic.getConfigByKey('treasury').then(treasury => {
            oldTreasury = treasury;
            return logic.setConfigByKey('treasury', testTreasury);
        }).then(() => {
            return logic.getConfigByKey('minimum_confirmations');
        }).then((confirmations) => {
            oldConfirmations = confirmations;
            return logic.setConfigByKey('minimum_confirmations', '0');
        }).then(() => {
            return logic.getConfigByKey('gas_price_selection').then(selection => {
                oldGasPriceSelection = selection;
                return logic.setConfigByKey('gas_price_selection', 'market');
            });
        });
    });

    after(function() {
        ethers.providers.JsonRpcProvider.constructor = JsonRpcProviderOriginal;

        return logic.setConfigByKey('treasury', oldTreasury).then(() => {
            return logic.setConfigByKey('minimum_confirmations', oldConfirmations);
        }).then(() => {
            return logic.setConfigByKey('gas_price_selection', oldGasPriceSelection);
        });
    });

    runBasicModuleTests(basicModuleTestsContext);

    describe('full transaction life cycle', function() {
        let testPriceWei;
        let testPubKey;

        const priceInUsd = 275.5;

        // Delete test order after tests have run
        after(function() {
            return dbQuery({
                text: `SELECT id FROM orders WHERE commerce = 'testing' AND pub_key = $1 AND order_currency = 'ETH'`,
                values: [testPubKey]
            }).then(res => {
                const id = res.rows[0].id;
                return dbQuery({
                    text: `DELETE FROM transfers WHERE order_id = $1`,
                    values: [id]
                }).then(() => {
                    return dbQuery({
                        text: `DELETE FROM payments WHERE order_id = $1`,
                        values: [id]
                    });
                }).then(() => {
                    return dbQuery({
                        text: `DELETE FROM orders where id = $1`,
                        values: [id]
                    });
                });
            });
        });

        it('calculate the price. create an order', function () {
            return Promise.all([
                logic.requestExchangeRate('USD', 'market', 3),
                logic.createCurrencyFormatter()
            ]).then(([fiatPerCrypto, currencyFormatter]) => {
                const fiatTotal = priceInUsd / fiatPerCrypto;

                testPriceWei = currencyFormatter.naturalToBase(fiatTotal);

                const order = {
                    orderTotal: testPriceWei,
                    commerce: 'testing',
                    convRate: fiatPerCrypto,
                    currency: 'USD'
                };
                return logic.createOrder(order);
            }).then(order => {
                testPubKey = order.address;
            });
        });

        it('send it some eth', function() {
            let gasPrice;
            return ethersProvider.getGasPrice().then((price) => {
                gasPrice = price;
                return ethersProvider.listAccounts();
            }).then((accounts) => {
                // pick the first address
                // this has money prepopulated in ganache
                const signer = ethersProvider.getSigner(accounts[0]);

                const gasLimit = ethers.utils.bigNumberify(21000);
                const toSend = ethers.utils.bigNumberify(testPriceWei).add( gasLimit.mul(gasPrice) );

                return signer.sendTransaction({
                    to: testPubKey,
                    gasLimit: gasLimit,
                    value: toSend
                });
            });
        });

        it('process the transactions', function() {
            return cron.processOrders();
        });

        it('check that the transaction got finished', function() {
            const sql = {
                text: `SELECT o.status, t.amount AS transfer_total FROM orders AS o
                       LEFT JOIN transfers AS t ON o.id = t.order_id AND
                       t.date_created = (SELECT max(date_created) FROM transfers WHERE order_id = t.order_id AND valid = true)
                       WHERE order_currency = 'ETH' AND pub_key = $1`,
                values: [testPubKey]
            };
            console.log('testPubKey', testPubKey);

            return dbQuery(sql).then((res) => {
                assert.strictEqual(res.rows[0].status, 'finished');
                const sent = ethers.utils.bigNumberify(res.rows[0].transfer_total);
                assert(sent.eq(testPriceWei));
            });
        });

        it('check that the treasury got some money!', function() {
            return ethersProvider.getBalance(testTreasury).then((balance) => {
                assert(balance.eq(testPriceWei));
            });
        });
    });

    describe('cancelling an order', function() {
        const testPriceWei = ethers.utils.parseEther('0.4');
        let testPubKey;

        let startBalance;

        before(function() {
            return ethersProvider.getBalance(testTreasury).then((balance) => {
                startBalance = balance;
            });
        });

        // Delete test order after tests have run
        after(function() {
            return dbQuery({
                text: `SELECT id FROM orders WHERE commerce = 'testing' AND pub_key = $1 AND order_currency = 'ETH'`,
                values: [testPubKey]
            }).then(res => {
                const id = res.rows[0].id;
                return dbQuery({
                    text: `DELETE FROM transfers WHERE order_id = $1`,
                    values: [id]
                }).then(() => {
                    return dbQuery({
                        text: `DELETE FROM payments WHERE order_id = $1`,
                        values: [id]
                    });
                }).then(() => {
                    return dbQuery({
                        text: `DELETE FROM orders where id = $1`,
                        values: [id]
                    });
                });
            });
        });

        it('create an order', function() {
            const order = {
                orderTotal: testPriceWei,
                commerce: 'testing',
                currency: 'USD',
                convRate: '1.0'
            };

            return logic.createOrder(order).then(res => {
                testPubKey = res.address;
            });
        });

        it('send it some eth', function() {
            let gasPrice;
            return ethersProvider.getGasPrice().then((price) => {
                gasPrice = price;
                return ethersProvider.listAccounts();
            }).then((accounts) => {
                // pick the first address
                // this has money prepopulated in ganache
                const signer = ethersProvider.getSigner(accounts[0]);

                const gasLimit = ethers.utils.bigNumberify(21000);
                const toSend = testPriceWei.add( gasLimit.mul(gasPrice) );

                return signer.sendTransaction({
                    to: testPubKey,
                    gasLimit: gasLimit,
                    value: toSend
                });
            });
        });

        it('cancel it', function() {
            const sql = {
                text: `SELECT id FROM orders WHERE order_currency = 'ETH' AND pub_key = $1`,
                values: [testPubKey]
            };

            return dbQuery(sql).then((res) => {
                return routeControllers.cancelOrder(res.rows[0].id);
            });
        });

        it('process the transactions', function() {
            return cron.processOrders();
        });

        it('check that the transaction got cancelled', function() {
            const sql = {
                text: `SELECT o.status, t.amount AS transfer_total FROM orders AS o
                       LEFT JOIN transfers AS t ON o.id = t.order_id AND
                       t.date_created = (SELECT max(date_created) FROM transfers WHERE order_id = t.order_id AND valid = true)
                       WHERE order_currency = 'ETH' AND pub_key = $1`,
                values: [testPubKey]
            };

            return dbQuery(sql).then((res) => {
                assert.strictEqual(res.rows[0].status, 'cancelled');
                const sent = ethers.utils.bigNumberify(res.rows[0].transfer_total);
                assert(sent.eq(testPriceWei));
            });
        });

        it('check that the treasury got some money!', function() {
            return ethersProvider.getBalance(testTreasury).then((balance) => {
                assert(balance.eq(testPriceWei.add(startBalance)));
            });
        });
    });
});
