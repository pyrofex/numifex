const express = require('express');
const path = require('path');
const fs = require('fs');
const createError = require('http-errors');

const indexRouter = require('./routes/index');

const app = express();


app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const debugAdminPath = path.normalize(`${__dirname}/../client/build`);
const debugPosPath = path.normalize(`${__dirname}/../plugins/pos/build`);

if (fs.existsSync(debugAdminPath)) {
    app.use('/admin', express.static(debugAdminPath) );
} else {
    app.use('/admin', express.static(path.join(__dirname, 'admin-static')));
}

if (fs.existsSync(debugPosPath)) {
    app.use('/sales', express.static(debugPosPath) );
} else {
    app.use('/sales', express.static(path.join(__dirname, 'pos-static')));
}

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: err
    });
});

module.exports = app;
