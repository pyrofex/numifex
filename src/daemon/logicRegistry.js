const { ETH } = require('../modules/logic/ETH');
const { BTC } = require('../modules/logic/BTC');
const { ERC20, getSymbols } = require('../modules/logic/ERC20');

const logics = [];

let logicsRegistered = false;
const registrationPromise = registerLogics().then(() => {
    logicsRegistered = true;
    console.log(`Logics registered!`);
}, err => {
    console.log(`Error while registering logics:`, err);
    setInterval(() => {
        process.abort(); // wait 2 seconds, then shutdown
    }, 2000);
});

function registerLogics() {
    // registered logics

    // BTC logic
    logics.push(new BTC('BTC', 'btc_config'));

    // ETH logic
    const ethModule = new ETH('ETH', 'eth_config');
    logics.push(ethModule);

    // ERC20 tokens logic
    const erc20Config = 'erc20_config';
    return getSymbols(erc20Config).then(erc20Symbols => {
        erc20Symbols.forEach(symbol => {
            logics.push(new ERC20(symbol, erc20Config, ethModule));
        });
    });
}

// functions
function getRegistry() {
    if (!logicsRegistered) return [];
    return logics;
}

function getLogic(name) {
    console.log(`getting logic`, name);
    if (!logicsRegistered) return;
    for (let i = 0; i < logics.length; i++) {
        if (logics[i].getName() === name) {
            return logics[i];
        }
    }
}

function registerRoutes(router) {
    registrationPromise.then(() => {
        logics.forEach(logic => logic.registerRoutes(router));
    });
}

function afterRegistration() {
    return registrationPromise;
}

function createFormatMap() {
    if (!logicsRegistered) return {};
    const formatPromises = logics.map(logic => {
        return logic.createCurrencyFormatter();
    });

    const formatMap = {};
    return Promise.all(formatPromises).then((pairs) => {
        pairs.forEach((formatter, i) => {
            formatMap[logics[i].getName()] = formatter;
        });

        return formatMap;
    });
}

module.exports.getRegistry = getRegistry;
module.exports.getLogic = getLogic;
module.exports.registerRoutes = registerRoutes;
module.exports.afterRegistration = afterRegistration;
module.exports.createFormatMap = createFormatMap;
