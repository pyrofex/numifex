const { WooCommerce } = require('../modules/commerce/wooCommerce');
const { MagentoCommerce } = require('../modules/commerce/magento');
//const { CustomCommerce } = require('../modules/commerce/custom');
const { PosCommerce } = require('../modules/commerce/pos');

// registered commerces
const commerces = [
    new WooCommerce(),
    new MagentoCommerce(),
    //    new CustomCommerce(),
    new PosCommerce()
];

const commerceMap = {};
commerces.forEach(c => commerceMap[c.name] = c);

// functions
function getNotificationMap() {
    const notificationMap = {};
    return Promise.all(commerces.map(commerce => {
        return commerce.getConfig().then(config => {
            notificationMap[commerce.name] = {
                commerce: commerce,
                config: config
            };
        });
    })).then(() => {
        return notificationMap;
    });
}

module.exports.getNotificationMap = getNotificationMap;
module.exports.commerceMap = commerceMap;
