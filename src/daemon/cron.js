const logicRegistry = require('./logicRegistry');
const commerceRegistry = require('./commerceRegistry');
const db = require('../controllers/db');

function getOrderTimeouts() { // all timeouts are in hours
    const sql = 'SELECT order_timeout_in_hours FROM config';
    return db.dbQuery(sql).then(res => {
        if (res.rows.length > 0) {
            const hardTimeout = res.rows[0].order_timeout_in_hours; // orders are ignored after hard timeout
            return {
                hardTimeout: hardTimeout
            };
        } else {
            return {
                hardTimeout: 168 // default to 1 week
            };
        }
    });
}

function cancelOldOrders() {
    return getOrderTimeouts().then(orderTimeouts => {
        if (orderTimeouts.hardTimeout === 0) {
            // timeout of 0 means disabled
            return Promise.resolve();
        }

        const sql = `UPDATE orders SET status = 'cancelling'
                    WHERE status = 'created'
                    AND now() >= date_created + INTERVAL '${orderTimeouts.hardTimeout} HOUR'`;

        return db.dbQuery(sql);
    });
}

function updateStatuses() {
    return cancelCancellingOrders().then(() => {
        return finishPaidOrders();
    });
}

function getOrdersToNotify() {
    const sql = `SELECT id, commerce_id, status, commerce FROM orders
                 WHERE (status = 'paid' OR status = 'cancelling')
                 AND commerce_notify_date IS NULL
                 AND commerce_id IS NOT NULL`;

    return db.dbQuery(sql).then(res => res.rows);
}

function cancelCancellingOrders() {
    const sql = `UPDATE orders SET status = 'cancelled'
                WHERE status = 'cancelling'
                AND EXISTS(SELECT 1 FROM transfers WHERE order_id = orders.id AND date_confirmed IS NOT NULL AND transfer_type = 'payout')
                AND (commerce_notify_date IS NOT NULL OR commerce_id is NULL)`;

    return db.dbQuery(sql);
}

function finishPaidOrders() {
    const sql = `UPDATE orders SET status = 'finished'
                WHERE status = 'paid'
                AND EXISTS(SELECT 1 FROM transfers WHERE order_id = orders.id AND date_confirmed IS NOT NULL AND transfer_type = 'payout')
                AND (commerce_notify_date IS NOT NULL OR commerce_id is NULL)`;

    return db.dbQuery(sql);
}

function finishNotification(order) {
    const sql = {
        text: 'UPDATE orders SET commerce_notify_date = $2 WHERE id = $1',
        values: [ order.id, new Date().toUTCString() ]
    };

    return db.dbQuery(sql);
}


/*
THE LIFE OF AN ORDER

There are two paths for an order

created -> paid -> finished
      |
      ---> cancelling -> cancelled

We say an order is ACTIVE if it has not been transferred to the treasury.
This implies an order status is either created, paid, or cancelling.

Orders have their balances regularly updated iff they are ACTIVE.

An order is moved from created to paid if its balance is >= to the total.
An order is moved from created to cancelling if it times out OR it's explicitly cancelled.

In order to advance from paid or cancelling
to the final stage, the order must complete
all intermediate steps (transfer, woocommerce notified, etc).

Completion of these steps is indicated by whether the timestamp is present.

*/

/*
The steps are broken out in this matter so that a failure
of one step will not ruin the order.

For example, there is a good chance either the woocommerce notification
or the blockchain transaction fails. Rather then break the status
we need to try again later.
*/


function processOrders() {
    // mark orders that have timed out
    // note that these orders are still ACTIVE
    // and will have their balances updated
    return cancelOldOrders().then(() => {
        return Promise.all(logicRegistry.getRegistry().map(logic => { // setup
            return logic.createContext().then((ctx) => {
                if (!ctx) {
                    console.log(`${logic.getName()} is not setup.`);
                } else {
                    return [logic, ctx];
                }
            }, err => { // catch a rejection during setup()
                console.error(`Failed to process orders for ${logic.getName()}.`, err);
            });
        })).then(preparedLogics => {
            return Promise.all(preparedLogics.map(pair => { // process orders
                if (pair) {
                    const [ logic, ctx ] = pair;
                    return logic.prepareActiveOrders(ctx).then(() => {
                        return logic.makeTransfers(ctx);
                    }).then(() => {
                        return logic.confirmActiveOrders(ctx);
                    }).catch(err => { // failure in updating should not stop other logics and notifications
                        console.error(`failed while updating orders for ${logic.getName()} with error:`, err);
                    }).then(() => {
                        ctx.shutdown();
                    });
                }
            }));
        });
    }).then(() => {
        return commerceRegistry.getNotificationMap();
    }).then(commerceMap => {
        return getOrdersToNotify().then(orders => {
            return Promise.all(orders.map(order => {
                const commerceType = order.commerce;
                if (!commerceType || !commerceMap[commerceType]) { // if type was found, hand it off to it's type handler
                    // if type is not found, then simply ignore it
                    // console.log(`failed to notify for ${order.id}\nCould not find commerce handler for commerce type: ${order.commerce}`);
                    return;
                }
                return commerceMap[commerceType].commerce.makeNotification(order, commerceMap[commerceType].config).then(() => {
                    return finishNotification(order); // mark the notification as sent
                }).catch((err) => {
                    console.log(`failed to notify for ${order.id}`, err);
                });
            }));
        });
    }).then(() => {
        return updateStatuses();
    });
}

module.exports.processOrders = processOrders;

module.exports.test = {
    getOrderTimeouts
};
