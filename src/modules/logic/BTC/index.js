const bigInt = require('big-integer');
const commonLogic = require('../commonLogic');
const { Context } = require('./context');
const util = require('./util');
const routes = require('./routes');
const { requestAxiaRate, requestCoinbaseRate } = require('../pricing');
const db = require('../../../controllers/db');

class BTC {
    constructor(name, dbTable) {
        this.name = name;
        this.dbTable = dbTable;
    }

    createContext() {
        return new Context(this.name, this.dbTable).init();
    }

    getName() {
        return this.name;
    }

    getConfigByKey(key) {
        return commonLogic.getConfigByKey(this.dbTable, key);
    }

    setConfigByKey(key, value) {
        return commonLogic.setConfigByKey(this.dbTable, key, value);
    }


    _getActiveOrders() {
        return commonLogic.getActiveOrders(this.name).then(orders => {
            orders.forEach(row => {
                row.order_total = bigInt(row.order_total);
            });
            return orders;
        });
    }

    prepareActiveOrders(context) {
        return Promise.all([
            this._getActiveOrders(),
            commonLogic.getOrdersToTransfer(this.name)
        ]).then(([activeOrders, transferOrders]) => {
            return this._verifyAllAddressesImported(context, [...activeOrders, ...transferOrders]).then(() => {
                return context.getBlockHeight().then(blockHeight => {
                    return Promise.all(activeOrders.map(order => {
                        return this._updateBalance(context, order, blockHeight).catch((err) => {
                            // catch individual errors, so one doesn't break the whole batch
                            console.log(`failed to update balance for ${order.id}`, err);
                        });
                    }));
                });
            }, timeToFinishImport => {
                console.log(`BTC detected a new bitcoin-core node and will pause to import all active addresses until ${timeToFinishImport}`);
                throw `Addresses importing`;
            });
        });
    }

    confirmActiveOrders(context) {
        return this._getActiveOrders().then(orders => {
            return Promise.all(orders.map(order => {
                return this._updateConfirmation(context, order).catch(err => {
                    // catch individual errors, so one doesn't break the whole batch
                    console.log(`failed to update confirm ${order.id}`, err);
                });
            }));
        });
    }

    makeTransfers(context) {
        return Promise.all([
            commonLogic.getOrdersToTransfer(this.name),
            context.getBlockHeight()
        ]).then(([orders, blockHeight]) => {
            return Promise.all(orders.map(order => {
                return this._transferToTreasury(context, order, blockHeight).catch((err) => {
                    // catch individual errors, so one doesn't break the whole batch
                    console.log(`failed to transfer for ${order.id}`, err);
                });
            }));
        });
    }

    registerRoutes(router) {
        return routes.registerRoutes(router);
    }

    // Accepts an order id and total, generates keys, and stores the order in DB
    // order total is a string in Satoshi
    createOrder({ commerceId, orderTotal, commerce, currency, convRate }) {
        // it requires connecting to bitcoin-core in order to import the address
        // which absolutely sucks.

        let context;
        return this.createContext().then((ctx) => {
            context = ctx;

            if (!ctx) {
                throw new Error(`btc is not setup.`);
            }

            const newWallet = util.createWallet(context.client.getNetworkName());
            const address = newWallet.address;
            const priv_key = newWallet.privKey;
            const script = newWallet.script;
            // add address as watch-only address in bitcoin-core
            return context.client.importAddress(address, '', false).then(() => {
                if (context) {
                    context.shutdown();
                }
            }).then(() => {
                const sql = {
                    text: `INSERT INTO orders(order_currency, commerce_id, order_total, commerce,
                currency, currency_conv_rate, pub_key, priv_key, script) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9)`,
                    values: [this.name, commerceId, bigInt(orderTotal).toString(), commerce, currency, convRate, address, priv_key, script]
                };

                return db.dbQuery(sql).then(() => {
                    return {
                        address: address
                    };
                });
            });
        });
    }

    requestExchangeRate(fiatCurrency, riskLevel, timeToSell) {
        return commonLogic.getAxiaCodexToken().then((axiaCodexToken) => {
            if (axiaCodexToken) {
                return requestAxiaRate(axiaCodexToken, this.name, fiatCurrency, riskLevel, timeToSell);
            } else {
                return requestCoinbaseRate(this.name, fiatCurrency, riskLevel, timeToSell);
            }
        });
    }

    qrCodeText(...args) {
        return util.qrCodeText(...args);
    }

    createCurrencyFormatter() {
        return Promise.resolve(new util.BtcFormatter());
    }

    _updateBalance(context, order, blockHeight) {
        const { minimum_confirmations } = context.config;
        return Promise.all([
            getPreviousBalance(order.id),
            context.getConfirmedInputs(order.pub_key, minimum_confirmations).then(util.sumInputs)
        ]).then(([previousBalance, currentBalance]) => {
            let currentBal = Promise.resolve(currentBalance);
            if (previousBalance === null || !previousBalance.eq(currentBalance)) {
                // if the balance changed, add a payment before continuing
                currentBal = commonLogic.addPayment(order.id, currentBalance.toString(), blockHeight).then(() => currentBalance);
            }
            return currentBal;
        }).then(currentBalance => {
            if (order.status === 'created' && currentBalance.geq(order.order_total)) { // greater or equals
                // if the order is not cancelled
                // Update payments table
                const sql = { // if the balance is equal or greater than the total order amount, status becomes 'paid'
                    text: `UPDATE orders SET status = 'paid' WHERE id = $1`,
                    values: [order.id]
                };
                return db.dbQuery(sql);
            }
        });
    }

    _updateConfirmation(context, order) {
        if (!order.transfer_tx) {
            return Promise.resolve();
        }

        const { minimum_confirmations } = context.config;

        // NOTE!!!: vout is always zero because treasury transfers always have only 1 output
        return context.client.getTxOut(order.transfer_tx, 0).then(txinfo => {
            if (!txinfo || txinfo.confirmations === null) {
                console.log(`transaction ${order.transfer_id} failed to confirm for order ${order.id}`);
                // the chain got "reorged"
                // and our transaction is no longer valid
                // so clear the transaction and try again
                const sql = {
                    text: `UPDATE transfers SET valid = false WHERE id = $1`,
                    values: [order.transfer_id]
                };

                return db.dbQuery(sql);
            } else {
                console.log('confirmations', txinfo.confirmations);
                if (txinfo.confirmations >= minimum_confirmations) {
                    console.log(`confirming transaction ${order.transfer_id} for order ${order.id}`);
                    // update confirmations
                    const sql = {
                        text: `UPDATE transfers SET date_confirmed = CURRENT_TIMESTAMP WHERE id = $1`,
                        values: [order.transfer_id]
                    };
                    return db.dbQuery(sql);
                }
            }
        });
    }

    // Once status is 'paid', BTC is transferred from the
    // individual order acccount to the treasury account.
    _transferToTreasury(context, order, blockHeight) {
        const { minimum_confirmations } = context.config;
        let { treasury } = context.config;
        const { pub_key: address, priv_key, id, script } = order;

        treasury = treasury.trim(); // could have trailing spaces which is not allowed

        return context.getConfirmedInputs(address, minimum_confirmations).then(unspentInputs => {
            const fee = util.getFeeForTransfer(context.client.getNetworkName(), unspentInputs, treasury, priv_key, context.feeRate);
            const balance = util.sumInputs(unspentInputs);
            let sendValue = balance.minus(fee);

            let transferPromise;
            if (!sendValue.isPositive()) {
                // too little to transfer
                // call it good
                sendValue = bigInt(0);
                transferPromise = Promise.resolve();
            } else {
                const hex = util.createRawTransaction(context.client.getNetworkName(), unspentInputs, treasury, priv_key, Buffer.from(script, 'hex'), fee);
                transferPromise = context.client.sendRawTransaction(hex, true);
            }

            return transferPromise.then(transferTx => {
                if (transferTx) {
                    console.log(`transfer for ${id} transaction ${transferTx}`);
                    const sql = {
                        text: `INSERT INTO transfers(order_id, from_address, to_address, amount, transfer_type, tx, block)
                            VALUES($1, $2, $3, $4, 'payout', $5, $6)`,
                        values: [id, address, treasury, sendValue.toString(), transferTx, blockHeight]
                    };

                    return db.dbQuery(sql);
                } else {
                    // this case is ok
                    // just insert a null tx into the database
                    console.log(`order ${id} did not have enough to cover fees. Balance: ${balance.toString()} Satoshi`);
                    const sql = {
                        text: `INSERT INTO transfers(order_id, amount, transfer_type, from_address, to_address, date_confirmed)
                            VALUES($1, $2, 'payout', '', '', CURRENT_TIMESTAMP)`,
                        values: [id, sendValue.toString()]
                    };

                    return db.dbQuery(sql);
                }
            });
        });
    }

    _verifyAllAddressesImported(context, orders) {
        if ((new Date()).getTime() < context.config.pause_until) { // first check if an import is ongoing
            return Promise.reject((new Date(parseInt(context.config.pause_until))).toString());
        }
        if (!context.config.node_changed) {
            return Promise.resolve(); // no need to import anything
        }
        if (orders.length === 0) { // no orders to import
            return db.dbQuery(`UPDATE ${this.dbTable} SET node_changed = false`); // then finish and don't delay
        }
        const pause_until = new Date((new Date()).getTime() + 1.5*60*60*1000); // add 1.5 hours to current time
        const sql = {
            text: `UPDATE ${this.dbTable} SET pause_until = $1, node_changed = false`,
            values: [pause_until.getTime()]
        };
        return db.dbQuery(sql).then(() => {
            const addressObjects = orders.map(order => ({
                address: order.pub_key,
                date: order.date_created
            }));
            return this._multiImport(context, addressObjects).then(() => {
                return Promise.reject(pause_until.toString()); // reject with the time the pause will end
            });
        });
    }

    _multiImport(context, addressObjects) {
        const request = addressObjects.map(addr => {
            const address = addr.address;
            const timestamp = Math.floor((new Date(addr.date)).getTime() / 1000);
            return {
                scriptPubKey: {
                    address: address
                },
                timestamp: timestamp
            };
        });
        const options = {
            rescan: true
        };
        if (request.length > 0) {
            return context.client.importMulti(request, options);
        }
        return Promise.resolve();
    }
}

function getPreviousBalance(order_id) {
    return commonLogic.getPreviousBalance(order_id).then(x => {
        if (x !== null){
            return bigInt(x);
        }
        return x;
    });
}

module.exports.BTC = BTC;
