const bigInt = require('big-integer');
const db = require('../../../controllers/db');
const commonLogic = require('../commonLogic');
const BTCRpc = require('./btcRPC');


class Context {
    constructor(name, dbTable) {
        this.name = name;
        this.dbTable = dbTable;
        this.client = null;
        this.config = null;
        this.feeRate = null;
    }

    init() {
        return this._prepareConfig().then(config => {
            if (!config ||
                !config.treasury) {
                return null;
            } else {
                this.config = config;
                return this._createClient().then(client => {
                    this.client = client;
                    if (this.config.fee_rate !== null) {
                        this.feeRate = this.config.fee_rate;
                        return this;
                    } else {
                        return this.getTransactionFeeRate().then(feeRate => {
                            this.feeRate = feeRate;
                            return this;
                        });
                    }
                });
            }
        });
    }

    shutdown() {
        delete this.config;
    }

    _prepareConfig() {
        const sql = `SELECT treasury, minimum_confirmations, btc_fee_rate, rpc_user, rpc_password, fee_estimate_block_target,
                            pause_until, node_changed, node_url FROM ${this.dbTable}`;
        return Promise.all([
            db.dbQuery(sql),
            commonLogic.getAxiaCodexToken()
        ]).then(([res, axiaCodexToken]) => {
            if (res.rows.length > 0) {
                const config = res.rows[0];
                config.axiaCodexToken = axiaCodexToken;
                if (config.btc_fee_rate) {
                    config.fee_rate = bigInt(config.btc_fee_rate);
                    delete config.btc_fee_rate;
                } else {
                    config.fee_rate = null;
                }
                return config;
            } else {
                throw new Error(`Could not get Bitcoin config from database`);
            }
        });
    }

    // returns a bitcoin-core client object
    _createClient() {
        const { node_url, rpc_user, rpc_password, axiaCodexToken } = this.config;

        const client = new BTCRpc.Client({
            username: rpc_user,
            password: rpc_password,
            bearer_token: axiaCodexToken,
            node_url: node_url
        });
        return client.connect().then(() => client);
    }

    getTransactionFeeRate() {
        const { fee_estimate_block_target } = this.config;

        if (!this.client) {
            return Promise.reject(new Error('bitcoin client not setup yet'));
        }

        if (this.client.getNetworkName() === 'bitcoin') {
            return this.client.estimateSmartFee(fee_estimate_block_target).then(res => {
                const feerate = res ? res.feerate : undefined;
                if (!feerate || isNaN(feerate)) {
                    let errorDescription = '';
                    if (res.errors) {
                        errorDescription = res.errors.join('. ');
                    }
                    throw new Error(`BTC fee rate is not available (is your node fully synced?). ${errorDescription}`);
                }
                // 1 BTC/1000 byte * 100,000,000 satoshi/BTC
                // = 100,000 satoshi/byte
                return bigInt(Math.ceil(feerate * 100000));
            });
        } else {
            const testFeeRate = bigInt(100);
            return Promise.resolve(bigInt(testFeeRate));
        }
    }

    getConfirmedInputs(address, minimum_confirmations) {
        return this.client.listUnspent(minimum_confirmations, null, [address]);
    }

    getBlockHeight() {
        return this.client.getBlockCount();
    }

}

module.exports.Context = Context;
