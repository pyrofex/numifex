const bigInt = require('big-integer');
const bitcoin = require('bitcoinjs-lib');
const urlLib = require('url');

function sumInputs(unspentInputs) {
    return unspentInputs.reduce((total, tx) => total.add(safeConvertBTCToSatoshi(tx.amount)), bigInt(0)); // convert to satoshi
}

// bigInteger | string => string
function safeConvertSatoshiToBTC(satoshi) {
    if (!bigInt.isInstance(satoshi)) {
        if (typeof satoshi === 'string') {
            satoshi = bigInt(satoshi);
        } else {
            throw new TypeError(`safe convert not a big number`);
        }
    }
    const satoshiPerBtc = bigInt(100000000);
    return `${satoshi.divide(satoshiPerBtc)}.${(satoshi.mod(bigInt(satoshiPerBtc))).toString().padStart(8, '0')}`;
}

// number | string => bigInteger
function safeConvertBTCToSatoshi(btc) {
    if (typeof btc !== 'number' && typeof btc !== 'string') {
        throw new TypeError(`btc not a string or a number`);
    }

    return bigInt(parseFloat(btc).toFixed(8).replace('.', ''));
}

function createRawTransaction(networkName, unspentInputs, outputAddress, inputPrivKey, redeemScript, fee) {
    const unspentTotal = sumInputs(unspentInputs);
    const sendAmount = unspentTotal.minus(fee);
    const network = bitcoin.networks[networkName];

    if (network === undefined) {
        throw new Error(`invalid network ${networkName}`);
    }

    const sender = bitcoin.ECPair.fromWIF(inputPrivKey, network);
    const txb = new bitcoin.TransactionBuilder(network, Number.MAX_VALUE); // the fee can never be too high

    txb.setVersion(1);
    txb.addOutput(outputAddress, sendAmount.toJSNumber());
    for (let i = 0; i < unspentInputs.length; i++) {
        const utxo = unspentInputs[i];
        txb.addInput(utxo.txid, utxo.vout);
    }
    for (let i = 0; i < unspentInputs.length; i++) {
        const amount = safeConvertBTCToSatoshi(unspentInputs[i].amount).toJSNumber();
        txb.sign(i, sender, redeemScript, null, amount);
    }

    const rawTransaction = txb.build().toHex();
    return rawTransaction;
}

function getFeeForTransfer(networkName, unspentInputs, outputAddress, inputPrivKey, feeRate) {
    if (unspentInputs.length === 0) {
        return bigInt(0);
    }
    const hex = createRawTransaction(networkName, unspentInputs, outputAddress, inputPrivKey, undefined, bigInt(0));
    return bigInt(hex.length).multiply(feeRate).divide(bigInt(2)); // each character is 0.5 Bytes
}

function createWallet(networkName) {
    const network = bitcoin.networks[networkName];
    if (network === undefined) {
        throw new Error(`invalid network ${networkName}`);
    }
    const keyPair = bitcoin.ECPair.makeRandom({
        network: network
    });
    const WIF = keyPair.toWIF();
    const { address, redeem } = bitcoin.payments.p2sh({
        redeem: bitcoin.payments.p2wpkh({
            pubkey: keyPair.publicKey,
            network: network
        }),
        network: network
    });

    return {
        privKey: WIF,
        address: address,
        script: redeem.output.toString('hex')
    };
}

// https://github.com/bitcoin/bips/blob/master/bip-0021.mediawiki#Transfer%20amount/size
function qrCodeText(address, amount) {
    // do some length checking here
    // so they can't just feed arbitrary data
    if (address.length < 20 ||
        address.length > 40) {
        return null;
    }

    const urlObj = {
        protocol: 'bitcoin',
        hostname: address,
        query: {}
    };

    if (amount !== undefined) {
        urlObj.query.amount = amount;
    }

    return urlLib.format(urlObj);
}

class BtcFormatter {
    naturalToBase(btc) {
        return safeConvertBTCToSatoshi(btc).toString();
    }

    baseToNatural(satoshi) {
        return safeConvertSatoshiToBTC(satoshi);
    }
}

module.exports.sumInputs = sumInputs;
module.exports.safeConvertSatoshiToBTC = safeConvertSatoshiToBTC;
module.exports.safeConvertBTCToSatoshi = safeConvertBTCToSatoshi;
module.exports.createRawTransaction = createRawTransaction;
module.exports.getFeeForTransfer = getFeeForTransfer;
module.exports.createWallet = createWallet;
module.exports.qrCodeText = qrCodeText;
module.exports.BtcFormatter = BtcFormatter;
