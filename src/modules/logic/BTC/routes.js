const bigInt = require('big-integer');
const auth = require('../../../controllers/auth');
const logicRegistry = require('../../../daemon/logicRegistry');
const { safeConvertBTCToSatoshi, safeConvertSatoshiToBTC } = require('./util');

function registerRoutes(router) {
    // requires admin token
    router.get('/btc-fee-rate-fetch', function (req, res) {
        console.log(`/btc-fee-rate-fetch`);
        auth.checkAdminToken(req.header('Authorization')).then(() => {
            const logic = logicRegistry.getLogic('BTC');
            return logic.getConfigByKey('btc_fee_rate').then((feeRateString) => {
                if (feeRateString) {
                    try {
                        const feeRate = bigInt(feeRateString);
                        res.status(200).json({
                            fee_rate_satoshi: feeRate.toString(),
                            fee_rate_btc: safeConvertSatoshiToBTC(feeRate.multiply(bigInt(1000)))
                        });
                    } catch(e) {
                        res.status(200).json({
                            fee_rate_satoshi: null,
                            fee_rate_btc: null
                        });
                    }
                } else {
                    res.status(200).json({
                        fee_rate_satoshi: null,
                        fee_rate_btc: null
                    });
                }
            });
        }).catch((err) => {
            console.error(err);
            const code = (err instanceof auth.AuthError) ? 401 : 500;
            res.status(code).json({
                error: err.message
            });
        });
    });

    // requires admin token
    router.post('/btc-fee-rate-submit', function(req, res) {
        console.log(`/btc-fee-rate-submit`, req.body);
        auth.checkAdminToken(req.header('Authorization')).then(() => {
            // custom route here for unit conversions
            // either satoshi/B or BTC/kB
            let satoshi;
            try {
                satoshi = bigInt(req.body.fee_rate_satoshi).toString();
            } catch(e) {
                satoshi = null;
            }

            if (!satoshi) {
                if(req.body.fee_rate_btc) {
                    satoshi = (safeConvertBTCToSatoshi(req.body.fee_rate_btc).divide(bigInt(1000))).toString();
                } else {
                    satoshi = null;
                }
            }
            const logic = logicRegistry.getLogic('BTC');
            return logic.setConfigByKey('btc_fee_rate', satoshi).then(() => {
                res.status(201).json({
                    response: 'success'
                });
            });
        }).catch((err) => {
            console.error(err);
            const code = (err instanceof auth.AuthError) ? 401 : 500;
            res.status(code).json({
                error: err.message
            });
        });
    });

    router.get('/btc-network-fee-rate', function(req, res) {
        console.log(`/btc-network-fee-rate`);
        auth.checkAdminToken(req.header('Authorization')).then(() => {
            const logic = logicRegistry.getLogic('BTC');

            let context;
            return logic.createContext().then((ctx) => {
                context = ctx;
                return context.getTransactionFeeRate();
            }).then((feeRate) => {
                context.shutdown();
                if (feeRate) {
                    const feeRateBtc = safeConvertSatoshiToBTC(feeRate.multiply(bigInt(1000)));
                    res.status(200).json({
                        fee_rate_satoshi: feeRate.toString(),
                        fee_rate_btc: feeRateBtc.toString()
                    });
                } else {
                    res.status(200).json({
                        fee_rate_satoshi: null,
                        fee_rate_btc: null
                    });
                }
            });
        }).catch((err) => {
            console.error(err);
            const code = (err instanceof auth.AuthError) ? 401 : 500;
            res.status(code).json({
                error: err.message
            });
        });
    });

    // TODO:
    // I really think this should just be ont he front end
    router.post('/btc-change-node', function(req, res) {
        console.log('/btc-change-node', req.body);
        auth.checkAdminToken(req.header('Authorization')).then(() => {
            const { node_url, reimport_addresses } = req.body;
            const logic = logicRegistry.getLogic('BTC');
            return logic.setConfigByKey('node_url', node_url).then(() => {
                if (reimport_addresses) {
                    return logic.setConfigByKey('node_changed', true);
                }
            }).then(() => {
                res.status(201).json({
                    response: 'success'
                });
            });
        }).catch((err) => {
            console.error(err);
            const code = (err instanceof auth.AuthError) ? 401 : 500;
            res.status(code).json({
                error: err.message
            });
        });
    });
}

module.exports.registerRoutes = registerRoutes;
