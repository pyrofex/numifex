const http = require('http');
const https = require('https');
const crypto = require('crypto');
const url = require('url');

// https://www.jsonrpc.org/specification
const Version = '2.0';

function* idGen() {
    let i = 0;
    const randomString = crypto.randomBytes(16).toString('hex');
    while (true) {
        yield `${randomString} -${i}`;
        i++;
    }
}

const networks = {
    bitcoin: 8332,
    testnet: 18332,
    regtest: 18443
};

const httpsPort = 443;


class BtcRpc {
    constructor({
        username,
        password,
        bearer_token,
        node_url
    }) {
        const nodeUrl = url.parse(node_url);

        const protocol = nodeUrl.protocol !== null ? nodeUrl.protocol : 'http:';
        this.config = {
            idGen: idGen(),
            bearer_token: bearer_token,
            // if no port is given, default to 443 for https, and bitcoin mainnet default port for http
            port: nodeUrl.port !== null ? nodeUrl.port : (protocol === 'https:' ? httpsPort : networks.main),
            protocol: protocol,
            hostname: nodeUrl.hostname,
            path: nodeUrl.path
        };
        if (username && password && (username !== '' || password !== '')) { // if auth is given, add it
            this.config.auth = `${username}:${password}`;
        }

        this.networkName = null;
        this.version = null;
    }

    connect() {
        return makeCall(this.config, 'getblockchaininfo').then(info => {
            // map chain name to network name
            const nameTable = {
                'main': 'bitcoin',
                'test': 'testnet',
                'regtest': 'regtest'
            };

            this.networkName = nameTable[info.chain];

            if (!this.networkName) {
                throw new Error(`unknown network ${info.chain}`);
            }

            return this.getNetworkInfo();
        }).then(networkInfo => {
            this.version = networkInfo.version;
            if (this.version < 170000) { // lastest supported version is 0.17.0
                throw new Error(`Bitcoin-core version is too old. Found version ${this.version}. We require at least 170000`);
            }
        });
    }

    getNetworkInfo() {
        return makeCall(this.config, 'getnetworkinfo');
    }

    getBlockCount() {
        return makeCall(this.config, 'getblockcount');
    }

    estimateSmartFee(blockTarget) {
        return makeCall(this.config, 'estimatesmartfee', [blockTarget]);
    }

    listUnspent(minConf, maxConf, addresses) {
        return makeCall(this.config, 'listunspent', [minConf, maxConf, addresses]);
    }

    sendRawTransaction(hex, allowHighFees = false) {
        return makeCall(this.config, 'sendrawtransaction', [hex, allowHighFees]);
    }

    importAddress(address, label, rescan = true) {
        return makeCall(this.config, 'importaddress', [address, label, rescan]);
    }

    importMulti(requests, options = {rescan: true}) {
        return makeCall(this.config, 'importmulti', [requests, options]);
    }

    generateToAddress(blockCount, address) {
        return makeCall(this.config, 'generatetoaddress', [blockCount, address]);
    }

    getTxOut(txid, vout, include_mempool = true) {
        return makeCall(this.config, 'gettxout', [txid, vout, include_mempool]);
    }

    getAddressInfo(address) {
        return makeCall(this.config, 'getaddressinfo', [address]);
    }

    getNetworkName() {
        return this.networkName;
    }
}

function makeCall(config, method, params = []) {
    const { idGen, hostname, port, auth, path, bearer_token } = config;
    const id = idGen.next().value;
    const httpOrS = config.protocol === 'https:' ? https : http;
    return new Promise((resolve, reject) => {
        const headers = {
            'Content-Type': 'application/json',
            'Accept': '*/*'
        };
        if (bearer_token && !auth) { // if the token is given and no auth, add it to the request
            headers['Authorization'] = `Bearer ${bearer_token}`;
        }
        const requestOptions = {
            hostname: hostname,
            path: path,
            port: port,
            method: 'POST',
            timeout: 20000,
            rejectUnauthorized: false,
            headers: headers
        };
        if (auth) { // if auth is given, add it to the request
            requestOptions.auth = auth;
        }
        const body = JSON.stringify({
            jsonrpc: Version,
            method: method,
            params: params,
            id: id
        });
        const chunks = [];
        const req = httpOrS.request(requestOptions, res => {
            if (res.statusCode >= 300) {
                reject(new Error(`rpc status code: ${res.statusCode} and message: ${res.statusMessage} for method ${method}`));
                return;
            }

            res.on('data', chunk => {
                chunks[chunks.length] = chunk; // much faster than push
                // chunks.push(chunk);
            });
            res.on('end', () => {
                try {
                    const bodyText = Buffer.concat(chunks).toString('utf8');
                    const response = JSON.parse(bodyText);
                    resolve(response);
                } catch(err) {
                    reject(err);
                }
            });
            res.on('error', err => {
                reject(err);
            });
        });
        req.on('error', (err) => {
            reject(err);
        });
        req.on('timeout', () => {
            req.abort();
            reject(new Error(`timeout for method ${method}`));
        });
        req.write(body);
        req.end();
    }).then(response => { // extract from RPC format
        if (response.error) {
            throw new Error(`RPC Error: ${JSON.stringify(response.error)}`);
        } else {
            return response.result;
        }
    });
}

module.exports.Client = BtcRpc;
module.exports.Networks = networks;
