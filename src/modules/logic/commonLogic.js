const db = require('../../controllers/db');

function getConfigByKey(configTable, key) {
    // DO NOT take a user paramater for the config table
    const query = {
        text: `SELECT * FROM ${configTable}`,
    };
    return db.dbQuery(query).then(res => {
        if (res.rows.length > 0) {
            return (res.rows[0])[key];
        } else {
            return undefined;
        }
    });
}

function setConfigByKey(configTable, key, value) {
    const query = {
        text: `SELECT * FROM ${configTable}`,
    };
    return db.dbQuery(query).then(res => {
        // check if the key is in any existing fields
        const field = res.fields.find((field) => field.name === key);

        if (field) {
            const sql = {
                text: `UPDATE ${configTable} SET ${field.name} = $1`, values: [value]};
            return db.dbQuery(sql);
        } else {
            throw new Error(`attempted to set key which is not in ${configTable}`);
        }
    });
}

function getActiveOrders(currency) {
    const sql = {
        text: `SELECT o.id, o.commerce_id, o.order_total, o.pub_key, o.status, o.date_created,
               t.id AS transfer_id, t.valid AS transfer_valid, t.tx AS transfer_tx, t.block AS transfer_block
               FROM orders AS o LEFT JOIN transfers AS t ON o.id = t.order_id AND
               t.date_created = (SELECT max(date_created) FROM transfers WHERE order_id = t.order_id AND valid = true AND transfer_type = 'payout')
               WHERE (status = 'created' OR status = 'cancelling' OR status = 'paid')
               AND order_currency = $1`,
        values: [currency]
    };

    return db.dbQuery(sql).then(res => res.rows);
}

function getOrdersToTransfer(currency) {
    const sql = {
        text: `SELECT id, pub_key, priv_key, status, script FROM orders
               WHERE (status = 'paid' OR status = 'cancelling')
               AND order_currency = $1 AND NOT EXISTS(SELECT 1 FROM transfers WHERE order_id = orders.id AND valid = true AND transfer_type = 'payout')`,
        values: [currency]
    };

    return db.dbQuery(sql).then(res => res.rows);
}

function getPreviousBalance(order_id) {
    const paymentSql = {
        text: `SELECT balance, date_created FROM payments
               WHERE order_id = $1
               ORDER BY date_created DESC
               LIMIT 1`,
        values: [order_id]
    };

    return db.dbQuery(paymentSql).then((res) => {
        if (res.rows.length > 0) {
            return res.rows[0].balance;
        } else {
            return null;
        }
    });
}

function addPayment(order_id, balance, blockHeight) {
    const sql = {
        text: 'INSERT INTO payments(order_id, balance, block_height) VALUES($1, $2, $3)',
        values: [order_id, balance, blockHeight]
    };

    console.log(`updating balance for order ${order_id} balance: ${balance}`);

    return db.dbQuery(sql);
}

function getAxiaCodexToken() {
    return db.dbQuery({
        text: `SELECT axiacodex_token FROM config`
    }).then(res => {
        if (res.rows && res.rows.length > 0) {
            return res.rows[0].axiacodex_token;
        } else {
            return null;
        }
    }).catch(() => {
        return null;
    });
}

module.exports.addPayment = addPayment;
module.exports.setConfigByKey = setConfigByKey;
module.exports.getConfigByKey = getConfigByKey;
module.exports.getOrdersToTransfer = getOrdersToTransfer;
module.exports.getPreviousBalance = getPreviousBalance;
module.exports.getActiveOrders = getActiveOrders;
module.exports.getAxiaCodexToken = getAxiaCodexToken;
