const url = require('url');
const https = require('https');

// currency is UPPER CASE ISO
// https://www.ibm.com/support/knowledgecenter/en/SSZLC2_7.0.0/com.ibm.commerce.payments.developer.doc/refs/rpylerl2mst97.htm

// Only axia codex knows how to use
// riskLevel and timeToSell properly.
// This function applies those same parameters
// in a compromised way for other pricing sources.
// It is basically a fixed percentage adjustment
function manuallyApplyRateSettings(rawFiatPerCrypto, riskLevel, timeToSell) {
    let percentSaftey = 0.0;
    // time to sell ranging from 0-7
    if (riskLevel === 'high') {
        // range form 3% to  6.5%
        percentSaftey = 0.03 + 0.005 * (timeToSell - 1);
    } else if (riskLevel === 'medium') {
        // range from 5% to 12%
        percentSaftey = 0.05 + 0.01 * (timeToSell - 1);
    } else if (riskLevel === 'low') {
        // range from 10% to 20.5%
        percentSaftey = 0.10 + 0.015 * (timeToSell - 1);
    }

    return Math.max(rawFiatPerCrypto - percentSaftey * rawFiatPerCrypto, 0.0);
}

function requestAxiaRate(axiaCodexToken, cryptoCurrency, fiatCurrency, riskLevel, timeToSell) {
    const axiacodexUrl = url.parse(url.format({
        protocol: 'https',
        hostname: 'axiacodex.io',
        pathname: '/api/adjustedMerchantPrice'
    }));

    const body = {
        clientExch: null, // don't specify an exchange by default
        fiatSymbol: fiatCurrency,
        coinSymbol: cryptoCurrency
    };

    if (riskLevel !== undefined) {
        body.riskLevel = riskLevel;
    }

    if (timeToSell !== undefined) {
        body.timeToSell = timeToSell;
    }

    return new Promise((resolve, reject) => {
        // make sure to follow node 8.x
        // docs here until we upgrade
        const req = https.request({
            method: 'POST',
            hostname: axiacodexUrl.hostname,
            path: axiacodexUrl.path,
            timeout: 5000,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${axiaCodexToken}`
            }
        }, (res) => {
            const chunks = [];
            res.on('data', (chunk) => {
                chunks.push(chunk);
            });

            res.on('end', () => {
                // TODO:
                // what if the coin isn't found?
                try {
                    const bodyText = Buffer.concat(chunks).toString('utf8');
                    const json = JSON.parse(bodyText);

                    try {
                        const exchangeRate = parseFloat(json['adjustedRate']);
                        resolve(exchangeRate);
                    } catch (err) {
                        reject(new Error(`invalid exchange data ${err.message}`));
                    }
                } catch (err) {
                    reject(err);
                }
            });
        });

        req.on('error', (err) => {
            reject(err);
        });

        req.on('timeout', () => {
            req.abort();
            reject(new Error('timeout'));
        });

        req.write(JSON.stringify(body), 'utf8');
        req.end();
    });
}

function requestCoinbaseRate(cryptoCurrency, fiatCurrency, riskLevel, timeToSell) {
    // https://developers.coinbase.com/api/v2#exchange-rates
    // Returned rates will define the exchange rate for one unit of the base currency.
    const exchangeUrl = url.parse(url.format({
        protocol: 'https',
        hostname: 'api.coinbase.com',
        pathname: '/v2/exchange-rates',
        query: {
            currency: cryptoCurrency
        }
    }));

    return new Promise((resolve, reject) => {
        // make sure to follow node 8.x
        // docs here until we upgrade
        const req = https.get({
            hostname: exchangeUrl.hostname,
            path: exchangeUrl.path,
            headers: {
                // Coinbase wants a user agent
                'User-Agent': 'Mozilla/5.0',
            },
            timeout: 5000
        }, (res) => {
            const chunks = [];
            res.on('data', (chunk) => {
                chunks.push(chunk);
            });

            res.on('end', () => {
                try {
                    if (res.statusCode >= 300) {
                        if (res.statusCode === 400) {
                            throw new Error('unknown currency');
                        } else {
                            throw new Error(`Received bad status code ${res.statusCode}`);
                        }
                    }

                    const bodyText = Buffer.concat(chunks).toString('utf8');
                    const json = JSON.parse(bodyText);

                    if (json['data']['rates'][fiatCurrency] === undefined) {
                        // coinbase does not track this fiat
                        reject(new Error('unknown fiat currency'));
                    } else {
                        let exchangeRate = parseFloat(json['data']['rates'][fiatCurrency]);
                        // apply settings because its coinbase
                        exchangeRate = manuallyApplyRateSettings(exchangeRate, riskLevel, timeToSell);
                        resolve(exchangeRate);
                    }
                } catch (err) {
                    reject(err);
                }
            });
        });

        req.on('error', (err) => {
            reject(err);
        });

        req.on('timeout', () => {
            req.abort();
            reject(new Error('timeout'));
        });
    });
}

module.exports.requestAxiaRate = requestAxiaRate;
module.exports.requestCoinbaseRate = requestCoinbaseRate;
