const ethers = require('ethers');
const db = require('../../../controllers/db');
const commonLogic = require('../commonLogic');
const gas = require('./gas');

// a context holds settings for a cron cycle
// and a connection to the node.

class Context {
    constructor(name, dbTable) {
        this.name = name;
        this.dbTable = dbTable;
        this.config = null;
        this.provider = null;
        this.gasPrice = null;
    }

    init() {
        return this._prepareConfig().then(config => {
            if (!config ||
                !config.treasury ||
                !config.eth_node) {
                return null;
            } else {
                this.config = config;
                this.provider = this._createProvider();
                return Promise.all([
                    this.provider.getGasPrice(),
                    this._getGasPriceLevels()
                ]).then(([nodeGasPrice, gasPriceLevels]) => {
                    this.gasPrice = this._chooseGasPrice(nodeGasPrice, gasPriceLevels);
                    return this;
                });
            }
        });
    }

    shutdown() {
        this.provider.polling = false;
        delete this.provider;
    }

    getBlockHeight() {
        return this.provider.getBlockNumber();
    }

    getGasPrice() {
        return this.gasPrice;
    }

    _prepareConfig() {
        const sql = `SELECT treasury, eth_gas_price, gas_price_selection, minimum_confirmations, eth_node, eth_node_auth_user, eth_node_auth_pass  FROM ${this.dbTable}`;
        return Promise.all([
            db.dbQuery(sql),
            commonLogic.getAxiaCodexToken()
        ]).then(([res, axiaCodexToken]) => {
            if (res.rows.length > 0) {
                const config = res.rows[0];
                config.axiaCodexToken = axiaCodexToken;
                return config;
            } else {
                throw new Error(`Could not get Ethereum config from database`);
            }
        });
    }

    // returns an ethers provider object
    _createProvider() {
        const { eth_node, axiaCodexToken, eth_node_auth_user, eth_node_auth_pass } = this.config;

        const connectionInfo = {
            url: eth_node
        };
        if (eth_node_auth_user && eth_node_auth_user.length > 0
            && eth_node_auth_pass && eth_node_auth_pass.length > 0) {
            connectionInfo.user = eth_node_auth_user;
            connectionInfo.password = eth_node_auth_pass;
        } else if (axiaCodexToken) {
            connectionInfo.headers = {
                'Authorization': `Bearer ${axiaCodexToken}`
            };
        }
        return new ethers.providers.JsonRpcProvider(connectionInfo);
    }

    _getGasPriceLevels() {
        if (this.config.axiaCodexToken) {
            return gas.getAxiaGasPrice(this.config.axiaCodexToken).catch(() => ({}));
        } else {
            return gas.getEtherGasStationPrice().catch(() => ({}));
        }
    }

    // if the user has set a gas_price or gas price level
    // use that, otherwise use the node's
    _chooseGasPrice(nodeGasPrice, gasPriceLevels) {
        const { gas_price_selection, eth_gas_price } = this.config;
        let selectedPrice;
        switch(gas_price_selection) {
            case 'low':
                selectedPrice = gasPriceLevels.low;
                break;
            case 'average':
                selectedPrice = gasPriceLevels.standard;
                break;
            case 'high':
                selectedPrice = gasPriceLevels.high;
                break;
            case 'custom':
                if (eth_gas_price) {
                    selectedPrice = ethers.utils.bigNumberify(eth_gas_price);
                } else {
                    selectedPrice = nodeGasPrice;
                }
                break;
            case 'market':
            default:
                selectedPrice = nodeGasPrice;
                break;
        }

        if (selectedPrice && selectedPrice.gt(ethers.utils.bigNumberify(0))) {
            return selectedPrice;
        } else {
            return nodeGasPrice;
        }
    }
}

module.exports.Context = Context;