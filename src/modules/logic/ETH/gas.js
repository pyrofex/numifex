const https = require('https');
const ethers = require('ethers');

function getAxiaGasPrice(axiaCodexToken) {
    return new Promise((resolve, reject) => {
        // make sure to follow node 8.x
        // docs here until we upgrade
        const req = https.get({
            hostname: 'axiacodex.io',
            path: '/api/ethGasPrice',
            timeout: 2500,
            headers: {
                'Authorization': `Bearer ${axiaCodexToken}`
            }
        }, (res) => {
            const chunks = [];
            res.on('data', (chunk) => {
                chunks.push(chunk);
            });

            res.on('end', () => {
                try {
                    const bodyText = Buffer.concat(chunks).toString('utf8');
                    const json = JSON.parse(bodyText);

                    try {
                        const key_pairs = {
                            low: 'safeLow',
                            standard: 'standard',
                            high: 'fast',
                            highest: 'fastest'
                        };
                        const result = {};
                        for (const key in key_pairs) {
                            result[key] = ethers.utils.parseUnits(json[key_pairs[key]].toString(), json['price_value']);
                        }
                        resolve(result);
                    } catch (err) {
                        reject(new Error(`invalid axiacodex gas price data ${err.message}`));
                    }
                } catch (err) {
                    reject(err);
                }
            });
        });

        req.on('error', (err) => {
            reject(err);
        });

        req.on('timeout', () => {
            req.abort();
            reject(new Error('timeout'));
        });
    });
}

function getEtherGasStationPrice() {
    return new Promise((resolve, reject) => {
        // make sure to follow node 8.x
        // docs here until we upgrade
        const req = https.get({
            hostname: 'ethgasstation.info',
            path: '/json/ethgasAPI.json',
            timeout: 2500
        }, (res) => {
            const chunks = [];
            res.on('data', (chunk) => {
                chunks.push(chunk);
            });

            res.on('end', () => {
                try {
                    const bodyText = Buffer.concat(chunks).toString('utf8');
                    const json = JSON.parse(bodyText);

                    try {
                        const big10 = ethers.utils.bigNumberify(10);
                        const key_pairs = {
                            low: 'safeLow',
                            standard: 'average',
                            high: 'fast',
                            highest: 'fastest'
                        };
                        const result = {};
                        for (const key in key_pairs) {
                            // prices are in 1/10s of a gwei
                            result[key] = ethers.utils.parseUnits(json[key_pairs[key]].toString(), 'gwei').div(big10);
                        }
                        resolve(result);
                    } catch (err) {
                        reject(new Error(`invalid axiacodex gas price data ${err.message}`));
                    }
                } catch (err) {
                    reject(err);
                }
            });
        });

        req.on('error', (err) => {
            reject(err);
        });

        req.on('timeout', () => {
            req.abort();
            reject(new Error('timeout'));
        });
    });
}

module.exports.getAxiaGasPrice = getAxiaGasPrice;
module.exports.getEtherGasStationPrice = getEtherGasStationPrice;

// defined in the Yellow paper
module.exports.globalGasLimit = 21000;
