const ethers = require('ethers');
const commonLogic = require('../commonLogic');
const auth = require('../../../controllers/auth');
const logicRegistry = require('../../../daemon/logicRegistry');
const gas = require('./gas');

// route registration was once a method on the module.
// we realized this didn't quite make sense
// as routes are more permanent than the class instances.

// we still want to separate them from the main code,
// so its not cluttered with eth specific things,
// but they don't need to unload/load dynamically


function registerRoutes(router) {
    // requires admin token
    router.get('/eth-gas-price-fetch', function (req, res) {
        console.log(`/eth-gas-price-fetch`);
        auth.checkAdminToken(req.header('Authorization')).then(() => {
            const logic = logicRegistry.getLogic('ETH');
            return logic.getConfigByKey('eth_gas_price').then((gasPriceString) => {
                if (gasPriceString) {
                    const gasPrice = ethers.utils.bigNumberify(gasPriceString);
                    res.status(200).json({
                        gas_price_wei: gasPrice.toString(),
                        gas_price_gwei: ethers.utils.formatUnits(gasPrice, 'gwei')
                    });
                } else {
                    res.status(200).json({
                        gas_price_wei: null,
                        gas_price_gwei: null
                    });
                }
            });
        }).catch((err) => {
            console.error(err);
            const code = (err instanceof auth.AuthError) ? 401 : 500;
            res.status(code).json({
                error: err.message
            });
        });
    });

    // requires admin token
    router.post('/eth-gas-price-submit', function(req, res) {
        console.log(`/eth-gas-price-submit`);
        auth.checkAdminToken(req.header('Authorization')).then(() => {
            // custom route here for unit conversions
            let weiValue = req.body.gas_price_wei;

            if (!weiValue) {
                weiValue = ethers.utils.parseUnits(req.body.gas_price_gwei, 'gwei');
            }

            const logic = logicRegistry.getLogic('ETH');
            return logic.setConfigByKey('eth_gas_price', weiValue.toString()).then(() => {
                res.status(201).json({
                    response: 'success'
                });
            });
        }).catch((err) => {
            console.error(err);
            const code = (err instanceof auth.AuthError) ? 401 : 500;
            res.status(code).json({
                error: err.message
            });
        });
    });

    router.get('/eth-node-gas-price', function (req, res) {
        console.log(`/eth-node-gas-price`);
        auth.checkAdminToken(req.header('Authorization')).then(() => {
            const logic = logicRegistry.getLogic('ETH');
            let context;
            return logic.createContext((ctx) => {
                context = ctx;
                return context.provider.getGasPrice();
            }).then((gasPrice) => {
                context.shutdown();
                res.status(200).json({
                    gas_price_wei: gasPrice.toString(),
                    gas_price_gwei: ethers.utils.formatUnits(gasPrice, 'gwei').toString()
                });
            });
        }).catch((err) => {
            res.status(500).json({
                error: err.message
            });
        });
    });

    router.get('/eth-gas-price-levels', function (req, res) {
        console.log(`/eth-gas-price-levels`);
        auth.checkAdminToken(req.header('Authorization')).then(() => {

            return commonLogic.getAxiaCodexToken().then(axiaCodexToken => {
                if (axiaCodexToken) {
                    return gas.getAxiaGasPrice(axiaCodexToken);
                } else {
                    return gas.getEtherGasStationPrice();
                }
            }).then(gasPrices => {
                res.status(200).json({
                    gas_prices_wei: {
                        low: gasPrices.low.toString(),
                        average: gasPrices.standard.toString(),
                        high: gasPrices.high.toString()
                    },
                    gas_prices_gwei: {
                        low: ethers.utils.formatUnits(gasPrices.low, 'gwei').toString(),
                        average: ethers.utils.formatUnits(gasPrices.standard, 'gwei').toString(),
                        high: ethers.utils.formatUnits(gasPrices.high, 'gwei').toString()
                    }
                });
            });
        }).catch((err) => {
            console.error(err);
            const code = (err instanceof auth.AuthError) ? 401 : 500;
            res.status(code).json({
                error: err.message
            });
        });
    });
}

module.exports.registerRoutes = registerRoutes;
