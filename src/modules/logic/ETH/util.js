const ethers = require('ethers');
const urlLib = require('url');

// https://eips.ethereum.org/EIPS/eip-681
function qrCodeText(address, amount) {
    // do some length checking here
    // so they can't just feed arbitrary data
    if (address.length !== 42) {
        return null;
    }

    const urlObj = {
        protocol: 'ethereum',
        hostname: address,
        query: {}
    };

    if (amount !== undefined) {
        // the 10^18 is for wei to ETH
        // see the spec
        urlObj.query.value = amount + 'e18';
    }

    return urlLib.format(urlObj);
}


class EthFormatter {
    baseToNatural(wei) {
        return ethers.utils.formatEther(wei);
    }

    naturalToBase(eth) {
        eth = parseFloat(eth);
        return ethers.utils.parseEther(eth.toFixed(6)).toString();
    }
}

module.exports.qrCodeText = qrCodeText;
module.exports.EthFormatter = EthFormatter;
