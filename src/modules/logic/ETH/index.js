const ethers = require('ethers');
const commonLogic = require('../commonLogic');
const { Context }  = require('./context');
const util = require('./util');
const routes = require('./routes');
const { requestAxiaRate, requestCoinbaseRate } = require('../pricing');
const db = require('../../../controllers/db');
const crypto = require('crypto');
const gas = require('./gas');

class ETH {
    // constructor properties should be fundamentl
    // things the module needs to function.
    // database id, table, coin, etc.

    // config properties should be loaded as needed
    // to ensure they stay up to date.
    constructor(name, dbTable) {
        this.name = name;
        this.dbTable = dbTable;
    }

    getName() {
        return this.name;
    }

    getConfigByKey(key) {
        return commonLogic.getConfigByKey(this.dbTable, key);
    }

    setConfigByKey(key, value) {
        return commonLogic.setConfigByKey(this.dbTable, key, value);
    }

    createContext() {
        return new Context(this.name, this.dbTable).init();
    }

    _getActiveOrders() {
        return commonLogic.getActiveOrders(this.name).then(orders => {
            orders.forEach(row => {
                row.order_total = ethers.utils.bigNumberify(row.order_total);
                row.transferTx = row.transfer_tx;
                delete row.transfer_tx;
                row.transferBlock = row.transfer_block;
                delete row.transfer_block;
            });
            return orders;
        });
    }

    getOrdersToTransfer() {
        return commonLogic.getOrdersToTransfer(this.name);
    }

    prepareActiveOrders(context) {
        return Promise.all([
            context.getBlockHeight(),
            this._getActiveOrders()
        ]).then(([blockHeight, orders]) => {
            return Promise.all(orders.map(order => {
                return this._updateBalance(context, order, blockHeight).catch((err) => {
                    // catch individual errors
                    // so one doesn't break the whole batch
                    console.error(`failed to update balance for ${order.id}`, err);
                });
            }));
        });
    }

    confirmActiveOrders(context) {
        return Promise.all([
            context.getBlockHeight(),
            this._getActiveOrders()
        ]).then(([blockHeight, orders]) => {
            return Promise.all(orders.map(order => {
                return this._updateConfirmation(context, order, blockHeight).catch((err) => {
                    // catch individual errors
                    // so one doesn't break the whole batch
                    console.error(`failed to update confirm ${order.id}`, err);
                });
            }));
        });
    }

    makeTransfers(context) {
        return this.getOrdersToTransfer().then(orders => {
            return Promise.all(orders.map(order => {
                return this._transferToTreasury(context, order).catch((err) => {
                    // catch individual errors
                    // so one doesn't break the whole batch
                    console.error(`failed to transfer for ${order.id}`, err);
                });
            }));
        });
    }

    registerRoutes(router) {
        return routes.registerRoutes(router);
    }

    // Accepts an order id and total, generates keys, and stores the order in DB
    // order total is a string in ether
    createOrder({commerceId, orderTotal, commerce, currency, convRate}) {
        orderTotal = ethers.utils.bigNumberify(orderTotal);
        // I don't trust their generation
        const entropy = crypto.randomBytes(32);
        const newWallet = ethers.Wallet.createRandom({
            extraEntropy: entropy
        });

        const pub_key = newWallet.address;
        const priv_key = newWallet.privateKey;

        const sql = {
            text: `INSERT INTO orders(order_currency, commerce_id, order_total, commerce,
            currency, currency_conv_rate, pub_key, priv_key) VALUES($1, $2, $3, $4, $5, $6, $7, $8)`,
            values: [this.name, commerceId, orderTotal.toString(), commerce, currency, convRate, pub_key, priv_key]
        };
        const res = {
            address: pub_key
        };

        return db.dbQuery(sql).then(() => {
            return res;
        });
    }

    requestExchangeRate(fiatCurrency, riskLevel, timeToSell) {
        return commonLogic.getAxiaCodexToken().then((axiaCodexToken) => {
            if (axiaCodexToken) {
                return requestAxiaRate(axiaCodexToken, this.name, fiatCurrency, riskLevel, timeToSell);
            } else {
                return requestCoinbaseRate(this.name, fiatCurrency, riskLevel, timeToSell);
            }
        });
    }

    qrCodeText(...args) {
        return util.qrCodeText(...args);
    }

    createCurrencyFormatter() {
        return Promise.resolve(new util.EthFormatter());
    }

    _updateBalance(context, order, blockHeight) {
        const { minimum_confirmations } = context.config;
        // get the balance from a few blocks back
        // to ensure it has been confirmed
        // https://ethereum.stackexchange.com/questions/319/what-number-of-confirmations-is-considered-secure-in-ethereum
        const blockToCheck = blockHeight - minimum_confirmations;
        return Promise.all([
            getPreviousBalance(order.id),
            context.provider.getBalance(order.pub_key, blockToCheck)
        ]).then(([previousBalance, currentBalance]) => {
            let currentBal = Promise.resolve(currentBalance);
            if (previousBalance === null || !previousBalance.eq(currentBalance)) {
                // if the balance changed, add a payment before continuing
                currentBal = commonLogic.addPayment(order.id, currentBalance.toString(), blockHeight).then(() => currentBalance);
            }
            return currentBal;
        }).then(currentBalance => {
            if (order.status === 'created' && currentBalance.gte(order.order_total)) {
                // if the order is not cancelled and
                // the balance is equal or greater than the total order amount, status becomes 'paid'
                const sql = {
                    text: `UPDATE orders SET status = 'paid' WHERE id = $1`,
                    values: [order.id]
                };
                return db.dbQuery(sql);
            }
        });
    }

    _updateConfirmation(context, order, blockHeight) {
        if (!order.transferTx) {
            return Promise.resolve();
        }

        // https://ethereum.stackexchange.com/questions/19389/chain-reorganization-tx-hash-change
        // How do we detect a reorg?
        // "If a reorganisation occurs, transactions in reverted blocks will cease to exist.
        //  Transactions in the remainder of the chain will remain."

        const { minimum_confirmations } = context.config;
        return context.provider.getTransaction(order.transferTx).then(tx => {
            // blockHash and blockNumber may be null
            // while the transaction is pending
            // but it would be bad to have a blockHash
            // and no block number
            if (!tx ||
                (tx.blockHash !== null && tx.blockNumber === null)) {

                // it looks like there is no transaction
                // either this is becasue of a reorg,
                // or we are requesting it too soon
                const blocksBeforeCancelling = 5;
                if (blockHeight - order.transferBlock >= blocksBeforeCancelling) {
                    console.log(`transaction ${order.transfer_id} failed to confirm for order ${order.id}`);
                    // the chain got "reorged"
                    // and our transaction is no longer valid
                    // so clear the transaction and try again
                    const sql = {
                        text: `UPDATE transfers SET valid = false WHERE id = $1`,
                        values: [order.transfer_id]
                    };

                    return db.dbQuery(sql);
                } else {
                    // we can't confirm it
                    // but its too early to cancel the transaction
                }
            } else {
                const confirmations = blockHeight - tx.blockNumber;
                if (confirmations >= minimum_confirmations) {
                    console.log(`confirming transaction ${order.transfer_id} for order ${order.id}`);
                    // update confirmations
                    const sql = {
                        text: `UPDATE transfers SET date_confirmed = CURRENT_TIMESTAMP WHERE id = $1`,
                        values: [order.transfer_id]
                    };
                    return db.dbQuery(sql);
                }
            }
        });
    }

    // Once status is 'paid', ETH is transferred from the
    // individual order acccount to the treasury account.
    _transferToTreasury(context, {pub_key, priv_key, id: order_id}) {
        const { treasury: treasury_address } = context.config;

        return Promise.all([
            getPreviousBalance(order_id),
            context.provider.getBlockNumber()
        ]).then(([balance, blockNumber]) => {
            const gasLimit = ethers.utils.bigNumberify(gas.globalGasLimit);
            let sendValue = balance.sub(gasLimit.mul(context.gasPrice));

            let transferPromise;
            const zero = ethers.utils.bigNumberify(0);
            if (sendValue.lt(zero)) {
                // too little to transfer
                // call it good
                sendValue = zero;
                transferPromise = Promise.resolve();
            } else {
                // properties here accept bigNumbers
                const rawTransaction = {
                    // from is populated from private key
                    'gasPrice': context.gasPrice,
                    'gasLimit': gasLimit,
                    'to': treasury_address,
                    'value': sendValue
                };

                const wallet = new ethers.Wallet(priv_key, context.provider);
                transferPromise = wallet.sendTransaction(rawTransaction);
            }
            console.log('made transferPromise');

            // resolve this here so we capture all the variables
            // we made above
            return transferPromise.then((receipt) => {
                console.log('passed promise');
                if (receipt) {
                    // order transfer information
                    console.log(`transfer for ${order_id} transaction ${receipt.hash}`);

                    const sql = {
                        text: `INSERT INTO transfers(order_id, amount, transfer_type, tx, block, from_address, to_address)
                            VALUES($1, $2, 'payout', $3, $4, $5, $6)`,
                        values: [order_id, sendValue.toString(), receipt.hash, blockNumber, pub_key, treasury_address]
                    };

                    return db.dbQuery(sql);
                } else {
                    // this case is ok
                    // just insert a null tx into the database
                    // and mark it as confirmed
                    console.log(`order ${order_id} did not have enough ${balance.toString()} to cover gas price`);

                    const sql = {
                        text: `INSERT INTO transfers(order_id, amount, transfer_type, from_address, to_address, date_confirmed)
                            VALUES($1, $2, 'payout', '', '', CURRENT_TIMESTAMP)`,
                        values: [order_id, sendValue.toString()]
                    };

                    return db.dbQuery(sql);
                }
            });
        });
    }
}

function getPreviousBalance(order_id) {
    return commonLogic.getPreviousBalance(order_id).then(x => {
        if (x !== null){
            return ethers.utils.bigNumberify(x);
        }
        return x;
    });
}

module.exports.ETH = ETH;
