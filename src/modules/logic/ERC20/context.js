//const ethers = require('ethers');
const db = require('../../../controllers/db');

class Context {
    constructor(name, dbTable, ethContext) {
        this.name = name;
        this.dbTable = dbTable;
        this.ethContext = ethContext;
        this.config = null;
        this.ethConfig = null;
    }

    init() {
        return this._prepareConfig().then(config => {
            if (!config ||
                !config.treasury ||
                !config.contract_address ||
                !config.reserve_priv_key) {
                return null;
            } else {
                this.config = config;
                return this;
            }
        });
    }

    shutdown() {
        this.ethContext.shutdown();
        delete this.ethContext;
    }

    _prepareConfig() {
        const sql = {
            text: `SELECT treasury, contract_address, contract_decimals, reserve_priv_key, erc20_gas_budget
                   FROM ${this.dbTable} WHERE contract_symbol = $1`,
            values: [this.name]
        };
        return db.dbQuery(sql).then(res => {
            if (res.rows.length > 0) {
                return res.rows[0];
            } else {
                throw new Error(`Could not get treasury, gas limit, 
                    or gas price from config for Ethereum treasury transfer`);
            }
        });
    }

    getProvider() {
        return this.ethContext.provider;
    }

    getBlockHeight() {
        return this.ethContext.getBlockHeight();
    }

    getGasPrice() {
        return this.ethContext.getGasPrice();
    }
}

module.exports.Context = Context;
