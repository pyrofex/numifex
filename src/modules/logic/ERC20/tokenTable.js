// a table of common ERC20 tokens
module.exports = {
    '0xB8c77482e45F1F44dE1745F52C74426C631bDD52': {
        name: 'Binance Coin',
        symbol: 'BNB',
        gasBudget: 52578
    },
    // this one is weird because its a proxy contract.
    // It forwards calls to the contract here:
    // 0x0882477e7895bdC5cea7cB1552ed914aB157Fe56
    // I still think the address below is the correct
    // address to interface with.
    // see more:
    //  https://ylv.io/circle-usdc-technical-overview/
    //  https://www.coinbase.com/usdc
    '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48': {
        name: 'USD Coin',
        symbol: 'USDC',
        gasBudget: 54669
    },

    '0x0D8775F648430679A709E98d2b0Cb6250d2887EF': {
        name: 'Basic Attention Token',
        symbol: 'BAT',
        gasBudget: 51788
    },

    '0x514910771AF9Ca656af840dff83E8264EcF986CA': {
        name: 'ChainLink Token',
        symbol: 'LINK',
        gasBudget: 52065
    }
};
