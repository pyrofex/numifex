const ethers = require('ethers');

class TokenFormatter {
    constructor(decimals) {
        this.decimals = decimals;
    }

    naturalToBase(natural) {
        natural = parseFloat(natural);
        return ethers.utils.parseUnits(natural.toFixed(6), this.decimals).toString();
    }

    baseToNatural(base) {
        return ethers.utils.formatUnits(base, this.decimals);
    }
}

module.exports.TokenFormatter = TokenFormatter;