const ethers = require('ethers');
const commonLogic = require('../commonLogic');
const { Context } = require('./context');
const { requestAxiaRate, requestCoinbaseRate } = require('../pricing');
const db = require('../../../controllers/db');
const crypto = require('crypto');
const { globalGasLimit } = require('../ETH/gas');
const abi = require('./abi');
const util = require('./util');

class ERC20 {
    constructor(name, dbTable, ethModule) {
        this.name = name;
        this.dbTable = dbTable;
        this.ethModule = ethModule;
    }

    getName() {
        return this.name;
    }

    getConfigByKey(key) {
        return commonLogic.getConfigByKey(this.dbTable, key);
    }

    setConfigByKey(key, value) {
        return commonLogic.setConfigByKey(this.dbTable, key, value);
    }

    createContext() {
        return this.ethModule.createContext().then((ethCtx) => {
            if (!ethCtx) {
                return null;
            } else {
                return new Context(this.name, this.dbTable, ethCtx).init();
            }
        });
    }

    _getActiveOrders() {
        return commonLogic.getActiveOrders(this.name).then(orders => {
            orders.forEach(row => {
                row.order_total = ethers.utils.bigNumberify(row.order_total);
                row.transferTx = row.transfer_tx;
                delete row.transfer_tx;
                row.transferBlock = row.transfer_block;
                delete row.transfer_block;
            });
            return orders;
        });
    }

    _getOrdersToFund() {
        const sql = {
            text: `SELECT o.id, o.pub_key, t.id AS transfer_id, t.tx AS transfer_tx, t.block AS transfer_block FROM orders o
                   LEFT JOIN transfers t ON t.order_id = o.id AND t.valid = true AND t.transfer_type = 'funding'
                   WHERE (o.status = 'paid' OR o.status = 'cancelling') AND t.date_confirmed IS NULL AND o.order_currency = $1`,
            values: [this.name]
        };

        return db.dbQuery(sql).then(res => res.rows);
    }

    _getOrdersToTransfer() {
        const sql = {
            text: `SELECT id, pub_key, priv_key FROM orders
                   WHERE (status = 'funded' OR status = 'cancelling') AND order_currency = $1
                   AND EXISTS(SELECT 1 FROM transfers WHERE order_id = orders.id AND valid = true AND transfer_type = 'funding' AND date_confirmed IS NOT NULL)
                   AND NOT EXISTS(SELECT 1 FROM transfers WHERE order_id = orders.id AND valid = true AND transfer_type = 'payout')`,
            values: [this.name]
        };

        return db.dbQuery(sql).then(res => res.rows);
    }

    prepareActiveOrders(context) {
        console.log('PREPARE ACTIVE ORDERS');
        return Promise.all([
            context.getBlockHeight(),
            this._getActiveOrders()
        ]).then(([blockHeight, orders]) => {
            return Promise.all(orders.map(order => {
                return this._recordBalancePromise(context, order, blockHeight).catch((err) => {
                    // catch individual errors
                    // so one doesn't break the whole batch
                    console.log(`failed to update balance for ${order.id}`, err);
                });
            }));
        });
    }

    confirmActiveOrders(context) {
        return Promise.all([
            context.getBlockHeight(),
            this._getActiveOrders()
        ]).then(([blockHeight, orders]) => {
            return Promise.all(orders.map(order => {
                return this._updateConfirmation(context, order, blockHeight).catch((err) => {
                    // catch individual errors
                    // so one doesn't break the whole batch
                    console.log(`failed to update confirm ${order.id}`, err);
                });
            }));
        });
    }

    makeTransfers(context) {
        return Promise.all([
            context.getBlockHeight(),
            this._getOrdersToFund()
        ]).then(([blockHeight, orders]) => {
            return Promise.all(orders.map(order => {
                return this._attemptToFundAccount(context, order, blockHeight).catch(err => {
                    // catch individual errors
                    // so one doesn't break the whole batch
                    console.log(`failed to fund contract for ${order.id}`, err);
                });
            }));
        }).then(() => {
            return Promise.all([
                context.getBlockHeight(),
                commonLogic.getOrdersToTransfer(this.name)
            ]).then(([blockHeight, orders]) => {
                return Promise.all(orders.map(order => {
                    return this._transferToTreasury(context, order, blockHeight).catch((err) => {
                        // catch individual errors
                        // so one doesn't break the whole batch
                        console.log(`failed to transfer for ${order.id}`, err);
                    });
                }));
            });
        });
    }

    // Accepts an order id and total, generates keys, and stores the order in DB
    // order total is a string in ether
    createOrder({commerceId, orderTotal, commerce, currency, convRate}) {
        //orderTotal = ethers.utils.bigNumberify(orderTotal);
        // I don't trust their generation
        const entropy = crypto.randomBytes(32);
        const newWallet = ethers.Wallet.createRandom({
            extraEntropy: entropy
        });
        console.log('CREATING ORDER, orderTotal: ' + orderTotal);

        const pub_key = newWallet.address;
        const priv_key = newWallet.privateKey;

        const sql = {
            text: `INSERT INTO orders(order_currency, commerce_id, order_total, commerce,
                   currency, currency_conv_rate, pub_key, priv_key)
                   VALUES('BAT', $1, $2, $3, $4, $5, $6, $7)`,
            values: [commerceId, orderTotal, commerce, currency, convRate, pub_key, priv_key]
        };

        return db.dbQuery(sql).then(() => {
            return { address: pub_key };
        });
    }

    requestExchangeRate(fiatCurrency, riskLevel, timeToSell) {
        return commonLogic.getAxiaCodexToken().then((axiaCodexToken) => {
            if (axiaCodexToken) {
                return requestAxiaRate(axiaCodexToken, this.name, fiatCurrency, riskLevel, timeToSell);
            } else {
                return requestCoinbaseRate(this.name, fiatCurrency, riskLevel, timeToSell);
            }
        });
    }

    registerRoutes(router) {
        // no routes for ERC20
    }

    qrCodeText(address, amount) {
        return address;
    }

    createCurrencyFormatter() {
        return this.getConfigByKey('contract_decimals').then((decimals) => {
            return new util.TokenFormatter(decimals);
        });
    }

    _recordBalancePromise(context, order, blockHeight) {
        const { contract_address } = context.config;
        const { minimum_confirmations } = context.ethContext.config;
        // get the balance from a few blocks back
        // to ensure it has been confirmed
        // https://ethereum.stackexchange.com/questions/319/what-number-of-confirmations-is-considered-secure-in-ethereum
        const blockToCheck = blockHeight - minimum_confirmations;
        const contract = new ethers.Contract(contract_address, abi.abi, context.getProvider());
        return Promise.all([
            getPreviousBalance(order.id),
            contract.balanceOf(order.pub_key, { blockTag: blockToCheck })
        ]).then(([previousBalance, currentBalance]) => {
            let currentBal = Promise.resolve(currentBalance);
            if (previousBalance === null || !previousBalance.eq(currentBalance)) {
                // if the balance changed, add a payment before continuing
                currentBal = commonLogic.addPayment(order.id, currentBalance.toString(),
                    blockHeight).then(() => currentBalance);
            }
            return currentBal;
        }).then(currentBalance => {
            if (order.status === 'created' && currentBalance.gte(order.order_total)) {
                // if the order is not cancelled and
                // the balance is equal or greater than the total order amount, status becomes 'paid'
                const sql = {
                    text: `UPDATE orders SET status = 'paid' WHERE id = $1`,
                    values: [order.id]
                };
                return db.dbQuery(sql);
            }
        });
    }

    _attemptToFundAccount(context, order, blockHeight) {
        const { minimum_confirmations } = context.ethContext.config;
        const { id, transfer_id, transfer_tx, transfer_block } = order;

        return getPreviousBalance(id).then(balance => {
            if (!balance.eq(0)) {
                if (!transfer_id) {
                    return this._fundAccount(context, order, blockHeight);
                } else {
                    return this.provider.getTransaction(transfer_tx).then(tx => {
                        if (!tx || (tx.blockHash !== null && tx.blockNumber === null)) {
                            const blocksBeforeCancelling = 5;
                            if (blockHeight - transfer_block >= blocksBeforeCancelling) {
                                console.log(`funding transaction ${transfer_id} failed to confirm for order ${id}`);
                                // the chain got "reorged"
                                // and our funding transaction is no longer valid
                                // so clear the transaction and try again
                                return db.dbQuery({
                                    text: `UPDATE transfers SET valid = false WHERE id = $1`,
                                    values: [transfer_id]
                                });
                            } // else do nothing and wait for more blocks to go by
                        } else {
                            const confirmations = blockHeight - tx.blockNumber;
                            if (confirmations >= minimum_confirmations) {
                                console.log(`confirming funding transaction ${transfer_id} for order ${id}`);
                                // update confirmations
                                return db.dbQuery({
                                    text: `UPDATE transfers SET date_confirmed = CURRENT_TIMESTAMP WHERE id = $1`,
                                    values: [transfer_id]
                                });
                            }
                        }
                    });
                }
            } else {
                return db.dbQuery({
                    text: `INSERT INTO transfers(
                           order_id, amount, transfer_type, from_address, to_address, date_confirmed)
                           VALUES($1, '0', 'funding', '', '', CURRENT_TIMESTAMP)`,
                    values: [id]
                });
            }
        });
    }

    _fundAccount(context, order, blockHeight) {
        const { id, pub_key } = order;
        const { reserve_priv_key, erc20_gas_budget } = context.config;
        const fundAmount = ethers.utils.formatUnits(context.getGasPrice(), 'gwei') * Number(erc20_gas_budget);
        const formattedAmount = ethers.utils.parseUnits(String(fundAmount), 'gwei');
        const rawTransaction = {
            'gasPrice': context.getGasPrice(),
            'gasLimit': globalGasLimit,
            'to': pub_key,
            'value': formattedAmount
        };

        const wallet = new ethers.Wallet(reserve_priv_key, context.getProvider());
        return wallet.sendTransaction(rawTransaction).then((receipt) => {
            const sql = {
                text: `INSERT INTO transfers(order_id, from_address, to_address, amount, transfer_type, tx, block)
                       VALUES($1, $2, $3, $4, 'funding', $5, $6)`,
                values: [id, wallet.getAddress(), pub_key, String(fundAmount), receipt.hash, blockHeight]
            };

            return db.dbQuery(sql);
        });
    }

    _updateConfirmation(context, order, blockHeight) {
        if (!order.transferTx) {
            return Promise.resolve();
        }

        // https://ethereum.stackexchange.com/questions/19389/chain-reorganization-tx-hash-change
        // How do we detect a reorg?
        // "If a reorganisation occurs, transactions in reverted blocks will cease to exist.
        //  Transactions in the remainder of the chain will remain."

        const { minimum_confirmations } = context.ethContext.config;
        return context.getProvider().getTransaction(order.transferTx).then(tx => {
            // blockHash and blockNumber may be null
            // while the transaction is pending
            // but it would be bad to have a blockHash
            // and no block number
            if (!tx || (tx.blockHash !== null && tx.blockNumber === null)) {
                // it looks like there is no transaction
                // either this is becasue of a reorg,
                // or we are requesting it too soon
                const blocksBeforeCancelling = 5;
                if (blockHeight - order.transferBlock >= blocksBeforeCancelling) {
                    console.log(`transaction ${order.transfer_id} failed to confirm for order ${order.id}`);
                    // the chain got "reorged"
                    // and our transaction is no longer valid
                    // so clear the transaction and try again
                    const sql = {
                        text: `UPDATE transfers SET valid = false WHERE id = $1`,
                        values: [order.transfer_id]
                    };

                    return db.dbQuery(sql);
                } else {
                    // we can't confirm it
                    // but its too early to cancel the transaction
                }
            } else {
                const confirmations = blockHeight - tx.blockNumber;
                console.log(confirmations);
                if (confirmations >= minimum_confirmations) {
                    console.log(`confirming transaction ${order.transfer_id} for order ${order.id}`);
                    // update confirmations
                    const sql = {
                        text: `UPDATE transfers SET date_confirmed = CURRENT_TIMESTAMP WHERE id = $1`,
                        values: [order.transfer_id]
                    };
                    return db.dbQuery(sql);
                }
            }
        });
    }

    // Once status is 'paid', BAT is transferred from the
    // individual order acccount to the treasury account.
    _transferToTreasury(context, order, blockHeight) {
        const { treasury, erc20_gas_budget, contract_address } = context.config;
        const { priv_key, pub_key, id } = order;

        return Promise.all([
            getPreviousBalance(id),
            context.getProvider().getBalance(pub_key)
        ]).then(([prevBalance, weiBalance]) => {
            const wallet = new ethers.Wallet(priv_key, context.getProvider());
            const contract = new ethers.Contract(contract_address, abi.abi, context.getProvider());
            const contractWithSigner = contract.connect(wallet);

            const budget = ethers.utils.bigNumberify(erc20_gas_budget);
            const options = {
                gasLimit: budget,
                gasPrice: weiBalance.div(budget)
            };
            return contractWithSigner.transfer(treasury, prevBalance, options).then((receipt) => {
                if (receipt) {
                    // order transfer information
                    console.log(`transfer for ${id} transaction ${receipt.hash}`);

                    const sql = {
                        text: `INSERT INTO transfers(
                               order_id, amount, tx, transfer_type, block, from_address, to_address)
                               VALUES($1, $2, $3, 'payout', $4, $5, $6)`,
                        values: [id, prevBalance.toString(), receipt.hash, blockHeight, pub_key, treasury]
                    };

                    return db.dbQuery(sql);
                } else {
                    // this case is ok
                    // just insert a null tx into the database
                    // and mark it as confirmed
                    console.log(`order ${id} did not have enough ${weiBalance.toString()} to cover gas price`);

                    const sql = {
                        text: `INSERT INTO transfers(
                               order_id, amount, transfer_type, from_address, to_address, date_confirmed)
                               VALUES($1, $2, 'payout', '', '', CURRENT_TIMESTAMP)`,
                        values: [id, prevBalance.toString()]
                    };

                    return db.dbQuery(sql);
                }
            });
        });
    }
}

function getPreviousBalance(order_id) {
    return commonLogic.getPreviousBalance(order_id).then(x => {
        if (x !== null){
            return ethers.utils.bigNumberify(x);
        }
        return x;
    });
}

module.exports.getSymbols = function(dbTable) {
    return db.dbQuery(`SELECT DISTINCT contract_symbol FROM ${dbTable}`).then(res => res.rows.map(row => row.contract_symbol));
};

module.exports.ERC20 = ERC20;
