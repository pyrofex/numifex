const common = require('./common');
const https = require('https');
const http = require('http');
const url = require('url');
const db = require('../../controllers/db');

// Sends a POST to Magento, changing the order status to 'processing'.
// order_id is retrieved from the DB during the Cronjob cycle.
// This function cannot be called manually and shouldn't be a security risk.
function setMagentoStatus(woo_order_id, increment_id, status, magentoConfig) {
    if (!magentoConfig.magento_url ||
        !magentoConfig.magento_token) {
        throw new Error('Magento config is not setup');
    }

    const data = JSON.stringify({
        entity: {
            entity_id: woo_order_id,
            status: status,
            increment_id: increment_id
        }
    });

    const magentoUrl = new url.URL(magentoConfig.magento_url);
    const options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + magentoConfig.magento_token,
        },
        hostname: magentoUrl.hostname,
        path: '/rest/V1/orders',
        method: 'POST',
        rejectUnauthorized: false,
        timeout: 5000
    };

    if (magentoUrl.port && magentoUrl.port.length > 0) {
        options.port = parseInt(magentoUrl.port);
    }

    return new Promise((resolve, reject) => {
        let httpOrHttps = http;
        // formatting sometimes has a : on the end
        if (magentoUrl.protocol === 'https:' ||
            magentoUrl.protocol === 'https') {
            httpOrHttps = https;
        }
        const req = httpOrHttps.request(options, (res) => {
            console.log(`STATUS: ${res.statusCode}`);
            if (res.statusCode >= 300) {
                reject(new Error('Received bad status code'));
                return;
            }

            res.setEncoding('utf8');
            var response;
            res.on('data', (chunk) => {
                response = JSON.parse(chunk);
            });
            res.on('end', () => {
                resolve(response);
            });
        });

        req.on('error', (e) => {
            console.error(e);
            reject(e);
        });
        req.on('timeout', () => {
            req.abort();
            reject(new Error('timeout'));
        });

        req.end(data);
    });
}

class MagentoCommerce {
    constructor() {
        this.name = 'magento';
    }

    getConfigByKey(key) {
        return common.getConfigByKey(db, 'magento_config', key);
    }

    setConfigByKey(key, value) {
        return common.setConfigByKey(db, 'magento_config', key, value);
    }

    getConfig() {
        return common.getConfig(db, 'magento_config');
    }

    makeNotification(order, config) {
        let magentoStatus;
        if (order.status === 'cancelling') {
            magentoStatus = 'cancelled';
        } else if (order.status === 'paid') {
            magentoStatus = 'processing';
        }

        // commerce id contains both increment and entity id
        const parts = order.commerce_id.split('_');
        const entityId = parts[0];
        const incrementId = parts[1];

        return setMagentoStatus(entityId, incrementId,
            magentoStatus, config).then(response => {
            console.log(`Notified Magento: ${order.id} 
                COMMERCE ID: ${order.commerce_id}  RESPONSE ${response.id} 
                STATUS: ${response.status}`);
        });
    }
}

module.exports.MagentoCommerce = MagentoCommerce;
