const common = require('./common');
const db = require('../../controllers/db');
const { URL } = require('url');
const http = require('http');
const https = require('https');

function setStatus(url, commerceId, status) {
    const data = JSON.stringify({
        id: commerceId,
        status: status
    });

    const integrationUrl = new URL(url);
    const options = {
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': data.length
        },
        method: 'POST',
        rejectUnauthorized: false,
        timeout: 5000
    };

    return new Promise((resolve, reject) => {
        let httpOrHttps = http;
        // formatting sometimes has a : on the end
        if (integrationUrl.protocol === 'https:' ||
            integrationUrl.protocol === 'https') {
            httpOrHttps = https;
        }
        const req = httpOrHttps.request(integrationUrl, options, (res) => {
            console.log(`STATUS: ${res.statusCode}`);
            if (res.statusCode >= 300) {
                reject(new Error('Received bad status code'));
                return;
            }

            const chunks = [];
            res.on('data', (chunk) => {
                chunks.push(chunk);
            });
            res.on('end', () => {
                try {
                    const bodyText = Buffer.concat(chunks).toString('utf8');
                    const response = JSON.parse(bodyText);
                    resolve(response);
                } catch(e) {
                    reject(e);
                }
            });
        });

        req.on('error', (e) => {
            console.error(e);
            reject(e);
        });
        req.on('timeout', () => {
            req.abort();
            reject(new Error('timeout'));
        });

        req.end(data);
    });
}

class CustomCommerce {
    constructor() {
        this.name = 'custom';
    }

    getConfigByKey(key) {
        return common.getConfigByKey(db, 'custom_integration_config', key);
    }

    setConfigByKey(key, value) {
        return common.setConfigByKey(db, 'custom_integration_config', key, value);
    }

    getConfig() {
        return common.getConfig(db, 'custom_integration_config');
    }

    makeNotification(order, config) {
        const { status, commerce_id } = order;
        const { url } = config;

        if (!url) {
            throw new Error('Custom integration config is not setup');
        }

        const statusToSend = {
            'paid': 'paid',
            'cancelling': 'canceled'
        }[status];

        return setStatus(url, commerce_id, statusToSend);
    }
}

module.exports.CustomCommerce = CustomCommerce;
