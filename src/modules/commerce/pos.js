const db = require('../../controllers/db');
const common = require('./common');

// numifex point of sale
class PosCommerce {
    constructor() {
        this.name = 'pos';
    }

    getConfigByKey(key) {
        return common.getConfigByKey(db, 'pos_config', key);
    }

    setConfigByKey(key, value) {
        return common.setConfigByKey(db, 'pos_config', key, value);
    }

    getConfig() {
        return common.getConfig(db, 'pos_config');
    }

    makeNotification(order, config) {
        // nothing to update
        return Promise.resolve();
    }
}

module.exports.PosCommerce = PosCommerce;