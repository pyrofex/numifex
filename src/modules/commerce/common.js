

function getConfig(db, table) {
    const sql = `SELECT * FROM ${table};`;
    return db.dbQuery(sql).then(res => {
        if (res.rows.length > 0) {
            return res.rows[0];
        } else {
            return undefined;
        }
    });

}

function getConfigByKey(db, table, key) {
    // we do individual keys thie way to ensure
    // the key exists beforehand
    return getConfig(db, table).then((res) => {
        if (res) {
            return res[key];
        }
    });
}

function setConfigByKey(db, table, key, value) {
    const sql = `SELECT * FROM ${table};`;

    return db.dbQuery(sql).then(res => {
        // check if the key is in any existing fields
        const field = res.fields.find((field) => field.name === key);

        if (field) {
            const sql = {
                text: `UPDATE ${table} SET ${field.name} = $1`, values: [value]};
            return db.dbQuery(sql);
        } else {
            throw new Error('attempted to set key which is not in config');
        }
    });
}

module.exports.getConfig = getConfig;
module.exports.getConfigByKey = getConfigByKey;
module.exports.setConfigByKey = setConfigByKey;
