const https = require('https');
const http = require('http');
const url = require('url');
const { Buffer } = require('buffer');
const db = require('../../controllers/db');
const common = require('./common');

function wooAuthToken(wooCommerceConfig) {
    // TODO: SOURCE LINK?
    const text = wooCommerceConfig.wordpress_user + ':' + wooCommerceConfig.wordpress_pass;
    return Buffer.from(text, 'utf8').toString('base64');
}

// Sends a POST to Woocommerce, changing the order status to 'processing'.
// order_id is retrieved from the DB during the Cronjob cycle.
// This function cannot be called manually and shouldn't be a security risk.
function setWooStatus(woo_order_id, status, wooCommerceConfig) {
    if (!wooCommerceConfig.wordpress_url ||
        !wooCommerceConfig.wordpress_user ||
        !wooCommerceConfig.wordpress_pass) {
        throw new Error('WooCommerce config is not setup');
    }

    const data = JSON.stringify({
        status: status
    });

    const wordpressUrl = new url.URL(wooCommerceConfig.wordpress_url);
    const options = {
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': data.length,
            'Authorization': 'Basic ' + wooAuthToken(wooCommerceConfig),
        },
        hostname: wordpressUrl.hostname,
        path: '/wp-json/wc/v2/orders/' + String(woo_order_id),
        method: 'POST',
        rejectUnauthorized: false,
        timeout: 5000
    };

    if (wordpressUrl.port && wordpressUrl.port.length > 0) {
        options.port = parseInt(wordpressUrl.port);
    }

    return new Promise((resolve, reject) => {
        let httpOrHttps = http;
        // formatting sometimes has a : on the end
        if (wordpressUrl.protocol === 'https:' ||
            wordpressUrl.protocol === 'https') {
            httpOrHttps = https;
        }
        const req = httpOrHttps.request(options, (res) => {
            console.log(`STATUS: ${res.statusCode}`);
            if (res.statusCode >= 300) {
                reject(new Error('Received bad status code'));
                return;
            }

            const chunks = [];
            res.on('data', (chunk) => {
                chunks.push(chunk);
            });
            res.on('end', () => {
                try {
                    const bodyText = Buffer.concat(chunks).toString('utf8');
                    const response = JSON.parse(bodyText);
                    resolve(response);
                } catch(e) {
                    reject(e);
                }
            });
        });

        req.on('error', (e) => {
            console.error(e);
            reject(e);
        });
        req.on('timeout', () => {
            req.abort();
            reject(new Error('timeout'));
        });

        req.end(data);
    });
}

class WooCommerce {
    constructor() {
        this.name = 'woocommerce';
    }

    getConfigByKey(key) {
        return common.getConfigByKey(db, 'woocommerce_config', key);
    }

    setConfigByKey(key, value) {
        return common.setConfigByKey(db, 'woocommerce_config', key, value);
    }

    getConfig() {
        return common.getConfig(db, 'woocommerce_config');
    }

    makeNotification(order, config) {
        let wooCommerceStatus;
        if (order.status === 'cancelling') {
            wooCommerceStatus = 'cancelled';
        } else if (order.status === 'paid') {
            wooCommerceStatus = 'processing';
        }

        return setWooStatus(order.commerce_id, wooCommerceStatus, config).then(response => {
            console.log(`Notified WooCommerce: ${order.id} COMMERCE ID: ${order.commerce_id}  RESPONSE ${response.id} STATUS: ${response.status}`);
        });
    }
}

module.exports.WooCommerce = WooCommerce;
