const db = require('./db');
const crypto = require('crypto');

class AuthError extends Error {

}

/*

CONCEPTS

admin: someone with full access to settings/configure.
- username
- password

admins authenticate with their username and password and obtain an access_token

api client: a commerce platform which has only limited access, such as submitting orders
- client_secret (like a password)

api clients authorize with client_secret and obtain an access token.

*/

// for passwords
// salt for added entropy
function createPasswordHash(password, salt) {
    const hash = crypto.createHash('sha256');
    hash.update(password, 'utf8');
    hash.update(salt, 'hex');
    return hash.digest().toString('base64');
}

// for api client secrets
function createSecretHash(secret) {
    const hash = crypto.createHash('sha256');
    hash.update(secret);
    return hash.digest('base64');
}

// for access tokens
// simple sha256 for speed
// no salt needed because these are
// high entropy already
function createTokenHash(secret) {
    return createSecretHash(secret);
}

// checks if the username and password match the admin
// if so, generates a token and fills it into the database
// returns a promise that resolves to the token
function authorizeAdmin(username, password) {
    return db.dbQuery({
        text: 'SELECT id, password_salt, password_hash FROM admin_clients WHERE username = $1',
        values: [username]
    }).then(result => {
        if (result.rows.length === 0) {
            throw new AuthError(`no admin with username: ${username}`);
        }
        const admin = result.rows[0];

        if (createPasswordHash(password, admin.password_salt) !== admin.password_hash) {
            throw new AuthError(`incorrect admin password.`);
        }

        const accessToken = crypto.randomBytes(48).toString('base64');
        const updateSql = {
            text: 'UPDATE admin_clients SET access_token_hash = $1, token_timestamp = CURRENT_TIMESTAMP WHERE id = $2',
            values: [createTokenHash(accessToken), admin.id]
        };
        return db.dbQuery(updateSql).then(() => {
            // gone after this return
            return accessToken;
        });
    });
}

// update the username and password of an admin
function updateAdminCredentials(oldUsername, oldPassword, newUsername, newPassword) {
    return db.dbQuery({
        text: 'SELECT id, password_hash, password_salt FROM admin_clients WHERE username = $1',
        values: [oldUsername]
    }).then(result => {
        if (result.rows.length === 0) {
            throw new AuthError(`no admin with username: ${oldUsername}`);
        }
        const admin = result.rows[0];
        if (createPasswordHash(oldPassword, admin.password_salt) !== admin.password_hash) {
            throw new AuthError(`incorrect admin password.`);
        }

        const newSalt = crypto.randomBytes(8).toString('hex'); // will always be 16 characters
        return db.dbQuery({
            text: 'UPDATE admin_clients SET username = $1, password_hash = $2, password_salt = $3, access_token = NULL WHERE id = $4',
            values: [newUsername, createPasswordHash(newPassword, newSalt), newSalt, admin.id]
        });
    });
}

// check if an admin token is valid
function checkAdminToken(fullToken) {
    if (!fullToken || fullToken.length <= 0) {
        return Promise.reject(new AuthError('requires an authorization.'));
    }
    const prefix = 'Bearer ';
    if (!fullToken.startsWith(prefix) &&
        !fullToken.startsWith(prefix.toLowerCase())) {
        return Promise.reject(new AuthError(`invalid admin token type. should be bearer`));
    }
    const token = fullToken.substring(prefix.length);

    return db.dbQuery({
        text: "SELECT id FROM admin_clients WHERE access_token_hash = $1 AND token_timestamp + INTERVAL '3 HOUR' > CURRENT_TIMESTAMP",
        values: [createTokenHash(token)]
    }).then(result => {
        if (result.rows.length === 0) {
            throw new AuthError(`invalid admin token`);
        }
    });
}

function authorizeClient(clientSecret) {
    return db.dbQuery({
        text: `SELECT id FROM api_clients WHERE client_secret_hash=$1`,
        values: [createSecretHash(clientSecret)]
    }).then (result => {
        if (result.rows.length === 0) {
            throw new AuthError('Invalid Client Secret');
        }
    }).then(() => {
        const accessToken = crypto.randomBytes(48).toString('base64');
        return db.dbQuery({
            text: 'UPDATE api_clients SET access_token_hash = $1, token_timestamp = CURRENT_TIMESTAMP WHERE client_secret_hash = $2',
            values: [createTokenHash(accessToken), createSecretHash(clientSecret)]
        }).then(() => {
            return accessToken;
        });
    });
}

function checkApiToken(fullToken) {
    if (!fullToken || fullToken.length <= 0) {
        return Promise.reject(new AuthError(`Invalid api token`));
    }
    const prefix = 'Bearer ';
    if (!fullToken.startsWith(prefix) &&
        !fullToken.startsWith(prefix.toLowerCase())) {
        return Promise.reject(new AuthError(`Invalid api token type. should be bearer`));
    }
    // strip "bearer"
    const token = fullToken.substring(prefix.length);
    const tokenHash = createTokenHash(token);

    return db.dbQuery({
        text: 'SELECT id FROM api_clients WHERE access_token_hash = $1',
        values: [tokenHash]
    }).then(result => {
        if (result.rows.length > 0) {
            // good token. pass
            return;
        }

        // UPGRADE PATH;
        // check if the token is using the old format
        return db.dbQuery({
            text: 'SELECT id FROM api_clients WHERE access_token = $1',
            values: [token]
        }).then(result => {
            if (result.rows.length <= 0) {
                // definitely a bad token
                throw new AuthError(`invalid api token`);
            }

            console.log('upgraded api token');
            // good token, but its the old format.
            // upgrade it to a hash. clear the old entry
            return db.dbQuery({
                text: 'UPDATE api_clients SET access_token_hash = $1, access_token = NULL WHERE access_token = $2',
                values: [tokenHash, token]
            });
        });
    });
}

function listApiClients() {
    return db.dbQuery({
        text: 'SELECT id, nickname, token_timestamp FROM api_clients ORDER BY id'
    });
}

function createApiClient(newSecret) {
    // can provide a secret manually
    if (!newSecret || newSecret.length <= 0) {
        newSecret = crypto.randomBytes(32).toString('base64');
    }

    const newSecretHash = createSecretHash(newSecret);
    return db.dbQuery({
        text: 'INSERT INTO api_clients(client_secret_hash) VALUES($1) RETURNING id',
        values: [newSecretHash]
    }).then((result) => {
        const id = result.rows[0].id;
        return {
            id: id,
            clientSecret: newSecret
        };
    });
}

function deleteApiClient(id) {
    return db.dbQuery({
        text: 'DELETE FROM api_clients WHERE id = $1',
        values: [id]
    });
}

function revokeApiClient(id) {
    return db.dbQuery({
        text: 'UPDATE api_clients SET access_token = NULL WHERE id = $1',
        values: [id]
    });
}

function nicknameApiClient(id, nickname) {
    return db.dbQuery({
        text: 'UPDATE api_clients SET nickname = $1 WHERE id = $2',
        values: [nickname, id]
    });
}

module.exports.authorizeAdmin = authorizeAdmin;
module.exports.updateAdminCredentials = updateAdminCredentials;
module.exports.checkAdminToken = checkAdminToken;
module.exports.authorizeClient = authorizeClient;
module.exports.checkApiToken = checkApiToken;
module.exports.listApiClients = listApiClients;
module.exports.createApiClient = createApiClient;
module.exports.deleteApiClient = deleteApiClient;
module.exports.revokeApiClient = revokeApiClient;
module.exports.nicknameApiClient = nicknameApiClient;
module.exports.AuthError = AuthError;