const config = require('../config');

const pg = require('pg');
const fs = require('fs');
const path = require('path');

// pg returns a string when querying for a numeric
// we want to leave the string as it is
// "null values are never parsed"
pg.types.setTypeParser(1700, (s) => s); // 1700 is the numeric type

//Open DB connection
let pool = new pg.Pool({
    user: config.databaseUser,
    host: config.databaseHost,
    database: config.databaseName,
    password: config.databasePass,
    port: config.databasePort
});

function closeDb() {
    pool.end();
    pool = null;
}

function dbQuery(sql) {
    return pool.query(sql);
}

function tableExists(schema, tableToTry) {
    const sql = {
        text: 'SELECT EXISTS ( SELECT 1 FROM pg_tables WHERE schemaname = $1 AND tablename = $2 );',
        values: [schema, tableToTry]
    };
    return pool.query(sql).then((result) => {
        return result.rows[0].exists;
    });
}

function determineDbVersion() {
    // the default schema
    const schema = 'public';

    // determine the database version
    // from the migrations entry in the numifex_info
    // or other means, if this is an old database

    return tableExists(schema, 'numifex_info')
        .then(migrationsExist => {
            if (migrationsExist) {
                // we have migration version
                // in the numifex_info table
                return pool.query({
                    text: `SELECT db_version FROM numifex_info`
                }).then(res => {
                    return res.rows[0].db_version.split('.').map(n => parseInt(n));
                });
            } else {
                // we DO NOT have a numifex_info table

                // either the database is brand new
                // or it is the "beta" schema
                // which had no migrations table
                return tableExists(schema, 'orders')
                    .then((ordersExists) => {
                        if (ordersExists) {
                            // it is in "beta"
                            // 0.1.1 is the last migration that does not have the numifex_info table
                            return [0, 1, 1];
                        } else {
                            // the database is empty
                            // run every migration that is available
                            return [0, 0, 0];
                        }
                    });
            }
        }
        );
}

function setupDb() {
    return determineDbVersion().then((version) => {
        return migrateDb(version); // do any neccessary database migrations
    }).catch((err) => {
        console.error(`FAILED to setup DB: `, err);
    });
}

function migrateDb(vFrom) {
    console.log(`database version: ${vFrom}`);
    if (vFrom.findIndex(x => x !== 0) === -1) {
        // 0.0.0
        console.log('NEW DATABASE.');
    }

    // get a list of all migrations in sql/migrations folder
    return new Promise((resolve, reject) => {
        const migrationsPath = path.join(__dirname, '..', 'sql', 'migrations');
        fs.readdir(migrationsPath, (err, files) => {
            if (err) {
                reject(err);
            } else {
                resolve(files);
            }
        });
    }).then(files => { // extract version numbers
        return files.map(file => {
            try {
                return {
                    file: file,
                    version: file.split('.').slice(0, -1).map(n => parseInt(n))
                };
            } catch(e) {
                return -1;
            }
        }).filter(f => f !== -1).sort((a, b) => { // sort in order of version
            return compareList(a.version, b.version);
        });
    }).then(migrations => {
        // find the first migration that is past the vFrom version, and cut the list there
        for(let i = 0; i < migrations.length; i++) {
            if (compareList(migrations[i].version, vFrom) > 0) {
                return migrations.slice(i).map(migration => migration.file);
            }
        }
        return [];
    }).then(migrationFiles => { // apply each migration in order, one at a time
        let migrationPromise = Promise.resolve();
        for(let i = 0; i < migrationFiles.length; i++) {
            migrationPromise = migrationPromise.then(() => { // keep appending migrations to make a chain
                const scriptPath = path.resolve(path.join(__dirname, '..', 'sql', 'migrations', migrationFiles[i]));
                const script = fs.readFileSync(scriptPath, 'utf8');
                return pool.query(script);
            }).then(() => {
                console.log('migrated database to version',
                    migrationFiles[i].substr(0, migrationFiles[i].lastIndexOf('.')));
            });
        }
        return migrationPromise;
    });
}

function compareList(a, b) {
    const n = Math.min(a.length, b.length);
    for (let i = 0; i < n; ++i)
    {
        if (a[i] < b[i]) {
            return -1;
        } else if (a[i] > b[i]) {
            return 1;
        }
    }
    if (a.length < b.length) {
        return -1;
    } else if (a.length > b.length) {
        return 1;
    } else {
        return 0;
    }
}

module.exports.dbQuery = dbQuery;
module.exports.closeDb = closeDb;
module.exports.setupDb = setupDb;
