const db = require('./db');
const logicRegistry = require('../daemon/logicRegistry');
const commerceRegistry = require('../daemon/commerceRegistry');

// Returns 50 orders from DB, based on page number
// tricky query to get the top payment
// for each order
// https://stackoverflow.com/a/2111420/425756

function listOrders(pageSize = 50, page = 1) {
    const sql = {
        text:    `SELECT o.id, o.commerce, o.commerce_id, o.date_created, o.order_total, o.order_currency,
                 o.pub_key, o.status, o.currency_conv_rate, p1.balance AS account_balance,
                 t.amount AS transfer_total
                 FROM orders o
                 LEFT JOIN payments p1
                        ON p1.order_id = o.id
                 LEFT JOIN payments p2
                        ON (p2.order_id = o.id
                        AND (p1.date_created < p2.date_created OR p1.date_created = p2.date_created AND p1.id < p2.id))
                 LEFT JOIN transfers t
                        ON t.order_id = o.id
                        AND t.date_created = (SELECT max(date_created) FROM transfers WHERE o.id = order_id)
                        AND t.date_confirmed IS NOT NULL
                 WHERE p2.id IS NULL
                 ORDER BY o.date_created DESC
                 OFFSET ($2 * ($1 - 1)) ROWS
                 FETCH NEXT ($2 + 1) ROWS ONLY`,
        values: [page, pageSize]
    };

    return Promise.all([
        db.dbQuery(sql),
        logicRegistry.createFormatMap()
    ]).then(([response, formatMap]) => {
        response.rows.forEach((order) => {
            const formatter = formatMap[order.order_currency];

            if (order.account_balance !== null) {
                // console.log(order.account_balance);
                // may be null if there are no payments yet
                order.account_balance = formatter.baseToNatural(order.account_balance);
            } else {
                order.account_balance = '0';
            }

            if (order.order_total !== null) {
                order.order_total = formatter.baseToNatural(order.order_total);
            } else {
                order.order_total = '0';
            }

            if (order.transfer_total !== null) {
                order.transfer_total = formatter.baseToNatural(order.transfer_total);
            }
        });
        return response.rows;
    });
}

function fullOrderDetails(orderId) {
    const sql = {
        text: `SELECT *, o.id AS orderid, o.date_created as datecreated, p1.balance AS account_balance
                FROM orders o
                LEFT JOIN payments p1
                        ON p1.order_id = o.id
                LEFT JOIN payments p2
                        ON (p2.order_id = o.id
                        AND (p1.date_created < p2.date_created OR p1.date_created = p2.date_created AND p1.id < p2.id))
                WHERE p2.id IS NULL AND o.id = $1`,
        values: [orderId]
    };

    return db.dbQuery(sql).then(response => {
        if (response.rows.length <= 0) {
            return null;
        }

        const order = response.rows[0];
        // remove priv_key from the order details
        delete order.priv_key;
        order.id = order.orderid;
        delete order.orderid;
        order.date_created = order.datecreated;
        delete order.datecreated;

        const paymentSql = {
            text: `SELECT date_created, balance FROM Payments WHERE order_id = $1`,
            values: [order.id]
        };

        const transferSql = {
            text: `SELECT * FROM transfers WHERE order_id = $1`,
            values: [order.id]
        };

        // nested then is to control return value
        const logic = logicRegistry.getLogic(order.order_currency);
        return Promise.all([
            logic.createCurrencyFormatter(),
            db.dbQuery(paymentSql),
            db.dbQuery(transferSql)
        ]).then(([formatter, paymentResponse, transferResponse]) => {

            // prepare the payments
            order.payments = paymentResponse.rows.map(payment => {
                payment.balance = formatter.baseToNatural(payment.balance);
                return payment;
            });

            // prepare the transfers
            order.transfers = transferResponse.rows.map(transfer => {
                transfer.amount = formatter.baseToNatural(transfer.amount);
                return transfer;
            });

            if (order.account_balance !== null) {
                // console.log(order.account_balance_wei);
                // may be null if there are no payments yet
                order.account_balance = formatter.baseToNatural(order.account_balance);
            } else {
                order.account_balance = '0';
            }

            order.order_total = formatter.baseToNatural(order.order_total);
            return order;
        });
    });
}

function listOrdersSummary(pageSize, page) {
    const sql = {
        text:    `SELECT o.id, o.commerce_id, o.date_created, o.order_total, o.order_currency,
                 o.pub_key, o.status, o.currency_conv_rate
                 FROM orders o
                 ORDER BY o.date_created DESC
                 OFFSET ($2 * ($1 - 1)) ROWS
                 FETCH NEXT ($2 + 1) ROWS ONLY`,
        values: [page, pageSize]
    };
    return Promise.all([
        db.dbQuery(sql),
        logicRegistry.createFormatMap()
    ]).then(([response, formatMap]) => {
        response.rows.forEach((order) => {
            const formatter = formatMap[order.order_currency];

            if (order.order_total !== null) {
                order.order_total = formatter.baseToNatural(order.order_total);
            } else {
                order.order_total = '0';
            }
        });
        return response.rows;
    });
}

function getOrderByCommerceId(commerce, commerceId) {
    const sql = {
        text: 'SELECT id, pub_key, commerce_id FROM orders WHERE commerce = $1 AND commerce_id = $2',
        values: [commerce, commerceId]
    };
    return db.dbQuery(sql).then(response => {
        return response.rows;
    });
}

function getConfigByKey(category, target, key) {
    if (category === 'default') {
        const query = {
            text: 'SELECT * FROM config',
        };

        return db.dbQuery(query).then(res => {
            if (res.rows.length > 0) {
                return (res.rows[0])[key];
            } else {
                return Promise.reject();
            }
        });
    } else if (category === 'currency') {
        const logic = logicRegistry.getLogic(target);
        if (!logic) {
            return Promise.reject();
        }
        return logic.getConfigByKey(key);
    } else if (category === 'commerce') {
        const commerce = commerceRegistry.commerceMap[target];
        if (!commerce) {
            return Promise.reject();
        }
        return commerce.getConfigByKey(key);
    }
    return Promise.reject();
}

function setConfigByKey(category, target, key, value) {
    if (category === 'default') {
        const query = {
            text: 'SELECT * FROM config',
        };

        return db.dbQuery(query).then(res => {
            // check if the key is in any existing fields
            const field = res.fields.find((field) => field.name === key);

            if (field) {
                const sql = {
                    text: `UPDATE config SET ${field.name} = $1`, values: [value]};
                return db.dbQuery(sql);
            } else {
                throw 'attempted to set key which is not in config';
            }
        });
    } else if (category === 'currency') {
        const logic = logicRegistry.getLogic(target);
        if (!logic) {
            throw `attempted to set key for invalid target ${target}`;
        }
        return logic.setConfigByKey(key, value);
    } else if (category === 'commerce') {
        const commerce = commerceRegistry.commerceMap[target];
        if (!commerce) {
            throw `attempted to set key for invalid target ${target}`;
        }
        return commerce.setConfigByKey(key, value);
    }
    throw 'attempted to set key in an invalid category';
}

function cancelOrder(order_id) {
    console.log(`cancelling order: ${order_id}`);

    const sql = {
        text: "UPDATE orders SET status = 'cancelling' WHERE id = $1",
        values: [order_id]
    };

    return db.dbQuery(sql);
}

module.exports.listOrders = listOrders;
module.exports.fullOrderDetails = fullOrderDetails;
module.exports.listOrdersSummary = listOrdersSummary;
module.exports.getOrderByCommerceId = getOrderByCommerceId;
module.exports.getConfigByKey = getConfigByKey;
module.exports.setConfigByKey = setConfigByKey;
module.exports.cancelOrder = cancelOrder;
