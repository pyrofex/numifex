const express = require('express');
const router = express.Router();
const numifex = require('../controllers/routeControllers');
const auth = require('../controllers/auth');
const logicRegistry = require('../daemon/logicRegistry');
const qrImage = require('qr-image');

//*** FRONT-END/UI ROUTES [START] ***/
router.get('/', function (req, res) {
    console.log('CHECK');
    res.send(`
        <html>
            <head><title>Numifex</title></head>

            <body>
                <h2>Numifex Payment Processor</h2>

                <p>
                    Hello Numifex! It looks like you have everything setup and working correctly.
                    You may want to visit the <a href="/admin">Admin</a> to configure settings.
                </p>
            </body>
        </html>
    `);
});

// requires api token
router.post('/placeorder', function(req, res) {
    auth.checkApiToken(req.header('Authorization')).then(() => {
        const order = {
            commerceId: req.body.commerce_id,
            orderTotal: req.body.order_total,
            orderCurrency: req.body.order_currency,
            commerce: req.body.commerce,
            convRate: req.body.currency_conv_rate,
            currency: req.body.currency
        };

        if (!order.currency) {
            order.currency = null;
            order.convRate = null;
        }

        if (order.orderTotal === null || order.orderTotal === undefined) {
            res.sendStatus(400).json({
                error: 'missing order total'
            });
            return;
        }

        const logic = logicRegistry.getLogic(order.orderCurrency);
        return logic.createOrder(order).then(orderInfo => {
            console.log('ORDER CREATED: ' + orderInfo.address);
            res.status(201).json(orderInfo);
        });
    }).catch((err) => {
        console.error(err);
        const code = (err instanceof auth.AuthError) ? 401 : 500;
        res.status(code).json({
            error: err.message
        });
    });
});

router.get('/qr/:crypto/:address', function (req, res) {
    auth.checkApiToken(req.header('Authorization')).then(() => {
        const logic = logicRegistry.getLogic(req.params.crypto.toUpperCase());

        if (!logic) {
            res.sendStatus(404);
            return;
        }
        const qrCode = logic.qrCodeText(req.params.address, req.query.amount);
        console.log('QR: ', qrCode);

        if (!qrCode) {
            res.sendStatus(400);
            return;
        }

        res.setHeader('Content-Type', 'image/svg+xml');
        res.status(201);

        const qrStream = qrImage.image(qrCode, { type: 'svg' });
        qrStream.pipe(res);
    }).catch((err) => {
        console.error(err);
        const code = (err instanceof auth.AuthError) ? 401 : 500;
        res.status(code).json({
            error: err.message
        });
    });
});

// requires api token
router.post('/calculate-price', function (req, res) {
    auth.checkApiToken(req.header('Authorization')).then(() => {
        let fiatCurrency;
        let subTotal;
        let riskLevel;
        let timeToSell;

        const orderCurrency = req.body.order_currency;
        if (req.body.currency !== undefined) {
            fiatCurrency = req.body.currency;
        } else {
            fiatCurrency = 'USD';
        }

        if (req.body.order_total !== undefined) {
            subTotal = parseFloat(req.body.order_total);
        } else {
            res.sendStatus(400);
            return;
        }

        // optional price settings
        if (req.body.risk_level !== undefined) {
            riskLevel = req.body.risk_level;
        }

        if (req.body.time_to_sell !== undefined) {
            timeToSell = req.body.time_to_sell;
        }

        const orderCurrencyLogic = logicRegistry.getLogic(orderCurrency);
        if (!orderCurrencyLogic) {
            res.status(400).json({
                'error': 'unknown cypto currency'
            });
            return;
        }

        let exchangeRatePromise;
        if (req.body.currency_conv_rate !== undefined) {
            // the route allows the user to specify a conversion
            // rate instead of grabbing one from a source
            // the math is still done server side to ensure
            // consistency and to add any price adjustments
            const exchangeRate = parseFloat(req.body.currency_conv_rate);
            exchangeRatePromise = Promise.resolve(exchangeRate);
        } else {
            // request it from a service
            exchangeRatePromise = orderCurrencyLogic.requestExchangeRate(fiatCurrency, riskLevel, timeToSell);
        }


        return Promise.all([
            exchangeRatePromise,
            orderCurrencyLogic.createCurrencyFormatter(),
        ]).then(([fiatPerCrypto, currencyFormatter]) => {
            // unit conversion
            // FIAT * (FIAT/CRYPTO)^-1 = FIAT * (CRYPTO/FIAT) = CRYPTO
            // For USD to ETH
            // USD * (USD/ETH)^-1 = USD * (ETH/USD) = ETH
            const baseTotal = currencyFormatter.naturalToBase(subTotal / fiatPerCrypto);
            const naturalTotal = currencyFormatter.baseToNatural(baseTotal);

            console.log(`calculate price. rate: ${fiatPerCrypto} total: ${baseTotal} formatted: ${naturalTotal}`);
            res.status(200).json({
                'total_formatted': naturalTotal,
                'total': baseTotal,
                'rate': String(fiatPerCrypto),
            });
        });
    }).catch((err) => {
        console.error(err);
        const code = (err instanceof auth.AuthError) ? 401 : 500;
        res.status(code).json({
            error: err.message
        });
    });
});

router.get('/list-orders-summary', function(req, res) {
    console.log(`/list-orders-summary`, req.query.page);
    auth.checkApiToken(req.header('Authorization')).then(() => {
        let page = 1;
        if (req.query.page && req.query.page > 0) {
            page = parseInt(req.query.page);
        }
        return numifex.listOrdersSummary(50, page).then(orders => {
            const hasNextPage = orders.length > 50;
            res.status(200).json({
                orders: orders.slice(0, 50),
                hasNextPage: hasNextPage
            });
        });
    }).catch((err) => {
        console.error(err);
        const code = (err instanceof auth.AuthError) ? 401 : 500;
        res.status(code).json({
            error: err.message
        });
    });
});



//*** FRONT-END/UI ROUTES [START] ***/

// requires admin token
router.get('/config-fetch/:category/:target/:param', function(req, res) {
    console.log('/config-fetch', req.params);
    auth.checkAdminToken(req.header('Authorization')).then(() => {
        const configCategory = req.params.category;
        const configTarget = req.params.target;
        const configParam = req.params.param;

        return numifex.getConfigByKey(configCategory, configTarget, configParam).then(value => {
            console.log('Return: ' + value);
            res.status(200).json({
                response: value
            });
        });
    }).catch((err) => {
        console.error(err);
        const code = (err instanceof auth.AuthError) ? 401 : 500;
        res.status(code).json({
            error: err.message
        });
    });
});

// requires admin token
router.post('/config-submit', function(req, res) {
    console.log(`/config-submit`, req.body);
    auth.checkAdminToken(req.header('Authorization')).then(() => {
        const category = req.body.category;
        const target = req.body.target;
        const key = req.body.key;
        const value = req.body.value;

        console.log('SUBMIT: ' + key + value);
        return numifex.setConfigByKey(category, target, key, value).then(() => {
            res.status(201).json({
                response: 'success'
            });
        });
    }).catch((err) => {
        console.error(err);
        const code = (err instanceof auth.AuthError) ? 401 : 500;
        res.status(code).json({
            error: err.message
        });
    });
});
//*** FRONT-END/UI ROUTES [END] ***/

// requires admin token
router.post('/cancelorder', function(req, res) {
    auth.checkAdminToken(req.header('Authorization')).then(() => {
        const orderId = req.body.order_id;
        return numifex.cancelOrder(orderId).then(() => {
            res.sendStatus(200);
        });
    }).catch((err) => {
        console.error(err);
        const code = (err instanceof auth.AuthError) ? 401 : 500;
        res.status(code).json({
            error: err.message
        });
    });
});

// requires admin token
router.get('/list-orders', function(req, res) {
    console.log(`/list-orders`, req.query.page);
    auth.checkAdminToken(req.header('Authorization')).then(() => {
        let page = 1;
        if (req.query.page && req.query.page > 0) {
            page = parseInt(req.query.page);
        }
        return numifex.listOrders(50, page).then(orders => {
            const hasNextPage = orders.length > 50;
            res.status(200).json({
                orders: orders.slice(0, 50),
                hasNextPage: hasNextPage
            });
        });
    }).catch((err) => {
        console.error(err);
        const code = (err instanceof auth.AuthError) ? 401 : 500;
        res.status(code).json({
            error: err.message
        });
    });
});

// requires admin token
router.get('/full-order/:order_id', function(req, res) {
    console.log(`/full-order`, req.params);
    auth.checkAdminToken(req.header('Authorization')).then(() => {
        const orderId = parseInt(req.params.order_id);
        return numifex.fullOrderDetails(orderId).then(order => {
            if (!order) {
                res.status(404).json({
                    error: 'order not found'
                });
            } else {
                res.status(200).json(order);
            }
        });
    }).catch((err) => {
        console.error(err);
        const code = (err instanceof auth.AuthError) ? 401 : 500;
        res.status(code).json({
            error: err.message
        });
    });
});

// This follows the oauth 2 spec:
// https://tools.ietf.org/html/rfc6749
router.post('/admin/authorize', function(req, res) {
    const grantType = req.body.grant_type;
    const username = req.body.username;
    const password = req.body.password;
    console.log(`${username} requesting admin auth token`);

    if (!grantType || !username || !password) {
        res.status(400).send({
            error: 'missing fields'
        });
        return;
    }

    if (grantType !== 'password') {
        res.status(422).send({
            error: 'incorrect grant_type'
        });
        return;
    }

    auth.authorizeAdmin(username, password).then(adminToken => {
        console.log(`${username} granted admin auth token`);
        res.send({
            access_token: adminToken,
            token_type: 'bearer'
        });
    }).catch((err) => {
        console.error(err);
        const code = (err instanceof auth.AuthError) ? 400 : 500;
        res.status(code).json({
            error: err.message
        });
    });
});

// the admin can change his login credentials
router.post('/admin/update', function(req, res) {
    const username = req.body.username;
    const password = req.body.password;
    const newUsername = req.body.new_username;
    const newPassword = req.body.new_password;
    console.log(`${username} requesting change admin credentials to ${newUsername}`);

    if (!username || !password || !newUsername || newUsername.length <= 0 || !newPassword) {
        res.status(400).send({
            error: 'missing fields'
        });
        return;
    }

    if (newPassword.length < 1) {
        res.status(400).send({
            error: 'password must be at least 6 characters'
        });
    }

    auth.updateAdminCredentials(username, password, newUsername, newPassword).then(() => {
        console.log(`${username} admin changed to ${newUsername}`);
        res.sendStatus(200);
    }).catch((err) => {
        console.error(err);
        const code = (err instanceof auth.AuthError) ? 401 : 500;
        res.status(code).json({
            error: err.message
        });
    });
});

// requires admin token
router.get('/api-clients/list', function(req, res) {
    auth.checkAdminToken(req.header('Authorization')).then(() => {
        return auth.listApiClients().then(clients => {
            res.send(clients.rows);
        });
    }).catch((err) => {
        console.error(err);
        const code = (err instanceof auth.AuthError) ? 401 : 500;
        res.status(code).json({
            error: err.message
        });
    });
});

// requires admin token
router.post('/api-clients/create', function(req, res) {
    auth.checkAdminToken(req.header('Authorization')).then(() => {
        // optional
        const customClientSecret = req.body.custom_secret;

        if (customClientSecret && customClientSecret.length < 6) {
            res.status(400).send({
                error: 'custom secret must be at least 6 characters'
            });
            return;
        }
        return auth.createApiClient(customClientSecret).then((newClient) => {
            console.log(`Generated new api client with id: ${newClient.id}`);
            res.send({
                client_id: newClient.id,
                client_secret: newClient.clientSecret
            });
        });
    }).catch((err) => {
        console.error(err);
        const code = (err instanceof auth.AuthError) ? 401 : 500;
        res.status(code).json({
            error: err.message
        });
    });
});

// requires admin token
router.post('/api-clients/delete', function(req, res) {
    auth.checkAdminToken(req.header('Authorization')).then(() => {
        const id = req.body.id;
        return auth.deleteApiClient(id).then(() => {
            console.log(`Delete api client with id ${id}`);
            res.sendStatus(200);
        });
    }).catch((err) => {
        console.error(err);
        const code = (err instanceof auth.AuthError) ? 401 : 500;
        res.status(code).json({
            error: err.message
        });
    });
});

// requires admin token
router.post('/api-clients/revoke', function(req, res) {
    auth.checkAdminToken(req.header('Authorization')).then(() => {
        const id = req.body.id;
        return auth.revokeApiClient(id).then(() => {
            res.sendStatus(200);
        });
    }).catch((err) => {
        console.error(err);
        const code = (err instanceof auth.AuthError) ? 401 : 500;
        res.status(code).json({
            error: err.message
        });
    });
});

// requires admin token
router.post('/api-clients/nickname', function(req, res) {
    auth.checkAdminToken(req.header('Authorization')).then(() => {
        const id = req.body.id;
        const nickname = req.body.nickname;

        return auth.nicknameApiClient(id, nickname).then(() => {
            res.sendStatus(200);
        });
    }).catch((err) => {
        console.error(err);
        const code = (err instanceof auth.AuthError) ? 401 : 500;
        res.status(code).json({
            error: err.message
        });
    });
});

// This follows the oauth 2 spec:
// https://tools.ietf.org/html/rfc6749
router.post('/api-clients/authorize', function(req, res) {
    console.log('AUTH');
    const nickname = req.body.username;
    const clientSecret = req.body.password;
    const grantType = req.body.grant_type;

    if (!nickname || !clientSecret && !grantType) {
        res.status(400).send({
            error: 'invalid_request'
        });
        return;
    }
    if (grantType !== 'password') {
        res.status(400).send({
            error: 'invalid_grant'
        });
        return;
    }
    auth.authorizeClient(clientSecret).then(clientToken => {

        console.log(clientSecret);
        res.send({
            access_token: clientToken,
            token_type: 'bearer'
        });
    }).catch((err) => {
        console.error(err);
        const code = (err instanceof auth.AuthError) ? 400 : 500;
        res.status(code).json({
            error: err.message
        });
    });
});

// include modular routes
logicRegistry.registerRoutes(router);

module.exports = router;
