BEGIN;

CREATE TABLE magento_config(
    Lock BOOLEAN NOT NULL UNIQUE DEFAULT(true) CHECK(Lock),
    magento_token TEXT,
    magento_url TEXT
);

INSERT INTO magento_config(magento_token, magento_url) VALUES(NULL, NULL);

-- magento needs 2 ids for the order
-- this was later removed
ALTER TABLE orders ADD COLUMN magento_increment_id TEXT;

UPDATE numifex_info SET db_version = '0.2.7';

COMMIT;