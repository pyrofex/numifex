BEGIN;

CREATE TABLE erc20_config(
    treasury CHAR(42),
    erc20_gas_budget NUMERIC(80, 0) NOT NULL CHECK(erc20_gas_budget >= 0),
    eth_price_adjustment NUMERIC(8, 4) NOT NULL,
    reserve_priv_key TEXT,
    contract_address TEXT,
    contract_symbol TEXT,
    contract_decimals INTEGER DEFAULT(18)
);

INSERT INTO erc20_config(treasury, erc20_gas_budget, eth_price_adjustment, reserve_priv_key, contract_address, contract_symbol, contract_decimals) VALUES( '0xc895ac7b8641577309EFaCa49BB2A175e2d7f9C1', 150000, 0.0,'75A906A4DF5357F8B8C19D54E5CC7723580941A069923081B4D2EB99D59173A8', '0xeB60a3b01E52CCd3e23624d732268c5CA47B3cab', 'BAT', 18);

-- need to support strings, not just numbers
ALTER TABLE orders
    ALTER COLUMN commerce_id TYPE TEXT;

-- new point of sale system
CREATE TABLE pos_config(
    Lock BOOLEAN NOT NULL UNIQUE DEFAULT(true) CHECK(Lock),
    currency TEXT
);

INSERT INTO pos_config(currency) VALUES('USD');

-- no longer need increment_id. This is handled by string commerce ids
ALTER TABLE orders DROP COLUMN magento_increment_id;

UPDATE numifex_info SET db_version = '0.3.3';

COMMIT;