BEGIN;

ALTER TABLE btc_config
    DROP COLUMN jwt,
    ALTER COLUMN rpc_user SET DEFAULT(''),
    ALTER COLUMN rpc_password SET DEFAULT('');

ALTER TABLE eth_config ADD COLUMN gas_price_selection TEXT DEFAULT('average');

ALTER TABLE config
    ADD COLUMN axiacodex_token TEXT,
    ADD COLUMN axiacodex_username TEXT;

CREATE TABLE custom_integration_config(
    Lock BOOLEAN NOT NULL UNIQUE DEFAULT(true) CHECK(Lock),
    url TEXT
);

UPDATE numifex_info SET db_version = '0.3.2';

COMMIT;