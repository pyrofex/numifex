-- Ensure Atomicity
BEGIN;

CREATE TYPE OrderStatus AS ENUM ('created', 'paid', 'finished', 'cancelling', 'cancelled');

CREATE TABLE orders(
    id SERIAL PRIMARY KEY,

    commerce_id INTEGER,
    commerce_notify_date TIMESTAMP,

    date_created TIMESTAMP NOT NULL,
	order_total_wei NUMERIC(80, 0) CHECK(order_total_wei >= 0),
    /* FIAT per CRYPTO */
    currency_conv_rate NUMERIC(12, 8) CHECK(currency_conv_rate >= 0),
    currency CHAR(8),

    /* Ethereum address */
    pub_key CHAR(42) NOT NULL UNIQUE,
    priv_key CHAR(66) NOT NULL UNIQUE,

    /* Solidity uses uint256 to store wei values */
    /* used to record the final amount that was transferred into the treasury */
    transfer_total_wei NUMERIC(80, 0) DEFAULT(0) CHECK(transfer_total_wei >= 0),
    transfer_date TIMESTAMP,
    transfer_tx CHAR(66),
    status OrderStatus NOT NULL DEFAULT('created')
);

/* used to record a history of payments
   in the account */
CREATE TABLE payments(
    id SERIAL PRIMARY KEY,
    order_id INTEGER REFERENCES orders(id),
    date_created TIMESTAMP,
    balance NUMERIC(80, 0) CHECK(balance >= 0)
);

CREATE TABLE api_clients(
    id SERIAL PRIMARY KEY,
    nickname VARCHAR(256),
    client_secret_hash CHAR(44) NOT NULL UNIQUE,
    access_token CHAR(64) UNIQUE,
    token_timestamp TIMESTAMP NOT NULL DEFAULT(CURRENT_TIMESTAMP)
);

CREATE TABLE admin_clients(
    id SERIAL PRIMARY KEY,
    username VARCHAR(256) NOT NULL,
    password_hash CHAR(44) NOT NULL,
    password_salt CHAR(16) NOT NULL,
    access_token CHAR(64) UNIQUE,
    token_timestamp TIMESTAMP DEFAULT(CURRENT_TIMESTAMP)
);

CREATE TABLE config(
    Lock BOOLEAN NOT NULL UNIQUE DEFAULT(true) CHECK(Lock),
    wordpress_user TEXT,
    wordpress_pass TEXT,
    wordpress_url TEXT,
    treasury CHAR(42),
    eth_node TEXT NOT NULL,
    /* when gas price is null, will use network gas price */
    eth_gas_price NUMERIC(80, 0) CHECK(eth_gas_price >= 0),
    /* gas limit could be an integer, but I want to keep it consistent 
        with gas_price to avoid confusion. */
    eth_gas_limit NUMERIC(80, 0) NOT NULL CHECK(eth_gas_limit >= 0),
    eth_price_adjustment NUMERIC(8, 4) NOT NULL,
    order_timeout_in_hours INTEGER NOT NULL DEFAULT(168) CHECK(order_timeout_in_hours >= 0)
);


INSERT INTO config(wordpress_user, wordpress_pass, wordpress_url, treasury, eth_node, eth_gas_limit, eth_price_adjustment, order_timeout_in_hours)
VALUES(NULL, NULL, NULL, NULL, 'https://api.myetherwallet.com/eth', 21000, 0.0, 168);
/* insert default admin with username: admin and password: admin */
INSERT INTO admin_clients(username, password_hash, password_salt) VALUES('admin', 'ZSLh2QTR1Ept+e7+tct9gRi28zNRO1BxtRrIH4vics4=', 'fb7a5d9c9f20436f');

COMMIT;
