BEGIN;

ALTER TABLE eth_config
    ALTER COLUMN eth_price_adjustment SET DEFAULT 0.0,
    ALTER COLUMN treasury TYPE TEXT;
ALTER TABLE orders ADD COLUMN script TEXT;
ALTER TABLE orders ALTER COLUMN currency TYPE VARCHAR(8);

ALTER TABLE payments ADD COLUMN block_height INTEGER;

CREATE TABLE btc_config(
    Lock BOOLEAN NOT NULL UNIQUE DEFAULT(true) CHECK(Lock),
    treasury TEXT,
    minimum_confirmations INTEGER NOT NULL DEFAULT(6) CHECK(minimum_confirmations >= 0),
    btc_fee_rate NUMERIC(80, 0) CHECK(btc_fee_rate >= 0), -- Satoshi/Byte
    fee_estimate_block_target INTEGER NOT NULL DEFAULT(12) CHECK(fee_estimate_block_target >= 0),
    btc_price_adjustment NUMERIC(8, 4) NOT NULL DEFAULT(0.0),
    rpc_user TEXT NOT NULL DEFAULT(''),
    rpc_password TEXT NOT NULL DEFAULT(''),
    network TEXT NOT NULL DEFAULT('bitcoin')
);

INSERT INTO btc_config(treasury) VALUES(NULL);

UPDATE numifex_info SET db_version = '0.3.0';

COMMIT;