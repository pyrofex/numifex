BEGIN;

-- CREATE NEW TABLES
-- add new database version table for migrations

CREATE TABLE numifex_info(
    Lock BOOLEAN NOT NULL UNIQUE DEFAULT(true) CHECK(Lock),
    db_version TEXT NOT NULL,
    -- db_version must be three numbers seperated by periods, and each cannot have leading zeroes unless it is zero
    CONSTRAINT db_version_format_check CHECK(db_version SIMILAR TO '([1-9][0-9]*|0)\.([1-9][0-9]*|0)\.([1-9][0-9]*|0)')
);

INSERT INTO numifex_info(db_version) VALUES('0.2.2');

CREATE TABLE eth_config(
    Lock BOOLEAN NOT NULL UNIQUE DEFAULT(true) CHECK(Lock),
    treasury CHAR(42),
    eth_node TEXT NOT NULL,
    -- when gas price is null, will use network gas price 
    eth_gas_price NUMERIC(80, 0) CHECK(eth_gas_price >= 0),
    -- gas limit could be an integer, but I want to keep it consistent  with gas price
    eth_gas_limit NUMERIC(80, 0) NOT NULL CHECK(eth_gas_limit >= 0),
    eth_price_adjustment NUMERIC(8, 4) NOT NULL,
    minimum_confirmations INTEGER NOT NULL DEFAULT(8) CHECK(minimum_confirmations >= 0)
);

CREATE TABLE woocommerce_config(
    Lock BOOLEAN NOT NULL UNIQUE DEFAULT(true) CHECK(Lock),
    wordpress_user TEXT,
    wordpress_pass TEXT,
    wordpress_url TEXT
);

-- ADD NEW FIELDS
ALTER TABLE orders
    RENAME COLUMN transfer_total_wei TO transfer_total;

ALTER TABLE orders
    RENAME COLUMN order_total_wei TO order_total;

ALTER TABLE orders
    RENAME COLUMN transfer_date TO transfer_confirm_date;

ALTER TABLE orders
    ADD COLUMN commerce TEXT,
    -- all original orders were in ETH
    ADD COLUMN order_currency TEXT NOT NULL DEFAULT('ETH'),
    ADD COLUMN transfer_block INTEGER;
 
ALTER TABLE orders
    ALTER COLUMN transfer_tx TYPE TEXT,
    ALTER COLUMN date_created SET DEFAULT CURRENT_TIMESTAMP,
    ALTER COLUMN date_created DROP NOT NULL,
    ALTER COLUMN currency_conv_rate TYPE NUMERIC(80, 8),
    ALTER COLUMN pub_key TYPE TEXT,
    ALTER COLUMN priv_key TYPE TEXT;

ALTER TABLE payments
    ALTER COLUMN date_created SET DEFAULT CURRENT_TIMESTAMP;

-- MIGRATE DATA
INSERT INTO woocommerce_config(wordpress_user, wordpress_pass, wordpress_url)
SELECT wordpress_user, wordpress_pass, wordpress_url FROM config;

INSERT INTO eth_config(treasury, eth_node, eth_gas_limit, eth_price_adjustment)
SELECT treasury, eth_node, eth_gas_limit, eth_price_adjustment FROM config;

-- all original orders were from woocommerce
UPDATE orders SET commerce = 'woocommerce' WHERE commerce IS NULL;


-- REMOVE OLD FIELDS
ALTER TABLE config
    DROP COLUMN eth_node,
    DROP COLUMN treasury,
    DROP COLUMN eth_gas_price,
    DROP COLUMN eth_gas_limit,
    DROP COLUMN eth_price_adjustment,
    DROP COLUMN wordpress_user,
    DROP COLUMN wordpress_pass,
    DROP COLUMN wordpress_url;

COMMIT;
