BEGIN;

-- upgrade access token security
ALTER TABLE admin_clients
    ALTER COLUMN username TYPE TEXT;

ALTER TABLE api_clients
    ALTER COLUMN nickname TYPE TEXT;

-- invalidates any current admin tokens
ALTER TABLE admin_clients
    DROP COLUMN access_token,
    ADD COLUMN access_token_hash CHAR(44);

-- can't invalidate these tokens.
-- there is an upgrade path in the code
ALTER TABLE api_clients
    ADD COLUMN access_token_hash CHAR(44);

UPDATE numifex_info SET db_version = '0.3.4';

COMMIT;