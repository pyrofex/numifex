BEGIN;

ALTER TABLE btc_config
    ADD COLUMN pause_until BIGINT,
    ADD COLUMN node_changed BOOLEAN NOT NULL Default(false),
    ADD COLUMN node_url TEXT DEFAULT('http://localhost:8332'),
    ADD COLUMN jwt TEXT,
    DROP COLUMN network,
    DROP COLUMN btc_price_adjustment;

CREATE TABLE transfers(
    id SERIAL PRIMARY KEY,
    order_id INTEGER REFERENCES orders(id),
    from_address TEXT NOT NULL,
    to_address TEXT NOT NULL,
    amount NUMERIC(80, 0) NOT NULL,
    transfer_type TEXT,
    date_created TIMESTAMP NOT NULL DEFAULT(CURRENT_TIMESTAMP),
    date_confirmed TIMESTAMP,
    tx TEXT,
    block INTEGER,
    valid BOOLEAN NOT NULL DEFAULT(true)
);

ALTER TABLE transfers ALTER COLUMN to_address SET DEFAULT '';
INSERT INTO transfers(order_id, from_address, amount, date_confirmed, tx, block)
    SELECT id, pub_key, transfer_total, transfer_confirm_date, transfer_tx, transfer_block
    FROM orders
    WHERE transfer_total IS NOT NULL;
UPDATE transfers SET to_address = (SELECT treasury FROM btc_config LIMIT 1) FROM orders WHERE EXISTS(SELECT treasury FROM btc_config) AND orders.id = transfers.order_id AND orders.order_currency = 'BTC';
UPDATE transfers SET to_address = (SELECT treasury FROM eth_config LIMIT 1) FROM orders WHERE EXISTS(SELECT treasury FROM eth_config) AND orders.id = transfers.order_id AND orders.order_currency = 'ETH';
ALTER TABLE transfers ALTER COLUMN to_address DROP DEFAULT;

-- REMOVE OLD FIELDS
ALTER TABLE eth_config
    DROP COLUMN eth_price_adjustment,
    DROP COLUMN eth_gas_limit;

ALTER TABLE orders
    DROP COLUMN transfer_total,
    DROP COLUMN transfer_confirm_date,
    DROP COLUMN transfer_tx,
    DROP COLUMN transfer_block;

UPDATE numifex_info SET db_version = '0.3.1';

COMMIT;
