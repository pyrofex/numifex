BEGIN;

UPDATE transfers SET transfer_type = 'payout';

UPDATE numifex_info SET db_version = '0.3.6';

COMMIT;