BEGIN;

ALTER TABLE eth_config
    ADD COLUMN eth_node_auth_user TEXT,
    ADD COLUMN eth_node_auth_pass TEXT;

    UPDATE numifex_info SET db_version = '0.3.5';

COMMIT;