# Numifex - A Decentralized Payment Processing API

## TODO

    1. Instructions for how to get started as a developer.
    2. Instructions for how to submit a merge request.


## Developer Getting Started

You are going to want to install postgres and nodejs in order to do development
on Numifex.

### Postgresql

Install Postgres:
```
sudo apt-get install postgresql postgresql-contrib postgresql-client-common
```

Add numifex system user if wanted
```
sudo adduser numifex --gecos "" --disabled-password
```
Change adduser with --home/--system if wanted

Add the `numifex` user with password `numifex`.
```
sudo -i -u postgres
createuser --interactive -P
# or alt version
create role {db_user} with login password '{db_pass}'
```

Create database numifex with owner numifex
```
sudo -u postgres createdb -O numifex -E Unicode -T template0 numifex
```


### Install Nodejs

You need curl.
```
sudo apt-get install curl
```

Probably want to get current repos.
```
sudo apt update
```

Install the latest 8.x version of nodejs. Yes, it's older than LTS. Shut up and do it.
```
curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
bash nodesource_setup.sh
sudo apt-get install -y nodejs
```

Install some dependencies for npm modlues
```
sudo apt-get install -y gcc g++ make 
````

You can confirm you're on a reasonable version with `node --version`. Here's
what mine says:
```
node --version
```

### Run the application
```
cd src
npm ci 
cd ../client
npm ci
npm run build
cd ../src
npm start
```
You may get some notifications but we will need to setup numifex via web admin.

### Go to web page and setup via admin

http://localhost:4000


### Building the Debian Package

Numifex is built and installed as a deb package. 
It's rimarily tested on Ubuntu 18.04 LTS.
It should work on most Debian based distros without much modification.
```
cd cicd/nodes/
./entrypoint.sh --node-name numifex --npm-install-options "--only=production"  --build-package
ls -laht ../../artifacts/numifex.deb
apt-get install ../../artifacts/numifex.deb
```

### Run unit tests
```
cd src
npm test
```

# Custom config
Edit src/config.json if needed.
