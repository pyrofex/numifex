#!/bin/bash
set -ex

shellcheck -e SC2029 -e SC2139 -e SC2015 cicd/nodes/entrypoint.sh
shellcheck cicd/nodes/install-main.sh
shellcheck cicd/nodes/integration-tests.sh
shellcheck cicd/nodes/geth/install.sh
shellcheck cicd/nodes/geth/test.sh
shellcheck cicd/nodes/numifex/install.sh
shellcheck cicd/nodes/numifex/test.sh
shellcheck cicd/nodes/runner/install.sh
shellcheck cicd/nodes/wordpress/install.sh
shellcheck cicd/nodes/wordpress/test.sh

php -l plugins/wp-numifex/numifex-plugin.php

cd src; npm run lint

