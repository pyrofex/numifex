import React from 'react';
import './App.css';
import axios from 'axios';


class Form3 extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      eth_node: '',
      eth_node_user: '',
      eth_node_pass: '',
      formState: 'saved',
      error: '',
      disabled: ''
    }
    this.handleChange1 = this.handleChange1.bind(this);
    this.handleChange2 = this.handleChange2.bind(this);
    this.handleChange3 = this.handleChange3.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.access = props.access;
    this.invalidateAccess = props.invalidateAccess;
  }

  componentDidMount() {
    axios.get(`/config-fetch/${this.props.category}/${this.props.name_target}/${this.props.name1}`,
      this.access).then(response => {
        this.setState({eth_node: response.data.response});
        if (this.state.eth_node.includes("axiacodex")) {
          this.setState({disabled: "disabled"});
          this.setState({eth_node_user: ''});
          this.setState({eth_node_pass: ''});
        }
    }).catch(err => {
      this.setState({formState: 'editing'});
      if (err.response.data.error) {
        this.setState({error: err.response.data.error})
      } else {
        this.setState({error: err.response.status + " " + err.response.statusText});
      }
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
    axios.get(`/config-fetch/${this.props.category}/${this.props.name_target}/${this.props.name2}`,
      this.access).then(response => {
        this.setState({eth_node_user: response.data.response});
    }).catch(err => {
      console.log(err);
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
    axios.get(`/config-fetch/${this.props.category}/${this.props.name_target}/${this.props.name3}`,
      this.access).then(response => {
        this.setState({eth_node_pass: response.data.response});
    }).catch(err => {
      console.log(err);
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
  }
  
  handleChange1(event) {
    this.setState({eth_node: event.target.value});
    if (event.target.value.includes("axiacodex")) {
      this.setState({disabled: "disabled"});
      this.setState({eth_node_user: ''});
      this.setState({eth_node_pass: ''});
    } else {
      this.setState({disabled: ''});
    }
  }

  handleChange2(event) {
    this.setState({eth_node_user: event.target.value});
  }

  handleChange3(event) {
    this.setState({eth_node_pass: event.target.value});
  }

  handleSubmit(event) {
    console.log(this.props.category);
    event.preventDefault();
    this.setState({formState: 'saved'});
    if (this.state.eth_node.includes("axiacodex")) {
      this.setState({disabled: "disabled"});
      this.setState({eth_node_user: ''});
      this.setState({eth_node_pass: ''});
    }
    axios.post('/config-submit', {
      category: this.props.category,
      target: this.props.name_target,
      key: this.props.name1,
      value: this.state.eth_node
    }, this.access).then(() => {
      axios.get(`/config-fetch/${this.props.category}/${this.props.name_target}/${this.props.name1}`,
        this.access);
    }).catch(err => {
      this.setState({formState: 'editing'});
      if (err.response.data.error) {
        this.setState({error: err.response.data.error})
      } else {
        this.setState({error: err.response.status + " " + err.response.statusText});
      }
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
    axios.post('/config-submit', {
      category: this.props.category,
      target: this.props.name_target,
      key: this.props.name2,
      value: this.state.eth_node_user
    }, this.access).then(() => {
      axios.get(`/config-fetch/${this.props.category}/${this.props.name_target}/${this.props.name2}`,
        this.access);
    }).catch(err => {
      console.log(err);
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
    axios.post('/config-submit', {
      category: this.props.category,
      target: this.props.name_target,
      key: this.props.name3,
      value: this.state.eth_node_pass
    }, this.access).then(() => {
      axios.get(`/config-fetch/${this.props.category}/${this.props.name_target}/${this.props.name3}`,
        this.access);
    }).catch(err => {
      console.log(err);
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
  }

  handleEdit(event) {
    event.preventDefault();
    this.setState({formState: 'editing'});
    axios.get(`/config-fetch/${this.props.category}/${this.props.name_target}/${this.props.name1}`,
      this.access).catch(err => {
      console.log(err);
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
    axios.get(`/config-fetch/${this.props.category}/${this.props.name_target}/${this.props.name2}`,
      this.access).catch(err => {
      console.log(err);
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
    axios.get(`/config-fetch/${this.props.category}/${this.props.name_target}/${this.props.name3}`,
      this.access).catch(err => {
      console.log(err);
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
  }

  render() {
    if (this.state.formState === "saved") {
      return (
        <form onSubmit={this.handleEdit} className="configForm" id="ethNodeAuth">
            <p className="section-header">{this.props.desc1}</p>
            <p><span className="form-value">{this.state.eth_node}</span>
            {this.props.units ? (<span className="form-units">{this.props.units}</span>) : ''}</p>
            <input type="submit" value="Edit" className="btn"/>
        </form>
      );
    } else if (this.state.formState === "editing") {
      return (
        <form onSubmit={this.handleSubmit} className="configForm expanded" id="ethNodeAuth">
            <p className="section-header">{this.props.desc1}</p>
            <p className="error">{this.state.error}</p>
            <input type="text" value={this.state.eth_node} onChange={this.handleChange1}  required minLength={this.props.minLength} maxLength={this.props.maxLength} pattern={this.props.pattern} title={this.props.title}/>
            <p className="section-header">{this.props.desc2}</p>
            <label>User Name: </label>
            <input type="text" value={this.state.eth_node_user} onChange={this.handleChange2} 
            placeholder={this.props.example1} required minLength={this.props.minLength} maxLength={this.props.maxLength} pattern={this.props.pattern} title={this.props.title} disabled={this.state.disabled}/><br></br>
            <label>Password: </label>
            <input type="password" value={this.state.eth_node_pass} onChange={this.handleChange3} 
            placeholder={this.props.example2} required minLength={this.props.minLength} maxLength={this.props.maxLength} pattern={this.props.pattern} title={this.props.title} disabled={this.state.disabled}/>
            <input type="submit" value="Save" className="btn"/>
        </form>
      );
    }
  }
}

export default Form3;
