import React from 'react';
import './App.css';
import axios from 'axios';


class Orders extends React.Component {
    constructor(props) {
      super(props);
  
      this.state = {
        orders: [],
        page: 1,
        hasNextPage: false,
        detailedOrder: null
      };
  
      this.refreshOrders = this.refreshOrders.bind(this);
      this.cancelOrder = this.cancelOrder.bind(this);
      this.previousPage = this.previousPage.bind(this);
      this.showDetailedOrder = this.showDetailedOrder.bind(this);
      this.exitDetailedOrder = this.exitDetailedOrder.bind(this);
      this.nextPage = this.nextPage.bind(this);
      this.access = props.access;
      this.invalidateAccess = props.invalidateAccess;
    }
  
    componentDidMount() {
      this.setState({page: 1, detailedOrder: null});
      this.refreshOrders(1);
    }
  
    refreshOrders(onPage) {
      if (typeof onPage !== 'number') {
        onPage = this.state.page;
      }
      console.log(`page number: ${onPage}`);
      axios.get(`/list-orders?page=${onPage}`, this.access).then(res => {
        console.log(`Refreshed with ${res.data.orders.length} Orders`);
        this.setState({orders: res.data.orders, hasNextPage: res.data.hasNextPage});
      }).catch(error => {
        console.log(error);
        if (error && error.response && error.response.status === 401) {
          this.invalidateAccess();
        }
      });
    }

    cancelOrder(order, e) {
      e.stopPropagation();
      order.cancel_is_clicked = true; // this doesn't let React know the state has changed
      this.forceUpdate();             // so force a re-render
      console.log('order', order);
      axios.post('/cancelorder', {
        order_id: order.id
      }, this.access).catch(error => {
        if (error && error.response && error.response.status === 401) {
          this.invalidateAccess();
        }
      });
    }

    showDetailedOrder(orderId) {
      axios.get(`/full-order/${orderId}`, this.access).then(res => {
        this.setState({detailedOrder: res.data});
      }).catch(error => {
        if (error && error.response && error.response.status === 401) {
          this.invalidateAccess();
        } else {
          this.exitDetailedOrder(); // go back to order list
        }
      });
    }

    exitDetailedOrder() {
      this.setState({detailedOrder: null});
    }

    previousPage() {
      if (this.state.page > 1) {
        const prevPage = this.state.page - 1;
        this.setState({page: prevPage});
        this.refreshOrders(prevPage);
      }
    }

    nextPage() {
      if (this.state.hasNextPage) {
        const nexPage = this.state.page + 1;
        this.setState({page: nexPage});
        this.refreshOrders(nexPage);
      }
    }
  
    render() {
      if (this.state.detailedOrder) {
        return (
          <DetailedOrder detailedorder={this.state.detailedOrder}
          refreshOrder={this.showDetailedOrder.bind(null, this.state.detailedOrder.id)} exitOrder={this.exitDetailedOrder} />
        );
      } else {
        return (
          <div className="ordersTable">
            <div>
              <button onClick={this.refreshOrders} className="btn">Refresh</button>
            </div>
            <div className="order-list-container">
              <div>
                <button onClick={this.previousPage} disabled={this.state.page === 1}>&lt;</button>
                <div className="page-number-container">Page {this.state.page}</div>
                <button onClick={this.nextPage} disabled={!this.state.hasNextPage}>&gt;</button>
              </div>
              <table>
                <thead>
                  <tr>
                    <th title="cancel order. The balance will be transferred to the treasury">Cancel Order</th>
                    <th>Created</th>
                    <th title="the commerce platform for this order">Commerce</th>
                    <th title="the id in its commerce platform">Commerce ID</th>
                    <th title="the amount the customer agreed to pay">Order Total</th>
                    <th title="the last balance of the settlement account">Settlement Balance</th>
                    <th title="the amount transferred to the treasury. This amount may be slightly lower than the order total because of transaction fees.">Deposited</th>
                    <th title="the address of the intermediate account that the customer sends to.">Crypto Address</th>
                    <th>Order Status</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    this.state.orders.map(order => {
                      let cryptoUnits = '';
                      let addressText;
                      if (order.order_currency === 'ETH') {
                        cryptoUnits = 'ETH';
                        addressText = (
                          <a target="_blank" rel="noopener noreferrer" href={ 'https://etherscan.io/address/' + order.pub_key}>
                            {order.pub_key}
                          </a>
                        );
                      } else if (order.order_currency === 'BTC') {
                        cryptoUnits = 'BTC';
                        addressText = (
                          <a target="_blank" rel="noopener noreferrer" href={ 'https://blockstream.info/address/' + order.pub_key}>
                            {order.pub_key}
                          </a>
                        );
                      } else {
                        addressText = (
                          <span>
                            {order.pub_key}
                          </span>
                        );
                      }

                      return (
                        <tr key={order.id} onClick={this.showDetailedOrder.bind(null, order.id)} title="See detailed view" className="order-list-order">
                          <td onClick={(e) => e.stopPropagation()} title="Cancel this order" className="order-list-cancel">
                            {order.status === 'created' && order.cancel_is_clicked !== true ?
                            (<button onClick={this.cancelOrder.bind(null, order)}>Cancel</button>)
                            : '' }
                          </td>
                          <td>
                            { new Date(order.date_created).toLocaleString('en-US') }
                          </td>
                          <td>
                            { order.commerce ? order.commerce.toUpperCase() : '' }
                          </td>
                          <td>
                            { order.commerce_id }
                          </td>
                          <td>
                            { `${parseFloat(order.order_total).toFixed(6)} ${cryptoUnits}` }
                          </td>
                          <td>
                            { `${parseFloat(order.account_balance).toFixed(6)} ${cryptoUnits}` }
                          </td>
                          <td>
                            {(order.transfer_total === null) ? ''
                            : `${parseFloat(order.transfer_total).toFixed(6)} ${cryptoUnits}` }
                          </td>
                          <td title="view address" onClick={(e) => e.stopPropagation()} className="order-list-address">
                              { addressText }
                          </td>
                          <td>
                            { order.status.toUpperCase() }
                          </td>
                        </tr>
                      );
                    })
                  }
                </tbody>
              </table>
            </div>
          </div>
        );
      }
    }
  }

function DetailedOrder(props) {
  const order = props.detailedorder;
  let cryptoUnits = '';
  let makeAddressText;
  let makeTransactionTX;
  let makeBlockLink;
  if (order.order_currency === 'ETH') {
    cryptoUnits = 'ETH';
    makeAddressText = pub_key => (
      <a target="_blank" rel="noopener noreferrer" href={ 'https://etherscan.io/address/' + pub_key}>
        {pub_key}
      </a>
    );
    makeTransactionTX = transfer_tx => (
      <a target="_blank" rel="noopener noreferrer" href={ 'https://etherscan.io/tx/' + transfer_tx}>
        {transfer_tx}
      </a>
    );
    makeBlockLink = block => (
      <a target="_blank" rel="noopener noreferrer" href={ 'https://etherscan.io/block/' + block}>
        {block}
      </a>
    );
  } else if (order.order_currency === 'BTC') {
    cryptoUnits = 'BTC';
    makeAddressText = pub_key => (
      <a target="_blank" rel="noopener noreferrer" href={ 'https://blockstream.info/address/' + pub_key}>
        {pub_key}
      </a>
    );
    makeTransactionTX = transfer_tx => (
      <a target="_blank" rel="noopener noreferrer" href={ 'https://blockstream.info/tx/' + transfer_tx}>
        {transfer_tx}
      </a>
    );
    makeBlockLink = block => (
      <a target="_blank" rel="noopener noreferrer" href={ 'https://blockstream.info/block-height/' + block}>
        {block}
      </a>
    );
  } else {
    makeAddressText = pub_key => (
      <span>
        {pub_key}
      </span>
    );
    makeTransactionTX = transfer_tx => (
      <span>
        {transfer_tx}
      </span>
    );
    makeBlockLink = block => (
      <span>
        {block}
      </span>
    );
  }
  return (
    <div className="ordersTable">
      <div>
        <button className="btn" onClick={props.refreshOrder}>Refresh</button>
        <button className="btn" onClick={props.exitOrder}>Back To Orders List</button>
      </div>
      <div className="section">
        <label htmlFor="detailedOrder">Detailed Order View</label>
        <table name="detailedOrder">
          <tbody>
            <tr>
              <td>ID</td>
              <td>{order.id}</td>
            </tr>
            <tr>
              <td>Commerce</td>
              <td>{order.commerce ? order.commerce.toUpperCase() : ''}</td>
            </tr>
            <tr>
              <td>Commerce ID</td>
              <td>{order.commerce_id}</td>
            </tr>
            <tr>
              <td>Commerce Notify Date</td>
              <td title="When the eccommerce platform was notified of the order being paid">
                {order.commerce_notify_date ? new Date(order.commerce_notify_date).toLocaleString('en-US') : ''}
              </td>
            </tr>
            <tr>
              <td>Creation Date</td>
              <td>{new Date(order.date_created).toLocaleString('en-US')}</td>
            </tr>
            <tr>
              <td>Crypto</td>
              <td>{order.order_currency.toUpperCase()}</td>
            </tr>
            <tr>
              <td>Order Total</td>
              <td>{`${order.order_total} ${cryptoUnits}`}</td>
            </tr>
            <tr>
              <td>Exchange Rate</td>
              <td>{ order.currency_conv_rate ?
                `${order.currency_conv_rate} ${order.currency ? order.currency.trim() : 'Fiat'}/${cryptoUnits}` : ''
              }</td>
            </tr>
            <tr>
              <td>Settlement Account Address</td>
              <td>{makeAddressText(order.pub_key)}</td>
            </tr>
            <tr>
              <td>Settlement Account Balance</td>
              <td>{`${order.account_balance} ${cryptoUnits}`}</td>
            </tr>
            <tr>
              <td>Transfer Total</td>
              <td title="The total amount that was transferred to the treasury address">
                {order.transfer_total ? `${order.transfer_total} ${cryptoUnits}` : ''}
              </td>
            </tr>
            <tr>
              <td>Confirmed Date</td>
              <td>{order.transfer_confirm_date ? new Date(order.transfer_confirm_date).toLocaleString('en-US') : ''}</td>
            </tr>
            <tr>
              <td>Transfer TX</td>
              <td title="the id for the transaction that transferred to the treasury">{makeTransactionTX(order.transfer_tx)}</td>
            </tr>
            <tr>
              <td>Status</td>
              <td>{order.status.toUpperCase()}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div className="section">
        {
          order.payments.length > 0 ? (
            <div>
              <label htmlFor="payments">Settlement Account History</label>
              <table name="payments">
                <thead>
                  <tr>
                    <td>Date</td>
                    <td>Balance</td>
                  </tr>
                </thead>
                <tbody>
                  {order.payments.map((payment, index) => {
                    return (
                      <tr key={index}>
                        <td>{new Date(payment.date_created).toLocaleString('en-US')}</td>
                        <td>{`${payment.balance} ${cryptoUnits}`}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          ) : "Settlement Account History is empty"
        }
      </div>
      <div className="section">
        {
          order.transfers.length > 0 ? (
            <div>
              <label htmlFor="transfers">Transfer History</label>
              <table name="transfers">
                <thead>
                  <tr>
                    <td>Date</td>
                    <td>Confirmed</td>
                    <td>Amount</td>
                    <td>TX</td>
                    <td>Block</td>
                    <td>From</td>
                    <td>To</td>
                    <td>Type</td>
                  </tr>
                </thead>
                <tbody>
                  {order.transfers.map((transfer, index) => {
                    return (
                      <tr key={index} className={transfer.valid ? '' : 'invalid-row'}>
                        <td>{new Date(transfer.date_created).toLocaleString('en-US')}</td>
                        <td>{transfer.date_confirmed ? new Date(transfer.date_confirmed).toLocaleString('en-US') : ''}</td>
                        <td>{`${transfer.amount} ${cryptoUnits}`}</td>
                        <td>{makeTransactionTX(transfer.tx)}</td>
                        <td>{makeBlockLink(transfer.block)}</td>
                        <td>{makeAddressText(transfer.from_address)}</td>
                        <td>{makeAddressText(transfer.to_address)}</td>
                        <td>{transfer.type ? transfer.type : ''}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          ) : "Transfer History is empty"
        }
      </div>
    </div>
  );
}

export default Orders;
