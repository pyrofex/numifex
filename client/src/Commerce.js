import React from 'react';
import './App.css';
import WooCommerce from './modules/WooCommerce.js';
import Magento from './modules/Magento.js';
import Custom from './modules/CustomCommerce.js';

function Commerce(props) {
    const {access, invalidateAccess} = props;
    return (
        <div className="commerce-list">
            <WooCommerce access={access} invalidateAccess={invalidateAccess} />
            <Magento access={access} invalidateAccess={invalidateAccess} />
            <Custom access={access} invalidateAccess={invalidateAccess} />
        </div>
    );
}

export default Commerce;
