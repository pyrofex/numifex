import React from 'react';
import './App.css';

function Tabs (props) {
  function handleClick() {
    props.onTabClick(props.tab);
  }
  return (
    <button className={"menuTab" + (props.isSelected ? " selected-tab" : "")} id={props.tab} onClick={handleClick}>
        {props.tab}
    </button>
  );
}

export default Tabs;
