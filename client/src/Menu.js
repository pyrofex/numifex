import logo from './public/logo.png';
import Tabs from './Tabs.js';
import React from 'react';
import './App.css';

function Menu (props) {
  return (
    <div id="menu">
      <div id="logo"><img src={logo} alt="Numifex"></img></div>
      <div className="tabs">
      {props.tabs.map(tab => {
        return (<Tabs tab={tab} key={tab} onTabClick={props.handleTabClick} isSelected={tab === props.tab} />);
      })}
      </div>
      <div className="menu-right-container">
        <a className="acknowledgement" href="license_disclaimer.txt" target="_blank">Acknowledgements</a>
        <button className="btn logout-btn" onClick={props.logout}>Logout</button>
      </div>
    </div>
  )
}

export default Menu;
