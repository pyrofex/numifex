import Tabs from './Tabs.js';
import Orders from './Orders.js';
import Cryptos from './Cryptos.js';
import Commerce from './Commerce.js';
import API from './API.js';
import Config from './Config.js';
import React from 'react';
import './App.css';

function DisplayContainer (props) {
  const {tab, tabs, access, invalidateAccess} = props;
  switch (tab) {
    case 'orders':
      return (
        <div className="viewWrapper" id={props.tab + "Wrapper"} >
          <Orders access={access} invalidateAccess={invalidateAccess} />
        </div>
      );
    case 'cryptos':
      return (
        <div className="viewWrapper" id={props.tab + "Wrapper"}>
          <Cryptos access={access} invalidateAccess={invalidateAccess} />
        </div>
      );
    case 'commerce':
      return (
        <div className="viewWrapper" id={props.tab + "Wrapper"}>
          <Commerce access={access} invalidateAccess={invalidateAccess} />
        </div>
      );
    case 'api':
      return (
        <div className="viewWrapper" id={props.tab + "Wrapper"}>
          <API access={access} invalidateAccess={invalidateAccess} />
        </div>
      );
    case 'config':
      return (
        <div className="viewWrapper" id={props.tab + "Wrapper"}>
          <Config access={access} invalidateAccess={invalidateAccess} />
        </div>
      );
    default: // just list all the tabs if you somehow selected a non-existant tab
      return (
        <div>
          <Tabs tabs={tabs} />
        </div>
      );
  }
}

export default DisplayContainer;
