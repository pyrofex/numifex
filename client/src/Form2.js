import React from 'react';
import './App.css';
import axios from 'axios';


class Form2 extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      value1: '',
      current1: '',
      value2: '',
      current2: ''
    };
    this.handleChange1 = this.handleChange1.bind(this);
    this.handleChange2 = this.handleChange2.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.access = props.access;
    this.invalidateAccess = props.invalidateAccess;
  }

  componentDidMount() {
    axios.get(`/config-fetch/${this.props.category1}/${this.props.name_target1}/${this.props.name1}`,
      this.access).then(response => {
        this.setState({current1: response.data.response});
    }).catch(err => {
      console.log(err);
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
    axios.get(`/config-fetch/${this.props.category2}/${this.props.name_target2}/${this.props.name2}`,
      this.access).then(response => {
        this.setState({current2: response.data.response});
    }).catch(err => {
      console.log(err);
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
  }
  
  handleChange1(event) {
    this.setState({value1: event.target.value});
  }

  handleChange2(event) {
    this.setState({value2: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();

    axios.post('/config-submit', {
      category: this.props.category1,
      target: this.props.name_target1,
      key: this.props.name1,
      value: this.state.value1
    }, this.access).then(response => {
      axios.get(`/config-fetch/${this.props.category1}/${this.props.name_target1}/${this.props.name1}`,
        this.access).then(response => {
          this.setState({current1: response.data.response});
          this.setState({value1: ''});
      });
    }).catch(err => {
      console.log(err);
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
    axios.post('/config-submit', {
        category: this.props.category2,
        target: this.props.name_target2,
        key: this.props.name2,
        value: this.state.value2
      }, this.access).then(response => {
        axios.get(`/config-fetch/${this.props.category2}/${this.props.name_target2}/${this.props.name2}`,
          this.access).then(response => {
            this.setState({current2: response.data.response});
            this.setState({value2: ''});
        });
      }).catch(err => {
        console.log(err);
        if (err['response']['status'] === 401) {
          this.invalidateAccess();
        }
      });
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit} className="configForm">
          <p className="section-header">{this.props.desc}</p>
          <p><label>Current {this.props.label1}:</label><span className="form-value">{this.state.current1}</span>
          {this.props.units1 ? (<span className="form-units">{this.props.units1}</span>) : ''}</p>
          <p><label>Current {this.props.label2}:</label><span className="form-value">{this.state.current2}</span>
          {this.props.units2 ? (<span className="form-units">{this.props.units2}</span>) : ''}</p>
          <input type="text" value={this.state.value1} onChange={this.handleChange1}
          placeholder={this.props.example1} minLength={this.props.minLength1} maxLength={this.props.maxLength1} pattern={this.props.pattern1} title={this.props.title1}/>
          <input type="text" value={this.state.value2} onChange={this.handleChange2}
          placeholder={this.props.example2} minLength={this.props.minLength2} maxLength={this.props.maxLength2} pattern={this.props.pattern2} title={this.props.title2}/>
          <input type="submit" value="Submit" className="btn"/>
      </form>
    );
  }
}

export default Form2;
