import React from 'react';
import './App.css';
import axios from 'axios';

class API extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clientKeys: [],
      newClient: null,
      customSecret: '',
      createErrorMessage: ''
    };

    this.refreshClients = this.refreshClients.bind(this);
    this.createClient = this.createClient.bind(this);
    this.deleteClient = this.deleteClient.bind(this);
    this.revokeClient = this.revokeClient.bind(this);
    this.nicknameClient = this.nicknameClient.bind(this);
    this.closeNewClientMessage = this.closeNewClientMessage.bind(this);
    this.setClientSecretInput = this.setClientSecretInput.bind(this);
    this.copyNewClientToClipboard = this.copyNewClientToClipboard.bind(this);
    this.handleSecretChange = this.handleSecretChange.bind(this);
    this.access = props.access;
    this.invalidateAccess = props.invalidateAccess;
  }

  componentDidMount() {
    this.refreshClients();
  }

  refreshClients() {
    axios.get('/api-clients/list', this.access).then(res => {
      this.setState({clientKeys: res.data});
    }).catch(error => {
      console.error(error);
      if (error['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
  }

  createClient() {
    axios.post('/api-clients/create', {
      custom_secret: this.state.customSecret
    }, this.access).then(res => {
      this.setState({newClient: res.data, customSecret: ''});
    }).then(this.refreshClients).catch(error => {
      console.error(error);
      if (error['response']['status'] === 401) {
        this.invalidateAccess();
      } else {
        this.setState({ createErrorMessage: error.response.data.error })
      }
    });
  }

  deleteClient(id) {
    axios.post('/api-clients/delete', {
      id: id
    }, this.access).then(this.refreshClients).catch(error => {
      console.error(error);
      if (error['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
  }

  revokeClient(id) {
    axios.post('/api-clients/revoke', {
      id: id
    }, this.access).catch(error => {
      console.error(error);
      if (error['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
  }

  nicknameClient(id) {
    const nickname = window.prompt(`New nickname for client with id: ${id}`);
    if (nickname == null) {
      return;
    }

    axios.post('/api-clients/nickname', {
      id: id,
      nickname: nickname
    }, this.access).then(this.refreshClients).catch(error => {
      console.error(error);
      if (error['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
  }

  closeNewClientMessage() {
    this.setState({newClient: null});
  }

  setClientSecretInput(input) {
    this.clientSecretInput = input;
  }

  copyNewClientToClipboard(e) {
    this.clientSecretInput.select();
    document.execCommand('copy');
    e.target.focus();
  }

  handleSecretChange(event) {
    this.setState({customSecret: event.target.value});
  }

  render() {
    if (this.state.newClient) {
      return (
        <NewClient newclient={this.state.newClient} close={this.closeNewClientMessage} setClientSecretInput={this.setClientSecretInput} copyToClipboard={this.copyNewClientToClipboard} />
      );
    } else {
      return (
        <div className="card api-card">
          <div className="section">
            <h4>Create new Client</h4>
            <div className="error-message">{this.state.createErrorMessage}</div>
            <button className="btn" onClick={this.createClient}>Create</button>
            <input type="password" placeholder="Custom Secret (optional)" value={this.state.customSecret} onChange={this.handleSecretChange}></input>
          </div>
          <div className="section">
            <h4>API Clients</h4>
            <table>
              <thead>
                <tr>
                  <th>Delete Client</th>
                  <th>ID</th>
                  <th>Nickname</th>
                  <th>Latest Token Authorization</th>
                  <th>Revoke Tokens</th>
                </tr>
              </thead>
              <tbody>
                {this.state.clientKeys.map(clientKey => {
                  return (
                    <tr key={clientKey.id}>
                      <td><button className="btn" onClick={this.deleteClient.bind(null, clientKey.id)}>Delete</button></td>
                      <td>{clientKey.id}</td>
                      <td onClick={this.nicknameClient.bind(null, clientKey.id)}>{clientKey.nickname ? clientKey.nickname : ''}</td>
                      <td>{
                        new Date(clientKey.token_timestamp).toLocaleString('en-US')
                      }
                      </td>
                      <td><button className="btn" title="force all users of this token to login again." onClick={this.revokeClient.bind(null, clientKey.id)}>Revoke</button></td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      );
    }
  }
}

function NewClient(props) {
  return (
    <div className="card api-new-client-card">
      <b>Your new API Client</b>
      <div className="section">
        <p>You will not be able to view this secret again once you close this message, so be sure to record it securely.</p>
        <div>
          <label htmlFor="client_id">Client ID:</label>
          <input type="text" id="client_id" name="client_id" value={props.newclient.client_id} readOnly />
        </div>
        <div>
          <label htmlFor="client_secret">Client Secret:</label>
          <input type="text" id="client_secret" name="client_secret" value={props.newclient.client_secret} ref={props.setClientSecretInput} readOnly />
        </div>
      </div>
      <div className="section">
        <button className="btn" onClick={props.copyToClipboard}>Copy Secret To Clipboard</button>
        <button className="btn" onClick={props.close}>Done</button>
      </div>
    </div>
  );
}

export default API;
