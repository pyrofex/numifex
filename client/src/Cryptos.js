import React from 'react';
import './App.css';
import ETH from './modules/ETH.js';
import BTC from './modules/BTC.js';

function Cryptos(props) {
    const {access, invalidateAccess} = props;
    return (
        <div className="crypto-list">
            <ETH access={access} invalidateAccess={invalidateAccess} />
            <BTC access={access} invalidateAccess={invalidateAccess} />
        </div>
    );
}

export default Cryptos;
