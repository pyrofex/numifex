import bitcoinLogo from '../public/bitcoin-logo.png';
import React from 'react';
import '../App.css';
import axios from 'axios';
import Form4 from '../Form4.js';
import NodeUrlForm from '../NodeUrlForm';

class BTC extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      expanded: false,
      value: '',
      network_fee_rate: '',
      fee_check: '',
      fee_setting: '0',
      fee_choice: 'the network',
      fee_rate: '',
      error: ''
    };
    this.collapseSelf = this.collapseSelf.bind(this);
    this.handleCheckChange = this.handleCheckChange.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.access = props.access;
    this.invalidateAccess = props.invalidateAccess;
  }

  componentDidMount() {
    axios.get('/btc-network-fee-rate', this.access).then(response => {
      if (response.data.fee_rate_satoshi !== null) {
        this.setState({network_fee_rate: response.data.fee_rate_satoshi});
      } else {
        this.setState({network_fee_rate: 'unknown'});
      }
      axios.get('/btc-fee-rate-fetch', this.access).then(response => {
        console.log('fee rate', response.data.fee_rate_satoshi);
        if (response.data.fee_rate_satoshi == null) {
            this.setState({fee_check: '', fee_rate: this.state.network_fee_rate});
        } else {
          this.setState({fee_check: 'checked', fee_setting: response.data.fee_rate_satoshi, 
            fee_choice: 'a custom', fee_rate: response.data.fee_rate_satoshi});
        }
      }).catch(err => {
        console.log(err);
        if (err.response.data.error) {
          this.setState({error: err.response.data.error})
        } else {
          this.setState({error: err.response.status + " " + err.response.statusText});
        }
        if (err['response']['status'] === 401) {
          this.invalidateAccess();
        }
      });
    }).catch(err => {
      console.log(err);
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
  }

  collapseSelf(event) {
    this.setState({expanded: !this.state.expanded});
  }

  handleCheckChange(event) {
    var check = event.target;
    if (check.checked === true) {
      this.setState({fee_check: 'checked'});
    } else {
      this.setState({fee_check: '', fee_choice: 'the network', 
        fee_rate: this.state.network_fee_rate, fee_setting: '0'});
      axios.post('/config-submit', {
        category: 'currency',
        target: 'BTC',
        key: 'btc_fee_rate' // no value sent, so it will become NULL
      }, this.access).catch(err => {
        console.log(err);
        this.invalidateAccess();
      });
    }
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();

    axios.post('/btc-fee-rate-submit', {
      'fee_rate_satoshi': this.state.value
    }, this.access).then(response => {
      return axios.get('/btc-fee-rate-fetch', this.access);
    }).then(response => {
      if (response.data.fee_rate_satoshi == null) {
        this.setState({fee_check: '', fee_setting: '0', 
          value: '', fee_choice: 'the network', fee_rate: this.state.network_fee_rate});
      } else {
        this.setState({fee_check: 'checked', fee_setting: response.data.fee_rate_satoshi, 
          value: '', fee_choice: 'a custom', fee_rate: response.data.fee_rate_satoshi});
      }
    }).catch(err => {
      console.log(err);
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
  }

  render() {
    if(!this.state.expanded) {
      return (
        <div className="card" onClick={this.collapseSelf}>
          <div className="header section">
            <img src={bitcoinLogo} alt="bitcoin"></img>
          </div>
          click to expand
        </div>
      );
    } else {
      const fee_check = this.state.fee_check;
      return (
        <div className="card">
          <div className="header section" onClick={this.collapseSelf}>
            <img src={bitcoinLogo} alt="bitcoin"></img>
          </div>
          
          <div className="section top-section">
            <p className="section-header">Fee Rate</p>
            <div>
              <form>
                <label>Set Custom Fee Rate
                  <input type="checkbox" id="fee_check" 
                    checked={fee_check} onChange={this.handleCheckChange}>
                  </input>
                </label>
              </form>
              <form onSubmit={this.handleSubmit} className={fee_check ? "configForm" : "configForm hidden"} id={fee_check}>
                <div className="feeRate">
                  <label>Current Custom Fee Rate:</label>
                  <p className="form-value">{this.state.fee_setting}</p>
                </div>
                <label>Enter your new fee rate:</label><br></br>
                <input type="text" value={this.state.value} onChange={this.handleChange}
                  required pattern="[0-9]+"
                  title="Please enter an integer with no more than 8 digits." placeholder="example: 72" maxLength="10"/>
                <input type="submit" value="Submit" className="btn"/>
              </form>
            </div>
            <div className="top-section-right">
              <p>You are using {this.state.fee_choice} fee rate.</p>
              <p className="error">{this.state.error}</p>
              <h1>{this.state.fee_rate}<span> satoshi/Byte</span></h1>
            </div>
          </div>
          
          <div className="section">
            <Form4 name="fee_estimate_block_target" category="currency" name_target="BTC"
              desc="(Estimate fee based on the number of blocks to wait before starting the transaction. Each block is about 10 minutes. Does not apply if you set a custom fee rate)."
              units="blocks" example="example: 12" pattern="[0-9]+"
              title="Please enter a positive integer or zero e.g. 12"
              access={this.access} invalidateAccess={this.invalidateAccess} />
          </div>
         <div className="section">
            <Form4 name="treasury" category="currency" name_target="BTC"
              desc="Treasury Account (your Bitcoin address where funds will be deposited)."
              example="example: 3J98t1WpEZ73CNmQviecrnyiWrnqXXXXXX"
              minLength="27" pattern="[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]*"
              title="Make sure your treasury is a mainnet BTC address (starts with 1 or 3)."
              access={this.access} invalidateAccess={this.invalidateAccess} />
          </div>
          <div className="section">
            <NodeUrlForm name1="node_url" name2="rpc_user" name3="rpc_password" category="currency" name_target="BTC"
              desc1="URL for Bitcoin Core node. Specifying a port is optional. The default port is 8332."
              desc2="PC Credentials (the username and password used to access your bitcoin-core node)."
              example="example: http://localhost:8332" 
              title="Please use a valid web address e.g. https://mybtcnode.com"
              access={this.access} invalidateAccess={this.invalidateAccess} checkEthNode={this.checkEthNode}/>
          </div>
          <div className="section">
            <Form4 name="minimum_confirmations" category="currency" name_target="BTC"
              desc="Minimum confirmations required to consider a transaction safe to use."
              units="confirmations" pattern="[0-9]+"
              title="Please enter a positive integer or zero e.g. 6"
              access={this.access} invalidateAccess={this.invalidateAccess} />
          </div>
        </div>
      );
    }
  }
}

export default BTC;
