import React from 'react';
import '../App.css';
import Form from '../Form.js';

class CustomCommerce extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false
    };
    this.collapseSelf = this.collapseSelf.bind(this);
  }

  collapseSelf(event) {
    this.setState({expanded: !this.state.expanded});
  }

  render() {
    const {access, invalidateAccess} = this.props;
    if(!this.state.expanded) {
      return (
        <div className="card" onClick={this.collapseSelf}>
          <div className="header section large-text">
            Custom
          </div>
          click to expand
        </div>
      );
    } else {
      return (
        <div className="card">
          <div className="header section large-text" onClick={this.collapseSelf}>
            Custom
          </div>
          <div className="section">
            <Form name="url" category="commerce" name_target="custom"
              desc="The URL for your custom commerce integration."
              example="example: http://mywebsite.com/api/numifex" pattern="https?://.+"
              access={access} invalidateAccess={invalidateAccess} />
          </div>
        </div>
      );
    }
  }
}

export default CustomCommerce;
