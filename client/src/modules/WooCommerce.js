import woocommerceLogo from '../public/woocommerce-logo.png';
import React from 'react';
import '../App.css';
import Form from '../Form.js';
import Form2 from '../Form2.js';

class WooCommerce extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false
    };
    this.collapseSelf = this.collapseSelf.bind(this);
  }

  collapseSelf(event) {
    this.setState({expanded: !this.state.expanded});
  }

  render() {
    const {access, invalidateAccess} = this.props;
    if(!this.state.expanded) {
      return (
        <div className="card" onClick={this.collapseSelf}>
          <div className="header section">
            <img src={woocommerceLogo} alt="WOOCOMMERCE"></img>
          </div>
          click to expand
        </div>
      );
    } else {
      return (
        <div className="card">
          <div className="header section" onClick={this.collapseSelf}>
            <img src={woocommerceLogo} alt="WOOCOMMERCE"></img>
          </div>
          <div className="section">
            <Form2 desc="The API Consumer Key and Consumer Secret for your WooCommerce site."
              name1="wordpress_user" category1="commerce" name_target1="woocommerce"
              label1="Key" example1="example: ck_3213afdbb94122610485d6827db6c8XXXXXXXXXX"
              pattern1="[a-zA-Z0-9_]+" minLength1="43" maxLength1="43"
              name2="wordpress_pass" category2="commerce" name_target2="woocommerce"
              example2="example: cs_651af653181c03bb1932356fabb68aXXXXXXXXXX"
              pattern2="[a-zA-Z0-9_]+" minLength2="43" maxLength2="43"
              access={access} invalidateAccess={invalidateAccess} />
          </div>
          <div className="section">
            <Form name="wordpress_url" category="commerce" name_target="woocommerce"
              desc="The URL for your Wordpress/WooCommerce site. "
              example="example: https://mysite.com" pattern="https?://.+"
              title="Please use a valid web address e.g. https://mywordpress.com or http://127.0.0.1"
              access={access} invalidateAccess={invalidateAccess} />
          </div>
        </div>
      );
    }
  }
}

export default WooCommerce;
