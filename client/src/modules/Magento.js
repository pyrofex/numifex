import magentoLogo from '../public/magento-logo.png';
import React from 'react';
import '../App.css';
import Form from '../Form.js';

class Magento extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false
    };
    this.collapseSelf = this.collapseSelf.bind(this);
  }

  collapseSelf(event) {
    this.setState({expanded: !this.state.expanded});
  }

  render() {
    const {access, invalidateAccess} = this.props;
    if(!this.state.expanded) {
      return (
        <div className="card" onClick={this.collapseSelf}>
          <div className="header section">
            <img src={magentoLogo} alt="MAGENTO"></img>
          </div>
          click to expand
        </div>
      );
    } else {
      return (
        <div className="card">
          <div className="header section" onClick={this.collapseSelf}>
            <img src={magentoLogo} alt="MAGENTO"></img>
          </div>
          <div className="section">
            <Form name="magento_token" category="commerce" name_target="magento"
              desc="The access token for you Magento site."
              example="example: b23ai50uffmm1yl2uy5mkiXXXXXXXXXX"
              pattern="[a-zA-Z0-9]+" minLength="32" maxLength="32"
              access={access} invalidateAccess={invalidateAccess} />
          </div>
          <div className="section">
            <Form name="magento_url" category="commerce" name_target="magento"
              desc="The URL for your Magento site. "
              example="example: https://mysite.com" pattern="https?://.+"
              title="Please use a valid web address e.g. https://mysite.com or http://127.0.0.1"
              access={access} invalidateAccess={invalidateAccess} />
          </div>
        </div>
      );
    }
  }
}

export default Magento;
