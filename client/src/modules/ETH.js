import ethereumLogo from '../public/ethereum-logo.png';
import React from 'react';
import '../App.css';
import axios from 'axios';
import Form3 from '../Form3.js';
import Form4 from '../Form4.js';


class ETH extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      expanded: false,
      gas_choice: 'average',
      gas_price_custom: '',
      gas_price_low: '',
      gas_price_average: '',
      gas_price_high: '',
      gas_price_market: '',
      gas_price_custom_value: '',
      ethNodeUrl: '',
      error: ''
    };
    this.checkEthNode = this.checkEthNode.bind(this);
    this.collapseSelf = this.collapseSelf.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleGasSelectionChange = this.handleGasSelectionChange.bind(this);
    this.access = props.access;
    this.invalidateAccess = props.invalidateAccess;
  }

  componentDidMount() {
    axios.get('/config-fetch/currency/ETH/gas_price_selection', this.access).then(response => {
      this.setState({gas_choice: response.data.response});
    });
    axios.get('/eth-gas-price-fetch', this.access).then(response => {
      let gas_price = response.data.gas_price_gwei;
      if(gas_price == null) {
        gas_price = '0.0';
      }
      this.setState({gas_price_custom: gas_price, gas_price_custom_value: gas_price});
    }).catch(err => {
      console.log(err);
      if (err.response.data.error) {
        this.setState({error: err.response.data.error})
      } else {
        this.setState({error: err.response.status + " " + err.response.statusText});
      }
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
    axios.get('/eth-gas-price-levels', this.access).then(response => {
      const gas_prices = response.data.gas_prices_gwei;
      this.setState({gas_price_low: gas_prices.low, gas_price_average: gas_prices.average, gas_price_high: gas_prices.high});
    }).catch(err => {
      console.log(err);
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
    axios.get('/eth-node-gas-price', this.access).then(response => {
      this.setState({gas_price_market: response.data.gas_price_gwei});
    }).catch(err => {
      console.log(err);
      if (err.response.data.error) {
        this.setState({error: err.response.data.error})
      } else {
        this.setState({error: err.response.status + " " + err.response.statusText});
      }
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
  }

  collapseSelf(event) {
    this.setState({expanded: !this.state.expanded});
  }

  handleGasSelectionChange(event) {
    var value = event.target.value;
    this.setState({gas_choice: value});
    axios.post('/config-submit', {
      category: 'currency',
      target: 'ETH',
      key: 'gas_price_selection',
      value: value
    }, this.access).catch(err => {
      console.log(err);
      if(err.response.status === 401) {
        this.invalidateAccess();
      }
    });
  }

  handleChange(event) {
    this.setState({gas_price_custom_value: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();

    axios.post('/eth-gas-price-submit', {
      'gas_price_gwei': this.state.gas_price_custom_value
    }, this.access).then(() => {
      return axios.get('/eth-gas-price-fetch', this.access);
    }).then(response => {
      let gas_price = response.data.gas_price_gwei;
      if(gas_price == null) {
        gas_price = '0.0';
      }
      this.setState({gas_price_custom: gas_price});
    }).catch(err => {
      console.log(err);
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
  }

  checkEthNode(link) {
    this.setState({ethNodeUrl: link});
  }
  
  render() {
    if (!this.state.expanded) {
      return (
        <div className="card" onClick={this.collapseSelf}>
          <div className="header section">
              <img src={ethereumLogo} alt="ethereum"></img>
          </div>
          click to expand
        </div>
      );
    } else {
      
      
      const {
        gas_choice,
        gas_price_custom,
        gas_price_low,
        gas_price_average,
        gas_price_high,
        gas_price_market,
        gas_price_custom_value
      } = this.state;
      const gas_price_current = {
        custom: gas_price_custom,
        low: gas_price_low,
        average: gas_price_average,
        high: gas_price_high,
        market: gas_price_market
      }[gas_choice];
      return (
        <div className="card">
          <div className="header section" onClick={this.collapseSelf}>
              <img src={ethereumLogo} alt="ethereum"></img>
          </div>

          <div className="section top-section">
            <p className="section-header">Gas Price</p>
            <div>
              <label>Select Gas Price:
                <select id="gas_choice" value={gas_choice} onChange={this.handleGasSelectionChange}>
                  <option value="custom">Custom</option>
                  <option value="low">Low</option>
                  <option value="average">Average</option>
                  <option value="high">High</option>
                  <option value="market">Market</option>
                </select>
              </label>
              {
                gas_choice === 'custom' ?
                (<form onSubmit={this.handleSubmit} className="configForm">
                  <div className="gasPrice">
                    <label>Current Custom Gas Price:</label>
                    <p className="form-value">{gas_price_custom}</p>
                  </div>
                  <label>Enter your new gas price:</label><br></br>
                  <input type="text" value={gas_price_custom_value} onChange={this.handleChange}
                    required pattern="[0-9]+.?[0-9]*"
                    title="Please enter a number with no more than 8 digits." placeholder="example: 7.2" maxLength="10"/>
                  <input type="submit" value="Submit" className="btn"/>
                </form>) : ''
              }
            </div>
            <div className="top-section-right">
              <p>You are using {gas_choice} gas price.</p>
              <p className="error">{this.state.error}</p>
              <h1>{gas_price_current}<span> gwei</span></h1>
            </div>
          </div>
          
          <div className="section">
            <Form3 name1="eth_node" name2="eth_node_auth_user" name3="eth_node_auth_pass" category="currency" name_target="ETH"
              desc1="The URL for your Ethereum Node."
              desc2="The username and password for your Ethereum Node (This is not required if you're using the Axia Codex node)."
              example="example: https://ethnode.url.com" 
              title="Please use a valid web address e.g. https://myethnode.com"
              access={this.access} invalidateAccess={this.invalidateAccess} checkEthNode={this.checkEthNode}/>
          </div>
          <div className="section">
            <Form4 name="treasury" category="currency" name_target="ETH"
              desc="Treasury Account (your Ethereum address where funds will be deposited)."
              minLength="42" maxLength="42" pattern="[a-zA-Z0-9]+"
              access={this.access} invalidateAccess={this.invalidateAccess} />
          </div>
          <div className="section">
            <Form4 name="minimum_confirmations" category="currency" name_target="ETH"
              desc="Minimum Confirmations (how many blocks to wait before considering a transaction confirmed.)"
              units="confirmations" pattern="[0-9]+"
              access={this.access} invalidateAccess={this.invalidateAccess} />
          </div>
        </div>
      );
    }
  }
}

export default ETH;
