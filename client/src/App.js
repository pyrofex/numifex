import Login from './Login.js';
import DisplayContainer from './DisplayContainer.js';
import Menu from './Menu.js';
import React from 'react';
import './App.css';
import Cookies from 'js-cookie';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      access: null,
      tab: 'orders'
    };

    this.handleTabClick = this.handleTabClick.bind(this);
    this.setAccessToken = this.setAccessToken.bind(this);
    this.invalidateAccess = this.setAccessToken.bind(this, null);
    this.tabs = ['orders', 'cryptos', 'commerce', 'api', 'config'];
    
    const lastUsedTab = Cookies.get('admin_tab');
    if (this.tabs.includes(lastUsedTab)) {
      this.state.tab = lastUsedTab;
    }

    const startToken = Cookies.get('admin_access_token');
    if (startToken) {
      this.state.access = {
        headers: {
          Authorization: 'Bearer ' + startToken
        }
      };
    }
  }

  handleTabClick(tab) {
    this.setState({tab: tab});
    Cookies.set('admin_tab', tab);
  }

  setAccessToken(access_token) {
    if (access_token) {
      this.setState({
        access: {
          headers: {
            Authorization: 'Bearer ' + access_token
          }
        }
      });
      Cookies.set('admin_access_token', access_token);
    } else {
      this.setState({access: null});
      Cookies.remove('admin_access_token');
    }
  }

  render() {
    if (this.state.access) {
      return (
        <div>
          <Menu tab={this.state.tab} tabs={this.tabs} 
            handleTabClick={this.handleTabClick} logout={this.invalidateAccess}/>
          <DisplayContainer tab={this.state.tab} tabs={this.tabs}
            access={this.state.access} invalidateAccess={this.invalidateAccess} />
        </div>
      );
    } else {
      return (
        <Login setAccessToken={this.setAccessToken} />
      );
    }
  }
}

export default App;
