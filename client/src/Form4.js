import React from 'react';
import './App.css';
import axios from 'axios';


class Form extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      value: '',
      current: '',
      link: '',
      formState: 'saved',
      error: ''
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.access = props.access;
    this.invalidateAccess = props.invalidateAccess;
  }

  componentDidMount() {
    axios.get(`/config-fetch/${this.props.category}/${this.props.name_target}/${this.props.name}`,
      this.access).then(response => {
        this.setState({value: response.data.response});
    }).catch(err => {
      this.setState({formState: 'error'});
      if (err.response.data.error) {
        this.setState({error: err.response.data.error})
      } else {
        this.setState({error: err.response.status + " " + err.response.statusText});
      }
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
  }
  
  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleEdit(event) {
      event.preventDefault();
      this.setState({formState: 'editing'});
  }

  handleSubmit(event) {
    event.preventDefault();
    axios.post('/config-submit', {
      category: this.props.category,
      target: this.props.name_target,
      key: this.props.name,
      value: this.state.value
    }, this.access).then(response => {
      axios.get(`/config-fetch/${this.props.category}/${this.props.name_target}/${this.props.name}`,
        this.access).then(response => {
          if (response['status'] === 200) {
            this.setState({current: response.data.response});
            if (this.props.name === "eth_node") {
              this.props.checkEthNode(response.data.response);
            };
            this.setState({formState: 'saved'});
            }
      });
    }).catch(err => {
      this.setState({formState: 'error'});
      if (err.response.data.error) {
        this.setState({error: err.response.data.error})
      } else {
        this.setState({error: err.response.status + " " + err.response.statusText});
      }
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
  }

  render() {
    if (this.state.formState === "saved") {
        return (
            <form onSubmit={this.handleEdit} className="configForm">
                <p className="section-header">{this.props.desc}</p>
                <p><span className="form-value">{this.state.value}</span>
                {this.props.units ? (<span className="form-units">{this.props.units}</span>) : ''}</p>
                <input type="submit" value="Edit" className="btn"/>
            </form>
        );
    } else if (this.state.formState === "editing") {
        return (
            <form onSubmit={this.handleSubmit} className="configForm expanded">
                <p className="section-header">{this.props.desc}</p>
                <input type="text" value={this.state.value} onChange={this.handleChange} 
                placeholder={this.props.example} required minLength={this.props.minLength} maxLength={this.props.maxLength} pattern={this.props.pattern} title={this.props.title} link={this.state.link}/>
                <input type="submit" value="Save" className="btn"/>
            </form>
          );
    } else if (this.state.formState === "error") {
      return (
        <form onSubmit={this.handleSubmit} className="configForm expanded">
            <p className="section-header">{this.props.desc}</p>
            <p className="error">{this.state.error}</p>
            <input type="text" value={this.state.value} onChange={this.handleChange} 
            placeholder={this.props.example} required minLength={this.props.minLength} maxLength={this.props.maxLength} pattern={this.props.pattern} title={this.props.title} link={this.state.link}/>
            <input type="submit" value="Save" className="btn"/>
        </form>
      );
    }
    
  }
}

export default Form;
