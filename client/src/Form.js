import React from 'react';
import './App.css';
import axios from 'axios';


class Form extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      value: '',
      current: '',
      link: ''
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.access = props.access;
    this.invalidateAccess = props.invalidateAccess;
  }

  componentDidMount() {
    axios.get(`/config-fetch/${this.props.category}/${this.props.name_target}/${this.props.name}`,
      this.access).then(response => {
        this.setState({current: response.data.response});
        if (this.props.name === "eth_node") {
          this.props.checkEthNode(response.data.response);
        };
    }).catch(err => {
      console.log(err);
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
  }
  
  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();

    axios.post('/config-submit', {
      category: this.props.category,
      target: this.props.name_target,
      key: this.props.name,
      value: this.state.value
    }, this.access).then(response => {
      axios.get(`/config-fetch/${this.props.category}/${this.props.name_target}/${this.props.name}`,
        this.access).then(response => {
          this.setState({current: response.data.response});
          if (this.props.name === "eth_node") {
            this.props.checkEthNode(response.data.response);
          };
          this.setState({value: ''});
      });
    }).catch(err => {
      console.log(err);
      if (err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit} className="configForm">
          <p className="section-header">{this.props.desc}</p>
          <p><label>Current:</label><span className="form-value">{this.state.current}</span>
          {this.props.units ? (<span className="form-units">{this.props.units}</span>) : ''}</p>
          <input type="text" value={this.state.value} onChange={this.handleChange} 
          placeholder={this.props.example} required minLength={this.props.minLength} maxLength={this.props.maxLength} pattern={this.props.pattern} title={this.props.title} link={this.state.link}/>
          <input type="submit" value="Submit" className="btn"/>
      </form>
    );
  }
}

export default Form;
