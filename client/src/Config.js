import axiaCodexLogo from './public/axia-codex-logo.png';
import React from 'react';
import './App.css';
import axios from 'axios';
import Form from './Form.js';

class Config extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        username: '',
        password: '',
        newUsername: '',
        newPassword: '',
        confirmNewPassword: '',
        invalidSubmit: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.access = props.access;
    this.invalidateAccess = props.invalidateAccess;
  }

  componentDidMount() {
    this.setState({invalidSubmit: ''});
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleSubmit(e) {
    e.preventDefault();

    if (!this.state.newUsername) {
      this.setState({invalidSubmit: 'Missing new username'});
      return;
    }

    if (!this.state.newPassword) {
      this.setState({invalidSubmit: 'Missing new password'});
      return;
    }

    if (this.state.newPassword !== this.state.confirmNewPassword) {
      this.setState({invalidSubmit: 'New password confirmation field does not match new password field'});
      return;
    }

    axios.post('/admin/update', {
      username: this.state.username,
      password: this.state.password,
      new_username: this.state.newUsername,
      new_password: this.state.newPassword
    }).then(res => {
      this.setState({invalidSubmit: ''});
      this.invalidateAccess();
    }).catch(err => {
      if (err && err.response) {
        if (err.response.status === 401) {
          this.setState({invalidSubmit: 'Invalid Username or Password'});
        } else if (err.response.status === 400) {
          this.setState({invalidSubmit: 'One or more fields are missing'});
        } else {
          this.setState({invalidSubmit: 'There was a server error, try again at a later time'});
        }
      }
    });
  }

  render() {
    return (
      <div>
        <div className="card" id="adminCard">
          <form className="configForm" id="admin" onSubmit={this.handleSubmit}>
            <div>
              Change Admin credentials
            </div>
            <div>
              <label htmlFor="username">Username</label>
              <input type="text" name="username" value={this.state.username} onChange={this.handleChange} />
            </div>
            <div>
              <label htmlFor="password">Password</label>
              <input type="password" name="password" value={this.state.password} onChange={this.handleChange} />
            </div>
            <div>
              <label htmlFor="newUsername">New Username</label>
              <input type="text" name="newUsername" value={this.state.newUsername} onChange={this.handleChange} />
            </div>
            <div>
              <label htmlFor="newPassword">New Password</label>
              <input type="password" name="newPassword" value={this.state.newPassword} onChange={this.handleChange} />
            </div>
            <div>
              <label htmlFor="confirmNewPassword">Confirm New Password</label>
              <input type="password" name="confirmNewPassword" value={this.state.confirmNewPassword} onChange={this.handleChange} />
            </div>
            <div>
              <button className="btn">Submit</button>
            </div>
            <div>
              {this.state.invalidSubmit}
            </div>
          </form>
        </div>
        <div className="card">
          <AxiaCodexLogin access={this.access} invalidateAccess={this.invalidateAccess} />
        </div>
        <div className="card">
          <Form name="order_timeout_in_hours" category="default" name_target="default"
            desc="Order timeout in hours (how long an order can still be unpaid before the system automatically cancels it)"
            units="hours" example="example: 168" pattern="[0-9]+"
            access={this.access} invalidateAccess={this.invalidateAccess} />
        </div>
      </div>
    );
  }
}

class AxiaCodexLogin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      autoupdate: true,
      submitMsg: '',
      connected: false,
      connecting: true
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.disconnectAxia = this.disconnectAxia.bind(this);
    this.access = props.access;
    this.invalidateAccess = props.invalidateAccess;
  }

  componentDidMount() {
    this.setState({submitMsg: ''});

    axios.get('/config-fetch/default/default/axiacodex_token', this.access).then(res => {
      this.setState({connecting: false});
      if(res && res.data && res.data.response) { // if the token exists
        this.setState({connected: true});
      } else {
        this.setState({connected: false});
      }
    }).catch(err => {
      if(err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
    axios.get('/config-fetch/default/default/axiacodex_username', this.access).then(res => {
      if(res && res.data) {
        this.setState({username: res.data.response});
      }
    }).catch(err => {
      if(err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
  }

  handleChange(e) {
    const { name, type } = e.target;
    const value = type === 'checkbox' ? e.target.checked : e.target.value;
    this.setState({ [name]: value });
  }

  handleSubmit(e) {
    e.preventDefault();

    if(this.state.connected) {
      this.disconnectAxia();
      return;
    }

    if (!this.state.username) {
      if (!this.state.password) {
        this.setState({submitMsg: 'Missing username and password'});
      } else {
        this.setState({submitMsg: 'Missing username'});
      }
      return;
    }

    if (!this.state.password) {
      this.setState({submitMsg: 'Missing password'});
      return;
    }

    this.setState({submitMsg: ''});
    axios.post('https://portal.axiacodex.io/api/auth', {
      username: this.state.username,
      password: this.state.password
    }).then(res => {
      if(!(res && res.data && res.data.access_token)) {
        this.setState({submitMsg: 'Invalid Username or Password'});
        return;
      }
      const username = this.state.username;
      return axios.post('/config-submit', {
        category: 'default',
        target: 'default',
        key: 'axiacodex_token',
        value: res.data.access_token.trim()
      }, this.access).then(() => {
        this.setState({submitMsg: 'Success!', username: '', password: '', connecting: true});
        axios.get('/config-fetch/default/default/axiacodex_token', this.access).then(res => {
          this.setState({connecting: false});
          if(res && res.data && res.data.response) {
            this.setState({connected: true});
          } else {
            this.setState({connected: false});
          }
        }).catch(err => {
          if(err['response']['status'] === 401) {
            this.invalidateAccess();
          }
        });
        axios.post('/config-submit', {
          category: 'default',
          target: 'default',
          key: 'axiacodex_username',
          value: username
        }, this.access).then(() => {
          return axios.get('/config-fetch/default/default/axiacodex_username', this.access).then(res => {
            if(res && res.data) {
              this.setState({username: res.data.response});
            }
          }).catch(err => {
            if(err['response']['status'] === 401) {
              this.invalidateAccess();
            }
          });
        }).catch(() => {});
        if(this.state.autoupdate) {
          axios.get('https://portal.axiacodex.io/api/get_node_urls').then(({data}) => {
            console.log('got node urls', data);
            let ETH_url, BTC_url;
            for(let i = 0; i < data.length; i++) {
              const node_url = data[i];
              switch(node_url.name) {
                case 'ethereum':
                  ETH_url = node_url.url;
                  break;
                case 'bitcoin':
                  BTC_url = node_url.url;
                  break;
                default:
                  break;
              }
            }
            if(ETH_url) {
              axios.post('/config-submit', {
                category: 'currency',
                target: 'ETH',
                key: 'eth_node',
                value: ETH_url
              }, this.access);
            }
            if(BTC_url) {
              axios.post('/config-submit', {
                category: 'currency',
                target: 'BTC',
                key: 'node_url',
                value: BTC_url
              }, this.access);
              axios.post('/config-submit', {
                category: 'currency',
                target: 'BTC',
                key: 'rpc_user',
                value: ''
              }, this.access);
              axios.post('/config-submit', {
                category: 'currency',
                target: 'BTC',
                key: 'rpc_password',
                value: ''
              }, this.access);
            }
          }).catch(() => {});
        }
      }, err => {
        if(err['response']['status'] === 401) {
          this.invalidateAccess();
        } else {
          this.setState({submitMsg: 'There was a numifex server error, try again at a later time'});
        }
      });
    }, err => {
      if (err && err.response) {
        if (err.response.status === 401) {
          this.setState({submitMsg: 'Invalid Username or Password'});
        } else if (err.response.status === 400) {
          this.setState({submitMsg: 'One or more fields are missing'});
        } else {
          this.setState({submitMsg: 'There was an axiacodex.io server error, try again at a later time'});
        }
      }
    });
  }

  disconnectAxia() {
    axios.post('/config-submit', {
      category: 'default',
      target: 'default',
      key: 'axiacodex_token',
      value: null
    }, this.access).then(() => {
      this.setState({connecting: true, username: '', password: ''});
      axios.get('/config-fetch/default/default/axiacodex_token', this.access).then(res => {
        this.setState({connecting: false});
        if(res && res.data && res.data.response) {
          this.setState({connected: true});
        } else {
          this.setState({connected: false});
        }
      }).catch(err => {
        if(err['response']['status'] === 401) {
          this.invalidateAccess();
        }
      });
    }, err => {
      if(err['response']['status'] === 401) {
        this.invalidateAccess();
      }
    });
    axios.post('/config-submit', {
      category: 'default',
      target: 'default',
      key: 'axiacodex_username',
      value: null
    }, this.access).catch(() => {}); // silently drop failures
  }

  render() {
    const connectMsg = this.state.connecting ? 'Connecting...' : this.state.connected ? 'Connected' : 'Not Connected';
    return (
      <form onSubmit={this.handleSubmit} className="configForm">
        <img src={axiaCodexLogo} alt="AxiaCodex"></img>
        <p className="section-header">Enter your AxiaCodex credentials so that Numifex can connect to AxiaCodex services</p>
        <p>Connection Status: {connectMsg}</p>
        <input type="text" name="username" value={this.state.username} onChange={this.handleChange}
        placeholder="Username" title="AxiaCodex Username" />
        <input type="password" name="password" value={this.state.password} onChange={this.handleChange}
        placeholder="Password" title="AxiaCodex Password" />
        <div>
          <input type="submit" value={this.state.connected ? 'Disconnect' : 'Connect'} className="btn"/>
          <input type="checkbox" name="autoupdate" id="auto-update-checkbox" checked={this.state.autoupdate} onChange={this.handleChange} />
          <label htmlFor="auto-update-checkbox">Replace blockchain node URLs with Axia Codex nodes.</label>
        </div>
        <div>
          <b>{this.state.submitMsg}</b>
        </div>
      </form>
    );
  }
}

export default Config;
