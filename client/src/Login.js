import logo from './public/logo.png';
import React from 'react';
import axios from 'axios';
import './App.css';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      loginError: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.setAccessToken = props.setAccessToken;
  }

  componentDidMount() {
    this.setState({loginError: ''}); // clear login errors when switching to this tab
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleSubmit(e) {
    e.preventDefault();

    axios.post('/admin/authorize', {
      username: this.state.username,
      password: this.state.password,
      grant_type: 'password'
    }).then(res => {
      this.setState({loginError: ''});
      this.setAccessToken(res.data.access_token);
    }).catch(res => {
      if (res && res.response.data.error) {
        this.setState({loginError: res.response.data.error })
      } else {
        this.setState({loginError: `unknown error: ${res.response.status}`});
      }
    });
  }

  render() {
    return (
      <div>
        <div id="menu">
          <div id="logo"><img src={logo} alt="Numifex"></img></div>
        </div>
        <div className="card" id="loginCard">
          <form className="configForm" id="login" onSubmit={this.handleSubmit}>
            <div>
              <label htmlFor="username">Username</label>
              <input type="text" name="username" value={this.state.username} onChange={this.handleChange} />
            </div>
            <div>
              <label htmlFor="password">Password</label>
              <input type="password" name="password" value={this.state.password} onChange={this.handleChange} />
            </div>
            <div className="form-submit-container">
              <button className="btn">Login</button>
            </div>
            <div>
              {this.state.loginError}
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Login;
