module.exports = {
  "env": {
      "browser": true,
      "es6": true,
      "amd" : true
  },
  "extends": "eslint:recommended",
  "globals": {
      "exports": true,
      "module" : true,
      "process": false,
      "__dirname": false,
      "describe": false,
      "it": false,
      "before": false,
      "after": false,
      "afterEach": false,
      "Buffer": false
  },
  "rules": {
      "indent": [
          "error",
          4,
          {
              "SwitchCase" : 1,
              "CallExpression" : { "arguments": 1 },
              "FunctionDeclaration": {
                  "body": 1,
                  "parameters": 2
              }
          }
      ],
      "linebreak-style": [
          "error",
          "unix"
      ],
      "quotes": [
          "error",
          "single",
          {
              "avoidEscape": true,
              "allowTemplateLiterals": true
          }
      ],
      "semi": [
          "error",
          "always"
      ],
      "eqeqeq": [
          "error",
          "smart"
      ],
      "prefer-const": [
          "error"
      ],
      "no-trailing-spaces": [
          "error"
      ],
      "no-constant-condition": [
          "error",
          { "checkLoops" : false }
      ],
      "no-unused-vars": [
          "error",
          { "args" : "none" }
      ],
      "no-console" : "off"
  }
};

