#!/usr/bin/env python3

# Simple tool to check url object timestamp and perform tasks based on it.
# Requires running instance of Virtualbox 6.x.
# Requires you to set vbox_vm_dir to match your env.
# Requires you to run create_db.
# Requires utils.py by pyro

from dateutil.parser import parse as parsedate
from pathlib import Path
from random import randint
import requests
import sqlite3
import time
import utils
import uuid


check_url_every_in_seconds = 30
db_file = 'ci.db'
img_src_url = 'https://repo.pyrofex.io/numifex/35d6925e-1dc3-11e9-b575-672a33f93a5a/dev/numifex.ova'
ssh_key = '.ssh/id_numifex'
vbox_vm_dir = '~/vbox'


def create_db():
    print(f'Creating database {db_file}.')
    conn = sqlite3.connect('ci.db')
    s = '''CREATE TABLE events(
           ID INTEGER PRIMARY KEY,
           name TEXT,
           notes TEXT,
           ts_object TIMESTAMP,
           ts_started TIMESTAMP,
           ts_completed TIMESTAMP,
           ts_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
           );'''
    conn.execute(s)
    conn.commit()
    conn.close()


def create_event(name, ts_object=None):
    conn = sqlite3.connect('ci.db')
    c = conn.cursor()
    c.execute(f"INSERT INTO events ('name', 'ts_object') VALUES ('{name}', '{ts_object}')")
    conn.commit()
    conn.close()


def object_ts_is_new(url_object_timestamp):
    conn = sqlite3.connect('ci.db')
    c = conn.cursor()
    s = f"SELECT * FROM events WHERE name = 'test_ova' AND id = (SELECT MAX(id) FROM events WHERE name ='test_ova') AND ts_object < '{url_object_timestamp}'"
    c.execute(s)
    r = c.fetchone()
    conn.close()
    if r:
        return True
    else:
        return False 


def show_events(name, limit):
    conn = sqlite3.connect('ci.db')
    c = conn.cursor()
    s = f"SELECT * FROM events WHERE name = '{name}' AND ts_created < datetime('now') limit {limit}"
    c.execute(s)
    r = c.fetchall()
    for i in r:
        print(i)
        print('=======')
    conn.close()


db_file_path = Path(f'{db_file}')
if not db_file_path.is_file():
    create_db()

while True:
    r = requests.head(img_src_url)
    url_object_timestamp = parsedate(r.headers['last-modified'])
    if object_ts_is_new(url_object_timestamp):
        print(f'URL object timestamp: {url_object_timestamp}.')
        print('============================================')
        show_events('test_ova', 10)
        tmp_uuid = uuid.uuid4()
        uid = f'numifex-{tmp_uuid}'
        print("New image available to run CI tests on. Running in 10 seconds.")
        time.sleep(10)
        print(f'vm name: {uid}')
        create_event('test_ova', url_object_timestamp)
        utils.send_email('busk@pyrofex.io', 'new numifex.ova image', 'Running vbox tests.')
        ova_file = f'ovas/{uid}.ova'
        utils.download(img_src_url, ova_file, uid, False)
        utils.shell_cmd(f'vboxmanage import {ova_file} --vsys 0 -vmname {uid}')
        utils.shell_cmd(f'vboxmanage modifyvm {uid} --uart1 0x3F8 4')
        utils.shell_cmd(f'vboxmanage modifyvm {uid} --uartmode1 file /dev/null')
        utils.shell_cmd(f'vboxmanage modifyvm {uid} --nic1 nat')
        ssh_nat_port = randint(10000, 20000)
        utils.shell_cmd(f'vboxmanage modifyvm {uid} --natpf1 "ssh,tcp,,{ssh_nat_port},,22"')
        # utils.shell_cmd(f'vboxmanage controlvm {uid} natpf1 "ssh,tcp,,{ssh_nat_port},,22"')
        utils.shell_cmd(f'vboxmanage startvm {uid} --type headless')
        utils.ssh_cmd('ping -c 4 8.8.8.8', 'ubuntu@localhost', ssh_nat_port, ssh_key)
        if len({uid}) > 10:
            utils.shell_cmd(f'vboxmanage controlvm {uid} poweroff')
            # vboxmanage closemedium disk ~/vbox/mydiskname --delete
            utils.shell_cmd(f'rm -rf {vbox_vm_dir}/{uid}')

    print(f'Check url object timestamp in {check_url_every_in_seconds}.')
    time.sleep(check_url_every_in_seconds)
