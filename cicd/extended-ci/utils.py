import click
from email.mime.text import MIMEText
import logging
import os
import requests
import smtplib
import subprocess
import sys


def shell_cmd(cmd):
    r = subprocess.run(cmd, shell=True, stderr=sys.stderr, stdout=sys.stdout)
    if r.returncode != 0:
        # raise SystemExit(f'E: Exit code error from {cmd} command.')
        send_email('busk@pyrofex.io', 'issue with command',
                   'E: Exit code error from {cmd} command.')
        print(f'E: Exit code error from {cmd} command.')
        return 1
    return 0


def ssh_cmd(cmd, host, port=22, ssh_key=None):
    if ssh_key:
        ssh_key_opt = f'-i {ssh_key}'
    ssh_opts = (f'-o ConnectTimeout=60 -o UserKnownHostsFile=/dev/null '
                f'-o StrictHostKeyChecking=no -p {port} {ssh_key_opt}')
    r = shell_cmd(f'ssh {ssh_opts} {host} "{cmd}"')

    if r != 0:
        return 1
    return 0


def download(img_src, img_dst, img_desc, force):
    # Simple method without downloader and check
    # r = requests.get(img_src_url, allow_redirects=True)
    # open(ova_file, 'wb').write(r.content)
    filename = os.path.basename(img_dst)
    print(f'creating {filename}')
    directory = os.path.dirname(img_dst)
    if not os.path.exists(directory):
        os.makedirs(directory)
    if os.path.isfile(img_dst) and not force:
        s = (f'File {img_dst} exists. '
             'You must use -f/--force to update/overwrite.')
        print(s)
    else:
        print(f'Downloading: {img_desc}')
        r = requests.get(img_src, stream=True)
        if r.status_code != requests.codes.ok:
            logging.log(level=logging.ERROR,
                        msg='Unable to connect {0}'.format(img_src))
            r.raise_for_status()
        total_size = int(r.headers.get('Content-Length'))
        with click.progressbar(
                r.iter_content(1024),
                length=total_size) as bar, open(img_dst, 'wb') as file:
                for chunk in bar:
                    file.write(chunk)
                    bar.update(len(chunk))


def send_email(recipients, subject, body):
    email_username = 'smtprelayaccount@pyrofex.io'
    # recipients = 'busk@pyrofex.io'
    # https://en.wikipedia.org/wiki/List_of_SMS_gateways
    # 10DigitNumber@text.att.net for AT&T
    # 10DigitNumber@vtext.com for Verizon
    # 10DigitNumber@msg.fi.google.com

    msg = MIMEText(body)
    msg['Subject'] = subject
    msg['From'] = f'no-reply <no-reply@pyrofex.io>'
    msg['To'] = recipients
    msg = msg.as_string()

    session = smtplib.SMTP('smtp.pyrofex.io', 587)
    session.ehlo()
    session.starttls()
    session.login(email_username, 'xxxxxxxxxxx')
    session.sendmail(email_username, recipients, msg)
    session.quit()
