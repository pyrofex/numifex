# Extended CI

## Requirements
A running instance of Virtualbox 6.x.
Set vbox_vm_dir and other vars if needed to match your env.
Run create_db function.
Requires utils.py, a utils library by Pyro

## Function
This is a simple tool for extended ci tests on numifex. 

Right now it checks current image from dev on modified changes and does some tests.

It uses sqlite3, as it is portable, for a persistent data store. It contains a start with a simple event log table that makes this app flexible for adding functionality.

It notifies via email on command failures. This can easily cause a failure by ucomment raiase SystemExit on shell_cmd line.
