#!/bin/bash
# Numifex install for bionic or buster
set -ex

PRETTY_NAME=$(cat /etc/*release | grep '^PRETTY_NAME=' | awk -F= '{print $2}' | sed 's/"//g')
if [[ "${PRETTY_NAME}" =~ "Debian GNU/Linux buster" ]]; then
    echo "Supported host operating system Debian 10 (Buster)."
elif [[ "${PRETTY_NAME}" == *"Ubuntu 18.04"* ]]; then
    echo "Fix serial needed on virtualbox box by removing ttyS0."
    # https://bugs.launchpad.net/cloud-images/+bug/1573095
    # sudo sed -i 's/ console=ttyS0//g' /etc/default/grub.d/50-cloudimg-settings.cfg
    # sudo update-grub
    # However, we need ttyS0 for virsh console <vm-name>

    echo "Supported host operating system Ubuntu 18.04+."
    sudo ufw allow 22,443,8545,4000/tcp
    echo "y" | sudo ufw enable
    sudo ufw status numbered
else
    echo "ERROR! Invalid host operating system."
    exit 1
fi

./install.sh
./install-nginx.sh
sudo cp -r os-files/* /
sudo systemctl reload nginx
