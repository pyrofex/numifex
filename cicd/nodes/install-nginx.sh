#!/bin/bash
# Install NGINX with self signed cert.
set -ex

sudo apt-get update
sudo apt-get install -y nginx python3-certbot-nginx
sudo openssl req -x509 -nodes -days 3650 -newkey rsa:4096 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt -subj "/C=US/ST=Utah/L=Provo/O=Pyrofex Corp/OU=CI/CN=pyrofex.io"
