# Geth - Go Ethereum Client Node

Go Ethereum Client is a great way to interface with the Ethereum ecosystem.

## Installing

 https://github.com/ethereum/go-ethereum/wiki/Installing-Geth

## Simple install using Pyrofex repo install file and curl

This is assuming you trust & have access to the repo. Probably key protected.

```bash
sudo bash <(curl -s https://git.pyr8.io/numifex/numifex/raw/dev/nodes/geth/install.sh)
sudo bash <(curl -s https://git.pyr8.io/numifex/numifex/raw/dev/nodes/common/install-nginx.sh)
```

##  Basic Admin Commands & Functions
* ss -lnt
* journalctl -f -u geth.service
* geth attach http://localhost:8545
* systemctl stop geth
* ls -lhat /var/lib/geth

## Running a few commands

Using geth command
```
geth attach
```
Run your regular eth commands like eth.blockNumber.

Using curl via unencrypted connection.
```
curl -H "Content-Type: application/json" -X POST -d '{"method":"eth_accounts","id":1}' localhost:8545
```

Get data using curl via encrypted connection on port 443 when using NGINX.
```
curl -s --insecure -H "Content-Type: application/json" -X POST -d '{"method":"eth_accounts","id":1}' https://localhost
```
Use Let's Encrypt to remove the need to use the "--insecure" option or specify key.

## More geth api query methods 

#$ Using Node to query Go Client via RPC running on 8545 

If node/npm is not installed see https://github.com/nodesource/distributions.

```
npm install web3
node # Enter REPL
Web3 = require("web3");
web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
```
Now you can interact with geth client the same as the Javascript Console:

web3.eth.blockNumber // Regular geth commands except connected to rpc 

ref: https://github.com/ethereum/web3.js/

## using LXD/LXC to run geth from Ubuntu
Assuming you have LXD installed.
* lxc launch ubuntu:xenial geth
* lxc exec geth /bin/bash
* Run install script

## Notes 

* You may look at using the js web3 lib to connect to use.
* When not on local host, it is recommended to ALWAYS use the https interface.

<hr>

# Alternate Methods of Running Geth
* https://github.com/ethereum/go-ethereum/wiki/Installing-Geth

## The Docker Way

### Install Docker

https://docs.docker.com/install/linux/docker-ce/ubuntu/

### Run Ethereum Go Client Using Docker

ref: https://github.com/ethereum/go-ethereum/wiki/Running-in-Docker

```
mkdir -p /data/.ethereum
```

```
sudo docker pull ethereum/client-go
```

```
sudo docker run -dit --name geth -p 8545:8545 -p 30303:30303 -v /data/.ethereum:/root/.ethereum ethereum/client-go --rpc --rpcaddr "0.0.0.0"
```

```
sudo docker exec -it geth /bin/sh
```
