#!/bin/bash
## Quick & simple install of ethereum node via ppa as a systemd service.

set -ex

USERNAME="geth"

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y software-properties-common
sudo add-apt-repository -y ppa:ethereum/ethereum
sudo apt-get update
sudo apt-get install -y ethereum

sudo bash -c " cat > /lib/systemd/system/${USERNAME}.service <<EOF
[Unit]
Description=Go Ethereum Node
After=network.target
[Service]
User=${USERNAME}
WorkingDirectory=/var/lib/${USERNAME}
ExecStart=/usr/bin/geth --syncmode light --ws --rpc --rpcaddr 0.0.0.0 --rpcport 8545 
Restart=on-failure
[Install]
WantedBy=multi-user.target
EOF"

sudo mkdir -p /var/lib/${USERNAME}
sudo useradd --home /var/lib/${USERNAME} ${USERNAME}
sudo chown ${USERNAME}:${USERNAME} /var/lib/${USERNAME}

sudo systemctl enable ${USERNAME}.service

echo "======================================================"
echo "To start service: systemctl start ${USERNAME}"
echo "Ethereum data location: /var/lib/${USERNAME}/.ethereum"

sudo systemctl start ${USERNAME}.service
