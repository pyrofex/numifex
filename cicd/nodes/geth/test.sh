#!/bin/bash
# Tests for Geth VM
set -ex

sudo apt-get update 
sudo apt-get install -y curl 

curl -s --insecure https://localhost:443 \
    -H "Content-Type: application/json" \
    -X POST -d \
    '{"jsonrpc":"2.0","method":"web3_clientVersion","params":[],"id":67}' \
    | grep -i geth \
    || { echo "E: Incorrect api output on https 443." ; exit 1; } 
