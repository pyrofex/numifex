# Bitcoin Multi-node/wallet Regtest Network 

This uses bitcoin's regtest capability to setup isolated network for testing.

Multinode setup simulates a more real world example as you can transfer between nodes.

## Usage

Run network via docker-compose command
```
docker-compose up
```

to start all the containers. This will start the bitcoin nodes, and expose RPC on all of them. The nodes will run on the following ports:

| Node | P2P port * | RPC port * | RPC Username | RPC Password |
| --- | --- | --- | --- | ---|
| miner1 | 18500 | 18400 | bitcoin | bitcoin |
| node1 | 18501 | 18401 | bitcoin | bitcoin |
| node2 | 18502 | 18402 | bitcoin | bitcoin |

\* Exposed port on Docker host.

# Stopping and removing resources.

To stop if running in background.
```
docker-compose stop
```

To stop and remove resources.
```
docker-compose down 
```

Remove images and resources
```
docker-compose down --rmi all  -v --remove-orphans
```
