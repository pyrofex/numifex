# Building & Deploying Numifex Packages to Repos

## Using Terraform & Cloud-init

Deploy to qemu-kvm via gitlab-ci runner using terraform-provider-libvirt.

With base Ubuntu install with sudo installed run ./prep-host-for-terraform.sh

From this directory run ./deploy-numifex-vm.sh

Your ssh public key of host/runner must be on the repo server for upload to succeed.

# Using Virsh to Test Images on Local Machine

## Install Packages

```
sudo apt-get install -y qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils virt-manager libguestfs-tools
```

## Run Loading Script 
Example script for Debian based Linux to load and run vm & console into

```
#!/bin/bash
vm_guest_name="vm-test"
dst_image="/var/lib/libvirt/images/$vm_guest.qcow2"
src_image="https://repo.pyrofex.io/pyrofex/35d6925e-1dc3-11e9-b575-672a33f93a5a/numifex-bionic-amd64.qcow2"

curl -o $image $src_image

# mkdir -p ~/storage/default
# virsh pool-create-as default --type dir --target ~/storage/default
# create if doesn't exist
virsh pool-define /dev/stdin <<EOF
<pool type='dir'>
  <name>default</name>
  <target>
    <path>/var/lib/libvirt/images</path>
  </target>
</pool>
EOF

virt-install -n $vm_guest_name -r 1024 --vcpus=1 \
    --disk path=$dst_image,size=16 \
    --vnc --noautoconsole --os-type linux --os-variant ubuntu18.04 --import

echo "user: ubuntu"
echo "pass: GetMeTheCrypt0_"
echo "Disconnect from virsh console via ctrl-shift-]"

virsh console $vm_guest_name

```

Run some commands to test out numifex api
```
curl -s http://127.0.0.1:4000
```

More commands
```
virsh domifaddr $vm_guest_name 
```

Delete the VM if wanted 
```
virsh destroy VM_NAME
virsh destroy --domain VM_NAME
virsh destroy --domain DOMAIN 

virsh undefine VM_NAME
virsh undefine --domain VM_NAME
virsh undefine --domain DOMAIN 
```

# Resources 

* https://cloudinit.readthedocs.io/en/latest/
* https://bugs.launchpad.net/cloud-images/+bug/1573095
* https://docs.gitlab.com/runner/install/linux-repository.html
* https://github.com/dmacvicar/terraform-provider-libvirt
* https://github.com/dmacvicar/terraform-provider-libvirt/blob/master/website/docs/r/cloudinit.html.markdown
* https://learn.hashicorp.com/terraform/getting-started/install.html
* https://www.nginx.com/resources/wiki/start/topics/examples/full/


# Other Options:
* Libvirt direct via virt tools or kickstart.
* Openstack
* Proxmox VE Rest API
* python-libvirt 
* Virtualbox
* and many more

 Terraform is a pretty good choice for now & future.
