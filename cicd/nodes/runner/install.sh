#!/bin/bash
# For base install of ubuntu bionic or debian buster.
# You will need to download and install ovftool https://www.vmware.com/support/developer/ovf/
# You alternately can use python3 cot to do ova validation.
set -ex
pkg_type="rpm"  # rpm or deb depending on platform. Recommended is fedora 29
if [[ "${pkg_type}" == "deb" ]]; then
    sudo_group="sudo"
    libvirt_service="libvirt-bin"
    pkg_cmd="apt-get"
    pkgs="libguestfs-tools qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils / 
          virt-manager lintian curl wget git pandoc unzip uuid php-cli"
elif [[ "${pkg_type}" == "rpm" ]]; then
    sudo_group="wheel"
    libvirt_service="libvirtd"
    pkg_cmd="yum"
    pkgs="virt-v2v virt-install qemu-kvm bridge-utils virt-manager curl wget git pandoc unzip uuid php-cli"
else
    echo "Missing package type."
    exit 1
fi

# Keys - optional & replaceable for use with ci/cd functions as needed.
# Usually you will use you CI variables but file keys can be useful.
# These would match keys in cicd/nodes/id_ed25519(.pub)
# These are here for example and should be replaced if actually used.
# YOU REALLY SHOULD REPLACE THESE KEYS WITH YOUR GENERATED KEYS IF USED!!!
gitlab_runner_ssh_private_key="-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
QyNTUxOQAAACAX7qAup1rlSZr6lVKKkTrNX67DEBBU8gJ40pkMg+FnygAAAJA1ePTDNXj0
wwAAAAtzc2gtZWQyNTUxOQAAACAX7qAup1rlSZr6lVKKkTrNX67DEBBU8gJ40pkMg+Fnyg
AAAEALpnCwSbicrbWgvBhe1R7Z+iuuz7H2gqWYQBaTvIug4xfuoC6nWuVJmvqVUoqROs1f
rsMQEFTyAnjSmQyD4WfKAAAACmJ1c2tAYnVza3cBAgM=
-----END OPENSSH PRIVATE KEY-----
"
gitlab_runner_ssh_public_key="ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBfuoC6nWuVJmvqVUoqROs1frsMQEFTyAnjSmQyD4WfK example@gitlab-runner"

sudo wget -o /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
curl -ssl https://get.docker.com/ | sh
sudo useradd --comment 'gitlab runner' --create-home gitlab-runner --shell /bin/bash
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start

mkdir -p /home/gitlab-runner/.ssh
echo "${gitlab_runner_ssh_private_key}" >> /home/gitlab-runner/.ssh/id_ed25519 
echo "${gitlab_runner_ssh_public_key}" >> /home/gitlab-runner/.ssh/id_ed25519.pub 
chown -r gitlab-runner:gitlab-runner -r /home/gitlab-runner/.ssh/*
chmod 0600 /home/gitlab-runner/.ssh/id_ed25519

read -p "register with gitlab instance now? yes/no" -n 1 -r
echo
if [[ "${REPLY}" =~ ^[Yy]$ ]]
then
    sudo gitlab-runner register
fi

sudo "${pkg_cmd}" install -y "${pkgs}" || true

install_terraform () {
    terraform_version="0.11.11"
    curl -s https://releases.hashicorp.com/terraform/${terraform_version}/terraform_${terraform_version}_linux_amd64.zip -o tmp.zip
    unzip tmp.zip 
    rm tmp.zip
    cp terraform /usr/local/bin/
}

install_libvirt_terraform_provider () {
    if [[ "$1" == release-build ]]; then
        # If using release build, please update for your needs.
        # https://github.com/dmacvicar/terraform-provider-libvirt/releases 
        distro="Ubuntu_18.04.amd64"  # CentOS_7.x86_64, Fedora_28.x86_64
        terraform_provider_libvirt_version="0.5.1"
        provider_artifact_url="https://github.com/dmacvicar/terraform-provider-libvirt/releases/download/v${terraform_provider_libvirt_version}/terraform-provider-libvirt-${terraform_provider_libvirt_version}.${distro}.tar.gz"
        sudo -iu gitlab-runner -- <<eof
        curl -s -l "${provider_artifact_url}" | tar zx
        mkdir -p ~/.terraform.d/plugins/linux_amd64
        mv terraform-provider-libvirt  ~/.terraform.d/plugins/linux_amd64/
eof
    else
        echo "Compile libvirt terraform from src."
        sudo "${pkg_cmd}" install -y golang-go libvirt-dev
        sudo -iu gitlab-runner -- <<eof
        cd /tmp
        go get github.com/dmacvicar/terraform-provider-libvirt
        go install github.com/dmacvicar/terraform-provider-libvirt
        mkdir -p ~/.terraform.d/plugins/linux_amd64
        cp ~/go/bin/terraform-provider-libvirt ~/.terraform.d/plugins/linux_amd64/
eof
        echo "security_driver=\"none\"" >> /etc/libvirt/qemu.conf
        sudo systemctl restart ${libvirt_service} 
        # ref: https://github.com/dmacvicar/terraform-provider-libvirt/commit/22f096d9 
    fi 
}

# More libvirt addtions.
# Note, add bridge names to /etc/qemu/bridge.conf for unpriv users 
user="gitlab-runner"
usermod -ag ${sudo_group} $user
usermod -ag libvirt $user
mkdir -p /home/$user/images

# Add another storage pool.
virsh pool-define /dev/stdin <<eof
<pool type='dir'>
  <name>$user</name>
  <target>
    <path>/home/$user/images</path>
  </target>
</pool>
eof
virsh pool-start $user 
virsh pool-autostart $user 

virsh net-define /dev/stdin <<eof
<network>
    <name>default</name>
    <uuid>d0e9964a-f91a-40c0-b769-a609aee41bf2</uuid>
    <forward mode='nat'>
      <nat>
        <port start='1024' end='65535'/>
      </nat>
    </forward>
    <bridge name='br0' stp='on' delay='0' />
    <mac address='52:54:00:60:f8:6e'/>
    <ip address='192.168.0.1' netmask='255.255.252.0'>
      <dhcp>
        <range start='192.168.0.2' end='192.168.2.254' />
      </dhcp>
    </ip>
  </network>
eof
virsh net-start default 
virsh net-autostart default 

virsh pool-define /dev/stdin <<eof
<pool type='dir'>
  <name>default</name>
  <target>
    <path>/var/lib/libvirt/images</path>
  </target>
</pool>
eof
virsh pool-start default 
virsh pool-autostart default 
