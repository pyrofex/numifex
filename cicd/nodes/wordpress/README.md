# Wordpress & WooCommerce

When working with Wordpress and it's many plugins it is best to use wp-cli 

In install.sh set siteurl variable to your needed situation
or in database you may need to do
```
update wp_options set option_value='https://mysite.example.org' where option_id = 1 or option_id = 2
```

or set 

Edit wp-config.php
```
define( 'WP_HOME', 'https://mysite.example.org' );
define( 'WP_SITEURL', 'https://mysite.example.org' );
```

Install WooCommerce and Run wp wc commands
```
wp plugin install woocommerce --activate
wp wc shop_order list --customer=1 --user=1 --fields=id,status
wp wc customer list --user=1 --fields=id,email
```

# References
* https://developer.wordpress.org/cli/commands/
* https://github.com/woocommerce/woocommerce/wiki/WC-CLI-Overview
* https://robotninja.com/blog/wp-cli-woocommerce-development/
* https://wp-cli.org/
