Reference README under specific node more detailed information.  

# Get latest pre-built packages & vm images for nodes

Built of dev branch

https://repo.pyrofex.io/dev/numifex/35d6925e-1dc3-11e9-b575-672a33f93a5a/

Built off master branch

https://repo.pyrofex.io/numifex/35d6925e-1dc3-11e9-b575-672a33f93a5a/

You can get deb package and install artifacts in gitlab via <br>
https://git.pyr8.io/numifex/numifex/-/jobs  or <your-username>/numifex

## Login Info

user: ubuntu
pass: PyrofexCryptofexNumifex

Use sudo for root privileges. 

If you want cloud-init features you will have to install via
```apt-get install cloud-init```

# Running a nodes service install on localhost

You can use qemu-kvm, libvirt, and terraform to run these in a vm or you can run on localhost.

*WARNING* this will wipeout resources on host where needed so use with caution.

/var/www/html, database by name or some of the resources blasted.

Please reference scripts first or try in lxd container or vm.

<node-name> is wordpress, geth, numifex,  etc. 

```    
cp install-main.sh install-nginx.sh <node-name>/
cd <node-name>/
./install-main.sh
```

Wordpress example
```    
cp install-main.sh install-nginx.sh wordpress/ 
cd wordpress/ 
./install-main.sh
```

# OVA Package Creation

We create it in code via tar command but you can export from virtualbox.
```
vboxmanage export bionic --output bionic.ova
```

