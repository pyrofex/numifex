#!/bin/bash
set -ex

EXPECTED_ARGS=1

if [ $# -ne $EXPECTED_ARGS ]; then
        echo "Usage: ${0} <package-type>"
        echo "Example: ${0} production"
        echo "Example: ${0} \"\""
        exit
fi
pkg_type="$1"

VERSION="0.4.2"
BUILD_NUMBER=1
ARTIFACTS_DIR="${CI_PROJECT_DIR}/.artifacts"
cd "${CI_PROJECT_DIR}/cicd/nodes/numifex/packaging/deb/" || exit 1
./build-debian-package.sh numifex $VERSION $BUILD_NUMBER numifex numifex numifex "${pkg_type}";

mkdir -p "${ARTIFACTS_DIR}"
cp "numifex_${VERSION}-${BUILD_NUMBER}_all.deb" "${ARTIFACTS_DIR}/numifex-current.deb"
# cp "numifex_${VERSION}-${BUILD_NUMBER}_all.deb" "${ARTIFACTS_DIR}/numifex_${VERSION}-${BUILD_NUMBER}_all-${pkg_type}.deb"
# cp "numifex_${VERSION}-${BUILD_NUMBER}_all.deb" "${ARTIFACTS_DIR}/numifex-${pkg_type}.deb"
