#!/bin/bash
# Tests for Numifex VM
set -ex

sudo apt-get update 
sudo apt-get install -y curl npm jq
db_host=$(sudo jq -r '.databaseHost' /var/lib/numifex/config.json)
db_name=$(sudo jq -r '.databaseName' /var/lib/numifex/config.json)
db_pass=$(sudo jq -r '.databasePass' /var/lib/numifex/config.json)
db_user=$(sudo jq -r '.databaseUser' /var/lib/numifex/config.json)
# init_pass=$(jq -r '.initialPass' /var/lib/numifex/config.json)
sudo apt-get install -y curl

curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
sudo apt-get install -y nodejs
sudo apt-get install -y gcc g++ make

echo "=== Running CI Tests ==="

journalctl -u numifex.service -b

curl -s http://localhost:4000 \
    | grep Numifex \
    || { echo "E: Incorrect api output on http tcp/4000." ; exit 1; }

curl -s --insecure https://localhost/ \
    | grep Numifex \
    || { echo "E: Incorrect api output on https tcp/443." ; exit 1; }

sudo ss -lntp \
    | grep 4000 \
    || { echo "E: Listener does not exist on port tcp/4000." ; exit 1; }

export PGPASSWORD="${db_pass}"
psql -U "${db_user}" -h "${db_host}" "${db_name}" -c "\dt" \
    | grep table \
    || { echo "E: Postgres localhost connection issue." ; exit 1; }

# npm i is for installing test packages
sudo chown -R numifex:numifex /var/lib/numifex
sudo chmod -R +rw /var/lib/numifex
cd /var/lib/numifex
sudo npm ci --unsafe-perm

sudo su -l numifex -s /bin/bash -c "cd /var/lib/numifex; npm test"
