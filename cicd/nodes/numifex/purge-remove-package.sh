#!/bin/bash
# This destroys numifex app resources
sudo rm /var/lib/dpkg/info/numifex.*
sudo apt -y remove numifex
sudo rm -rf /var/lib/numifex
sudo su - postgres psql -c "dropdb numifex"
