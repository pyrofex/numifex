#!/bin/bash
set -ex
sudo -u postgres psql -c "drop database numifex"
sudo -u postgres psql -c "drop role numifex"
sudo apt-get -yq remove numifex
sudo rm -rf /var/lib/numifex 

curl -s -o /tmp/numifex-current.deb https://repo.pyrofex.io/pyrofex/beta/numifex-current.deb
sudo apt-get -y install /tmp/numifex-current.deb 

systemctl enable numifex
systemctl start numifex
