#!/bin/bash
# Numifex install for bionic or buster
set -ex

# EXPECTED_ARGS=1
# 
# if [ $# -ne $EXPECTED_ARGS ]; then
#         echo "Usage: ${0} <deb-package-name>"
#         echo "Example: ${0} numifex"
#         exit
# fi

if [[ "$1" ]]; then
    deb_package_name="$1"
else
    deb_package_name="numifex.deb"
fi

cd /tmp/
sudo apt-get update
sudo apt-get install -y ./"${deb_package_name}"
sudo systemctl enable numifex
sudo systemctl start numifex
