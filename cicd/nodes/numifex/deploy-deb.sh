#!/bin/bash
set -ex
VERSION="0.4.2"
BUILD_NUMBER=1
cd "${CI_PROJECT_DIR}/cicd/nodes/numifex/packaging/deb/" || exit 1
./build-debian-package.sh numifex $VERSION $BUILD_NUMBER numifex numifex numifex "--only=prod";

scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \
    ./numifex_${VERSION}-${BUILD_NUMBER}_all.deb repo.pyrofex.io:/var/www/html/pyrofex/beta/numifex-current.deb
scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \
    ./numifex_${VERSION}-${BUILD_NUMBER}_all.deb repo.pyrofex.io:/var/www/html/pyrofex/beta/
