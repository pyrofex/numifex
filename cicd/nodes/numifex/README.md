See nodes/runner README for prepping host or getting resources/artifacts.

```
sudo apt install ./numifex.deb
sudo systemctl enable numifex
sudo systemctl start numifex
```
Check some files and services
```
ls -lhat /var/lib/numifex
ss -lnt | grep 4000
curl -s http://127.0.0.1:4000
journalctl -u numifex
```

# Enable https by Using NGINX

## Install NGINX
```
sudo apt-get install nginx 
```

## Use openssl to create self signed key
Remove -subj to receive input prompts
```
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt -subj "/C=US/ST=Utah/L=Provo/O=Pyrofex Corp/OU=Testing/CN=pyrofex.io"
```

## Update NGINX Default Site Config
This is an as is example to show some basic functions. Modify as needed or wanted

nano /etc/nginx/sites-available/default
```
server {
    listen 80 default_server;
    listen [::]:80 default_server;
    if ($scheme != "https") {
        return 301 https://$host$request_uri;
    }

    listen 443 ssl default_server;
    listen [::]:443 ssl default_server;
    ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;
    ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;

    # server_name example.com www.example.com;
    server_name _;
    root /var/www/html;
    # ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

    location / {
        client_max_body_size 10m;
        proxy_pass http://localhost:4000;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-NginX-Proxy true;
    }
}
```

## Install Let's Encrypt Certbot & Gen Cert
Use this if you want Let's Encrypt features.<br>
https://letsencrypt.org/<br>
https://certbot.eff.org/all-instructions
```
sudo apt-get install python-certbot-nginx 
sudo certbot --nginx
```

## Reload NGINX Service
```
nginx -t
sudo systemctl reload nginx
```

## Check Service
```
curl -s --insecure https://localhost/
```

## Check SSL Report
Enter your FQDN in textbox.
```
https://www.ssllabs.com/ssltest/
```
