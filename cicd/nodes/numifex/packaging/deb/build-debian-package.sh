#!/usr/bin/env bash
# Simple script that uses dpkg-deb to build deb package
# Author: Jeremy Busk <jeremy@pyrofex.net>
# Copyright (c) 2018, Pyrofex Corporation. All Rights Reserved.
#
set -exo pipefail

EXPECTED_ARGS=7

if [ $# -ne $EXPECTED_ARGS ]; then
        echo "Usage: ${0} <app-name> <version> <build-number> <db-name> <db-user> <db-pass>"
        echo "Example: ${0} numifex 0.1.0 1 numifex numifex numifex production"
        exit
fi

app_name="${1}" #  This should be the same as the username app is running as.
version="${2}"
build_number="${3}"
db_name="${2}"
db_user="${3}"
db_pass="${4}"
npm_install_options="${7}"  # --only=prod or production or empty using "" for non-production/dev

app_root='../../../../../'
clean_version=$(echo ${version} | sed s'/~//g') # for changelog requirement

line="${app_name}-${clean_version} (0:${version}-${build_number}) stable; urgency=low"
sed -i "1s/.*/$line/" ./changelog.Debian
sed -i "s/Version: .*/Version: ${version}-${build_number}/" control

mkdir -p debian
DIRS="etc etc/${app_name} DEBIAN usr usr/share usr/share/man "\
"usr/share/man/man1 usr/share/doc usr/share/doc/${app_name} lib "\
"lib/systemd lib/systemd/system var var/lib var/lib/${app_name} "\
"var/lib/${app_name}/bin"
(
    cd debian;
    for x in ${DIRS} ; do {
        mkdir -p ${x} && chmod 0755 ${x};
    } ; done
)

(
    cd ${app_root}/src
    npm ci ${npm_install_options}
    cd ../client
    npm ci
    npm run build
    cd ../plugins/pos
    npm ci
    npm run build
)

ls ${app_root}
cp -rp ${app_root}/src/* ./debian/var/lib/${app_name}/
mkdir -p ./debian/var/lib/${app_name}/admin-static
mkdir -p ./debian/var/lib/${app_name}/pos-static
cp -rp ${app_root}/client/build/* ./debian/var/lib/${app_name}/admin-static
cp -rp ${app_root}/plugins/pos/build/* ./debian/var/lib/${app_name}/pos-static

## Build out the filesystem tree and help ensure permissions are correct.

cp ./conffiles ./debian/DEBIAN/
cp ./control ./debian/DEBIAN/
cp ./copyright ./debian/usr/share/doc/${app_name}/
chmod 0755 postinst
cp ./prerm ./preinst ./postinst ./postrm ./debian/DEBIAN/
cp ${app_name}.service ./debian/lib/systemd/system/${app_name}.service
cp ${app_name}.1 ./debian/usr/share/man/man1
gzip -n -f --best ./debian/usr/share/man/man1/${app_name}.1
cp ./changelog.Debian ./debian/usr/share/doc/${app_name}/
gzip -n -f --best ./debian/usr/share/doc/${app_name}/changelog.Debian

# NB(leaf): Lintian will complain bitterly about file permissions with warnings
# like the following. Gitlab has a bug where it wants to check things out with
# a umask that results in group-writable files. As a result, this is a
# ham-handed attempt to fix things up without having to list every single file.
#
# W: numifex: non-standard-file-perm var/lib/numifex/package.json 0664 != 0644
#
# The Gitlab bug:
# https://gitlab.com/gitlab-org/gitlab-runner/issues/1736

(
    cd debian;
    find ${DIRS} -perm 775 -exec chmod 755 {} \;
    find ${DIRS} -perm 664 -exec chmod 644 {} \;
)

chmod -x debian/lib/systemd/system/${app_name}.service \
    debian/usr/share/doc/${app_name}/changelog.Debian.gz \
    debian/usr/share/doc/${app_name}/copyright \
    debian/usr/share/man/man1/${app_name}.1.gz \
    debian/DEBIAN/conffiles

fakeroot dpkg-deb --nocheck --build ./debian
DEBPKG="${app_name}_${version}-${build_number}_all.deb"

ls -lat
mv debian.deb ${DEBPKG}

# NB(leaf): We disable the "binaries" check because the websocket node module
# fails the 'binary-or-shlib-defines-rpath' and I don't know why it does that.
LINTIAN_EXCLUDE_CHECKS=$(printf "%s" "binaries")
lintian --no-tag-display-limit  -X ${LINTIAN_EXCLUDE_CHECKS} ${DEBPKG} || true
