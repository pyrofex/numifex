#!/usr/bin/env sh
# A simple script for creating debian package and then scrubbing it via lintian

dir_root="./debian"

# scrubs node_modules files from npm install --save using nodejs 8 LTS running on buster 
# run this script before "./build-debian-package.sh"
# rmdir debian if you want

fakeroot dpkg-deb --build ./debian

for file in $(lintian debian.deb | grep unstripped-binary-or-object | awk '{print $4}'); do
    cmd="strip --strip-unneeded ${dir_root}/${file}";
    echo $cmd
    # bash -c "$cmd" 
    eval "$cmd" 
done

for file in $(lintian debian.deb | grep shlib-with-executable-bit | awk '{print $4}'); do
    cmd="chmod 0640 ${dir_root}/${file}";
    echo $cmd
    eval "$cmd" 
done

for file in $(lintian debian.deb | grep "package-contains-npm-ignore-file\|package-contains-eslint-config-file\|package-contains-vcs-control-file" | awk '{print $4}'); do
    cmd="rm ${dir_root}/${file}";
    echo $cmd
    eval "$cmd" 
done

# E: numifex: wrong-path-for-interpreter var/lib/numifex/node_modules/ethers/node_modules/uuid/benchmark/bench.gnu (#!/opt/local/bin/gnuplot != /usr/bin/gnuplot)
files="var/lib/numifex/node_modules/ethers/node_modules/uuid/benchmark/bench.gnu var/lib/numifex/node_modules/web3-eth-accounts/node_modules/uuid/benchmark/bench.gnu"
for file in ${files}; do
    cmd="sed -i '1 s/^.*$/#!\/usr\/bin\/gnuplot -persist/' ${dir_root}/${file}"
    echo $cmd
    eval "$cmd"
done

# Create MD5 Checksums
rm debian/DEBIAN/md5sums
cd $dir_root
find . -type f ! -regex '.*.hg.*' ! -regex '.*?debian-binary.*' ! -regex '.*?DEBIAN.*' -printf '%P ' | xargs md5sum > DEBIAN/md5sums
cd ..

fakeroot dpkg-deb --build ./debian

lintian debian.deb | grep "E: " > errors.out
# files are in debian/var/lib/numifex/node_modules
