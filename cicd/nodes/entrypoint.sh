#!/bin/bash
set -ex
shopt -s expand_aliases

# Variables
if [[ ! "${GITLAB_CI}" ]];then
    CI_PROJECT_DIR=$(pwd | rev | cut -d'/' -f3- | rev)
    CI_PIPELINE_IID=1
fi

ssh_opts="-o StrictHostKeyChecking=no \
    -o UserKnownHostsFile=/dev/null \
    -o ConnectionAttempts=10"
ssh_cmd="ssh -p 22 ${ssh_opts}"
alias ssh="${ssh_cmd}"
alias scp="scp ${ssh_opts}"
alias rsync="rsync -avz --rsync-path=\"sudo rsync\" -e \"${ssh_cmd}\""

## Exports
export LIBVIRT_DEFAULT_URI="qemu:///session"

## Defaults
# Please replace defaults where wanted/needed for function/security.
app_name="numifex"
artifacts_dir="${CI_PROJECT_DIR}/artifacts"
build_number="${CI_PIPELINE_IID}"
build_version="0.4.2"
cloud_init_meta_data_file_src="meta-data.template"
cloud_init_user_data_file_src="user-data.template"
datetime=$(date "+%FT%T")
db_name="numifex"
db_pass="numifex"
db_user="numifex"
node_name="generic"
repo_dir="/var/www/html/numifex/downloads"
repo_host="repo.pyrofex.io"
# uuid=$(uuid)
# support shorter length for update in current libvirt tools on fedora 29 <18
uuid=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 16 ; echo '')
# Use your own base cloud init image by replacing line below.
vm_base_image_url="https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img"
vm_pass="GetMeTheCrypt0_"
vm_user="ubuntu"
base_vm_format="qcow2"

while true; do
  case "$1" in
    --app-name ) app_name="$2"; shift 2;;
    --build-docs ) build_docs=true; shift ;;
    --build-package ) build_package=true; shift ;;
    --build-number ) build_number="$2"; shift 2 ;;
    --build-version ) build_version="$2"; shift 2 ;;
    --build-install-vm ) build_vm=true; shift ;;
    --build-vm ) build_vm=true; shift ;;
    --build-vm-image-formats ) build_vm_image_formats=true; shift ;;
    --cloud-init-meta-file-src ) cloud_init_meta_data_file_src="$2"; shift 2 ;;
    --cloud-init-user-data-file-src ) cloud_init_user_data_file_src="$2"; shift 2 ;;
    --db-name ) db_name="$2"; shift 2 ;;
    --db-pass ) db_pass="$2"; shift 2 ;;
    --db-user ) db_user="$2"; shift 2 ;;
    --deploy-artifacts-to-repo ) deploy_artifacts_to_repo=true; shift ;;
    --node-name ) node_name="$2"; shift 2 ;;
    --npm-install-options ) npm_install_options="$2"; shift 2 ;;
    --refresh_live ) refresh_live=true; shift ;;
    --remove-cloud-pkg ) remove_cloud_pkg=true; shift ;;
    --repo-dir ) repo_dir="$2"; shift 2 ;;
    --run-install ) run_install=true; shift ;;
    --run-tests ) run_tests=true; shift ;;
    --save-vm-image ) save_vm_image=true; shift ;;
    --vm-guest-user ) vm_user="$2"; shift 2 ;;
    --vm-guest-pass ) vm_pass="$2"; shift 2 ;;
    -h | --help ) help=true; shift ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

deb_package_name="${node_name}.deb"
repo_host_dir="${repo_host}:$repo_dir/${build_version}"
# vm_name="${app_name}-${node_name}-${uuid}"
vm_name="${uuid}"

if [[ "${help}" ]]; then
    echo 'Usage:
    --app-name | numifex 
    --build-packages | true if exists
    --build-number | 0.1.0
    --build-version | 1 
    --cloud-init-network-file-src | cloud_init_network.cfg.template
    --cloud-init-user-data-file-src | cloud_init_user_data.cfg.template
    --db-name | numifex
    --db-pass | numifex 
    --db-user | numifex
    --deploy-packages | true if exists
    --deploy-vm-images | true if exists
    --keep-cloud-pkg | true if exists
    --node-name | numifex/geth 
    --npm-install-options | "--only=production"
    --refresh-live | true if exists
    --run-tests | true if exists
    --tf-file-dst | bionic.tf.template 
    --tf-file-src | my-vm-terraform.tf 
    --tfl-file | my-vm-terraform.tfl
    -h | --help |
    '

    echo "Example:
    ${0} --app-name numifex --node-name numifex --build-version 0.1.0 --build-number 1 --db-name numifex --db-user numifex --db-pass numifex --cloud-init-user-data-file-src cloud_init_user_data.cfg.template --cloud-init-network-file-src cloud_init_network.cfg.template --build-vm --run-install --run-tests --deploy-vm-images --deploy-vm-packages"
    exit
fi

mkdir -p "${artifacts_dir}"
tmp_dir="${vm_name}"
mkdir -p "${tmp_dir}"
chmod 0777 "${tmp_dir}"
echo "Running node entrypoint: ${node_name}" >> "${tmp_dir}/node.log"

echo "=================================="
echo "App Name: ${app_name}"
echo "Node Name: ${node_name}"
echo "CI_PROJECT_DIR: ${CI_PROJECT_DIR}"
echo "Datetime: ${datetime}"
echo "Temporary directory: ${tmp_dir}"
echo "VM Guest Name: ${vm_name}"
echo "=================================="

# Functions

build_docs () {
    cd ../../docs/
    make html
    cp -r build/html ../artifacts/docs
    cd ../
    cd plugins/
    zip -r wp-numifex.zip wp-numifex/
    mv wp-numifex.zip ../artifacts/
    cd ../cicd/nodes/
}

build_package () {
    echo "Building Packages."
    cd numifex/packaging/deb/ || exit 1
    ./build-debian-package.sh numifex "${build_version}" "${build_number}" \
        "${db_name}" "${db_user}" "${db_pass}" "${npm_install_options}";

    cp "numifex_${build_version}-${build_number}_all.deb" \
        "${artifacts_dir}"/"${deb_package_name}"
    cd ../../../ || exit 1
}

build_vm () {
    echo "Building VM."
    cp "${artifacts_dir}"/* "${tmp_dir}/" || true
    cp ./*.sh "${tmp_dir}/"
    cp ./id_* "${tmp_dir}/"
    cp "${node_name}"/*.sh "${tmp_dir}/"
    cp -rp "${node_name}"/os-files  "${tmp_dir}/"
    sed "s/{{vm_pass}}/${vm_pass}/g" \
        "${cloud_init_user_data_file_src}" > "${tmp_dir}/user-data"
    sed "s/{{vm_name}}/${node_name}/g" \
        "${cloud_init_meta_data_file_src}" > "${tmp_dir}/meta-data"
    cp network-config "${tmp_dir}"
    cp ova.ovf.template "${artifacts_dir}"

    tar -zcvf "${artifacts_dir}/${node_name}_install_files.tgz" "${tmp_dir}"
    cd "${tmp_dir}"

    vm_create "$vm_name"
    ipv4=$(get_ipv4 "$vm_name")

    echo "Local vm connection info:"
    echo "console: virsh console ${vm_name}"
    echo "user: ${vm_user}"
    echo "pass: ${vm_pass}"
    echo "pass: virsh console ${vm_name}"
    echo "ssh: ssh ${vm_user}@${ipv4}"

    rsync --exclude={*.iso,*.vmdk,*.mf,*.ovf,*.qcow2,*.img,id_*} \
        ./* "${vm_user}@${ipv4}:/tmp/"
}

build_vm_image_formats () {
    cd "${artifacts_dir}"

    image_formats=(vmdk)  # "vmdk raw qcow2
    for filename in *."${base_vm_format}"; do
        [[ -e $filename ]] || break
        filename_extension="${filename##*.}"
        filename_noextension="${filename%.*}"
        ova_name="${filename_noextension}"
        
        for image_format in "${image_formats[@]}"; do
            echo "Convert ${filename_noextension}.${filename_extension}" \
                "to ${filename_noextension}.${image_format} vm image."
            if [[ "${image_format}" == vmdk ]]; then
                qemu_img_options="subformat=streamOptimized"
            else
                qemu_img_options=""
            fi
            qemu-img convert -o "${qemu_img_options}" "${filename}" \
                -O "${image_format}" "${filename_noextension}.${image_format}"

            if [[ "${image_format}" == vmdk ]]; then
                file_size_bytes=$(du -b "${filename_noextension}".vmdk | awk '{print $1}')
                sed "s/{{name_id}}/${filename_noextension}/g" ova.ovf.template \
                    > "${ova_name}".ovf 
                sed -i "s/{{file_size}}/${file_size_bytes}/g" "${filename_noextension}".ovf

                ovftool "${ova_name}".ovf "${ova_name}".ova
                # vm_disk_sha=$(sha256sum "${filename_noextension}".vmdk | awk '{print $1}')
                # ovf_sha=$(sha256sum "${ova_name}.ovf" | awk '{print $1}')
                # echo "SHA256(${filename_noextension}.vmdk)= ${vm_disk_sha}" > "${ova_name}.mf"
                # echo "SHA256(${ova_name}.ovf)= ${ovf_sha}" >>"${ova_name}.mf"

                # tar cf "${ova_name}.ova" \
                #     "${ova_name}.ovf" \
                #     "${filename_noextension}.vmdk" \
                #     "${ova_name}.mf"
		        # rm "${filename_noextension}.ovf"
	            # rm "${filename_noextension}.mf"	
                ovftool --schemaValidate "${filename_noextension}.ova"
                rm "${filename_noextension}.vmdk"
            fi
        done
    done
    rm ova.ovf.template
}

cleanup () {
    if [[ "${build_vm}" ]]; then
        virsh destroy "${vm_name}" || true
        virsh undefine "${vm_name}" || true
        virsh pool-destroy "${vm_name}" || true
        virsh pool-undefine "${vm_name}" || true
    fi
}

deploy_artifacts_to_repo () {
    echo "Deploying artifacts to ${repo_host_dir} in repo."

    cd "${artifacts_dir}"
    find -- * ! \( -path "doc*" -o -name 'wp-*' -o -name "*.ova" \
        -o -name "*.qcow2" -o -name "*.deb" \
        -o -name "*.sh" -o -path . -o -path .. \) -exec rm -rf {} +
    find -- * ! -path "doc*" -type f -exec sha256sum {} + >> SHA256SUMS.txt
    timestamp=$(date +%Y%m%d%H%M.%S) 
    find . -exec touch -t "${timestamp}" {} +
    
    if [[ -z "${CI_COMMIT_TAG}" ]]; then
        rsync --chown=www-data:www-data --chmod=0770 ./* "${repo_host}:$repo_dir/dev/"
    else
        ## If build version changes point latest symlink to new release 
        if ssh "${repo_host}" "[ ! -d \"${repo_dir}/${build_version}\" ]"; then
            ssh "${repo_host}" sudo mkdir -m 0777 "${repo_dir}/${build_version}"
            ssh "${repo_host}" sudo unlink "${repo_dir}"/latest
            ssh "${repo_host}" sudo ln -s "${repo_dir}/${build_version}" "${repo_dir}"/latest
        fi
        rsync --chown=www-data:www-data --chmod=0770 ./* "${repo_host_dir}"/
    fi
}

get_ipv4 () {
    # Get ip address of running vm guest
    # "virsh domifaddr <vm-name>" works sometimes but not all the time
    vm_name=$1
    for i in {1..20}; do
        mac=$(virsh domiflist "${vm_name}" | grep -o -E "([0-9a-f]{2}:){5}([0-9a-f]{2})")
        ip=$(ip neighbor | grep "${mac}" | grep -o -P "^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}") || true
        if [[ "${ip}" ]]; then
            echo "${ip}"
            break
        fi
        echo "Waiting for ip address." >&2
        sleep 5
        vm_check=$(virsh list | grep "${vm_name}")
        if [[ ! "${vm_check}" ]]; then
            echo "E: No vm guest instance found running for ${vm_name}." >&2
            exit 1 
        fi
    done
    if [[ ! "${ip}" ]]; then
        echo "E: No ip address assigned for vm guest ${vm_name}." >&2
        exit 1 
    fi
}

refresh_live () {
    echo "Running numifex dev live refresh."
    echo "${SSH_PRIVATE_KEY}" | tr -d '\r' > id_gitlab-ci
    eval "$(ssh-agent -s)"
    chmod 600 id_gitlab-ci
    chmod 600 id_ed25519 
    ssh-add id_gitlab-ci
    ssh-add id_ed25519 
    cd "${node_name}"
    scp reinstall.sh root@numifex-dev.pyr8.io:/tmp/ 
    ssh root@numifex-dev.pyr8.io /tmp/reinstall.sh
}

run_install () {
    echo "Running install."
    ssh "${vm_user}"@"${ipv4}" "cd /tmp; ./install-main.sh"
}

run_tests () {
    echo "Running tests."
    ssh "${vm_user}"@"${ipv4}" "cd /tmp; ./test.sh"
}

save_vm_image () {
    echo "Saving ${node_name} image."

    if [[ "${remove_cloud_pkg}" ]]; then
        echo "Removing cloud-init package on host losing cloud-init feature use."
        ssh "${vm_user}"@"${ipv4}" sudo apt-get remove -y cloud-init || true
    fi

    virsh shutdown "${vm_name}"
    for i in {1..5}; do
        echo "Attempt ${i}."
        qemu-img info "${vm_image}" && break || sleep 5
    done
    qemu-img convert -c  "${vm_image}" -O qcow2 "${artifacts_dir}/${node_name}.${base_vm_format}"
}

vm_create () {
    eval "$(ssh-agent -s)"
    chmod 600 id_ed25519 
    ssh-add id_ed25519 

    vm_name="$1"
    vm_desc="build number: ${build_number}"
    vm_base_image="../os.${base_vm_format}"
    export vm_image="${vm_name}.${base_vm_format}"
    init_iso="init.iso"
    os_variant="ubuntu18.04"  # fedora28, ubuntu18.04, rhel7,  ...

    if [[ ! -f "${vm_base_image}" ]]; then
        curl -q -o "${vm_base_image}" "${vm_base_image_url}"
    fi
    cp "${vm_base_image}" "${vm_image}"

    qemu-img resize "${vm_image}" 10G
    # Resize vm images if needed from default of 10G
    # Make geth big so it can hold blockchain on root partition.
    # if [[ "${node_name}" == *"geth"* ]]; then
    #     qemu-img resize "${vm_image}" 500G
    # fi

    genisoimage -input-charset utf-8 -output init.iso -volid cidata -joliet -rock user-data meta-data network-config
    virt-install --name "${vm_name}" \
    --description "${vm_desc}" \
    --ram 2048 \
    --vcpus 1 \
    --disk path=./"${vm_image}" \
    --os-type linux \
    --os-variant "${os_variant}" \
    --network bridge=virbr0 \
    --cdrom "${init_iso}" \
    --noautoconsole
    ipv4=$(get_ipv4 "$vm_name")
}

# Main 

if [[ "${build_docs}" ]]; then
    build_docs
fi

if [[ "${build_package}" ]]; then
    build_package
fi

if [[ "${build_vm}" ]]; then
    build_vm
fi

if [[ "${run_install}" ]]; then
    run_install 
fi

if [[ "${run_tests}" ]]; then
    run_tests
fi

if [[ "${save_vm_image}" ]]; then
    save_vm_image 
fi

if [[ "${build_vm_image_formats}" ]]; then
    build_vm_image_formats 
fi

if [[ "${deploy_artifacts_to_repo}" ]]; then
    deploy_artifacts_to_repo
fi

if [[ "${refresh_live}" ]]; then
    refresh_live
fi

if [[ "${CI}" ]]; then
    if (( "${build_vm_image_formats}" || "${build_package}" || "${save_vm_image}" )); then
        git add "${artifacts_dir}"  # For artifact job passing
    fi
fi

cleanup
