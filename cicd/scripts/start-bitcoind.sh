#!/bin/bash
set -e
bitcoind -regtest -printtoconsole > bitcoind.log 2>&1 &

# Make sure service comes up in window
for i in {1..5}; do
    sleep 5 
    echo "Checking bitcoind api availability."
    if [[ $(bitcoin-cli -regtest getmininginfo) ]]; then
        echo "bitcoind is running"
        break
    fi
done
